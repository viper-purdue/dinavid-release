The vis_input directory contains files used for generating a 3D rendering of image volumes.
default.json contains the information and meta-data necessary for the 3D rendering engine to visualize volumes.
index.html is not used by the system.
test_old.html contains the visual components of the 3D rendering controls which uses a single contrast, brightness, and saturation value for all channels.
test.html contains the visual components of the 3D rendering controls which uses gamma, brightness, and offset for each channel in RGB separately.
visual3d_old.js contains the functionality of the 3D rendering controls which uses a single contrast, brightness, and saturation value for all channels.
visual3d.js contains the functionality of the 3D rendering controls which uses gamma, brightness, and offset for each channel in RGB separately.
readme.txt is this file.