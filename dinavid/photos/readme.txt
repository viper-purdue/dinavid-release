
/dinavid-gui/dinavid/photos/ is an example of the "application" concept in django. Under /dinavid-gui/dinavid/photos are the following files and directories:

- checkpoint/ - This directory contains the trained models for DeepSynth
- migrations/ - This directory contains a log of model structure changes when changes to fields are made to the database
- result/ - Not used. We probably need to clean this up.
- static/ - This directory contains static javascript files used for this application.
- templates/ - This directory contains the html files used for this application.
- templatetags/ - This directory contains django template tags we created that are used in the html files in templates/
- apps.py - This file defines what application this is.
- views.py - This file is how django renders each view so the correct html and response can be displayed.
- urls.py - This file is how django knows where to map urls to a certain page inside this application.
- models.py - This file describes the structure of the database. If you make any changes to this file, you would need to do a migration.
- forms.py - This file is how django handles custom forms in the html files.
- tasks.py - This file is the file that communicates with blackpill. The same file must also be in blackpill to run correctly.
- tileImages_input.py - This file is a back-up of the same file in blackpill. It contains all processing and analysis steps.
- writeJSON.py - This file writes information about process that the user runs in disk, under the user's directory under media.
- copyFiles.py - This file creates the necessary files under a user's directory in media/ to visualize results.
- model.py - This file defines the deep neural network model for DeepSynth
- basicblock.py - This file defines a basic block of the deep neural network model for DeepSynth
- CC2.py - This file performs color coding for the watershed step.
- cmap.txt - This file contains the list of colors used in CC2.py
- segment_options.py - This file contains all the segmentation technique wrappers and functions.