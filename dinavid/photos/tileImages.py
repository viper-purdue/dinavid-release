# tileImages.py
#
# Script for stitching images from multiple 
# 
# Implemented by Soonam Lee
# Date: 09-20-2017 5:00PM
#--------------------------------------------------------------------------

import skimage.io as io
import numpy as np
import scipy.misc
from skimage import util
from skimage import measure
from scipy.ndimage.measurements import label
import time
import os
from skimage.color import rgb2gray
import cv2

def tileImages(X,Y,Z,dirVis,path):
	

	# Measure time
	start1 = time.time()

	# Make tiled image. It should be squared image to input
	tile_W = int(np.ceil(np.sqrt(float(Z))));
	tile_H = tile_W;
	boolColor = -1;

	# Load first file in path and check whether it is color image or gray image
	kk = 0;
	lenDirPath = len(path)
	for dirname, subdirs, files in os.walk(path):
		for filename in files:
			# We need only one image
			if (kk > 0):
				break;
			filepath = os.path.join(dirname, filename);
			I = io.imread(filepath);
			if (len(np.shape(I)) == 2):
				boolColor = 0;
			elif (len(np.shape(I)) > 2):
				boolColor = 1;
			kk = kk + 1;

	# Initialization
	if (boolColor == 0):
		Itiled = util.img_as_ubyte(np.zeros([tile_H*Y, tile_W*X]));
		V = util.img_as_ubyte(np.zeros([Y, X, Z]));

	elif (boolColor == 1):
		Itiled = util.img_as_ubyte(np.zeros([tile_H*Y, tile_W*X, 3]));
		V = util.img_as_ubyte(np.zeros([Y, X, Z, 3]))

	# Load all files in path
	for k in range(0,Z):
		filepath = path + "z%04d.png" % (k+1)
		print(filepath)
		I = io.imread(filepath);
		j = int(float(k)/float(tile_W));
		i = k % tile_W;
		# print(k,j,i)
		if (boolColor == 0):
			I = rgb2gray(I);
			# Tiled each stack of gray scale images
			Itiled[j*Y:(j+1)*Y,i*X:(i+1)*X] = I;
		else:
			# Tiled each stack of color images
			Itiled[j*Y:(j+1)*Y,i*X:(i+1)*X,:] = I;



	# Write stitched image
	fname = dirVis + "tileImage.png"
	cv2.imwrite(fname,Itiled);

	## Measure time
	end1 = time.time()
	print((end1-start1))


