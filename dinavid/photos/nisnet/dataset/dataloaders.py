import os
from numpy.core.numeric import full
from torch.utils.data import Dataset
import torch
import numpy as np
from dinavid.photos.nisnet.models import nisnet
import skimage.io as io
from dinavid.photos.nisnet.options.train_options import TrainOptions
import skimage.util as util
from skimage import exposure
from .data_utils import *
from dinavid.photos.nisnet.utils.instance_seg import small_obj_removal

class CustomizedDatasetTrain(Dataset):
    def __init__(self, opt):
        """
        Args:
        :param root_dir (string): Directory with all the images
        :param img_id (list): lists of image id
        """
        self.root_dir = opt.data_dir
        syn_names = sorted(os.listdir(os.path.join(opt.data_dir, 'syn')))
        gt_names = sorted(os.listdir(os.path.join(opt.data_dir, 'gt')))
        self.syn_names = syn_names
        self.gt_names = gt_names
        self.pad_mode = opt.pad_mode
        self.small_obj_vox = opt.small_obj_vox
        self.num_classes = opt.num_classes

    def __len__(self):
        return len(self.syn_names)
    
    def __getitem__(self, idx):
        # read images
        syn_dir = os.path.join(self.root_dir, 'syn', self.syn_names[idx])
        vol_syn = util.img_as_ubyte(io.imread(syn_dir))
        gt_dir = os.path.join(self.root_dir, 'gt', self.gt_names[idx])
        vol_gt = io.imread(gt_dir)
        # make sure dimension is correct
        vol_syn, vol_gt = prepare(vol_syn, vol_gt)
        # vol_syn = np.pad(vol_syn.copy(), ((8, 8), (8, 8),(8, 8)), self.pad_mode)
        # vol_gt = np.pad(vol_gt.copy(), ((8, 8), (8, 8),(8, 8)), self.pad_mode)

        # vol_syn = vol_syn[np.newaxis, :, :, :, np.newaxis]
        # vol_gt = vol_gt[np.newaxis, :, :, :]
        # print('-----------', vol_syn.shape, vol_gt.shape)
        vol_syn, vol_gt = aug_one_sample(vol_syn, vol_gt)
        # print(vol_syn.shape, vol_gt.shape)
        # remove small objects
        vol_gt = small_obj_removal(vol_gt, self.small_obj_vox)
        # print(vol_gt.shape)
        # generate vector flow volume in a fly if data augmentation is needed
        img_ctrs = nisnet.get_object_code(vol_gt, vox_threshold=self.small_obj_vox)
        target = nisnet.encode(img_ctrs, vol_gt, vol_gt.shape[0], vol_gt.shape[1], vol_gt.shape[2], 
                                None, self.num_classes,self.small_obj_vox)
        
        vol_syn = ((vol_syn[np.newaxis,:])/255).astype(np.float32)
        target = target.astype(np.float32)

        sample = {'img_id': self.syn_names[idx], 'image':vol_syn.copy(), 'target': target.copy()}
        
        return sample
    
class CustomizedDatasetTest(Dataset):
    def __init__(self, opt):
        """
        Args:
        :param root_dir (string): Directory with all the images
        :param img_id (list): lists of image id
        """
        self.root_dir = opt.data_dir
        syn_names = sorted(os.listdir(os.path.join(opt.data_dir, 'syn')))
        # syn_names = [syn_names[0]]
        self.syn_names = syn_names
        self.num_classes = opt.num_classes
        self.gaussian = opt.gaussian
        self.normalize = opt.normalize
        self.clahe = opt.clahe
        self.small_obj_vox = opt.small_obj_vox
        self.data_shape = io.imread(os.path.join(self.root_dir, 'syn', self.syn_names[0])).shape
        self.opt = opt
        self.pad_mode = opt.pad_mode
        self.eval = opt.eval
        if self.eval:
            gt_names = sorted(os.listdir(os.path.join(opt.data_dir, 'gt')))
            self.gt_names = gt_names

    def __len__(self):
        return len(self.syn_names)
        
    def getsize(self):
        return self.data_shape[2], self.data_shape[0], self.data_shape[1]
    
    def __getitem__(self, idx):
        # read images
        syn_dir = os.path.join(self.root_dir, 'syn', self.syn_names[idx])
        vol_syn = util.img_as_ubyte(io.imread(syn_dir))
        vol_orig = vol_syn.copy()[np.newaxis,:]

        # if we need to evaluate the accuracy then need to load the ground truth
        target = np.array([], np.float32)
        if self.eval:
            gt_dir = os.path.join(self.root_dir, 'gt', self.gt_names[idx])
            vol_gt = io.imread(gt_dir)

            if self.opt.block_size == 0:    # if not using block inference
                vol_syn, vol_gt = prepare(vol_syn, vol_gt, mode=self.pad_mode)    # pad to same size
                # vol_syn = np.pad(vol_syn, ((8, 8), (8, 8),(8, 8)), self.pad_mode)
                # vol_gt = np.pad(vol_gt, ((8, 8), (8, 8),(8, 8)), self.pad_mode)

            # remove small objects
            vol_gt = small_obj_removal(vol_gt, self.small_obj_vox)

            # generate vector flow volume in a fly 
            img_ctrs = nisnet.get_object_code(vol_gt, vox_threshold=self.small_obj_vox)
            target = nisnet.encode(img_ctrs, vol_gt, vol_gt.shape[0], vol_gt.shape[1], vol_gt.shape[2], 
                                    None, self.num_classes, self.small_obj_vox)
            target = target.astype(np.float32)

        # if apply gaussian filter
        if self.gaussian != 0:
            vol_syn = gaussian(vol_syn, preserve_range = True, sigma=self.gaussian)
        if self.clahe:
            print("using CLAHE")
            vol_syn = exposure.equalize_adapthist(vol_syn.astype(np.uint8))
            vol_syn = np.uint8(vol_syn*255)
        if self.normalize:
            vol_syn = normalize(vol_syn)
        # normalize vol_syn
        vol_syn = ((np.float32(vol_syn[np.newaxis,:]))/255)

        # 'obj_code' requires bach_size=1, because not every image has the same number of objects
        sample = {'img_id': self.syn_names[idx], 'image':vol_syn, 'target': target, 'orig_img':vol_orig}        
        return sample
    
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    opt = parser.parse_args()
    opt.data_dir = "/pub2/wu1114/Documents/dataset/zebrafish/train_3Dtif"
    opt.num_classes = 2
    opt.small_obj_vox = 50
    # val_dir = os.path.join(data_dir, 'val_3Dtif')
    # test_dir = os.path.join(Config.test_dir, 'test_3D_tif')
    # full_dataset = CustomizedDatasetTrain(train_dir, num_classes = Config.num_classes)
    full_dataset = CustomizedDatasetTrain(opt)
   
    num_train = int(0.9*len(full_dataset))
    num_val = len(full_dataset) - num_train
    train_dataset, test_dataset = torch.utils.data.random_split(full_dataset, [num_train, num_val])
    print(len(full_dataset), len(train_dataset), len(test_dataset))
    # print(len(full_dataset))
    testing_set_loader = torch.utils.data.DataLoader(test_dataset, batch_size=1, shuffle=False, num_workers=0)

    # validation_set_loader = torch.utils.data.DataLoader(validation_set, batch_size=1, shuffle=False, num_workers=4)
    # test_set_loader = torch.utils.data.DataLoader(test_set, batch_size=1, shuffle=False, num_workers=4)
    # print(training_set.getsize())
    from tqdm import tqdm
    for i, sample in tqdm(enumerate(testing_set_loader)):
        print('batch #' + str(i+1))
    # for i, sample in enumerate(validation_set_loader):
    #     print(sample['obj_code'].shape)
    # for i, sample in enumerate(test_set_loader):
    #     print(sample['img_id'])

    # for i in range(0, len(validation_set)):
    #     print(training_set[i]['image'].shape, training_set[i]['target'].shape, training_set[i]['obj_code'].shape)
