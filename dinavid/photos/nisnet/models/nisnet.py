import torch
import math
import warnings
import numpy as np
import torch.nn as nn
import torch.autograd
import weakref
from typing import List
import torch.nn.modules.loss
from torch.optim.optimizer import Optimizer
from functools import wraps
import torch.nn.functional as F
from skimage.feature import peak_local_max
from skimage.transform import rescale
from dinavid.photos.nisnet.models.backbones.resAttUNet import ResAttUNet

class NISNet(torch.nn.Module):
    def __init__(self, opt):
        torch.nn.Module.__init__(self)
        self.backbone = ResAttUNet(in_dim=1, out_dim=opt.num_classes+3, num_filter=opt.num_filters)
        self.num_classes = opt.num_classes
        self.num_channels = opt.num_channels

    def forward(self, x: torch.Tensor):
        return self.backbone(x)

    def __str__(self):
        return f"NISNet: {self.backbone}"

class MSELoss(nn.Module):
    def __init__(self):
        nn.Module.__init__(self)
        self.loss = 0

    def forward(self, result, target):
        mse_loss = nn.MSELoss(reduction='mean')
        loss = mse_loss(result, target)
        self.loss = loss.item()
        return loss

class DiceLoss(nn.Module):
    def __init__(self):
        super(DiceLoss, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        #comment out if your model contains a sigmoid or equivalent activation layer
        # inputs = F.sigmoid(inputs)       
        #flatten label and prediction tensors
        inputs = inputs.reshape(-1)
        targets = targets.reshape(-1)
        
        intersection = (inputs * targets).sum()                            
        dice_loss = 1 - (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)  
        return dice_loss

class FocalLoss(nn.Module):
    def __init__(self):
        super(FocalLoss, self).__init__()

    def forward(self, inputs, targets, alpha=0.8, gamma=2, smooth=1):
        inputs = inputs.reshape(-1)
        targets = targets.reshape(-1)
        # first compute binary cross-entropy 
        BCE = F.binary_cross_entropy(inputs, targets, reduction='mean')
        BCE_EXP = torch.exp(-BCE)
        focal_loss = alpha * (1-BCE_EXP)**gamma * BCE
        return focal_loss

class TverskyLoss(nn.Module):
    def __init__(self, alpha=0.3, beta=0.7, smooth=1):
        super(TverskyLoss, self).__init__()
        self.alpha = alpha
        self.beta = beta
        self.smooth = smooth

    def forward(self, inputs, targets):
                
        #flatten label and prediction tensors
        inputs = inputs.reshape(-1)
        targets = targets.reshape(-1)
        
        #True Positives, False Positives & False Negatives
        TP = (inputs * targets).sum()    
        FP = ((1-targets) * inputs).sum()
        FN = (targets * (1-inputs)).sum()
       
        Tversky = (TP + self.smooth) / (TP + self.alpha*FP + self.beta*FN + self.smooth)  
        
        return 1 - Tversky

class WeightedBCE(nn.Module):

    def __init__(self, weights=[0.4, 0.6]):
        super(WeightedBCE, self).__init__()
        self.weights = weights

    def forward(self, logit_pixel, truth_pixel):
        # print("====",logit_pixel.size())
        logit = logit_pixel.view(-1)
        truth = truth_pixel.view(-1)
        assert(logit.shape==truth.shape)
        loss = F.binary_cross_entropy(logit, truth, reduction='none')
        pos = (truth>0.5).float()
        neg = (truth<0.5).float()
        pos_weight = pos.sum().item() + 1e-12
        neg_weight = neg.sum().item() + 1e-12
        loss = (self.weights[0]*pos*loss/pos_weight + self.weights[1]*neg*loss/neg_weight).sum()

        return loss

class WeightedDiceLoss(nn.Module):
    def __init__(self, weights=[0.5, 0.5]): # W_pos=0.8, W_neg=0.2
        super(WeightedDiceLoss, self).__init__()
        self.weights = weights

    def forward(self, logit, truth, smooth=1e-5):
        batch_size = len(logit)
        logit = logit.view(batch_size,-1)
        truth = truth.view(batch_size,-1)
        assert(logit.shape==truth.shape)
        p = logit.view(batch_size,-1)
        t = truth.view(batch_size,-1)
        w = truth.detach()
        w = w*(self.weights[1]-self.weights[0])+self.weights[0]
        # p = w*(p*2-1)  #convert to [0,1] --> [-1, 1]
        # t = w*(t*2-1)
        p = w*(p)
        t = w*(t)
        intersection = (p * t).sum(-1)
        union =  (p * p).sum(-1) + (t * t).sum(-1)
        dice  = 1 - (2*intersection + smooth) / (union +smooth)
        # print "------",dice.data

        loss = dice.mean()
        return loss

class WeightedDiceBCE(nn.Module):
    def __init__(self,dice_weight=1,BCE_weight=1):
        super(WeightedDiceBCE, self).__init__()
        self.BCE_loss = WeightedBCE(weights=[0.5, 0.5])
        self.dice_loss = WeightedDiceLoss(weights=[0.5, 0.5])
        self.BCE_weight = BCE_weight
        self.dice_weight = dice_weight

    def _show_dice(self, inputs, targets):
        inputs[inputs>=0.5] = 1
        inputs[inputs<0.5] = 0
        # print("2",np.sum(tmp))
        targets[targets>0] = 1
        targets[targets<=0] = 0
        hard_dice_coeff = 1.0 - self.dice_loss(inputs, targets)
        return hard_dice_coeff

    def forward(self, inputs, targets):
        # inputs = inputs.contiguous().view(-1)
        # targets = targets.contiguous().view(-1)
        # print "dice_loss", self.dice_loss(inputs, targets)
        # print "focal_loss", self.focal_loss(inputs, targets)
        dice = self.dice_loss(inputs, targets)
        BCE = self.BCE_loss(inputs, targets)
        # print "dice",dice
        # print "focal",focal
        dice_BCE_loss = self.dice_weight * dice + self.BCE_weight * BCE

        return dice_BCE_loss

class _LRScheduler(object):

    def __init__(self, optimizer, last_epoch=-1):

        # Attach optimizer
        if not isinstance(optimizer, Optimizer):
            raise TypeError('{} is not an Optimizer'.format(
                type(optimizer).__name__))
        self.optimizer = optimizer

        # Initialize epoch and base learning rates
        if last_epoch == -1:
            for group in optimizer.param_groups:
                group.setdefault('initial_lr', group['lr'])
        else:
            for i, group in enumerate(optimizer.param_groups):
                if 'initial_lr' not in group:
                    raise KeyError("param 'initial_lr' is not specified "
                                   "in param_groups[{}] when resuming an optimizer".format(i))
        self.base_lrs = list(map(lambda group: group['initial_lr'], optimizer.param_groups))
        self.last_epoch = last_epoch

        # Following https://github.com/pytorch/pytorch/issues/20124
        # We would like to ensure that `lr_scheduler.step()` is called after
        # `optimizer.step()`
        def with_counter(method):
            if getattr(method, '_with_counter', False):
                # `optimizer.step()` has already been replaced, return.
                return method

            # Keep a weak reference to the optimizer instance to prevent
            # cyclic references.
            instance_ref = weakref.ref(method.__self__)
            # Get the unbound method for the same purpose.
            func = method.__func__
            cls = instance_ref().__class__
            del method

            @wraps(func)
            def wrapper(*args, **kwargs):
                instance = instance_ref()
                instance._step_count += 1
                wrapped = func.__get__(instance, cls)
                return wrapped(*args, **kwargs)

            # Note that the returned function here is no longer a bound method,
            # so attributes like `__func__` and `__self__` no longer exist.
            wrapper._with_counter = True
            return wrapper

        self.optimizer.step = with_counter(self.optimizer.step)
        self.optimizer._step_count = 0
        self._step_count = 0

        self.step()

    def state_dict(self):
        """Returns the state of the scheduler as a :class:`dict`.

        It contains an entry for every variable in self.__dict__ which
        is not the optimizer.
        """
        return {key: value for key, value in self.__dict__.items() if key != 'optimizer'}

    def load_state_dict(self, state_dict):
        """Loads the schedulers state.

        Arguments:
            state_dict (dict): scheduler state. Should be an object returned
                from a call to :meth:`state_dict`.
        """
        self.__dict__.update(state_dict)

    def get_last_lr(self):
        """ Return last computed learning rate by current scheduler.
        """
        return self._last_lr

    def get_lr(self):
        # Compute learning rate using chainable form of the scheduler
        raise NotImplementedError

    def step(self, epoch=None):
        # Raise a warning if old pattern is detected
        # https://github.com/pytorch/pytorch/issues/20124
        if self._step_count == 1:
            if not hasattr(self.optimizer.step, "_with_counter"):
                warnings.warn("Seems like `optimizer.step()` has been overridden after learning rate scheduler "
                              "initialization. Please, make sure to call `optimizer.step()` before "
                              "`lr_scheduler.step()`. See more details at "
                              "https://pytorch.org/docs/stable/optim.html#how-to-adjust-learning-rate", UserWarning)

            # Just check if there were two first lr_scheduler.step() calls before optimizer.step()
            elif self.optimizer._step_count < 1:
                warnings.warn("Detected call of `lr_scheduler.step()` before `optimizer.step()`. "
                              "In PyTorch 1.1.0 and later, you should call them in the opposite order: "
                              "`optimizer.step()` before `lr_scheduler.step()`.  Failure to do this "
                              "will result in PyTorch skipping the first value of the learning rate schedule. "
                              "See more details at "
                              "https://pytorch.org/docs/stable/optim.html#how-to-adjust-learning-rate", UserWarning)
        self._step_count += 1

        class _enable_get_lr_call:

            def __init__(self, o):
                self.o = o

            def __enter__(self):
                self.o._get_lr_called_within_step = True
                return self

            def __exit__(self, type, value, traceback):
                self.o._get_lr_called_within_step = False
                return self

        with _enable_get_lr_call(self):
            if epoch is None:
                self.last_epoch += 1
                values = self.get_lr()
            else:
                self.last_epoch = epoch
                if hasattr(self, "_get_closed_form_lr"):
                    values = self._get_closed_form_lr()
                else:
                    values = self.get_lr()

        for param_group, lr in zip(self.optimizer.param_groups, values):
            param_group['lr'] = lr

        self._last_lr = [group['lr'] for group in self.optimizer.param_groups]

class CosineAnnealingWarmRestarts(_LRScheduler):
    r"""Set the learning rate of each parameter group using a cosine annealing
    schedule, where :math:`\eta_{max}` is set to the initial lr, :math:`T_{cur}`
    is the number of epochs since the last restart and :math:`T_{i}` is the number
    of epochs between two warm restarts in SGDR:

    .. math::
        \eta_t = \eta_{min} + \frac{1}{2}(\eta_{max} - \eta_{min})\left(1 +
        \cos\left(\frac{T_{cur}}{T_{i}}\pi\right)\right)

    When :math:`T_{cur}=T_{i}`, set :math:`\eta_t = \eta_{min}`.
    When :math:`T_{cur}=0` after restart, set :math:`\eta_t=\eta_{max}`.

    It has been proposed in
    `SGDR: Stochastic Gradient Descent with Warm Restarts`_.

    Args:
        optimizer (Optimizer): Wrapped optimizer.
        T_0 (int): Number of iterations for the first restart.
        T_mult (int, optional): A factor increases :math:`T_{i}` after a restart. Default: 1.
        eta_min (float, optional): Minimum learning rate. Default: 0.
        last_epoch (int, optional): The index of last epoch. Default: -1.

    .. _SGDR\: Stochastic Gradient Descent with Warm Restarts:
        https://arxiv.org/abs/1608.03983
    """

    def __init__(self, optimizer, T_0, T_mult=1, eta_min=0, last_epoch=-1):
        if T_0 <= 0 or not isinstance(T_0, int):
            raise ValueError("Expected positive integer T_0, but got {}".format(T_0))
        if T_mult < 1 or not isinstance(T_mult, int):
            raise ValueError("Expected integer T_mult >= 1, but got {}".format(T_mult))
        self.T_0 = T_0
        self.T_i = T_0
        self.T_mult = T_mult
        self.eta_min = eta_min

        super(CosineAnnealingWarmRestarts, self).__init__(optimizer, last_epoch)

        self.T_cur = self.last_epoch

    def get_lr(self):
        if not self._get_lr_called_within_step:
            warnings.warn("To get the last learning rate computed by the scheduler, "
                          "please use `get_last_lr()`.", DeprecationWarning)

        return [self.eta_min + (base_lr - self.eta_min) * (1 + math.cos(math.pi * self.T_cur / self.T_i)) / 2
                for base_lr in self.base_lrs]

    def step(self, epoch=None):
        """Step could be called after every batch update

        Example:
            >>> scheduler = CosineAnnealingWarmRestarts(optimizer, T_0, T_mult)
            >>> iters = len(dataloader)
            >>> for epoch in range(20):
            >>>     for i, sample in enumerate(dataloader):
            >>>         inputs, labels = sample['inputs'], sample['labels']
            >>>         scheduler.step(epoch + i / iters)
            >>>         optimizer.zero_grad()
            >>>         outputs = net(inputs)
            >>>         loss = criterion(outputs, labels)
            >>>         loss.backward()
            >>>         optimizer.step()

        This function can be called in an interleaved way.

        Example:
            >>> scheduler = CosineAnnealingWarmRestarts(optimizer, T_0, T_mult)
            >>> for epoch in range(20):
            >>>     scheduler.step()
            >>> scheduler.step(26)
            >>> scheduler.step() # scheduler.step(27), instead of scheduler(20)
        """

        if epoch is None and self.last_epoch < 0:
            epoch = 0

        if epoch is None:
            epoch = self.last_epoch + 1
            self.T_cur = self.T_cur + 1
            if self.T_cur >= self.T_i:
                self.T_cur = self.T_cur - self.T_i
                self.T_i = self.T_i * self.T_mult
        else:
            if epoch < 0:
                raise ValueError("Expected non-negative epoch, but got {}".format(epoch))
            if epoch >= self.T_0:
                if self.T_mult == 1:
                    self.T_cur = epoch % self.T_0
                else:
                    n = int(math.log((epoch / self.T_0 * (self.T_mult - 1) + 1), self.T_mult))
                    self.T_cur = epoch - self.T_0 * (self.T_mult ** n - 1) / (self.T_mult - 1)
                    self.T_i = self.T_0 * self.T_mult ** (n)
            else:
                self.T_i = self.T_0
                self.T_cur = epoch
        self.last_epoch = math.floor(epoch)

        class _enable_get_lr_call:

            def __init__(self, o):
                self.o = o

            def __enter__(self):
                self.o._get_lr_called_within_step = True
                return self

            def __exit__(self, type, value, traceback):
                self.o._get_lr_called_within_step = False
                return self

        with _enable_get_lr_call(self):
            for param_group, lr in zip(self.optimizer.param_groups, self.get_lr()):
                param_group['lr'] = lr

        self._last_lr = [group['lr'] for group in self.optimizer.param_groups]


def calc_vector_distance_by_object(gt_vol, y_coords: List[int], x_coords: List[int], z_coords: List[int], image_height: int, image_width: int, image_depth: int, max_dist, vox_threshold:int):
    assert (len(y_coords) == len(x_coords)), "list of coordinates should be the same"
    assert (len(y_coords) > 0), "No centroids in source image"

    shape = [image_height, image_width, image_depth]
    image_coords = np.indices(shape)
    image_coords_planar = np.transpose(image_coords, [1, 2, 3, 0])
    vec_ctr = np.zeros([image_height, image_width, image_depth, 3], dtype=np.float32)
    # for (i, (y, x, z)) in enumerate(zip(y_coords, x_coords, z_coords)):
    nuclei_intensity = list(np.unique(gt_vol))
    nuclei_intensity.remove(0)
    for i in nuclei_intensity:
        (xs, ys, zs) = np.where(gt_vol == i)
        y1, x1, z1, y2, x2, z2 = np.min(xs), np.min(ys), np.min(zs), np.max(xs), np.max(ys), np.max(zs)
        y, x, z = round((y2-y1)/2+y1), round((x2-x1)/2+x1), round((z2-z1)/2+z1) # get centroid
        nucleus_vox = len(gt_vol[gt_vol == i].flatten())
        if nucleus_vox>=vox_threshold:
            vec = np.array([y, x, z]) - image_coords_planar
            # only keep the vector in an object
            obj_mask = np.uint8(gt_vol==i)
            vec = np.float32(vec)*obj_mask[:,:,:,np.newaxis]
            # normalize vec to (-1,1)
            # vec_min, vec_max = np.amin(vec), np.amax(vec)
            # if vec_min < 0:
            #     vec[vec<0] = vec[vec<0]/np.abs(vec_min)
            # if vec_max > 0:
            #     vec[vec>0] = vec[vec>0]/np.abs(vec_max)
            vec_ctr = vec_ctr + vec   # add object distance map to vec_cube
    
    # Clip vectors
    if not max_dist is None:
        active = np.sqrt(np.sum(vec_ctr ** 2, axis=3)) > max_dist
        vec_ctr[active, :] = 0

    return vec_ctr

def calc_vote_image(centroid_vectors: np.array, f):
    channels, height, width, depth = centroid_vectors.shape

    size = np.array(np.array((height, width, depth), dtype=np.float) * ((1/f), (1/f), (1/f)), dtype=np.int)
    indices = np.indices((height, width, depth), dtype=centroid_vectors.dtype)

    # Calculate absolute vectors
    vectors = ((centroid_vectors + indices) * (1/f)).astype(np.int)
    nimage = np.zeros((size[0], size[1], size[2]))

    # Clip pixels
    logic = np.logical_and(np.logical_and(np.logical_and(vectors[0] >= 0, vectors[1] >= 0), vectors[2] >= 0), 
                            np.logical_and(np.logical_and(vectors[0] < size[0], vectors[1] < size[1]), vectors[2] < size[2]))
    coords = vectors[:, logic]

    # Accumulate
    np.add.at(nimage, (coords[0], coords[1], coords[2]), 1)
    return np.expand_dims(nimage, axis=0).astype(np.float32)

def get_object_code(vol_gt, vox_threshold):
    # object volume smaller than vol_threshold will be ignored, this function returns all nuclei centroid
    nuclei_intensity = list(np.unique(vol_gt))
    nuclei_intensity.remove(0)
    object_code = []
    for idx in nuclei_intensity:
        # for ith nuclei
        # print(i, idx)
        h,w,z = vol_gt.shape
        (xs, ys, zs) = np.where(vol_gt == idx)
        y1, x1, z1, y2, x2, z2 = np.min(xs), np.min(ys), np.min(zs), np.max(xs), np.max(ys), np.max(zs)
        y, x, z = round((y2-y1)/2+y1), round((x2-x1)/2+x1), round((z2-z1)/2+z1)
        nucleus_vox = len(vol_gt[vol_gt == idx].flatten())
        if nucleus_vox>=vox_threshold:
            object_code.append([y, x, z, y1, y2, x1, x2, z1, z2, 0])

    return np.array(object_code, np.int16)


def encode(coords, vol_gt, image_height: int, image_width: int, image_depth: int, max_dist: int, num_classes: int, vox_threshold:int):
    if len(coords)!=0:
        y_coords, x_coords, z_coords, _, _, _, _, _, _, _, = np.transpose(coords)
        #Encode vectors
        target_vectors = calc_vector_distance_by_object(vol_gt, y_coords, x_coords, z_coords, image_height, image_width, image_depth, max_dist,vox_threshold)
    else:
        target_vectors = np.zeros([image_height, image_width, image_depth, 3], dtype=np.float32)

    if not max_dist is None:
        target_vectors /= max_dist
    target_vectors = np.transpose(target_vectors, [3, 0, 1, 2])

    #Encode logits (bounding box is drawn as ellipses)
    target_logits = np.zeros((num_classes, image_height, image_width, image_depth))
    target_logits[0] = 1
    target_logits[0][vol_gt>0] = 0
    target_logits[1][vol_gt>0] = 1

    target = np.concatenate((target_vectors, target_logits,vol_gt[np.newaxis,:]))
    return target


def decode(input : np.ndarray, max_dist: int, binning: int, nm_size: int, centroid_threshold: int):
    _, image_height, image_width, image_depth = input.shape
    # centroid_vectors = input[0:3] * max_dist  # no need this line if no normalization is applied during training
    centroid_vectors = input[0:3] 
    logits = input[3:]

    #Calculate class ids and class probabilities
    class_ids = np.expand_dims(np.argmax(logits, axis=0), axis=0).astype(np.int16)
    sum_logits = np.expand_dims(np.sum(logits, axis=0), axis=0)
    class_probs = np.expand_dims(np.max((logits / sum_logits), axis=0), axis=0)
    class_probs = np.clip(class_probs, 0, 1)

    # Calculate the centroid images
    votes = calc_vote_image(centroid_vectors, binning)
    votes_nm = peak_local_max(votes[0], min_distance=nm_size, threshold_abs=centroid_threshold, indices=False, exclude_border=False,num_peaks_per_label=1)
    votes_nm = np.expand_dims(votes_nm, axis=0)
    votes_nm = np.uint8(votes_nm)*255
    # Calculate list of centroid statistics
    coords = np.transpose(np.where(votes_nm[0] > 0))
    centroids = [[y * binning, x * binning, z * binning, class_ids[0, y * binning, x * binning, z * binning] - 1, class_probs[0, y * binning, x * binning, z * binning]] for (y, x, z) in coords]
    # convert the votes and votes_nm to the same dimension as input image
    votes_nm = np.zeros((1, image_height, image_width, image_depth), np.uint8)
    for (y,x,z) in coords:
        votes_nm[0, y*binning, x*binning, z*binning] = 255
    votes_rescale = rescale(votes[0], scale=binning, order=0)   # interpolate with nearest neighbor
    votes_rescale = votes_rescale[np.newaxis,:]

    # length of centroids cannot represent the number of centroids because some peaks might have multiple pixels, if they are adjacent peaks. But it does not affect marker based watershed.
    return centroid_vectors, votes_rescale, class_ids, class_probs, votes_nm, centroids