import argparse

class TestOptions():
    def __init__(self):
        self.parser = argparse.ArgumentParser(prog='dinavid')
        self.parser.add_argument('--data_name', type=str, default='test', help='name of testing subvolume: wsm, immu, sherry, intestine')
        self.parser.add_argument('--mode', type=str, default='test', help='train or test or inference')
        self.parser.add_argument('--backbone', type=str, default='resAttUNet', help='network name: resAttUNet or encoder_decoder')
        self.parser.add_argument('--data_dir', type=str, default='test', help='testing data director which contains folder: syn and gt')
        self.parser.add_argument('--model_dir', type=str, default='test', help='director for loading a model')
        self.parser.add_argument('--num_filters', type=int, default=16, help='number of filters for CNN model')
        self.parser.add_argument('--loss_func', type=str, default='BCEDiceLoss', help='BCEDiceLoss or FocalTverskyLoss')
        self.parser.add_argument('--block_size', type=int, default=128, help='if use block size then specify the block size: 64 or 128, if it is 0 then do not use block inference')
        self.parser.add_argument('--num_channels', type=int, default=1, help='number of channels for input images')
        self.parser.add_argument('--num_classes', type=int, default=2, help='Number of classes. This is including the background')
        self.parser.add_argument('--dev', type=str, default='cuda:0', help='cuda device')
        self.parser.add_argument('--n_workers', type=int, default='1', help='number of workers for loading data')
        self.parser.add_argument('--eval', type=int, default='1', help='if to evaluate the results?')
        self.parser.add_argument('--iou_thresholds', type=tuple, default=(0.25,0.45), help='Iou thresholds for evaluate object voxel based mAP')
        self.parser.add_argument('--small_obj_vox', type=int, default=50, help='The object with how many voxels considered as a small object')
        self.parser.add_argument('--morph', type=int, default=0, help='morphlogical operation')
        self.parser.add_argument('--gaussian', type=float, default=0, help='sigma for gaussian blur, if use gaussian, then should > 0')
        self.parser.add_argument('--normalize', action='store_true', help='if normalize the input image to 0-1 after loading it')
        self.parser.add_argument('--clahe', action='store_true', help='if use CLAHE')
        self.parser.add_argument('--alpha', type=float, default=0.6, help='alpha channel for overlay masks')
        self.parser.add_argument('--pad_mode', type=str, default='symmetric', help='method for padding the volume if not square')
        # centroid decoding parameters
        self.parser.add_argument('--binning', type=int, default=1, help='The amount of spatial binning to use (Values could be: 1, 2, 4, etc.), \
                                                                        Increase binning for increasing the robustness of the detection\
                                                                        Decrease binning to increase spatial accuracy')
        self.parser.add_argument('--nm_size', type=int, default=2, help='How far apart should two centroids minimally be. (values could be: 3, 7, 11, 15, 17, etc.) \
                                                                        Increase value to get less detection close together\
                                                                        Decrease value to improve detection which are very close together')
        self.parser.add_argument('--ctr_threshold', type=int, default=2, help='How many votes constitutes a centroid \
                                                                            Increase value to increase precision and decrease recall (less detections)\
                                                                            Decrease value to increase recall and decrease precision (more detectons)\
                                                                            Determine a correct value by reviewing votes.npy')
        # gradient decoding parameters
        self.parser.add_argument('--vec_decode', type=int, default=1, help='if use vector flow decoding to split touch objects')
        self.parser.add_argument('--condition_erode', type=int, default=1, help='if use conditional erosion to refine markers')
        self.parser.add_argument('--k_val', type=float, default=1, help='boundary decoding value, the smaller the more split between touching nuclei')
        self.parser.add_argument('--T1', type=int, default=2000, help='voxel threshold for coarse eroding if "condition_erode" is enabled')
        self.parser.add_argument('--T2', type=int, default=200, help='voxel threshold for fine eroding if "condition_erode" is enabled')
        self.parser.add_argument('--small_marker_vox', type=int, default=20, help='The markers with how many voxels considered as small markers')


    def parse(self):
        self.opt = self.parser.parse_known_args()

        return self.opt