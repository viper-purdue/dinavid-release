import argparse

class TrainOptions():
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('--data_name', type=str, required=True, help='name of volume: wsm, immu, sherry, intestine')
        self.parser.add_argument('--mode', type=str, default='train', help='train or test or inference')
        self.parser.add_argument('--backbone', type=str, default='resAttUNet', help='network name: resAttUNet or encoder_decoder')
        self.parser.add_argument('--data_dir', type=str, required=True, help='training data director')
        self.parser.add_argument('--pre_train', type=str, default=None, help='pretrained model location')
        self.parser.add_argument('--loss_func', type=str, default='BCEDiceLoss', help='BCEDiceLoss or FocalTverskyLoss, or WBCEDiceLoss')
        self.parser.add_argument('--optim', type=str, default='Adam', help='Adam or SGD optimizer')
        self.parser.add_argument('--cosineLR', type=int, default=0, help='If use Stochastic Gradient Descent with Warm Restarts')
        self.parser.add_argument('--num_filters', type=int, default=16, help='number of filters for CNN model')
        self.parser.add_argument('--epochs', type=int, default=200, help='number of training epochs')
        self.parser.add_argument('--batch_size', type=int, default=8, help='number of training batch size')
        self.parser.add_argument('--lr_rate', type=float, default=0.001, help='learning rate for training')
        self.parser.add_argument('--train_ratio', type=float, default=0.9, help='how many images for training? The rest will be for validation')
        self.parser.add_argument('--num_channels', type=int, default=1, help='number of channels for input images')
        self.parser.add_argument('--num_classes', type=int, default=2, help='Number of classes. This is including the background')
        self.parser.add_argument('--val_interval', type=int, default=5, help='validation interval')
        self.parser.add_argument('--dev', type=str, default='cuda:0', help='cuda device')
        self.parser.add_argument('--n_workers', type=int, default='8', help='number of workers for loading data')
        self.parser.add_argument('--plot_every', type=int, default='5', help='number of iterations for every plot')
        self.parser.add_argument('--small_obj_vox', type=int, default=50, help='The object with how many voxels considered as a small object')
        self.parser.add_argument('--pad_mode', type=str, default='symmetric', help='method for padding the volume if not square')
        
    def parse(self):
        opt = self.parser.parse_args()
        return opt

