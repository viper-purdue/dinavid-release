#%% instance segmentaion base on the output of NISNet3D which includes:
# centroids and output from the network
# some of the ideas are from: https://arxiv.org/pdf/1812.06499.pdf
import numpy as np
import matplotlib.pyplot as plt
from numpy.lib.scimath import sqrt
import skimage.io as io
import os
import cv2 as cv
from skimage.filters import unsharp_mask
from scipy import ndimage as ndi
from scipy import ndimage
from skimage.feature import peak_local_max
from skimage.segmentation import watershed
from dinavid.photos.nisnet.utils.CC2 import CC2
from skimage import measure
from skimage.morphology import erosion, dilation, ball
from skimage.morphology import convex_hull_image
from scipy.ndimage.morphology import binary_fill_holes
from skimage.filters import gaussian

def sobel3d(direction, dim=3):
    if dim==3:
        kernel = np.zeros((3, 3, 3), dtype=np.float32)
        plane = np.array([
            [1, 2, 1],
            [2, 4, 2],
            [1, 2, 1],
        ])
        # plane = np.array([
        #     [2, 3, 2],
        #     [3, 5, 3],
        #     [2, 3, 2],
        # ])
        if direction == 'x':
            kernel[0, :, :] = plane
            kernel[2, :, :] = -plane
        if direction == 'y':
            kernel[:, 0, :] = plane
            kernel[:, 2, :] = -plane
        if direction == 'z':
            kernel[:, :, 0] = plane
            kernel[:, :, 2] = -plane
    elif dim==5:
        kernel = np.zeros((5, 5, 5), dtype=np.float32)
        plane1 = np.array([
            [1,2,3,2,1],
            [2,3,4,3,2],
            [3,4,5,4,3],
            [2,3,4,3,2],
            [1,2,3,2,1]
        ])
        plane2 = np.array([
            [2,3,4,3,2],
            [3,4,5,4,3],
            [4,5,6,5,4],
            [3,4,5,4,3],
            [2,3,4,3,2]
        ])

        if direction == 'x':
            kernel[0, :, :] = plane2
            kernel[1, :, :] = plane1
            kernel[3, :, :] = -plane1
            kernel[4, :, :] = -plane2
        if direction == 'y':
            kernel[:, 0, :] = plane2
            kernel[:, 1, :] = plane1
            kernel[:, 3, :] = -plane1
            kernel[:, 4, :] = -plane2
        if direction == 'z':
            kernel[:, :, 0] = plane2
            kernel[:, :, 1] = plane1
            kernel[:, :, 3] = -plane1
            kernel[:, :, 4] = -plane2
    return kernel


def shift_centroids(centroids_vol):
    y, x, z = np.where(centroids_vol == 255)
    y[y <= 3] += 6
    x[x <= 3] += 6
    z[z <= 3] += 6
    y[y >= centroids_vol.shape[0]-3] -= 3
    x[x >= centroids_vol.shape[1]-3] -= 3
    z[z >= centroids_vol.shape[2]-3] -= 3
    new_vol = np.zeros(centroids_vol.shape, centroids_vol.dtype)
    new_vol[y, x, z] = 255
    return new_vol

def get_M(vol_seg, Tsm, t1, t2, th_small, opt):
    M = np.clip(vol_seg - Tsm, a_min=0, a_max=None)
    # M = concave_to_convex(M)  # some instances cannot be seperated
    M = binary_fill_holes(M).astype(np.float32)
    if opt.condition_erode:
        print('using conditional erosion...')
        M = condition_erosion3d(M, t1, t2) # use conditional erosion on M
        # remove small objects
        cc = measure.label(M, connectivity=1)
        props = measure.regionprops(cc)
        for k in range(0, np.amax(cc)):
            if props[k].area < th_small:
                cc[props[k].coords[:, 0], props[k].coords[:, 1], props[k].coords[:, 2]] = 0
        M = np.uint8(cc > 0)*255

    return np.uint8(M)

def get_M_dist_transform(vol_seg, t1, t2, th_small, opt):
    # generate markers using simple distance transform
    # Perform the distance transform algorithm
    dist = ndi.distance_transform_edt(vol_seg)
    # Normalize the distance image for range = {0.0, 1.0}
    # so we can visualize and threshold it
    cv.normalize(dist, dist, 0, 1.0, cv.NORM_MINMAX)
    _, M = cv.threshold(dist, 0.4, 1.0, cv.THRESH_BINARY)
    if opt.condition_erode:
        print('using conditional erosion...')
        M = condition_erosion3d(M, t1, t2) # use conditional erosion on M
        # remove small objects
        cc = measure.label(M, connectivity=1)
        props = measure.regionprops(cc)
        for k in range(0, np.amax(cc)):
            if props[k].area < th_small:
                cc[props[k].coords[:, 0], props[k].coords[:, 1], props[k].coords[:, 2]] = 0
        M = np.uint8(cc > 0)*255

    return np.uint8(M)

def watershed3d(M, root_dir,vol_seg):
    # markers: centroids
    # Energy landscape: distance transform of M
    # local_maxi = shift_centroids(centroids)
    markers = ndi.label(M)[0]     # using M as the markers is not as good as using centroid as markers
    markers = ndi.label(markers)[0]

    distance = ndi.distance_transform_edt(M)
    # distance = gaussian(distance, 1)
    io.imsave(os.path.join(root_dir, 'vol_dist.tif'), distance.astype(np.float32))
    labels = watershed(-distance, markers, mask=vol_seg,watershed_line=True)
    # labels = dilation(labels, ball(3))
    labels_vol = np.uint16(labels)
    return labels_vol

def concave_to_convex(labels_vol):
    cc = measure.label(labels_vol, connectivity=1)
    props = measure.regionprops(cc)
    convex_volume = np.zeros(cc.shape, dtype=cc.dtype)
    for ii in range(0, np.amax(cc)):
        # if props[ii].area >= 10:    # small area may cause error when generating convex image
        try:
            # print('trying to convert to convex')
            y_min, x_min, z_min, y_max, x_max, z_max = props[ii].bbox
            convex_img = props[ii].convex_image * props[ii].label
            y,x,z = np.where(convex_img!=0)
            y = y + y_min   # find the coordinates of the sub-volume in the entire volume
            x = x + x_min
            z = z + z_min
            convex_volume[y,x,z] = props[ii].label
        except:
            y_min, x_min, z_min, y_max, x_max, z_max = props[ii].bbox
            convex_img = props[ii].image * props[ii].label
            y,x,z = np.where(convex_img!=0)
            y = y + y_min   # find the coordinates of the sub-volume in the entire volume
            x = x + x_min
            z = z + z_min
            convex_volume[y,x,z] = props[ii].label

    return convex_volume

def small_obj_removal(vol, Tsmall):
    cc = measure.label(vol, connectivity=1)
    props = measure.regionprops(cc)
    for k in range(0, np.amax(cc)):
        if props[k].area < Tsmall:
            cc[props[k].coords[:, 0], props[k].coords[:, 1],
                props[k].coords[:, 2]] = 0
    vol = measure.label(cc, connectivity=1).astype(np.uint16)
    return vol

# k_meta = {'immu':5, 'sherry':1, 'sherry_update':1, 'wsm':0, 'tarek':5,'zebrafish':5,'F44':1,'intestine':1,'15_0047_0418':1,'15_0418':0,'THN':0, 'liver':1}  # thresholding for the sobel filter derivatives
# T1 = {'wsm':700,'immu':3000, 'sherry':2000,'sherry_update':2000,'F44':2000,'zebrafish':4000, 'intestine':2000,'15_0047_0418':1000,'15_0418':1000,'THN':1000,'liver':200} # T1 for conditional erosion
# T2 = {'wsm':200,'immu':500, 'sherry':700,'sherry_update':700,'F44':500, 'zebrafish':500, 'intestine':1000,'15_0047_0418':500,'15_0418':500,'THN':500, 'liver':100} # T2 for conditional erosion
# th_small_M = {'wsm':20,'immu':50,'sherry':50, 'sherry_update':50,'F44':20,'zebrafish':20, 'intestine':20,'15_0047_0418':20,'15_0418':20, 'THN':20,'liver':5}
# th_small_obj = {'wsm':50, 'immu':50, 'sherry':100,'sherry_update':100,'F44':50,'zebrafish':100, 'intestine':50,'15_0047_0418':50,'15_0418':50,'THN':20, 'liver':20}

def instance_seg(vector, class_id, root_dir, opt):
    data_name = opt.data_name
    # root_dir = os.path.join(os.curdir, 'instance_seg_results', data_name)
    # input volume from output of NisNet3D
    img = vector
    q = class_id
    # centroids = tif.imread(os.path.join(root_dir, 'centroids.tif'))
    h = 0.5 # parameter for thresholding probability mask from the segmentation branch
    k = opt.k_val
    t1 = opt.T1
    t2 = opt.T2
    th_M = opt.small_marker_vox
    th_obj = opt.small_obj_vox
    #3d
    # vol_y = img[:,0,:,:]
    # vol_x = img[:,1,:,:]
    # vol_z = img[:,2,:,:]
    vol_y = img[:,:,:,0]
    vol_x = img[:,:,:,1]
    vol_z = img[:,:,:,2]
    sobel_vol_y = ndimage.convolve(vol_y, sobel3d('y', dim=3))  # be careful of the order of 'x', 'y', and 'z' for convolve
    sobel_vol_x = ndimage.convolve(vol_x, sobel3d('z', dim=3))
    sobel_vol_z = ndimage.convolve(vol_z, sobel3d('x', dim=3))
    # sobel_vol_y_y = ndimage.convolve(vol_y, sobel3d('y', dim=3))
    # sobel_vol_y_x = ndimage.convolve(vol_y, sobel3d('z', dim=3))
    # sobel_vol_y_z = ndimage.convolve(vol_y, sobel3d('x', dim=3))
    # sobel_vol_y = sqrt(np.power(sobel_vol_y_x, 2) + np.power(sobel_vol_y_y, 2) +np.power(sobel_vol_y_z, 2))
    # sobel_vol_x_y = ndimage.convolve(vol_x, sobel3d('y', dim=3))
    # sobel_vol_x_x = ndimage.convolve(vol_x, sobel3d('z', dim=3))
    # sobel_vol_x_z = ndimage.convolve(vol_x, sobel3d('x', dim=3))
    # sobel_vol_x = sqrt(np.power(sobel_vol_x_x, 2) + np.power(sobel_vol_x_y, 2) +np.power(sobel_vol_x_z, 2))
    # sobel_vol_z_y = ndimage.convolve(vol_z, sobel3d('y', dim=3))
    # sobel_vol_z_x = ndimage.convolve(vol_z, sobel3d('z', dim=3))
    # sobel_vol_z_z = ndimage.convolve(vol_z, sobel3d('x', dim=3))
    # sobel_vol_z = sqrt(np.power(sobel_vol_z_x, 2) + np.power(sobel_vol_z_y, 2) +np.power(sobel_vol_z_z, 2))

    io.imsave(os.path.join(root_dir, 'sobel_vol_y.tif'), sobel_vol_y)
    io.imsave(os.path.join(root_dir, 'sobel_vol_x.tif'), sobel_vol_x)
    io.imsave(os.path.join(root_dir, 'sobel_vol_z.tif'), sobel_vol_z)
    sobel_vol_M = np.max(np.array([sobel_vol_x,sobel_vol_y,sobel_vol_z]), axis=0)
    # sobel_vol_M = np.sum(np.array([sobel_vol_x,sobel_vol_y,sobel_vol_z]), axis=0) # this does not work
    # sobel_vol_M = np.sqrt(sobel_vol_x**2 + sobel_vol_y**2 + sobel_vol_z**2) # this does not work
    io.imsave(os.path.join(root_dir, 'sobel_vol_M.tif'), sobel_vol_M)
    #
    vol_seg = (q>h).astype(np.float32)
    vol_seg = binary_fill_holes(vol_seg).astype(np.float32) # fill holes will help watershed
    # vol_seg = small_obj_removal(vol_seg, th_obj)
    # vol_seg, _ = fill_holes(vol_seg)
    io.imsave(os.path.join(root_dir, 'vol_seg.tif'), np.uint8(vol_seg*255))

    Tsm = sobel_vol_M.copy()
    Tsm[Tsm<k] = 0
    Tsm[Tsm>0] = 1
    io.imsave(os.path.join(root_dir, 'vol_Tsm.tif'), np.uint8(Tsm*255))
    #
    if opt.vec_decode:
        M = get_M(vol_seg, Tsm, t1, t2, th_M, opt)
    else:
        M = get_M_dist_transform(vol_seg, t1, t2, th_M, opt)
    io.imsave(os.path.join(root_dir, 'M.tif'), M)
   
    # ---------------------marker-controlled watershed-------------------
    labels_vol = watershed3d(M, root_dir,vol_seg)
    cc_instance_seg_vol = CC2(np.transpose(labels_vol, (1,2,0)), th_obj, 0)
    labels_vol = small_obj_removal(labels_vol, th_obj)
    io.imsave(os.path.join(root_dir, 'seg_CC2.tif'), np.transpose(cc_instance_seg_vol,(2,0,1,3)))

    return labels_vol, np.transpose(cc_instance_seg_vol,(2,0,1,3))

def condition_erosion3d(volume, T1, T2):
    # volume: binary volume
    # T1: nuclei voxels threshold for condition erosion with coarse erosion
    # T2: nuclei voxels threshold for condition erosion with fine erosion

    # Define fine structure
    fine_structure = ball(1)
    # Define coarse structure
    coarse_structure = get_coarse_structure(2)

    # Erode on volumes with coarse structure until the size of objects is smaller than T1
    while(True):
        cc = measure.label(volume, connectivity=1)
        props = measure.regionprops(cc)
        erosion_nuclei_vol = np.zeros(volume.shape, dtype=volume.dtype)
        count_big = 0
        for ii in range(0, np.amax(cc)):
            if props[ii].area > T1:
                count_big += 1
                temp_vol = np.zeros(volume.shape, dtype=volume.dtype)
                temp_vol[props[ii].coords[:, 0],
                         props[ii].coords[:, 1], props[ii].coords[:, 2]] = 1
                bbox = props[ii].bbox
                direction = np.argmax(
                    [bbox[3]-bbox[0], bbox[4]-bbox[1], bbox[5]-bbox[2]])
                # temp_vol = erosion(temp_vol, coarse_structure[direction])
                temp_vol = erosion(temp_vol, ball(2))
                erosion_nuclei_vol = erosion_nuclei_vol + temp_vol
            else:
                erosion_nuclei_vol[props[ii].coords[:, 0],
                                   props[ii].coords[:, 1], props[ii].coords[:, 2]] = 1

        volume = erosion_nuclei_vol
        if count_big == 0:
            break

    # Erode on volumes with fine structure until the size of objects is smaller than T2
    while(True):
        cc = measure.label(volume, connectivity=1)
        props = measure.regionprops(cc)
        erosion_nuclei_vol = np.zeros(volume.shape, dtype=volume.dtype)
        count_big = 0
        for ii in range(0, np.amax(cc)):
            if props[ii].area > T2:
                count_big += 1
                temp_vol = np.zeros(volume.shape, dtype=volume.dtype)
                temp_vol[props[ii].coords[:, 0],
                         props[ii].coords[:, 1], props[ii].coords[:, 2]] = 1
                bbox = props[ii].bbox
                direction = np.argmax(
                    [bbox[3]-bbox[0], bbox[4]-bbox[1], bbox[5]-bbox[2]])
                temp_vol = erosion(temp_vol, fine_structure)
                erosion_nuclei_vol = erosion_nuclei_vol + temp_vol
            else:
                erosion_nuclei_vol[props[ii].coords[:, 0],
                                   props[ii].coords[:, 1], props[ii].coords[:, 2]] = 1
        volume = erosion_nuclei_vol
        if count_big == 0:
            break
    return volume

def get_coarse_structure(size):
    coarse_z = ball(size)
    coarse_z[:, :, 0] = 0
    coarse_z[:, :, -1] = 0

    coarse_y = ball(size)
    coarse_y[:, 0, :] = 0
    coarse_y[:, -1, :] = 0

    coarse_x = ball(size)
    coarse_x[0, :, :] = 0
    coarse_x[-1, :, :] = 0

    return [coarse_x, coarse_y, coarse_z]

def save_slices(vol, path):
    if not os.path.exists(path):
        os.makedirs(path)
    for i in range(vol.shape[0]):
        io.imsave(os.path.join(path, 'z'+str(i+1).zfill(3)+'.png'),vol[i,:,:,:])