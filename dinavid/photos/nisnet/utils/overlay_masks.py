import os
import numpy as np
from skimage import io, transform
from matplotlib import pyplot as plot
from tqdm import tqdm
import skimage.io as io
from skimage.color import rgb2gray
from skimage import measure
from scipy.ndimage.measurements import label

'''
Utility function for overlay color coded masks to the orignal microscopy volumes
'''
os.system("find . -name '.DS_Store' -delete")
def apply_mask(image, mask, color, alpha=0.5):
    """Apply the given mask to the image (2d).
    """
    # color mask to gray-scale
    gray_mask = np.uint8(rgb2gray(mask)*255)
    cc = measure.label(gray_mask, connectivity = 1)
    props = measure.regionprops(cc)
    for ii in range(1,np.amax(cc)+1):
        for c in range(3):
            image[:, :, c] = np.where(cc == ii,
                                    image[:, :, c] * (1 - alpha) + alpha * mask[:,:,c],
                                    image[:, :, c])
    return image

def apply_mask3d(image, mask, color, alpha=0.5):
    """Apply the given mask to the image (2d).
    """
    # color mask to gray-scale
    gray_mask = np.uint8(rgb2gray(mask)*255)
    cc = measure.label(gray_mask, connectivity = 1)
    props = measure.regionprops(cc)
    # for ii in tqdm(range(1,np.amax(cc)+1)):
    for c in range(3):
        image[:, :, :, c] = np.where(cc > 0,
                                image[:, :, :, c] * (1 - alpha) + alpha * mask[:,:,:,c],
                                image[:, :, :, c])
    return image



if __name__ == '__main__':
    # test apply_mask function
    # import skimage.io.tifffile as tif

    # image = io.imread('utils/image.png')
    # mask = io.imread('utils/mask.png')
    # image = np.stack((image,)*3, axis=-1)
    # # mask = np.uint8(mask>0)
    # result_img = apply_mask(image, mask, color=[1,0,0], alpha=0.2)
    # io.imsave('utils/result_img.png', result_img)

    # image = io.imread('utils/microscopy_2021_02_13_nistnet_v1.0/sherry_split1_vol06.tif')
    # mask = io.imread('utils/microscopy_2021_02_13_nistnet_v1.0/seg_CC2.tif')
    # image = np.stack((image,)*3, axis=-1)
    # result_img = apply_mask3d(image, mask, color=[1,0,0], alpha=0.5)
    # io.imsave('utils/microscopy_2021_02_13_nistnet_v1.0/overlay/result_img.tif', result_img)
    # root = 'sherry_entire'
    # orig = 'sherry_orig_201_415.tif'
    # for file_name in os.listdir(root):
    #     if 'orig' in file_name or os.path.isdir(root+'/'+file_name):
    #         continue
    #     print('processing ' + file_name)
    #     image = io.imread(root+'/'+orig)

    #     mask = io.imread(root+ '/' + file_name)
    #     image = np.stack((image,)*3, axis=-1)
    #     result_img = apply_mask3d(image, mask, color=[1,0,0], alpha=0.6)
    #     io.imsave(root+'/overlay/' + file_name, result_img)
    orig_dir = None
    image = None
    mask = None
    for root, dirs, files in os.walk("."):
        vol_dir = None
        vol_name = None
        for name in files:
            if not 'liver' in name:
                break

            if 'overlay' in name:
                break
            if 'orig' in name and 'tif' in name:
                orig_dir = os.path.join(root, name)
                image = io.imread(orig_dir)
                image = np.stack((image,)*3, axis=-1)
            elif 'tif' in name:
                vol_name = name
                vol_dir = os.path.join(root,name)
                mask = io.imread(vol_dir)
            else:
                continue
            if orig_dir and vol_dir:
                print(orig_dir, vol_dir)
                result_img = apply_mask3d(image.copy(), mask, color=[1,0,0], alpha=0.6)
                io.imsave(os.path.join(root, 'overlay_'+vol_name), result_img)
                print('saving...'+os.path.join(root, 'overlay_'+vol_name))
