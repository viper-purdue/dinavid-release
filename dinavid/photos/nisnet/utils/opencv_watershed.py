import numpy as np
import scipy as scp
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy.ndimage as nd
import skimage
from skimage import io
from skimage.segmentation import watershed
from skimage.feature import peak_local_max
from skimage.color import gray2rgb
from skimage.filters import gaussian

#read files jpeg file
image = io.imread('utils/F44/z0015.png')
image_thresh = image > 140
labels = nd.label(image_thresh)[0]

distance = nd.distance_transform_edt(image_thresh)
#apply Gaussian filter to the distance map to merge several local maxima into one
distance=gaussian(distance,3)

local_maxi = peak_local_max(distance, labels=labels, footprint=np.ones((9, 9)))
markers = nd.label(local_maxi)[0]
labelled_image = watershed(-distance, markers, mask=image_thresh, watershed_line=True)
labelled_image = np.uint8(labelled_image>0)*255
#find outline of objects for plotting
# boundaries = find_boundaries(labelled_image)
# img_rgb = gray2rgb(image)
# overlay = np.flipud(visualize_boundaries(img_rgb,boundaries))
io.imsave('overlay.png', overlay)
