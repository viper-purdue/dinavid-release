import os
import numpy as np
from skimage import io, transform
from matplotlib import pyplot as plot
from tqdm import tqdm
'''
Utility function for overlay color coded masks to the orignal microscopy volumes
'''
os.system("find . -name '.DS_Store' -delete")
def apply_mask(image, mask, color, alpha=0.5):
    """Apply the given mask to the image (2d).
    """
    # color mask to gray-scale
    gray_mask = np.uint8(rgb2gray(mask)*255)
    cc = measure.label(gray_mask, connectivity = 1)
    props = measure.regionprops(cc)
    for ii in range(1,np.amax(cc)+1):
        for c in range(3):
            image[:, :, c] = np.where(cc == ii,
                                    image[:, :, c] * (1 - alpha) + alpha * mask[:,:,c],
                                    image[:, :, c])
    return image

def apply_mask3d(image, mask, color, alpha=0.5):
    """Apply the given mask to the image (2d).
    """
    # color mask to gray-scale
    gray_mask = np.uint8(rgb2gray(mask)*255)
    cc = measure.label(gray_mask, connectivity = 1)
    props = measure.regionprops(cc)
    # for ii in tqdm(range(1,np.amax(cc)+1)):
    for c in range(3):
        image[:, :, :, c] = np.where(cc > 0,
                                image[:, :, :, c] * (1 - alpha) + alpha * mask[:,:,:,c],
                                image[:, :, :, c])
    return image



if __name__ == '__main__':
    # test apply_mask function
    import skimage.io as io
    from skimage.color import rgb2gray
    from skimage import measure
    from scipy.ndimage.measurements import label
    # import skimage.io.tifffile as tif

    # image = io.imread('utils/image.png')
    # mask = io.imread('utils/mask.png')
    # image = np.stack((image,)*3, axis=-1)
    # # mask = np.uint8(mask>0)
    # result_img = apply_mask(image, mask, color=[1,0,0], alpha=0.2)
    # io.imsave('utils/result_img.png', result_img)

    # image = io.imread('utils/microscopy_2021_02_13_nistnet_v1.0/sherry_split1_vol06.tif')
    # mask = io.imread('utils/microscopy_2021_02_13_nistnet_v1.0/seg_CC2.tif')
    # image = np.stack((image,)*3, axis=-1)
    # result_img = apply_mask3d(image, mask, color=[1,0,0], alpha=0.5)
    # io.imsave('utils/microscopy_2021_02_13_nistnet_v1.0/overlay/result_img.tif', result_img)

    for file_name in os.listdir('utils/sherry_entire'):
        if 'orig' in file_name or os.path.isdir('utils/sherry_entire/'+file_name):
            continue
        image = io.imread('utils/sherry_entire/sherry_orig_201_415.tif')

        mask = io.imread('utils/sherry_entire/' + file_name)
        image = np.stack((image,)*3, axis=-1)
        result_img = apply_mask3d(image, mask, color=[1,0,0], alpha=0.6)
        io.imsave('utils/sherry_entire/overlay/' + file_name, result_img)
