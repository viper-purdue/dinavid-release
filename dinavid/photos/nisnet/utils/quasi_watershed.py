import numpy as np
from scipy import ndimage as ndi
import os
import scipy.ndimage as nd
from skimage import measure
import skimage.io as io
from skimage.segmentation import watershed
from skimage.feature import peak_local_max
from skimage.filters import gaussian

def quasi_watershed_2d(image):
    distance = ndi.distance_transform_edt(image)
    distance=gaussian(distance,2)
    coords = peak_local_max(distance, footprint=np.ones((7, 7)), labels=image, min_distance=7)
    mask = np.zeros(distance.shape, dtype=bool)
    mask[tuple(coords.T)] = True
    markers, _ = ndi.label(mask)
    labels = watershed(-distance, markers, mask=image, watershed_line=True,connectivity=2)
    labels = np.uint8(labels>0)*255
    # image_thresh = image > 140
    # labels = nd.label(image_thresh)[0]
    # distance = nd.distance_transform_edt(image_thresh)
    # props = measure.regionprops(labels, ['Centroid'])
    # coords = np.array([np.round(p['Centroid']) for p in props], dtype=int)
    # # Create marker image where blob centroids are marked True
    # markers = np.zeros(image.shape, dtype=bool)
    # markers[tuple(np.transpose(coords))] = True
    # labelled_image = watershed(-distance, markers, mask=image_thresh,watershed_line=True)
    # labels = np.uint8(labelled_image>0)*255
    io.imsave('quasi_seg.png', labels)


if __name__ == '__main__':
    # img = io.imread('utils/F44/z0015.png')
    img = io.imread('utils/sherry_binary_seg-1.png')
    quasi_watershed_2d(img)