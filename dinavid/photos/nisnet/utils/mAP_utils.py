"""
MIT License

Copyright (c) 2020 Sergei Belousov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import numpy as np
import pandas as pd
from skimage import measure

def sort_by_col(array, idx=1):
    """Sort np.array by column."""
    order = np.argsort(array[:, idx])[::-1]
    return array[order]

def compute_precision_recall(tp, fp, n_positives):
    """ Compute Preision/Recall.

    Arguments:
        tp (np.array): true positives array.
        fp (np.array): false positives.
        n_positives (int): num positives.

    Returns:
        precision (np.array)
        recall (np.array)
    """
    tp = np.cumsum(tp)
    fp = np.cumsum(fp)
    recall = tp / max(float(n_positives), 1)
    precision = tp / np.maximum(tp + fp, np.finfo(np.float64).eps)
    return precision, recall

def compute_precision_recall_f1(tp,fp,n_positives):
    # this function is used for evaluating object voxel based precision, recall and f1
    tp = np.sum(tp)
    fp = np.sum(fp)
    precision = tp/np.maximum(tp + fp, np.finfo(np.float64).eps) 
    recall = tp/max(float(n_positives), 1)
    f1 = 2*precision*recall/(precision+recall)
    return precision, recall, f1

def compute_average_precision(precision, recall):
    """ Compute Avearage Precision by all points.

    Arguments:
        precision (np.array): precision values.
        recall (np.array): recall values.

    Returns:
        average_precision (np.array)
    """
    precision = np.concatenate(([0.], precision, [0.]))
    recall = np.concatenate(([0.], recall, [1.]))
    for i in range(precision.size - 1, 0, -1):
        precision[i - 1] = np.maximum(precision[i - 1], precision[i])
    ids = np.where(recall[1:] != recall[:-1])[0]
    average_precision = np.sum((recall[ids + 1] - recall[ids]) * precision[ids + 1])
    return average_precision

def compute_average_precision_with_recall_thresholds(precision, recall, recall_thresholds):
    """ Compute Avearage Precision by specific points.

    Arguments:
        precision (np.array): precision values.
        recall (np.array): recall values.
        recall_thresholds (np.array): specific recall thresholds.

    Returns:
        average_precision (np.array)
    """
    average_precision = 0.
    for t in recall_thresholds:
        p = np.max(precision[recall >= t]) if np.sum(recall >= t) != 0 else 0
        average_precision = average_precision + p / recall_thresholds.size
    return average_precision

def compute_distance(pred, gt):
    """ Calculates Euclidean distance of two sets of centroids:
            distance = sqrt((x1-y2)^2+(y1-y2)^2+(z1-z2)^2)

        Parameters:
            Coordinates of centroids are supposed to be in the following form: [x1, y1, z1]
            pred (np.array): predicted centroids
            gt (np.array): ground truth centroids

        Return value:
            dist (np.array): Euclidean distance
    """
    _gt = np.tile(gt, (pred.shape[0], 1))
    _pred = np.repeat(pred, gt.shape[0], axis=0)

    distance = np.sqrt(np.sum((_gt[:,:3]-_pred[:,:3])**2, axis=1))  # debug this

    distance = distance.reshape(pred.shape[0], gt.shape[0])
    return distance

def get_iou(coor1, coor2):
    # get iou of two objects coordinates coor1, coor2
    # coor1: a Nx3 numpy array, coor2: a Mx3 numpy array
    set1 = set(tuple(map(tuple, coor1)))
    set2 = set(tuple(map(tuple, coor2)))
    union = len(set1.union(set2))
    intersection = len(set1.intersection(set2))
    if union == 0:
        return 0
    else:
        return intersection/union

def compute_iou(pred, gt):
    """ Calculates IoU (Jaccard index) of two sets of object coordinates:
            IOU = pred ∩ gt / pred U gt

        Parameters:
            Coordinates of objs are supposed to be in the following form: np.array([[y1,x1,z1],[y2,x2,z2]...])
            pred (np.array): predicted obj coords
            gt (np.array): ground truth obj coords

        Return value:
            iou (np.array): intersection over union
    """
    ious = np.zeros((pred.shape[0], gt.shape[0]))
    for i in range(len(pred)):
        for j in range(len(gt)):
            pred_obj = pred[i][0]
            gt_obj = gt[j][0]
            ious[i,j] = get_iou(pred_obj, gt_obj)
    return ious

def compute_match_table_dist(preds, gt, img_id):
    """ Compute match table.

    Arguments:
        preds (np.array): predicted boxes.
        gt (np.array): ground truth boxes.
        img_id (int): image id

    Returns:
        match_table (pd.DataFrame)


    Input format:
        preds: [xmin, ymin, xmax, ymax, class_id, confidence]
        gt: [xmin, ymin, xmax, ymax, class_id, difficult, crowd]

    Output format:
        match_table: [img_id, confidence, iou, difficult, crowd]
    """
    # iou = compute_iou(preds, gt).tolist()
    distance = compute_distance(preds, gt).tolist()
    
    img_ids = [img_id for i in range(preds.shape[0])]
    confidence = preds[:, 4].tolist()
    difficult = np.repeat(gt[:, 4], preds.shape[0], axis=0).reshape(preds[:, 4].shape[0], -1).tolist()
    crowd = np.repeat(gt[:, 5], preds.shape[0], axis=0).reshape(preds[:, 4].shape[0], -1).tolist()
    match_table = {
        "img_id": img_ids,
        "confidence": confidence,
        "distance": distance,
        "difficult": difficult,
        "crowd": crowd
    }
    return pd.DataFrame(match_table, columns=list(match_table.keys()))

def compute_match_table_iou(preds, gt, img_id):
    """ Compute match table.

    Arguments:
        preds (np.array): predicted boxes.
        gt (np.array): ground truth boxes.
        img_id (int): image id

    Returns:
        match_table (pd.DataFrame)


    Input format:
            preds:  [obj coords, class_id, confidence]
            gt:     [obj coords, class_id, difficult, crowd]
 
    Output format:
        match_table: [img_id, confidence, iou, difficult, crowd]
    """
    ious = compute_iou(preds, gt).tolist()
    # distance = compute_distance(preds, gt).tolist()
    
    img_ids = [img_id for i in range(preds.shape[0])]
    confidence = preds[:, 2].tolist()
    difficult = np.repeat(gt[:, 2], preds.shape[0], axis=0).reshape(preds[:, 2].shape[0], -1).tolist()
    crowd = np.repeat(gt[:, 3], preds.shape[0], axis=0).reshape(preds[:, 2].shape[0], -1).tolist()
    match_table = {
        "img_id": img_ids,
        "confidence": confidence,
        "iou": ious,
        "difficult": difficult,
        "crowd": crowd
    }
    return pd.DataFrame(match_table, columns=list(match_table.keys()))

def row_to_vars_dist(row):
    """ Convert row of pd.DataFrame to variables.

    Arguments:
        row (pd.DataFrame): row

    Returns:
        img_id (int): image index.
        conf (flaot): confidence of predicted box.
        distance (np.array): distance between predicted centroid and gt centroid.
        difficult (np.array): difficult of gt centroids.
        crowd (np.array): crowd of gt centroids.
        order (np.array): sorted order of centroids's.
    """
    img_id = row["img_id"]
    conf = row["confidence"]
    distance = np.array(row["distance"])
    difficult = np.array(row["difficult"])
    crowd = np.array(row["crowd"])
    # order = np.argsort(distance)[::-1]
    order = np.argsort(distance)
    return img_id, conf, distance, difficult, crowd, order

def row_to_vars_iou(row):
    """ Convert row of pd.DataFrame to variables.

    Arguments:
        row (pd.DataFrame): row

    Returns:
        img_id (int): image index.
        conf (flaot): confidence of predicted box.
        iou (np.array): iou between detected obj and gt obj.
        difficult (np.array): difficult of gt obj.
        crowd (np.array): crowd of gt objs.
        order (np.array): sorted order of objs.
    """
    img_id = row["img_id"]
    conf = row["confidence"]
    iou = np.array(row["iou"])
    difficult = np.array(row["difficult"])
    crowd = np.array(row["crowd"])
    order = np.argsort(iou)[::-1]
    return img_id, conf, iou, difficult, crowd, order

def check_distance(distance, difficult, crowd, order, matched_ind, dist_threshold, mpolicy="greedy"):
    """ Check distance for tp/fp/ignore.

    Arguments:
        distance (np.array): Euclidean distance between predicted centroids and gt centroids.
        difficult (np.array): difficult of gt centroids.
        order (np.array): sorted order of distances's.
        matched_ind (list): matched gt indexes.
        dist_threshold (flaot): Euclidean distance threshold.
        mpolicy (str): centroids matching policy.
                       greedy - greedy matching like VOC PASCAL.
                       soft - soft matching like COCO.
    """
    assert mpolicy in ["greedy", "soft"]
    if len(order):
        result = ('fp', -1)
        n_check = 1 if mpolicy == "greedy" else len(order)
        for i in range(n_check):
            idx = order[i]
            if distance[idx] <= dist_threshold:
                if not difficult[idx]:
                    if idx not in matched_ind:
                        result = ('tp', idx)
                        break
                    elif crowd[idx]:
                        result = ('ignore', -1)
                        break
                    else:
                        continue
                else:
                    result = ('ignore', -1)
                    break
            else:
                result = ('fp', -1)
                break
    else:
        result = ('fp', -1)
    return result

def check_iou(iou, difficult, crowd, order, matched_ind, iou_threshold, mpolicy="greedy"):
    """ Check distance for tp/fp/ignore.

    Arguments:
        iou (np.array): iou between detected obj and ground truth obj.
        difficult (np.array): difficult of gt object.
        order (np.array): sorted order of iou's.
        matched_ind (list): matched gt indexes.
        iou_threshold (flaot): iou threshold for matching.
        mpolicy (str): iou matching policy.
                       greedy - greedy matching like VOC PASCAL.
                       soft - soft matching like COCO.
    """
    assert mpolicy in ["greedy", "soft"]
    if len(order):
        result = ('fp', -1)
        n_check = 1 if mpolicy == "greedy" else len(order)
        for i in range(n_check):
            idx = order[i]
            if iou[idx] >= iou_threshold:
                if not difficult[idx]:
                    if idx not in matched_ind:
                        result = ('tp', idx)
                        break
                    elif crowd[idx]:
                        result = ('ignore', -1)
                        break
                    else:
                        continue
                else:
                    result = ('ignore', -1)
                    break
            else:
                result = ('fp', -1)
                break
    else:
        result = ('fp', -1)
    return result


def get_obj_coords(gt_vol, pred_vol):
    cc_gt = measure.label(gt_vol, connectivity=1)
    props_gt = measure.regionprops(cc_gt)
    cc_pred = measure.label(pred_vol, connectivity=1)
    props_pred = measure.regionprops(cc_pred)

    gt = []
    for i in range(np.max(cc_gt)):
        gt.append([props_gt[i].coords,0,0,0])
    pred = []
    for i in range(np.max(cc_pred)):
        pred.append([props_pred[i].coords,0,1])
    
    return np.array(gt), np.array(pred)
