"""
MIT License

Copyright (c) 2020 Sergei Belousov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import numpy as np
from dinavid.photos.nisnet.utils.mAP_utils import *
import skimage.io as io

class MeanAveragePrecisionDist:
    """ Mean Average Precision for object detection using centroid distance as metric.

    Arguments:
        num_classes (int): number of classes.
    """
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self._init()

    def reset(self):
        """Reset stored data."""
        self._init()

    def add(self, preds, gt):
        """ Add sample to evaluation.

        Arguments:
            preds (np.array): predicted boxes.
            gt (np.array): ground truth boxes.

        Input format:
            preds: [xmin, ymin, xmax, ymax, class_id, confidence]
            gt: [xmin, ymin, xmax, ymax, class_id, difficult, crowd]
        """
        assert preds.ndim == 2 and preds.shape[1] == 5
        assert gt.ndim == 2 and gt.shape[1] == 6
        class_counter = np.zeros((1, self.num_classes), dtype=np.int32)
        for c in range(self.num_classes):
            gt_c = np.zeros((0, 6))
            if gt.shape[0] > 0:
                gt_c = gt[gt[:, 3] == c]
                class_counter[0, c] = gt_c.shape[0]
            if preds.shape[0] > 0:
                preds_c = preds[preds[:, 3] == c]
                match_table = compute_match_table_dist(preds_c, gt_c, self.imgs_counter)
                self.match_table[c] = self.match_table[c].append(match_table)
        self.imgs_counter = self.imgs_counter + 1
        self.class_counter = np.concatenate((self.class_counter, class_counter), axis=0)

    def value(self, dist_thresholds=[0.5], recall_thresholds=None, mpolicy="greedy"):
        """ Evaluate Mean Average Precision.

        Arguments:
            dist_thresholds (list of float): Euclidean distance thresholds.
            recall_thresholds (np.array or None): specific recall thresholds to the
                                                  computation of average precision.
            mpolicy (str): box matching policy.
                           greedy - greedy matching like VOC PASCAL.
                           soft - soft matching like COCO.

        Returns:
            metric (dict): evaluated metrics.

        Output format:
            {
                "mAP": float.
                "<iou_threshold_0>":
                {
                    "<cls_id>":
                    {
                        "ap": float,
                        "precision": np.array,
                        "recall": np.array,
                    }
                },
                ...
                "<iou_threshold_N>":
                {
                    "<cls_id>":
                    {
                        "ap": float,
                        "precision": np.array,
                        "recall": np.array,
                    }
                }
            }
        """
        if isinstance(dist_thresholds, float):
            dist_thresholds = [dist_thresholds]

        metric = {}
        aps = np.zeros((0, self.num_classes), dtype=np.float32)
        for t in dist_thresholds:
            metric[t] = {}
            aps_t = np.zeros((1, self.num_classes), dtype=np.float32)
            for class_id in range(self.num_classes):
                aps_t[0, class_id], precision, recall = self._evaluate_class(
                    class_id, t, recall_thresholds, mpolicy
                )
                metric[t][class_id] = {}
                metric[t][class_id]["ap"] = aps_t[0, class_id]
                metric[t][class_id]["precision"] = precision
                metric[t][class_id]["recall"] = recall
            aps = np.concatenate((aps, aps_t), axis=0)
        metric["mAP"] = aps.mean(axis=1).mean(axis=0)
        return metric

    def _evaluate_class(self, class_id, dist_threshold, recall_thresholds, mpolicy="greedy"):
        """ Evaluate class.

        Arguments:
            class_id (int): index of evaluated class.
            dist_threshold (float): Euclidean distance threshold.
            recall_thresholds (np.array or None): specific recall thresholds to the
                                                  computation of average precision.
            mpolicy (str): box matching policy.
                           greedy - greedy matching like VOC PASCAL.
                           soft - soft matching like COCO.

        Returns:
            average_precision (np.array)
            precision (np.array)
            recall (np.array)
        """
        table = self.match_table[class_id].sort_values(by=['confidence'], ascending=False)
        matched_ind = {}
        nd = len(table)
        tp = np.zeros(nd, dtype=np.float64)
        fp = np.zeros(nd, dtype=np.float64)
        for d in range(nd):
            img_id, conf, distance, difficult, crowd, order = row_to_vars_dist(table.iloc[d])
            if img_id not in matched_ind:
                matched_ind[img_id] = []
            res, idx = check_distance(
                distance,
                difficult,
                crowd,
                order,
                matched_ind[img_id],
                dist_threshold,
                mpolicy
            )
            if res == 'tp':
                tp[d] = 1
                matched_ind[img_id].append(idx)
            elif res == 'fp':
                fp[d] = 1
        precision, recall = compute_precision_recall(tp, fp, self.class_counter[:, class_id].sum())
        if recall_thresholds is None:
            average_precision = compute_average_precision(precision, recall)
        else:
            average_precision = compute_average_precision_with_recall_thresholds(
                precision, recall, recall_thresholds
            )
        return average_precision, precision, recall

    def _init(self):
        """ Initialize internal state."""
        self.imgs_counter = 0
        self.class_counter = np.zeros((0, self.num_classes), dtype=np.int32)
        columns = ['img_id', 'confidence', 'distance', 'difficult', 'crowd']
        self.match_table = []
        for i in range(self.num_classes):
            self.match_table.append(pd.DataFrame(columns=columns))

class MeanAveragePrecisionIoU:
    """ Mean Average Precision for object detection using object base IoU as metric.

    Arguments:
        num_classes (int): number of classes.
    """
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self._init()

    def reset(self):
        """Reset stored data."""
        self._init()

    def add(self, pred_vol, gt_vol):
        """ Add sample to evaluation.

        Arguments:
            preds (np.array): segmentation volumes, each obj labeled in different intensity.
            gt (np.array): gt volume.

        Input format:
            preds:  [obj coords, class_id, confidence]
            gt:     [obj coords, class_id, difficult, crowd]
        """
        gt, preds = get_obj_coords(gt_vol, pred_vol)

        assert preds.ndim == 2 and preds.shape[1] == 3
        assert gt.ndim == 2 and gt.shape[1] == 4
        class_counter = np.zeros((1, self.num_classes), dtype=np.int32)
        for c in range(self.num_classes):
            gt_c = np.zeros((0, 4))
            if gt.shape[0] > 0:
                gt_c = gt[gt[:, 1] == c]    # select entries belong to class c
                class_counter[0, c] = gt_c.shape[0]
            if preds.shape[0] > 0:
                preds_c = preds[preds[:, 1] == c]
                match_table = compute_match_table_iou(preds_c, gt_c, self.imgs_counter)
                self.match_table[c] = self.match_table[c].append(match_table)
        self.imgs_counter = self.imgs_counter + 1
        self.class_counter = np.concatenate((self.class_counter, class_counter), axis=0)

    def value(self, iou_thresholds=[0.5], recall_thresholds=None, mpolicy="greedy"):
        """ Evaluate Mean Average Precision.

        Arguments:
            iou_thresholds (list of float): Euclidean distance thresholds.
            recall_thresholds (np.array or None): specific recall thresholds to the
                                                  computation of average precision.
            mpolicy (str): box matching policy.
                           greedy - greedy matching like VOC PASCAL.
                           soft - soft matching like COCO.

        Returns:
            metric (dict): evaluated metrics.

        Output format:
            {
                "mAP": float.
                "<iou_threshold_0>":
                {
                    "<cls_id>":
                    {
                        "ap": float,
                        "precision": np.array,
                        "recall": np.array,
                    }
                },
                ...
                "<iou_threshold_N>":
                {
                    "<cls_id>":
                    {
                        "ap": float,
                        "precision": np.array,
                        "recall": np.array,
                    }
                }
            }
        """
        if isinstance(iou_thresholds, float):
            iou_thresholds = [iou_thresholds]

        metric = {}
        aps = np.zeros((0, self.num_classes), dtype=np.float32)
        Ps = np.zeros((0, self.num_classes), dtype=np.float32)
        Rs = np.zeros((0, self.num_classes), dtype=np.float32)
        F1s = np.zeros((0, self.num_classes), dtype=np.float32)
        for t in iou_thresholds:
            metric[t] = {}
            aps_t = np.zeros((1, self.num_classes), dtype=np.float32)
            P_t = np.zeros((1, self.num_classes), dtype=np.float32)
            R_t = np.zeros((1, self.num_classes), dtype=np.float32)
            F1_t = np.zeros((1, self.num_classes), dtype=np.float32)
            for class_id in range(self.num_classes):
                aps_t[0, class_id], precision, recall = self._evaluate_class(
                    class_id, t, recall_thresholds, mpolicy
                )
                P_t[0,class_id], R_t[0,class_id], F1_t[0,class_id] = self._P_R_F1(
                    class_id, t, recall_thresholds, mpolicy
                )
                # add results for each threshold 't' and each class 'class_id'
                metric[t][class_id] = {}
                metric[t][class_id]["ap"] = aps_t[0, class_id]
                metric[t][class_id]["precision"] = precision
                metric[t][class_id]["recall"] = recall
                metric[t][class_id]["P"] = P_t[0,class_id]
                metric[t][class_id]["R"] = R_t[0,class_id]
                metric[t][class_id]["F1"] = F1_t[0,class_id]
            aps = np.concatenate((aps, aps_t), axis=0)
            Ps = np.concatenate((Ps, P_t), axis=0)
            Rs = np.concatenate((Rs, R_t), axis=0)
            F1s = np.concatenate((F1s, F1_t), axis=0)
        # add results for average
        metric["mAP"] = aps.mean(axis=1).mean(axis=0)
        metric["P"] = Ps.mean(axis=1).mean(axis=0)
        metric["R"] = Rs.mean(axis=1).mean(axis=0)
        metric["F1"] = F1s.mean(axis=1).mean(axis=0)
        return metric

    def _evaluate_class(self, class_id, iou_threshold, recall_thresholds, mpolicy="greedy"):
        """ Evaluate class.

        Arguments:
            class_id (int): index of evaluated class.
            dist_threshold (float): Euclidean distance threshold.
            recall_thresholds (np.array or None): specific recall thresholds to the
                                                  computation of average precision.
            mpolicy (str): box matching policy.
                           greedy - greedy matching like VOC PASCAL.
                           soft - soft matching like COCO.

        Returns:
            average_precision (np.array)
            precision (np.array)
            recall (np.array)
        """
        table = self.match_table[class_id].sort_values(by=['confidence'], ascending=False)
        matched_ind = {}
        nd = len(table)
        tp = np.zeros(nd, dtype=np.float64)
        fp = np.zeros(nd, dtype=np.float64)
        for d in range(nd):
            img_id, conf, iou, difficult, crowd, order = row_to_vars_iou(table.iloc[d])
            if img_id not in matched_ind:
                matched_ind[img_id] = []
            res, idx = check_iou(
                iou,
                difficult,
                crowd,
                order,
                matched_ind[img_id],
                iou_threshold,
                mpolicy
            )
            if res == 'tp':
                tp[d] = 1
                matched_ind[img_id].append(idx)
            elif res == 'fp':
                fp[d] = 1
        precision, recall = compute_precision_recall(tp, fp, self.class_counter[:, class_id].sum())
        if recall_thresholds is None:
            average_precision = compute_average_precision(precision, recall)
        else:
            average_precision = compute_average_precision_with_recall_thresholds(
                precision, recall, recall_thresholds
            )
        return average_precision, precision, recall

    def _P_R_F1(self, class_id, iou_threshold, recall_thresholds, mpolicy="greedy"):
        """ get precision recall and F1 score.

        Arguments:
            class_id (int): index of evaluated class.
            dist_threshold (float): Euclidean distance threshold.
            recall_thresholds (np.array or None): specific recall thresholds to the
                                                  computation of average precision.
            mpolicy (str): box matching policy.
                           greedy - greedy matching like VOC PASCAL.
                           soft - soft matching like COCO.

        Returns:
            Precision, Recall, F1
        """
        table = self.match_table[class_id].sort_values(by=['confidence'], ascending=False)
        matched_ind = {}
        nd = len(table)
        tp = np.zeros(nd, dtype=np.float64)
        fp = np.zeros(nd, dtype=np.float64)
        for d in range(nd):
            img_id, conf, iou, difficult, crowd, order = row_to_vars_iou(table.iloc[d])
            if img_id not in matched_ind:
                matched_ind[img_id] = []
            res, idx = check_iou(
                iou,
                difficult,
                crowd,
                order,
                matched_ind[img_id],
                iou_threshold,
                mpolicy
            )
            if res == 'tp':
                tp[d] = 1
                matched_ind[img_id].append(idx)
            elif res == 'fp':
                fp[d] = 1
        P, R, F1 = compute_precision_recall_f1(tp, fp, self.class_counter[:, class_id].sum())
        return P, R, F1

    def _init(self):
        """ Initialize internal state."""
        self.imgs_counter = 0
        self.class_counter = np.zeros((0, self.num_classes), dtype=np.int32)
        columns = ['img_id', 'confidence', 'iou', 'difficult', 'crowd']
        self.match_table = []
        for i in range(self.num_classes):
            self.match_table.append(pd.DataFrame(columns=columns))


if __name__ == '__main__':
    # -------------------- Test Using Centroid Distance as Metric ------------------- 
    # [x, y, z, class_id, difficult, crowd]
    gt = np.array([
        [15, 15, 10, 0, 0, 0],
        [15, 25, 50, 0, 0, 0],
    ])

    # [x, y, z, class_id, confidence]
    preds = np.array([
        [15, 15, 10, 0, 1],
        [15, 25, 60, 0, 1],
    ])

    # create metric_fn
    metric_fn = MeanAveragePrecisionDist(num_classes=1)

    # add some samples to evaluation
    for i in range(2):
        metric_fn.add(preds, gt)

    # # compute PASCAL VOC metric
    # print(f"VOC PASCAL mAP: {metric_fn.value(iou_thresholds=0.5, recall_thresholds=np.arange(0., 1.1, 0.1))['mAP']}")

    # # compute PASCAL VOC metric at the all points
    # print(f"VOC PASCAL mAP in all points: {metric_fn.value(iou_thresholds=0.5)['mAP']}")

    # compute metric COCO metric
    print(
        f"COCO mAP: {metric_fn.value(dist_thresholds=np.arange(6, 13, 1), mpolicy='soft')['mAP']}")

    # -------------------- Test Using Object Voxel IoU as Metric ------------------- 
    gt_vol = io.imread('utils/vol_gt.tif')
    # pred_vol = io.imread('utils/vol_pred.tif')
    pred_vol = io.imread('utils/vol_gt.tif')

    # create metric_fn
    metric_fn = MeanAveragePrecisionIoU(num_classes=1)

    # add some samples to evaluation
    for i in range(1):
        metric_fn.add(pred_vol, gt_vol)

    # # compute PASCAL VOC metric
    # print(f"VOC PASCAL mAP: {metric_fn.value(iou_thresholds=0.5, recall_thresholds=np.arange(0., 1.1, 0.1))['mAP']}")

    # # compute PASCAL VOC metric at the all points
    print(f"VOC PASCAL mAP in all points: {metric_fn.value(iou_thresholds=0.5)['mAP']}")

    # compute metric COCO metric
    print(
        f"COCO mAP: {metric_fn.value(iou_thresholds=np.arange(0.1, 0.5, 0.1), mpolicy='soft')['mAP']}")