
# Stores commonly used paths
# To use this file, do 
# from dinavid.photos.path_names import *


HOME_MEDIA_PATH = ''
SHARE_MEDIA_PATH = ''
HOME_PATH = ''
DATA_MEDIA_PATH = ''
DATA_FILES_PATH = ''
DATA_FILES_PATH_NON_SPECIAL_USER = ''
CMAP_PATH = ""
VIZ_PATH = ""
VIZ_INPUT_PATH = ""
CHECKPOINT_PATH = ""	
WATERSHED_FILE_PATH = ''
MODELS_HOME_PATH = ''
VIZ_3D_ADDRESS = ""
SPECIAL_USER_LIST = [] 

