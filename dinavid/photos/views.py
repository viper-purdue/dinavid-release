
import os
import sys

import time
from dinavid.photos import test_all_pad
from datetime import datetime

from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.views import View
from .forms import PhotoForm
from .models import Photo, Result, Visual, VisualInput, PreprocessResult
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from dinavid.photos.test_all_pad import *

from django.urls import reverse
import zipfile
from django.core.files import File
import shutil
import json
import subprocess
from dinavid.photos.CC2 import CC2
# visualization
from dinavid.photos.tileImages import tileImages
from dinavid.photos.tileImages_input import tileImages_input, tileImages_input_dropdown, tileImages_input_workflow
from dinavid.photos.writeJSON import writeJSON
from dinavid.photos.copyFiles import copyFiles
from dinavid.photos.writeJSON_input import writeJSON_input
from dinavid.photos.copyFiles_input import copyFiles_input
# timezone
from pytz import timezone
import math
import stat
import glob

from .tasks import celery_process, make_visual_celery, make_visual_celery_dropdown, make_visual_celery_dropdown_nuc, make_visual_celery_workflow, delete_extra_copy, clean_up_folder, delete_extra_copy_pre, remove_orig_files
from celery.result import AsyncResult
######## background subtract ############
from dinavid.photos.background_subtractor import subtract_background_rolling_ball

#dummy data to exporting to csv
import csv
from django.contrib.auth.models import User


#composite images
import tifffile as tiff
import cv2
import numpy as np
import re
import copy

from dinavid.photos.segment_options import segment_3d_watershed, segment_deepsynth, segment_3d_watershed_preview, segment_deepsynth_preview, segment_layercake, segment_layercake_preview, segment_nisnet_preview, segment_nisnet, segment_cellpose_preview, segment_cellpose

from dinavid.photos.scatter_plot import scatter_plot


#Used paths
from dinavid.photos.path_names import *
from dinavid.photos.dinavid_options import *

from django.http import HttpResponseRedirect


#Unzip a zipped folder
def unzip(source_filename, dest_dir):
	with zipfile.ZipFile(source_filename) as zf:
		zf.extractall(dest_dir)

#Find the X,Y dimension of a focal plane/slice.
def findsize(path_image):
	file_base = None
	kk = 0;
	for dirname, subdirs, files in os.walk(path_image):
		for filename in sorted(files):
			# We need only one image
			if (kk > 0):
				break;
			if file_base is None:
				file_base = filename[:-10]
			filepath = os.path.join(dirname, filename);
			I = io.imread(filepath);
			# Check whether it is color or gray image
			if (len(np.shape(I)) == 2):
				[Y, X] = np.shape(I);
				boolColor = 0;
			elif (len(np.shape(I)) > 2):
				[Y, X, _] = np.shape(I);
				boolColor = 1;
			kk = kk + 1;
	return X, Y

#Call to a celery process of the preprocessing steps.
def celeryprocess(request, modelname, colorcode,x1,x2,y1,y2,thSmall,watershed,dataselect,nuc_channel_in,seg_dilate,segdilate_se_size):
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')

	user = request.user
	uname = user.username
	if not uname or uname.isspace():
		 uname = 'default'

	original_data_address = HOME_MEDIA_PATH + uname + '/data/'
	## Need to edit here to enable selection of nuclei channel and take the data from /stack
	visual_zip_temp = HOME_MEDIA_PATH + uname + '/preprocess_temp/'

	if not os.path.exists(visual_zip_temp):
		os.makedirs(visual_zip_temp)

	## let the user select which data to use, preprocessed or original
	if dataselect == "orig_data":
		input_address = original_data_address
	else:
		unzip(HOME_MEDIA_PATH+ dataselect, visual_zip_temp)
		input_address = visual_zip_temp

	copy_address = HOME_MEDIA_PATH + uname + '/'


	##copy all file in user folder to remote machine
	subprocess.call(['scp', '-r', input_address, '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, copy_address)])
  
	#To add the choice for dilate the result
	mytask = celery_process.delay(nuc_channel_in,input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,watershed,seg_dilate,segdilate_se_size)
	mytask.get()
	
	if colorcode =="YES":
		path = HOME_MEDIA_PATH+uname+'/color_coded/'
	else:
		path = HOME_MEDIA_PATH+uname+'/cc_small_removed/'


	visual_name = HOME_MEDIA_PATH+uname+'/visualization/visual_' + datetime_now + '/'
	visual_copy = HOME_MEDIA_PATH+uname+'/visualization/'
	if not os.path.exists(visual_name):
		os.makedirs(visual_name)

	subprocess.call(['scp', '-r', '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, visual_name), visual_copy])

	address_fake = VIZ_3D_ADDRESS
	#address_fake does not matter

	zipfilename = HOME_MEDIA_PATH+uname+'/results_'+ datetime_now +'.zip' 
	zipfilename_copy = HOME_MEDIA_PATH+uname+'/'   
 
	subprocess.call(['scp', '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, zipfilename), zipfilename_copy])

	f = open(zipfilename)
	os.remove(zipfilename)

	mytask_delete = delete_extra_copy.delay(colorcode,path,uname,zipfilename)
	mytask_delete.get()

	if dataselect != "orig_data":
		shutil.rmtree(visual_zip_temp)

#Generate 3D visualization for processing steps. 
#This is the version currently in use.
def make_visual_dropdown_nuc(nuc_channel_in, request, x1, x2, y1, y2, operation, resolution, k_size,  \
	sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel, num_z, num_ch):
	
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')
	user = request.user
	uname = user.username
	
	if not uname or uname.isspace():
		uname = 'default'
	
	original_data_address = HOME_MEDIA_PATH + uname + '/data/orig/'
	visual_temp = HOME_MEDIA_PATH + uname + '/visual_temp/'
	
   
	if not os.path.exists(visual_temp):
		os.makedirs(visual_temp)
	
	#Clean up original folder
	#mytask_delete = clean_up_folder.delay(uname)
	#mytask_delete.get()

	print("task deleted finished")
	input_address = HOME_MEDIA_PATH + uname + '/data/orig'
		
	visual_input_folder = HOME_MEDIA_PATH+uname+'/visualization_input/'
	visual_input_name = HOME_MEDIA_PATH+uname+'/visualization_input/preprocess_visual_' + datetime_now + '/'
   
	preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess/'

	preprocess_url = HOME_MEDIA_PATH+uname+'/preprocess_url/'
	preprocess_url_storage = HOME_MEDIA_PATH+uname+'/preprocess_url_storage/'
	preprocess_orig_to_compare = HOME_MEDIA_PATH+uname+'/preprocess_orig/'
	
	if not os.path.exists(visual_input_name):
		os.makedirs(visual_input_name)
	recursive_file_permissions(visual_input_name,0o775)  
	
	if os.path.exists(preprocess_download):
		shutil.rmtree(preprocess_download)
	if not os.path.exists(preprocess_download):
		os.makedirs(preprocess_download)	
	recursive_file_permissions(preprocess_download,0o775)  

	if not os.path.exists(preprocess_url):
		os.makedirs(preprocess_url)		
	recursive_file_permissions(preprocess_url,0o775)  

	if not os.path.exists(preprocess_url_storage):
		os.makedirs(preprocess_url_storage)   
	recursive_file_permissions(preprocess_url_storage,0o775)  


	print("start delay")
	#Copy from web server to GPU machine.
	copy_address = HOME_MEDIA_PATH + uname + '/'

	W, H, Z, fnamenew, downsample_W, downsample_H = make_visual_celery_dropdown_nuc(nuc_channel_in,uname,datetime_now, visual_input_name, preprocess_download, original_data_address, x1, x2, y1, y2, operation, resolution, k_size,  \
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel, num_z, num_ch,preprocess_orig_to_compare)
		
		
	#Copy JSON file with image name and dimension
	writeJSON_input(W/downsample_W,H/downsample_H,Z,visual_input_name);
	#Copy visual3d.js and test.html file
	copyFiles_input(visual_input_name);
	
	address_fake = VIZ_3D_ADDRESS

	process_folder = HOME_MEDIA_PATH+uname+'/' 

	zipfilename_pre = preprocess_url_storage +'preprocess_' + datetime_now +'_'+ fnamenew +'.zip' 


	if os.path.exists(visual_temp):
		shutil.rmtree(visual_temp)
	return zipfilename_pre

#Intro page
class IntroView(View):   

	def get(self, request):

		return render(self.request, 'photos/tool_intro/index.html')
 
#Getting started page
class GetStartedView(View):   

	def get(self, request):

		return render(self.request, 'photos/tool_intro/get_started.html')
  
 
#Contact page  
class WebContact(View):   

	def get(self, request):

		return render(self.request, 'photos/tool_intro/contact.html')

#Used for display single 3D on a single page
class VisualDisplayView(View):

	def get(self, request):

		return render(self.request, 'photos/visualdisplay/index.html',{'uname':request.user.username, 'address':VIZ_3D_ADDRESS})


	def post(self, request):
		disname = request.POST['disname'] 
		
		return render(self.request, 'photos/visualdisplay/index.html', {'disname': disname, 'uname':request.user.username,'address':VIZ_3D_ADDRESS})


class BasicUploadView(View):


	def get(self, request):

		# photos_list = Photo.objects.all()
		photos_list = Photo.objects.filter(user=request.user)

		return render(self.request, 'photos/basic_upload/index.html', {'photos': photos_list})

	def post(self, request):
		form = PhotoForm(self.request.POST, self.request.FILES)
		if form.is_valid():
			photo = form.save(commit=False)
			photo.user = request.user
			photo.save()
			data = {'is_valid': True, 'name': photo.file.name, 'url': photo.file.url}
		else:
			data = {'is_valid': False}
		return JsonResponse(data)


class ProgressBarUploadView(View):
	def get(self, request):
		photos_list = Photo.objects.filter(user=request.user, iszip=False)
		return render(self.request, 'photos/progress_bar_upload/index.html', {'photos': photos_list})

	def post(self, request):
		time.sleep(1)  # You don't need this line. This is just to delay the process so you can see the progress bar testing locally.
		form = PhotoForm(self.request.POST, self.request.FILES)
		if form.is_valid():
			photo = form.save(commit=False)
			photo.user = request.user
			photo.save()
			data = {'is_valid': True, 'name': photo.file.name, 'url': photo.file.url}
		else:
			data = {'is_valid': False}
		return JsonResponse(data)


class DragAndDropUploadView(View):
	def get(self, request):
		photos_list = Photo.objects.filter(user=request.user, iszip=False)
		return render(self.request, 'photos/drag_and_drop_upload/index.html', {'photos': photos_list})

	def post(self, request):
		form = PhotoForm(self.request.POST, self.request.FILES)
		if form.is_valid():
			photo = form.save(commit=False)
			photo.user = request.user
			photo.save()
			data = {'is_valid': True, 'name': photo.file.name, 'url': photo.file.url}
		else:
			data = {'is_valid': False}
		return JsonResponse(data)
	
#Prints the info about the post request generated by the website.
def print_sequal(request):
	print((request.POST))
	logger.info= request
	return redirect(request.POST.get('next'))

#Clears the list of volumes.
def clear_database(request):
	for photo in Photo.objects.filter(user=request.user):
		photo.file.delete()
		photo.delete()
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	stack_path = HOME_MEDIA_PATH+uname+'/data/stacks/'
	colorstack_path = HOME_MEDIA_PATH+uname+'/data/colorstacks/'
	path = HOME_MEDIA_PATH+uname+'/data'
	if os.path.exists(stack_path):
		shutil.rmtree(stack_path)
	if os.path.exists(colorstack_path):
		shutil.rmtree(colorstack_path)
	if os.path.exists(path):
		if os.path.islink(path):
			os.unlink(path)
		else:
			shutil.rmtree(path)
		
	return redirect(request.POST.get('next'))

#Clears the list of segmentations.
def clear_results(request):
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	vname = HOME_MEDIA_PATH+uname+'/visualization/'
	if os.path.exists(vname):
		shutil.rmtree(vname)
	for photo in Result.objects.filter(user=request.user):
		photo.file.delete()
		photo.delete()
	for photo in Visual.objects.filter(user=request.user):
		photo.delete()
	return redirect(request.POST.get('next'))

#Clear the list of visualizations.
def clear_visual_input(request):
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	vname = HOME_MEDIA_PATH+uname+'/visualization_input/'
	if os.path.exists(vname):
		shutil.rmtree(vname)
	vname1 = HOME_MEDIA_PATH+uname+'/preprocess_url/'
	if os.path.exists(vname1):
		shutil.rmtree(vname1)
	vname2 = HOME_MEDIA_PATH+uname+'/preprocess/'
	if os.path.exists(vname2):
		shutil.rmtree(vname2)	
	for photo in VisualInput.objects.filter(user=request.user):
		photo.delete()
	for photo in PreprocessResult.objects.filter(user=request.user):
		photo.delete()
	return redirect(request.POST.get('next'))


#Test exporting to CVS with dummy data
def export_users_csv(request):
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="processed_list.csv"'
	preprocess_list = PreprocessResult.objects.filter(user=request.user).values_list('file', 'uploaded_at')
	writer = csv.writer(response)
	writer.writerow(['File name', 'Date and Time Uploaded at'])

	for p in preprocess_list:
		writer.writerow(p)

	return response
	
	
#Updates the images with the desired colors assigned to each channel on a slice by slice basis.
def update_channel_color_slice(request):

	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	ch_color = request.GET.getlist('ch_color[]') #This will be an array of hex color strings.
	dis_ch = request.GET.getlist('dis_ch[]') #This will be an array of 0s and 1s. Might be in string format.
	num_ch = int(request.GET.getlist('num_ch')[0]) #Number of channels
	ch_path = HOME_MEDIA_PATH + uname + '/data/stacks/' 
	num_slices = len([name for name in os.listdir(ch_path) if os.path.isfile(os.path.join(ch_path, name))])/num_ch
	
	ch_gamma = request.GET.getlist('ch_gamma[]')
	ch_brightness = request.GET.getlist('ch_brightness[]')
	ch_offset = request.GET.getlist('ch_offset[]')
	slice = int(request.GET['curr_slice'])
	
	#Equation to get updated value for each channel: new_val = pow(old_val, gamma)*brightness+offset;
	
	colorstack_path = HOME_MEDIA_PATH+uname+'/data/colorstacks/'
	
	if not os.path.exists(colorstack_path):
		os.makedirs(colorstack_path)
		
	color_list = []
	#Hex string to tuple.
	for channel in range(0,num_ch):
		h = (ch_color[channel]).lstrip('#')
		color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))
		
	#Read the images from stacks
	file_list = sorted(os.listdir(ch_path))
	
	
	s = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch]),0)
	ss = s.shape
	sss = list(ss)
	sss.append(num_ch)
	comp_stack = np.zeros(sss,np.uint8)
	#Apply gamma, brightness, offset.
	if dis_ch[0] == "1":
		comp_stack[:,:,0] = s
		comp_stack[:,:,0] = np.uint8(255.0*np.clip((np.power(comp_stack[:,:,0]/255.0,np.double(ch_gamma[0]))*np.double(ch_brightness[0])+np.double(ch_offset[0])),0,1))
	for channel in range(1,num_ch):
		if dis_ch[channel] == "1":
			comp_stack[:,:,channel] = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch+channel]),0)
			comp_stack[:,:,channel] = np.uint8(255.0*np.clip((np.power(comp_stack[:,:,channel]/255.0,np.double(ch_gamma[channel]))*np.double(ch_brightness[channel])+np.double(ch_offset[channel])),0,1))
	sss = list(ss)
	sss.append(3)
	color_stack = np.zeros(list(sss),np.uint8)
	#Does a maximum intensity projection.
	for ch in range(num_ch):
		for co in range(3):
			color_stack[:,:,co] = np.maximum(color_stack[:,:,co], comp_stack[:,:,ch]/255.0*color_list[ch][2-co] )
			
	#Save image to disk.
	color_name = colorstack_path+'z%04d_color.png' % (slice)
	cv2.imwrite(color_name,color_stack)
	
	#Prepare information for the response to the request.
	for color_dirpath, dirnames, color_files in os.walk(colorstack_path):
		color_file_list = color_files
	color_file_list=sorted(color_file_list)	
	color_file = color_file_list[slice]
	color_dir_path = '../../'+color_dirpath.split('dinavid-gui/')[1]
	
	data = { 'message' : 'This is a successful ajax request in update_channel_color_slice', 'color_dirpath': color_dir_path, 'color_file': color_file }
	return JsonResponse(data)

# Updates channel name with user's input

def update_channel_name(request):
	
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	ch_name_path = SHARE_MEDIA_PATH + '/ch_name/'
	if not os.path.exists(ch_name_path):
		os.makedirs(ch_name_path)	
	
	# Read the original data from ch_name.txt
	old_ch_name = {}
	with open(os.path.join(ch_name_path, 'ch_name.txt'), 'r') as f:
		old_ch_name = json.load(f)

	ch_name = request.GET.getlist('ch_name[]')
	data_name = request.GET['ch_set_name']
	new_ch_name = old_ch_name.copy()

	
	new_ch_name[data_name] = []
	#Fill the rest of the channels
	for i in range(19):
		new_ch_name[data_name].append( "channel %d"%(i+1) )
	#Fill in the actual user input
	for i in range(0, len(ch_name)):
		new_ch_name[data_name][i] = ch_name[i]
	# Update the original data with new data
	with open(os.path.join(ch_name_path, 'ch_name.txt'), 'w') as outfile:
		json.dump(new_ch_name, outfile)
	data = { 'message' : 'This is a successful ajax request in update_channel_name', 'ch_names': new_ch_name[data_name], 'ch_name' : data_name}
	return JsonResponse(data)
	
	
#Load the selected channel name set
def load_channel_names(request):
	data_name = request.GET['im_name']
	ch_name = request.GET.getlist('ch_name[]')
	ch_gamma = request.GET.getlist('ch_gamma[]')
	ch_brightness = request.GET.getlist('ch_brightness[]')
	ch_offset = request.GET.getlist('ch_offset[]')
	ch_color = request.GET.getlist('ch_color[]')
	dis_ch = request.GET.getlist('dis_ch[]')										
	ch_name_set = request.GET.get('ch_set_select')	

	#Load channel nmes from the ch_name.txt file.
	ch_name_path = SHARE_MEDIA_PATH + '/ch_name/'
	with open(os.path.join(ch_name_path, 'ch_name.txt'), 'r') as f:
		ch_name = json.load(f)

	data = { 'message' : 'This is a successful ajax request in load_channel_names', 'ch_name': ch_name[ch_name_set], 'ch_name_set' : ch_name_set}
	return JsonResponse(data)	
	

# Updates selector parameters with user's select
def update_channel_params(request):
	data_name = request.GET['im_name']
	ch_name = request.GET.getlist('ch_name[]')
	ch_gamma = request.GET.getlist('ch_gamma[]')
	ch_brightness = request.GET.getlist('ch_brightness[]')
	ch_offset = request.GET.getlist('ch_offset[]')
	ch_color = request.GET.getlist('ch_color[]')
	dis_ch = request.GET.getlist('dis_ch[]')										
	update_for_all_users = True

	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	if update_for_all_users:
		ch_params_path = SHARE_MEDIA_PATH + '/ch_params/'
	else:
		ch_params_path = HOME_MEDIA_PATH + uname + '/ch_params/'
	if not os.path.exists(ch_params_path):
		os.makedirs(ch_params_path)	

	# Read the original data from ch_params.txt
	old_ch_params = {}
	with open(os.path.join(ch_params_path, 'ch_params.txt'), 'r') as f:
		old_ch_params = json.load(f)

	new_ch_params = old_ch_params.copy()
	for i in range(0, len(ch_gamma)):
		new_ch_params[data_name][i] = [ch_gamma[i], ch_brightness[i], ch_offset[i],ch_color[i],dis_ch[i]]

	# # Update the original data with new data
	"""with open(os.path.join(ch_params_path, 'ch_params.txt'), 'w') as outfile:
		json.dump(new_ch_params, outfile)"""

	data = { 'message' : 'This is a successful ajax request in update_channel_params', 'ch_params': new_ch_params[data_name]}
	return JsonResponse(data)




#Save the LUT
def save_channel_params(request):
	data_name = request.GET['im_name']
	ch_name = request.GET.getlist('ch_name[]')
	ch_gamma = request.GET.getlist('ch_gamma[]')
	ch_brightness = request.GET.getlist('ch_brightness[]')
	ch_offset = request.GET.getlist('ch_offset[]')
	ch_color = request.GET.getlist('ch_color[]')
	dis_ch = request.GET.getlist('dis_ch[]')										
	lut_name = request.GET.get('lut_name')	

	# Read the lut from the luts.txt file.
	ch_params_path = SHARE_MEDIA_PATH + '/ch_params/'
	with open(os.path.join(ch_params_path, 'luts.txt'), 'r') as f:
		ch_params = json.load(f)
		
	#Assign the luts.
	ch_params[lut_name] = 	copy.deepcopy(ch_params['DINAVID_default'])
	for i in range(0, len(ch_gamma)):
		ch_params[lut_name][i] = [ch_gamma[i], ch_brightness[i], ch_offset[i],ch_color[i],dis_ch[i]]
		
	# Save the lut to the luts.txt file.
	with open(os.path.join(ch_params_path, 'luts.txt'), 'w') as outfile:
		json.dump(ch_params, outfile)
		
	data = { 'message' : 'This is a successful ajax request in save_channel_params', 'ch_params': ch_params[lut_name], 'lut' : lut_name, 'lut_keys':list(ch_params.keys())}
	return JsonResponse(data)

#Load the selected LUT
def load_channel_params(request):
	data_name = request.GET['im_name']
	ch_name = request.GET.getlist('ch_name[]')
	ch_gamma = request.GET.getlist('ch_gamma[]')
	ch_brightness = request.GET.getlist('ch_brightness[]')
	ch_offset = request.GET.getlist('ch_offset[]')
	ch_color = request.GET.getlist('ch_color[]')
	dis_ch = request.GET.getlist('dis_ch[]')										
	lut = request.GET.get('lut_select')	


	ch_params_path = SHARE_MEDIA_PATH + '/ch_params/'
	with open(os.path.join(ch_params_path, 'luts.txt'), 'r') as f:
		ch_params = json.load(f)

	data = { 'message' : 'This is a successful ajax request in load_channel_params', 'ch_params': ch_params[lut], 'lut' : lut}
	return JsonResponse(data)
	
#Updates the images with the desired colors assigned to each channel.
def update_channel_color(request):
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'

	ch_color = request.GET.getlist('ch_color_3d[]') #This will be an array of hex color strings.
	dis_ch = request.GET.getlist('dis_ch_3d[]') #This will be an array of 0s and 1s. Might be in string format.
	num_ch = int(request.GET.getlist('num_ch_3d')[0]) #Number of channels
	ch_path = HOME_MEDIA_PATH + uname + '/data/stacks/' 
	num_slices = int(len([name for name in os.listdir(ch_path) if os.path.isfile(os.path.join(ch_path, name))])/num_ch)
	
	ch_gamma = request.GET.getlist('ch_gamma_3d[]')
	ch_brightness = request.GET.getlist('ch_brightness_3d[]')
	ch_offset = request.GET.getlist('ch_offset_3d[]')
	
	#Equation to get updated value for each channel: new_val = pow(old_val, gamma)*brightness+offset;
	
	colorstack_path = HOME_MEDIA_PATH+uname+'/data/colorstacks/'
	if not os.path.exists(colorstack_path):
		#shutil.rmtree(colorstack_path)
		os.makedirs(colorstack_path)
	
	color_list = []
	#Hex string to tuple.
	for channel in range(0,num_ch):
		h = (ch_color[channel]).lstrip('#')
		color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))
		
	#Read the images from stacks
	file_list = sorted(os.listdir(ch_path))
	for slice in range(num_slices):
		s = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch]),0)
		ss = s.shape
		sss = list(ss)
		sss.append(num_ch)
		comp_stack = np.zeros(sss,np.uint8)
		#Apply gamma, brightness, and offset to each channel that is to be displayed.
		if dis_ch[0] == "1":
			comp_stack[:,:,0] = s
			comp_stack[:,:,0] = np.uint8(255.0*np.clip((np.power(comp_stack[:,:,0]/255.0,np.double(ch_gamma[0]))*np.double(ch_brightness[0])+np.double(ch_offset[0])),0,1))
		for channel in range(1,num_ch):
			if dis_ch[channel] == "1":
				comp_stack[:,:,channel] = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch+channel]),0)
				comp_stack[:,:,channel] = np.uint8(255.0*np.clip((np.power(comp_stack[:,:,channel]/255.0,np.double(ch_gamma[channel]))*np.double(ch_brightness[channel])+np.double(ch_offset[channel])),0,1))
		sss = list(ss)
		sss.append(3)
		color_stack = np.zeros(list(sss),np.uint8)
		for ch in range(num_ch):
			for co in range(3):
				color_stack[:,:,co] = np.maximum(color_stack[:,:,co], comp_stack[:,:,ch]/255.0*color_list[ch][2-co] )
				
		
		color_name = colorstack_path+'z%04d_color.png' % (slice)
		cv2.imwrite(color_name,color_stack)
	
	for color_dirpath, dirnames, color_files in os.walk(colorstack_path):
		color_file_list = color_files
	color_file_list.sort() 
	color_dir_path = '../../'+color_dirpath.split('dinavid-gui/')[1]
	data = { 'message' : 'This is a successful ajax request in update_channel_color', 'ch_color' : ch_color, 'dis_ch' : dis_ch, 'num_ch': num_ch, 'num_slices': num_slices, 'color_dirpath': color_dir_path, 'color_file_list': color_file_list}	
		
	return JsonResponse(data)
	
#Selects which image to display_name
def select_image_display(request):
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	im_name = request.GET['image_select']
	
	if(uname in SPECIAL_USER_LIST):
		src = os.path.join(DATA_FILES_PATH,im_name)
	else:
		src = os.path.join(DATA_FILES_PATH_NON_SPECIAL_USER,uname,im_name)
	dst = HOME_MEDIA_PATH + uname + '/data' 
	
	if not os.path.exists(HOME_MEDIA_PATH + uname):
		os.makedirs(HOME_MEDIA_PATH + uname)
	
	if os.path.exists(dst):
		if os.path.islink(dst): 
			os.unlink(dst)
	os.symlink(src, dst)

	# Load the user's channel name
	ch_path_dir = SHARE_MEDIA_PATH + '/ch_name'
	ch_path = ch_path_dir+'/ch_name.txt'
	ch_names = None
	paths = get_paths(uname)
	
	if os.path.exists(ch_path):
		with open(ch_path, 'r') as f:
			ch_names = json.load(f)
	else:
		# Create a default channel name
		ch_names = {}
		ch_names_list = ['channel_%d'%i for i in range(1,20)]
		ch_names_list_no_underscore = ["channel %d"%i for i in range(1,20)]
		#for p in paths:
		temp_dict = {}
		for i in range(1,20):	
			temp_dict[ch_names_list[i-1]]=ch_names_list_no_underscore[i-1]
		ch_names["DINAVID_default"]=temp_dict
		if not os.path.exists(ch_path_dir):
				os.mkdir(ch_path_dir)
		with open(ch_path, 'w') as f:
			json.dump(ch_names, f)
		# Load the user's channel name base on the data name
	
	
	try:
		ch_name = ch_names[im_name] 
		ch_name_set = im_name
	except:
		ch_name_set = "DINAVID_default"
		"""ch_names_list = ['channel_%d'%i for i in range(1,20)]
		ch_names_list_no_underscore = ["channel %d"%i for i in range(1,20)]
		temp_dict = {}
		for i in range(1,20):	
			temp_dict[ch_names_list[i-1]]=ch_names_list_no_underscore[i-1]
		ch_names[im_name]=temp_dict
		ch_name = ch_names[im_name]
		with open(ch_path, 'w') as f:
			json.dump(ch_names, f)
		"""	

	data = { 'message' : 'This is a successful ajax request in select_image_display', 'folder' : im_name, 'ch_name_set' : ch_name_set }	
	
	return JsonResponse(data)
	
#Processing steps for the DEMO page.
def workflow_analysis(request):		
	nuc_channel_in = request.GET['nuclei_channel']
		
	x1 = request.GET['x1_2d']
	x2 = request.GET['x2_2d']
	y1 = request.GET['y1_2d']
	y2 = request.GET['y2_2d']	
	
	num_ch = int(request.GET['numch_2d'])
	num_z = int(request.GET['numz_2d'])
	
	#List of operations
	operation = request.GET.getlist('operation[]')
	
	#Downsampling
	resolution = request.GET.getlist('resolution[]')
	
	#Gaussian Smoothing
	k_size = request.GET.getlist('k_size[]')
	sigma = request.GET.getlist('sigma[]')
	
	#Threshold/clamping
	threshtype = request.GET.getlist('threshtype[]')
	threshold = request.GET.getlist('threshold[]')

	#Morphology
	morph_type = request.GET.getlist('morph_type[]')
	se_type = request.GET.getlist('se_type[]')
	se_size = request.GET.getlist('se_size[]')
	
	#Rolling ball
	ball_size = request.GET.getlist('ball_size[]')
	
	#Median filter
	mediankernel = request.GET.getlist('mediankernel[]')
	
	#Median background substract
	mediansubkernel = request.GET.getlist('mediansubkernel[]') 
	
	#Peforms Processing Steps
	make_visual_dropdown_nuc(nuc_channel_in, request, x1, x2, y1, y2, operation, resolution, k_size,  
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel, num_z, num_ch)
		
	user = request.user 
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	paths = HOME_MEDIA_PATH + uname + '/preprocess/'   
	
	all_subdirs = [x[0] for x in os.walk(paths)]
	all_subdirs.sort()
	
	subs = all_subdirs[-1]
	
	#Segmentation options
	seg_opt = request.GET['seg_opt'] #This could be either 'watershed' or 'deepsynth'
	#Performs segmentation.
	if seg_opt == 'watershed':
		otsu_seg_val = request.GET['otsu_seg_val'] #If watershed is selected, this tells you whether otsu's method was selected. 'true' or 'false'
		seg_thresh_val = request.GET['seg_thresh_val'] #If watershed is selected and the Otsu option is not selected, then use this as the threshold.
		seg_path = segment_3d_watershed_like(uname,subs,otsu_seg_val,seg_thresh_val)
	elif seg_opt == 'deepsynth':
		modelname = request.GET['pretrainmodelname'] 
		colorcode = request.GET['colorcode']
		thSmall = request.GET['thSmall']
		watershed = request.GET['watershed']	
		seg_path = segment_deepsynth(request.user,subs,nuc_channel_in,modelname,colorcode,thSmall,watershed,x1,x2,y1,y2)
	
		   
	#get only files and not directories	
	f_list = [f for f in os.listdir(seg_path) if os.path.isfile(os.path.join(seg_path,f))]	
	f_list.sort()
	len_list = len(f_list)	   
	
	viz_link = "../../" + seg_path.split('dinavid-gui/')[1]
	data = { 'message' : 'This is a successful ajax request in analysis', 'viz_link' : viz_link, 'file_list' : f_list, 'len':len_list }	
		
	return JsonResponse(data)
  
#Nuclei quantification step in the main page.
def workflow_quantification(request):

	rad = request.GET['radius_voxels']
	user = request.user 
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	[chosen_nucli_disp_path, csv_file_path, scatter_plot_path, channel_num, scatter_plot_shape, scatter_plot_coor, pixel_per_unit, x_lim, y_lim] = scatter_plot(request, uname,rad)
	#Read the files	   
	for root,dirs,files in os.walk(chosen_nucli_disp_path):
		f_list =  []
		for name in files:
			f_list.append(name)
		f_list.sort()
		len_list = len(f_list)
		
	viz_link = "../../" + chosen_nucli_disp_path.split('dinavid-gui/')[1]	
	csv_link = "../../" + csv_file_path.split('dinavid-gui/')[1]   
	scatter_link = "../../" + scatter_plot_path.split('dinavid-gui/')[1]  

	# Load the user's channel name
	im_name = request.GET['im_name']
	ch_path = SHARE_MEDIA_PATH + '/ch_name/ch_name.txt'
	ch_names = None
	if os.path.exists(ch_path):
		with open(ch_path, 'r') as f:
			ch_names = json.load(f)
	try:
		ch_name = ch_names[im_name]
	except:
		ch_name = ch_names["DINAVID_default"]
	data = { 'message' : 'This is a successful ajax request in quantification', 
			'viz_link' : viz_link, 'file_list' : f_list, 'len':len_list, 'csv_link':csv_link, 
			'scatter_link':scatter_link,'channel_num':channel_num,'scatter_plot_shape': scatter_plot_shape, 
			'scatter_plot_coor': scatter_plot_coor, 'pixel_per_unit': pixel_per_unit, 'x_lim':x_lim, 'y_lim':y_lim, 'ch_names': ch_name
	}		 

	return JsonResponse(data)
	
#Nuclei quantification step in the main page.
def workflow_quantification_preview(request):

	rad = request.GET['radius_voxels']
	user = request.user 
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	#Seems that the only difference is these 5 lines
	x0 = int(request.GET['x0'])
	x1 = int(request.GET['x1'])
	y0 = int(request.GET['y0'])
	y1 = int(request.GET['y1'])
	
	[chosen_nucli_disp_path, csv_file_path, scatter_plot_path, channel_num, scatter_plot_shape, scatter_plot_coor, pixel_per_unit, x_lim, y_lim] = scatter_plot(request, uname,rad, is_preview=True,im_x0=x0,im_x1=x1,im_y0=y0,im_y1=y1)
	#Read the files	   
	for root,dirs,files in os.walk(chosen_nucli_disp_path):
		f_list =  []
		for name in files:
			f_list.append(name)
		f_list.sort()
		len_list = len(f_list)
		
	viz_link = "../../" + chosen_nucli_disp_path.split('dinavid-gui/')[1]	
	csv_link = "../../" + csv_file_path.split('dinavid-gui/')[1]   
	scatter_link = "../../" + scatter_plot_path.split('dinavid-gui/')[1]  

	# Load the user's channel name
	im_name = request.GET['im_name']
	ch_path = SHARE_MEDIA_PATH + '/ch_name/ch_name.txt'
	ch_names = None
	if os.path.exists(ch_path):
		with open(ch_path, 'r') as f:
			ch_names = json.load(f)
	try:
		ch_name = ch_names[im_name]
	except:
		ch_name = ch_names["DINAVID_default"]
	data = { 'message' : 'This is a successful ajax request in quantification preview', 
			'viz_link' : viz_link, 'file_list' : f_list, 'len':len_list, 'csv_link':csv_link, 
			'scatter_link':scatter_link,'channel_num':channel_num,'scatter_plot_shape': scatter_plot_shape, 
			'scatter_plot_coor': scatter_plot_coor, 'pixel_per_unit': pixel_per_unit, 'x_lim':x_lim, 'y_lim':y_lim, 'ch_names': ch_name
	}   
	return JsonResponse(data)	

#Updates to the nuclei quantification step in the main page.
def update_workflow_quantification(request):
	rad = request.GET['radius_voxels']
	user = request.user 
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	xmin = request.GET['scatter_xmin']
	xmax = request.GET['scatter_xmax']
	ymin = request.GET['scatter_ymin']
	ymax = request.GET['scatter_ymax']
	gated_xmin = float(request.GET['gated_xmin'])  # the region of drawing on the "Gated Cells" panel
	gated_ymin = float(request.GET['gated_ymin'])
	gated_xmax = float(request.GET['gated_xmax'])
	gated_ymax = float(request.GET['gated_ymax'])
	# gated_width_scale = int(request.GET['gated_width'])/512
	# gated_height_scale = int(request.GET['gated_height'])/512
	if_scatter = request.GET['if_scatter_region'] 
	if if_scatter == 'True':
		if_scatter = True
	else:
		if_scatter = False

	x_disp_type= request.GET['x_statistic_type'] 
	y_disp_type= request.GET['y_statistic_type'] 

	x_disp_chan = str(int(request.GET['nuclei_channel_x'])+1)
	y_disp_chan = str(int(request.GET['nuclei_channel_y'])+1)
	
	#Generate scatter plot.
	[chosen_nucli_disp_path, csv_file_path, scatter_plot_path, channel_num, scatter_plot_shape, scatter_plot_coor, pixel_per_unit, x_lim, y_lim] = scatter_plot(request,uname,rad,if_scatter,xmin = float(xmin),xmax = float(xmax),ymin = float(ymin),ymax = float(ymax),
		x_disp_type = int(x_disp_type),x_disp_chan = int(x_disp_chan)-1,y_disp_type = int(y_disp_type),y_disp_chan = int(y_disp_chan)-1, gated_xmin=int(gated_xmin), gated_ymin=int(gated_ymin), gated_xmax=int(gated_xmax), gated_ymax=int(gated_ymax))
	
	for root,dirs,files in os.walk(chosen_nucli_disp_path):
		f_list =  []
		for name in files:
			f_list.append(name)
		f_list.sort()
		len_list = len(f_list)
		
	viz_link = "../../" + chosen_nucli_disp_path.split('dinavid-gui/')[1]	
	csv_link = "../../" + csv_file_path.split('dinavid-gui/')[1]   
	scatter_link = "../../" + scatter_plot_path.split('dinavid-gui/')[1]   
	data = { 'message' : 'This is a successful ajax request in preview', 
			'viz_link' : viz_link, 'file_list' : f_list, 'len':len_list, 'csv_link':csv_link, 
			'scatter_link':scatter_link,'channel_num':channel_num, 'scatter_plot_shape': scatter_plot_shape, 
			'scatter_plot_coor': scatter_plot_coor, 'pixel_per_unit': pixel_per_unit, 'x_lim':x_lim, 'y_lim':y_lim
	}		 
	return JsonResponse(data)
	
	
#Updates to the nuclei quantification step in the main page.
def update_workflow_quantification_preview(request):
	rad = request.GET['radius_voxels']
	user = request.user 
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	xmin = request.GET['scatter_xmin']
	xmax = request.GET['scatter_xmax']
	ymin = request.GET['scatter_ymin']
	ymax = request.GET['scatter_ymax']
	gated_xmin = float(request.GET['gated_xmin'])  # the region of drawing on the "Gated Cells" panel
	gated_ymin = float(request.GET['gated_ymin'])
	gated_xmax = float(request.GET['gated_xmax'])
	gated_ymax = float(request.GET['gated_ymax'])
	if_scatter = request.GET['if_scatter_region'] 
	if if_scatter == 'True':
		if_scatter = True
	else:
		if_scatter = False

	x_disp_type= request.GET['x_statistic_type'] 
	y_disp_type= request.GET['y_statistic_type'] 

	x_disp_chan = str(int(request.GET['nuclei_channel_x'])+1)
	y_disp_chan = str(int(request.GET['nuclei_channel_y'])+1)
	
	#Generate scatter plot. Only these are the only differences between the preview and regular version it seems.
	x0 = int(request.GET['x0'])
	x1 = int(request.GET['x1'])
	y0 = int(request.GET['y0'])
	y1 = int(request.GET['y1'])
	[chosen_nucli_disp_path, csv_file_path, scatter_plot_path, channel_num, scatter_plot_shape, scatter_plot_coor, pixel_per_unit, x_lim, y_lim] = scatter_plot(request,uname,rad,if_scatter,xmin = float(xmin),xmax = float(xmax),ymin = float(ymin),ymax = float(ymax),
		x_disp_type = int(x_disp_type),x_disp_chan = int(x_disp_chan)-1,y_disp_type = int(y_disp_type),y_disp_chan = int(y_disp_chan)-1, gated_xmin=int(gated_xmin), gated_ymin=int(gated_ymin), gated_xmax=int(gated_xmax), gated_ymax=int(gated_ymax), 
		is_preview=True,im_x0=x0,im_x1=x1,im_y0=y0,im_y1=y1)
	
	for root,dirs,files in os.walk(chosen_nucli_disp_path):
		f_list =  []
		for name in files:
			f_list.append(name)
		f_list.sort()
		len_list = len(f_list)
		
	viz_link = "../../" + chosen_nucli_disp_path.split('dinavid-gui/')[1]	
	csv_link = "../../" + csv_file_path.split('dinavid-gui/')[1]   
	scatter_link = "../../" + scatter_plot_path.split('dinavid-gui/')[1]   
	data = { 'message' : 'This is a successful ajax request in preview', 
			'viz_link' : viz_link, 'file_list' : f_list, 'len':len_list, 'csv_link':csv_link, 
			'scatter_link':scatter_link,'channel_num':channel_num, 'scatter_plot_shape': scatter_plot_shape, 
			'scatter_plot_coor': scatter_plot_coor, 'pixel_per_unit': pixel_per_unit, 'x_lim':x_lim, 'y_lim':y_lim
	}		 
	return JsonResponse(data)	
	
#Previews image processing without segmentation by first doing a 2x downsampling.
def workflow_preview(request):
	
	try:
		nuc_channel_in = request.GET['nuclei_channel']
			
		#Coordinates for subregion.
		x1 = int(request.GET['x1_2d'])
		x2 = int(request.GET['x2_2d'])
		y1 = int(request.GET['y1_2d'])
		y2 = int(request.GET['y2_2d']) 
		
		num_ch = int(request.GET['numch_2d'])
		num_z = int(request.GET['numz_2d'])
		
		slice = int(request.GET['curr_slice_analysis']) 

		#List of operations
		operation = request.GET.getlist('operation[]')

		#Downsampling
		resolution = request.GET.getlist('resolution[]')

		#Gaussian Smoothing
		k_size = request.GET.getlist('k_size[]')
		sigma = request.GET.getlist('sigma[]')

		#Threshold/clamping
		threshtype = request.GET.getlist('threshtype[]')
		threshold = request.GET.getlist('threshold[]')

		#Morphology
		morph_type = request.GET.getlist('morph_type[]')
		se_type = request.GET.getlist('se_type[]')
		se_size = request.GET.getlist('se_size[]')

		#Rolling ball
		ball_size = request.GET.getlist('ball_size[]')

		#Median filter
		mediankernel = request.GET.getlist('mediankernel[]')

		#Median background substract
		mediansubkernel = request.GET.getlist('mediansubkernel[]') 
	except:
		print("no processing steps selected")
		operation = ["noop"]
		resolution = ["1.0"]
	if not operation:
		operation = ["noop"]
		resolution = ["1.0"]

	
	#Peforms Processing Steps
	#Now we want to perform the preview on all focal planes now.
	make_visual_dropdown_nuc_preview(nuc_channel_in, request, x1, x2, y1, y2, operation, resolution, k_size,  
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel, num_z, num_ch, slice=-1)
	   
		
	user = request.user 
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	paths = HOME_MEDIA_PATH + uname + '/preprocess_preview/'   
	

	#Prepare data to be sent back to browser.
	for root,dirs,files in os.walk(paths):
		f_list =  []
		for name in files:
			f_list.append(name)
		f_list.sort()
		len_list = len(f_list)
	
		
	viz_link = "../../" + paths.split('dinavid-gui/')[1]

	preprocess_orig_to_compare = HOME_MEDIA_PATH+uname+'/preprocess_orig_prev/'

	for root,dirs,files in os.walk(preprocess_orig_to_compare):
		orig_f_list =  []
		for name in files:
			orig_f_list.append(name)
		orig_f_list.sort()
	
	orig_viz_link = "../../" + preprocess_orig_to_compare.split('dinavid-gui/')[1]
	
	data = { 'message' : 'This is a successful ajax request in workflow preview', 'viz_link' : viz_link, 'file_list' : f_list, 'width': str(x2-x1), 'height': str(y2-y1), 'orig_file_list': orig_f_list, 'orig_viz_link':orig_viz_link }  
	return JsonResponse(data) 	   
  
#This performs the actual segmentation.
def workflow_seg(request):

	#First do preprocessing, in case the user has changed the parameters.
	pre_data = workflow_process(request)
	
	pre_resp = json.loads(pre_data.content)
	#get links and file names from the preview preprocessing step.
	pre_file_list = pre_resp["file_list"]
	pre_vix_link = pre_resp["viz_link"]
	
	#Segmentation options
	seg_opt = request.GET['seg_opt'] #This could be either 'watershed' or 'deepsynth'
	user = request.user 
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	
	paths = HOME_MEDIA_PATH + uname + '/preprocess/'   
	has_processed = 1# int(request.GET['has_processed'])
	nuc_in_seg = int(request.GET['nuc_in_seg'])
	num_ch_seg = int(request.GET['num_ch_seg'])
	all_subdirs = [x[0] for x in os.walk(paths)]
	all_subdirs.sort()
	
	x1_2d = int(request.GET['x1_2d'])
	x2_2d = int(request.GET['x2_2d'])
	y1_2d = int(request.GET['y1_2d'])
	y2_2d = int(request.GET['y2_2d'])
	
	subs = all_subdirs[-1]
	
	#Performs segmentation based on the selected option.
	if seg_opt == 'watershed':
		otsu_seg_val = request.GET['otsu_seg_val'] #If watershed is selected, this tells you whether otsu's method was selected. 'true' or 'false'
		seg_thresh_val = request.GET['seg_thresh_val'] #If watershed is selected and the Otsu option is not selected, then use this as the threshold.
		seg_path,zipfilename = segment_3d_watershed(user,subs,otsu_seg_val,seg_thresh_val,has_processed=has_processed,nuc_in_seg=nuc_in_seg,num_ch_seg=num_ch_seg)
	elif seg_opt == 'deepsynth':
		modelname = request.GET['pretrainmodelname'] 
		colorcode = request.GET['colorcode']
		thSmall = request.GET['thSmall']
		watershed = request.GET['watershed']	
		seg_path,zipfilename = segment_deepsynth(user,subs,modelname,colorcode,thSmall,watershed,has_processed=has_processed,nuc_in_seg=nuc_in_seg,num_ch_seg=num_ch_seg)
	elif seg_opt == 'nisnet':
		modelname = request.GET['pretrainmodelnamenis'] 
		colorcode = request.GET['colorcodenis']
		thSmall = request.GET['thSmallnis'] 
		seg_path,zipfilename = segment_nisnet(user,subs,modelname,colorcode,thSmall,"NO",has_processed=has_processed,nuc_in_seg=nuc_in_seg,num_ch_seg=num_ch_seg)
	elif seg_opt == 'layercake':
		otsu_layercake_val = request.GET['otsu_layercake_val']
		layercake_thresh_val = int(request.GET['layercake_thresh_val'])
		radius_layercake = float(request.GET['radius_layercake'])
		morph_type_layercake = request.GET['morph_type_layercake']
		morph_radius_layercake = float(request.GET['morph_radius_layercake'])
		seg_path,zipfilename = segment_layercake(user,subs,otsu_layercake_val,layercake_thresh_val,has_processed=has_processed,nuc_in_seg=nuc_in_seg,num_ch_seg=num_ch_seg,radius=radius_layercake,morph_radius=morph_radius_layercake,morph_option=morph_type_layercake)
	elif seg_opt == 'cellpose':
		modelname = request.GET['pretrainmodelname'] 
		#modelname = request.GET['pretrainmodelnamecp'] 
		colorcode = request.GET['colorcodecp']
		thSmall = request.GET['thSmallcp'] 
		watershed = request.GET['watershed']	
		seg_path,zipfilename = segment_deepsynth(user,subs,modelname,colorcode,thSmall,watershed,has_processed=has_processed,nuc_in_seg=nuc_in_seg,num_ch_seg=num_ch_seg)
		#seg_path,zipfilename = segment_cellpose(user,subs,modelname,colorcode,thSmall,"NO",has_processed=has_processed,nuc_in_seg=nuc_in_seg)
	

	f_list = [f for f in os.listdir(seg_path) if os.path.isfile(os.path.join(seg_path,f))]	
	f_list.sort()
	len_list = len(f_list)	   
	
	#Generate subwindow view.
	seg_sub_path = HOME_MEDIA_PATH+uname+'/seg_sub/'
	if os.path.exists(seg_sub_path):
		shutil.rmtree(seg_sub_path)
	if not os.path.exists(seg_sub_path):
		os.mkdir(seg_sub_path)
	for f in f_list:	 
		im=cv2.imread(os.path.join(seg_path,f)) #Read segmented result
		cv2.imwrite(os.path.join(seg_sub_path,f),im[y1_2d:y2_2d,x1_2d:x2_2d,:]) #Save subwindowed result
	
	viz_link = "../../" + seg_sub_path.split('dinavid-gui/')[1]
	

	nuc_channel_in = int(request.GET['nuclei_channel'])
	num_ch = int(request.GET['numch_2d'])
	num_z = int(request.GET['numz_2d'])
	
	#Create directory needed to preview the segmentation.
	orig_dirname = HOME_MEDIA_PATH + uname + '/data/orig'
	files = os.listdir(orig_dirname)
	orig_to_compare = HOME_MEDIA_PATH + uname + '/refresh_seg'
	if os.path.exists(orig_to_compare):
		shutil.rmtree(orig_to_compare)
	if not os.path.exists(orig_to_compare):
		os.mkdir(orig_to_compare)
	
	#Original view
	for index, file in enumerate(sorted(files)[nuc_channel_in:-1:num_ch]):
		#Color image (3 channel)
		im_slice = cv2.imread(os.path.join(orig_dirname, file),0)
		orig_imgs = os.path.join(orig_to_compare, file)
		cv2.imwrite(os.path.splitext(orig_imgs)[0]+'.png', im_slice[y1_2d:y2_2d,x1_2d:x2_2d])
	orig_f_list = sorted(os.listdir(orig_to_compare))
	orig_viz_link = "../../" + orig_to_compare.split('dinavid-gui/')[1]
	
	zip_link = None
	if (zipfilename):
		zip_link = "../../" + zipfilename.split('dinavid-gui/')[1]
	
	#Return the information as a json response.
	data = { 'message'	  :   'This is a successful ajax request in segmentation', 
			'viz_link'	  :   viz_link, 
			'file_list'	 :   f_list, 
			'len'		   :   len_list, 
			'orig_file_list':   orig_f_list, 
			'orig_viz_link' :   orig_viz_link,
			'zip_link'	  :   zip_link,
			'pre_file_list' :   pre_file_list, 
			'pre_viz_link'  :   pre_vix_link
			}
	
	return JsonResponse(data)	

#This performs a preview of segmentation.
def workflow_seg_preview(request):

	#First do preprocessing, in case the user has changed the parameters.
	pre_data = workflow_preview(request)
	
	pre_resp = json.loads(pre_data.content)
	#get links and file names from the preview preprocessing step.
	pre_file_list = pre_resp["file_list"]
	pre_vix_link = pre_resp["viz_link"]
	
	#Segmentation options
	
	seg_opt = request.GET['seg_opt'] #This could be either 'watershed' or 'deepsynth'
	user = request.user 
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	has_processed =  1#int(request.GET['has_processed'])
	nuc_in_seg=int(request.GET['nuc_in_seg'])
	
	paths = HOME_MEDIA_PATH + uname + '/preprocess_preview'   
	
	all_subdirs = [x[0] for x in os.walk(paths)]
	all_subdirs.sort()
	
	subs = all_subdirs[-1]
	
	#Performs segmentation preview based on the selected option.
	if seg_opt == 'watershed':
		otsu_seg_val = request.GET['otsu_seg_val'] #If watershed is selected, this tells you whether otsu's method was selected. 'true' or 'false'
		seg_thresh_val = request.GET['seg_thresh_val'] #If watershed is selected and the Otsu option is not selected, then use this as the threshold.
		seg_path = segment_3d_watershed_preview(user,subs,otsu_seg_val,seg_thresh_val,has_processed=has_processed,nuc_in_seg=nuc_in_seg)
	elif seg_opt == 'deepsynth':
		modelname = request.GET['pretrainmodelname'] 
		colorcode = request.GET['colorcode']
		thSmall = request.GET['thSmall']
		watershed = request.GET['watershed']	
		seg_path = segment_deepsynth_preview(user,paths,modelname,colorcode,thSmall,watershed,has_processed=has_processed,nuc_in_seg=nuc_in_seg)
	elif seg_opt == 'nisnet':
		modelname = request.GET['pretrainmodelnamenis'] 
		colorcode = request.GET['colorcodenis']
		thSmall = request.GET['thSmallnis']   
		seg_path = segment_nisnet_preview(user,paths,modelname,colorcode,thSmall,"NO",has_processed=has_processed,nuc_in_seg=nuc_in_seg)
	elif seg_opt == 'layercake':
		otsu_layercake_val = request.GET['otsu_layercake_val']
		layercake_thresh_val = int(request.GET['layercake_thresh_val'])
		radius_layercake = float(request.GET['radius_layercake'])
		morph_type_layercake = request.GET['morph_type_layercake']
		morph_radius_layercake = float(request.GET['morph_radius_layercake'])
		seg_path = segment_layercake_preview(user,subs,otsu_layercake_val,layercake_thresh_val,has_processed=has_processed,nuc_in_seg=nuc_in_seg,radius=radius_layercake,morph_radius=morph_radius_layercake,morph_option=morph_type_layercake)
	elif seg_opt == 'cellpose':
		modelname = request.GET['pretrainmodelname'] 
		#modelname = request.GET['pretrainmodelnamecp']  
		colorcode = request.GET['colorcodecp']
		thSmall = request.GET['thSmallcp'] 
		watershed = request.GET['watershed']	
		seg_path = segment_deepsynth_preview(user,paths,modelname,colorcode,thSmall,watershed,has_processed=has_processed,nuc_in_seg=nuc_in_seg)
		#seg_path,zipfilename = segment_cellpose_preview(user,subs,modelname,colorcode,thSmall,"NO",has_processed=has_processed,nuc_in_seg=nuc_in_seg)
		
	f_list = [f for f in os.listdir(seg_path) if os.path.isfile(os.path.join(seg_path,f))]	
	f_list.sort()
	len_list = len(f_list)	   
	
	viz_link = "../../" + seg_path.split('dinavid-gui/')[1]
	
	#Prepare files for visualizing the preview. 
	preprocess_orig_to_compare = HOME_MEDIA_PATH+uname+'/preprocess_orig_prev/'
	all_subdirs = [x[0] for x in os.walk(preprocess_orig_to_compare)]
	all_subdirs.sort()
	subs = all_subdirs[-1]
	for root,dirs,files in os.walk(subs):
		orig_f_list =  []
		for name in files:
			orig_f_list.append(name)
		orig_f_list.sort()
	orig_viz_link = "../../" + subs.split('dinavid-gui/')[1]

	data = { 'message' : 'This is a successful ajax request in seg preview', 'viz_link' : viz_link, 'file_list' : f_list, 'len':len_list, 'orig_file_list': orig_f_list, 'orig_viz_link':orig_viz_link, 'pre_file_list': pre_file_list, 'pre_viz_link':pre_vix_link  }  
	return JsonResponse(data) 
	
	
#Used to generate preview of processing steps.
#Everything is the same as the non-preview version except for the preprocess_download_path
def make_visual_dropdown_nuc_preview(nuc_channel_in, request, x1, x2, y1, y2, operation, resolution, k_size,  \
	sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel, num_z, num_ch, slice):
	
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')
	user = request.user
	uname = user.username
	if not uname or uname.isspace():
		 uname = 'default'
	original_data_address = HOME_MEDIA_PATH + uname + '/data/'
	visual_temp = HOME_MEDIA_PATH + uname + '/visual_temp/'
	
	#Prepare all necessary directories.	
	if not os.path.exists(visual_temp):
		os.makedirs(visual_temp)


	input_address = HOME_MEDIA_PATH + uname + '/data/orig'
	
		
	visual_input_folder = HOME_MEDIA_PATH+uname+'/visualization_input/'
	visual_input_name = HOME_MEDIA_PATH+uname+'/visualization_input/preprocess_visual_' + datetime_now + '/'
   
	preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess_preview/'

	preprocess_url = HOME_MEDIA_PATH+uname+'/preprocess_url/'
	preprocess_url_storage = HOME_MEDIA_PATH+uname+'/preprocess_url_storage/'
	preprocess_orig_to_compare = HOME_MEDIA_PATH+uname+'/preprocess_orig_prev/'
	
	if not os.path.exists(visual_input_name):
		os.makedirs(visual_input_name)

	if not os.path.exists(preprocess_download):
		os.makedirs(preprocess_download)

	if not os.path.exists(preprocess_url):
		os.makedirs(preprocess_url)

	if not os.path.exists(preprocess_url_storage):
		os.makedirs(preprocess_url_storage)

	copy_address = HOME_MEDIA_PATH + uname + '/'

	#Call to preprocessing steps
	W, H, Z, fnamenew, downsample_W, downsample_H = make_visual_celery_dropdown_nuc(nuc_channel_in,uname,datetime_now, visual_input_name, preprocess_download, input_address, x1, x2, y1, y2, operation, resolution, k_size,  \
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel, num_z, num_ch, preprocess_orig_to_compare, slice)
		
	#Copy JSON file with image name and dimension
	writeJSON_input(W/downsample_W,H/downsample_H,Z,visual_input_name);
	#Copy visual3d.js and test.html file
	copyFiles_input(visual_input_name);
	

	if os.path.exists(visual_temp):
		shutil.rmtree(visual_temp)
	return	 

#Set file permissions recursively to the given mode.
def recursive_file_permissions(path,mode):
	os.chmod(path,mode)
	for item in glob.glob(path+'/*'):
		if os.path.isdir(item):
			recursive_file_permissions(os.path.join(path,item),mode)
		else:
			try:
				os.chmod(os.path.join(path,item),mode)
			except:
				print('File permissions on {0} not updated due to error.'.format(os.path.join(path,item)))
	
#This preforms the actual preprocessing steps.
def workflow_process(request):
	
	try:
		nuc_channel_in = request.GET['nuclei_channel']
			
		#We want to process on the entire image.
		x1 = 0
		x2 = int(request.GET['width_2d'])
		y1 = 0
		y2 = int(request.GET['height_2d']   ) 
		
		#This is used for display purposes only.
		x1_2d = int(request.GET['x1_2d'])
		x2_2d = int(request.GET['x2_2d'])
		y1_2d = int(request.GET['y1_2d'])
		y2_2d = int(request.GET['y2_2d'])
		
		num_ch = int(request.GET['numch_2d'])
		num_z = int(request.GET['numz_2d'])
		
		#List of operations
		operation = request.GET.getlist('operation[]')
		
		#Downsampling
		resolution = request.GET.getlist('resolution[]')
		
		#Gaussian Smoothing
		k_size = request.GET.getlist('k_size[]')
		sigma = request.GET.getlist('sigma[]')
		
		#Threshold/clamping
		threshtype = request.GET.getlist('threshtype[]')
		threshold = request.GET.getlist('threshold[]')

		#Morphology
		morph_type = request.GET.getlist('morph_type[]')
		se_type = request.GET.getlist('se_type[]')
		se_size = request.GET.getlist('se_size[]')
		
		#Rolling ball
		ball_size = request.GET.getlist('ball_size[]')
		
		#Median filter
		mediankernel = request.GET.getlist('mediankernel[]')
		
		#Median background substract
		mediansubkernel = request.GET.getlist('mediansubkernel[]') 
	except:
		print("no processing steps selected")
		operation = ["noop"]
		resolution = ["1.0"]
	if not operation:
		operation = ["noop"]
		resolution = ["1.0"]
		
	#Set permissions of folders
	user = request.user 
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	#recursive_file_permissions(HOME_MEDIA_PATH + uname + '/cropped_slices',0o775)
	#recursive_file_permissions(HOME_MEDIA_PATH + uname + '/preprocess_preview',0o775)	
	#recursive_file_permissions(HOME_MEDIA_PATH + uname + '/preprocess_url',0o775) 
	
	
	#Peforms Processing Steps
	make_visual_dropdown_nuc(nuc_channel_in, request, x1, x2, y1, y2, operation, resolution, k_size,  
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel, num_z, num_ch)
	
	paths = HOME_MEDIA_PATH + uname + '/preprocess/'   
		   
	all_subdirs = [x[0] for x in os.walk(paths)]
	all_subdirs.sort()
	
	subs = all_subdirs[-1]
	
	for root,dirs,files in os.walk(subs):
		f_list =  []
		for name in files:
			f_list.append(name)
		f_list.sort()
		len_list = len(f_list)
		
	#Generate subwindow view.
	pro_sub_path = HOME_MEDIA_PATH+uname+'/pro_sub/'
	if os.path.exists(pro_sub_path):
		shutil.rmtree(pro_sub_path)
	if not os.path.exists(pro_sub_path):
		os.mkdir(pro_sub_path)
	for f in f_list:	 
		im=cv2.imread(os.path.join(subs,f)) #Read segmented result
		cv2.imwrite(os.path.join(pro_sub_path,f),im[y1_2d:y2_2d,x1_2d:x2_2d,:]) #Save subwindowed result
	
		
	viz_link = "../../" + pro_sub_path.split('dinavid-gui/')[1]
	data = { 'message' : 'This is a successful ajax request in process', 'viz_link' : viz_link, 'file_list' : f_list, 'len':len_list }  
		
	return JsonResponse(data)   
	
		
#Test the html elements. If it works, move to WorkflowCompactView
#Try to get everything on one page
class WorkflowTestView(View):
	def get(self, request):

		uname = request.user.username
		if not uname or uname.isspace():
			 uname = 'default'
		user_path = HOME_MEDIA_PATH + uname
		if not os.path.exists(user_path):
			os.mkdir(user_path)

																		  
		color_file_list = "None"
		color_dir_path = "None"
		num_ch = "None"
		num_z = "None"
		shape = "None"
		file_list = "None"
		dirpath = None
		base_path = HOME_MEDIA_PATH+uname+'/data'
		
		ch_color_list = ['#0000ff', '#ff0000', '#00ff00' , '#f58231', '#4363d8', '#911eb4', '#42d4f4', '#f032e6', '#bfef45', '#fabebe', '#469990', '#e6beff', '#9A6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075']
		paths = get_paths(uname)#os.listdir(DATA_FILES_PATH)
		
		#If no image is selected, choose the default AQtest_human image. 
								
					
		if not os.path.exists(base_path):
			os.symlink(os.path.join(DATA_FILES_PATH,'AQtest_human'),base_path)
		#The symlink is broken
		if not os.path.exists(os.readlink(base_path)):
			os.unlink(base_path)
			os.symlink(os.path.join(DATA_FILES_PATH,'AQtest_human'),base_path)
		try:
			im_name = os.readlink(base_path).split('/')[-1]
		except:
			im_name = "None"
	
		#Load lut
		lut_file = ch_params_path = SHARE_MEDIA_PATH + '/ch_params/luts.txt'
		f = open(lut_file) 
		luts = json.load(f)
		f.close()
		
		if im_name in luts.keys():
			lut = luts[im_name]
			lut_name = im_name
		else:
			lut = luts['DINAVID_default']
			lut_name = 'DINAVID_default'
	
		#Create a default colorized image of the first four channels. 
		#However, this may be deprecated as once the page is loaded, a javascript function to apply the 
		#visualization parameters to the image is called.
		if os.path.exists(base_path):
			stack_name_list = os.listdir(base_path)
			#Deal with composite images. This should probably be moved to a different file or to blackpill
			stack_path = HOME_MEDIA_PATH+uname+'/data/stacks/'
			orig_path = HOME_MEDIA_PATH+uname+'/data/orig'
			colorstack_path = HOME_MEDIA_PATH+uname+'/data/colorstacks/'
			colorstack_orig_path = HOME_MEDIA_PATH+uname+'/data/colorstack_orig/'
			
			
			#Only need to do this if the preview does not exist already.
			if not os.path.exists(stack_path):
				
				if not os.path.exists(stack_path):
					os.mkdir(stack_path)
				if not os.path.exists(colorstack_path):
					os.mkdir(colorstack_path)
				if not os.path.exists(colorstack_orig_path):
					os.mkdir(colorstack_orig_path)
				stack_name = os.path.join(base_path,stack_name_list[0])
				
				#make sure an image volume is actually present.
				if os.path.isfile(stack_name):
					stack = tiff.imread(stack_name)
					shape = stack.shape
					
					#Extend to 4D to fit the multi-channel theme.
					if len(shape)==3:
						stack = np.expand_dims(stack, axis=1)
						shape = stack.shape
						
						
					base_name = (stack_name.split('/')[-1]).split('.tif')[0]
					#Check correct dimensions.
					if len(shape)==4:
						slices=shape[0]
						channels=shape[1]
						height = shape[2]
						width = shape[3]
						
						#This is a single-channel image instead of having multiple channels.															   
						if shape[3] == 3:
							channels=shape[3]
							height = shape[1]
							width = shape[2]
						
						max_intensity=float(np.max(stack))
						#Create png files bc only png files can be displayed on the web.
						for sl in range(0,slices):
							for ch in range(0,channels):
								
								fname = stack_path+base_name+'_z%04d_ch%02d.png' % (sl,ch)
								if uname in SPECIAL_USER_LIST:
									full_res_path = DATA_FILES_PATH+'/'+im_name+'/'+base_name
								else:
									full_res_path = DATA_FILES_PATH_NON_SPECIAL_USER +'/'+uname+'/'+im_name+'/'+base_name
								if not os.path.exists(full_res_path):
									os.mkdir(full_res_path)
								full_res_fname =  full_res_path+"/"+base_name+'_z%04d_ch%02d.png' % (sl,ch)
								if shape[3] == 3:
									xyslice = stack[sl,:,:,ch]						   
								else:
									xyslice = stack[sl,ch,:,:]  
								#Convert to 8-bit
								xyslice_256 = xyslice / max_intensity * 255.0
								cv2.imwrite(full_res_fname,np.uint8(xyslice_256))
								cv2.imwrite(fname,cv2.resize(np.uint8(xyslice_256),(0,0),fx=512.0/width, fy=512.0/width))
								
							#color
							
							#If 3 channel, the channel number will be the last axis.
							first_slice_8bit = np.uint8(stack[sl,:,:,:] / max_intensity * 255.0)
							if shape[3] == 3:
								first_slice_8bit = np.transpose(first_slice_8bit, (2,0,1))
								
														 
							#Apply default LUT to the image. Only for the first 4 channels.
							lut_1=np.zeros((256,3),np.uint8)	
							for ii in range(0,256):
								lut_1[ii,0]=min(255.0/84.0*ii,255)
								lut_1[ii,1]=min(255.0/169.0*ii,255)
								lut_1[ii,2]=max(255.0/(255-170)*(ii-170),0)		  
							color_image_1 = np.zeros((height, width,3),np.uint8)
							color_image_1[:,:,0]=cv2.LUT(first_slice_8bit[0,:,:],lut_1[:,0])
							color_image_1[:,:,1]=cv2.LUT(first_slice_8bit[0,:,:],lut_1[:,1])
							color_image_1[:,:,2]=cv2.LUT(first_slice_8bit[0,:,:],lut_1[:,2])	
							
							if channels >= 2:
								lut_2=np.zeros((256,3),np.uint8)	
								for ii in range(0,256):
									lut_2[ii,0]=max(255.0/(255-170)*(ii-170),0)	
									lut_2[ii,1]=min(255.0/169.0*ii,255)
									lut_2[ii,2]=min(255.0/84.0*ii,255)	  
								color_image_2 = np.zeros((height, width,3),np.uint8)
								color_image_2[:,:,0]=cv2.LUT(first_slice_8bit[1,:,:],lut_2[:,0])
								color_image_2[:,:,1]=cv2.LUT(first_slice_8bit[1,:,:],lut_2[:,1])
								color_image_2[:,:,2]=cv2.LUT(first_slice_8bit[1,:,:],lut_2[:,2])	
							else :
								color_image_2 = np.zeros((height, width,3),np.uint8)
							
							if channels >= 3:
								lut_3=np.zeros((256,3),np.uint8)	
								for ii in range(0,256):
									lut_3[ii,1]=ii 
								color_image_3 = np.zeros((height, width,3),np.uint8)
								color_image_3[:,:,0]=cv2.LUT(first_slice_8bit[2,:,:],lut_3[:,0])
								color_image_3[:,:,1]=cv2.LUT(first_slice_8bit[2,:,:],lut_3[:,1])
								color_image_3[:,:,2]=cv2.LUT(first_slice_8bit[2,:,:],lut_3[:,2])	
							else :
								color_image_3 = np.zeros((height, width,3),np.uint8)
								
							if channels >= 4:   
								lut_4=np.zeros((256,3),np.uint8)	
								for ii in range(0,256):
									lut_4[ii,0]=min(255.0/127.0*ii,255)
									lut_4[ii,1]=max(255.0/(255-128)*(ii-128),0)	
									lut_4[ii,2]=min(255.0/127.0*ii,255)
								color_image_4 = np.zeros((height, width,3),np.uint8)
								color_image_4[:,:,0]=cv2.LUT(first_slice_8bit[3,:,:],lut_4[:,0])
								color_image_4[:,:,1]=cv2.LUT(first_slice_8bit[3,:,:],lut_4[:,1])
								color_image_4[:,:,2]=cv2.LUT(first_slice_8bit[3,:,:],lut_4[:,2])
							else:
								color_image_4 = np.zeros((height, width,3),np.uint8)
							color_image = np.maximum(np.maximum(np.maximum(color_image_1,color_image_2),color_image_3),color_image_4)
							color_name = colorstack_orig_path+'z%04d_color.png' % (sl)
							cv2.imwrite(color_name,color_image)
							color_name = colorstack_path+'z%04d_color.png' % (sl)
							cv2.imwrite(color_name,cv2.resize(color_image,(512,512)))
					os.symlink(full_res_path,orig_path)
				elif os.path.isdir(stack_name):
					#Read each image and store to stack
					if not os.path.exists(orig_path):
						os.symlink(stack_name, orig_path)
					for dirpath, dirnames, files in os.walk(stack_name):
						orig_file_list = files
					if orig_file_list != 'None':
						max_ch_num = 0
						min_ch_num = 1
						for f in orig_file_list:
							img = cv2.imread(os.path.join(stack_name,f),0)
							height = 900
							width = int(img.shape[1] * 900 / img.shape[0])
							dim = (width, height)
							resized = cv2.resize(img,dim)
							cv2.imwrite(os.path.join(stack_path,f.partition('.')[0]+'.png'),resized)
							res = re.search(r"_ch[0-9]+",f).span()
							channel_number_in_file = int(f[res[0]:res[1]][3:])
							if channel_number_in_file > max_ch_num:
								max_ch_num = channel_number_in_file
							if channel_number_in_file < min_ch_num:
								min_ch_num = channel_number_in_file	
						ch_color = ['#ff0000', '#00ff00', '#0000ff', '#f58231', '#4363d8', '#911eb4', '#42d4f4', '#f032e6', '#bfef45', '#fabebe', '#469990', '#e6beff', '#9A6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075']
						num_ch = max_ch_num - min_ch_num + 1
						uname = request.user.username
						if not uname or uname.isspace():
							uname = 'default'
						ch_path = HOME_MEDIA_PATH + uname + '/data/stacks/' 
						num_slices = len([name for name in os.listdir(ch_path) if os.path.isfile(os.path.join(ch_path, name))])/num_ch
						
						colorstack_path = HOME_MEDIA_PATH+uname+'/data/colorstacks/'
						if os.path.exists(colorstack_path):
							shutil.rmtree(colorstack_path)
						os.mkdir(colorstack_path)
						
						color_list = []
						#Hex string to tuple.
						for channel in range(0,num_ch):
							h = (ch_color[channel]).lstrip('#')
							color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))
							
						#Read the images from stacks
						file_list = sorted(os.listdir(ch_path))
						for slice in range(num_slices):
							s = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch]),0)
							ss = s.shape
							sss = list(ss)
							sss.append(num_ch)
							comp_stack = np.zeros(sss,np.uint8)
							
							comp_stack[:,:,0] = s
							for channel in range(1,num_ch):
								comp_stack[:,:,channel] = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch+channel]),0)
							sss = list(ss)
							sss.append(3)
							color_stack = np.zeros(list(sss),np.uint8)
							for ch in range(num_ch):
								for co in range(3):
									color_stack[:,:,co] = np.maximum(color_stack[:,:,co], comp_stack[:,:,ch]/255.0*color_list[ch][2-co] )
							
							color_name = colorstack_path+'z%04d_color.png' % (slice)
							cv2.imwrite(color_name,color_stack) #Generate slices of color images of all channels with the colors supplied.
						
			#Get number of channels and number of z-slices
			for dirpath, dirnames, files in os.walk(stack_path):
				file_list = files
			if file_list != 'None':
				file_list.sort()
				
				first = file_list[0]
				first_re = re.search(r"z[0-9]+_ch[0-9]+", first ).span()
				first_z_ch = first[first_re[0]:first_re[1]]
				first_z_ch = first_z_ch.split('z')[1].split('_ch')
				first_z = int(first_z_ch[0])
				first_ch = int(first_z_ch[1])
				
				last = file_list[-1]
				last_re = re.search(r"z[0-9]+_ch[0-9]+", last ).span()
				last_z_ch = last[last_re[0]:last_re[1]]
				last_z_ch = last_z_ch.split('z')[1].split('_ch')
				last_z = int(last_z_ch[0])
				last_ch = int(last_z_ch[1])
				
				num_ch = last_ch - first_ch + 1
				num_z = last_z - first_z + 1
				
				tt = repr(sys.prefix)

				img1 = cv2.imread(dirpath+first,0)
				shape = img1.shape
				
				for color_dirpath, dirnames, color_files in os.walk(colorstack_path):
					color_file_list = color_files
				color_file_list.sort() 
				color_dir_path = '../../'+color_dirpath.split('dinavid-gui/')[1]
				
			#Create maximum projection images (downsampled)
			max_proj_path = HOME_MEDIA_PATH+uname+'/data/max_proj/'
			if not os.path.exists(max_proj_path):
				os.mkdir(max_proj_path)
				vol = np.zeros((num_ch,num_z,shape[0],shape[1]))
				curr_ch = 0
				for z, f in enumerate(file_list):
					im = cv2.imread(os.path.join(dirpath,f),0)
					vol[curr_ch,z%num_z,:,:] = im
					curr_ch = (curr_ch+1)%num_ch
				for c in range(0,num_ch):
					vol[c,:,:,:] = np.clip(vol[c,:,:,:]/(np.max(vol[c,:,:,:])),0,1)*255.0
				for c in range(0,num_ch):
					cv2.imwrite(os.path.join(max_proj_path,"max_proj_ch%02d.png"%c), np.amax(vol[c,:,:,:],axis=0))	   
			   
		#Get the dimensions of the original image.	
		if dirpath is None:
			dirpath = 'None'
		else:
			dirpath = dirpath.split('dinavid-gui/')[1]
		if file_list is None:
			file_list = 'None'
		orig_path = HOME_MEDIA_PATH+uname+'/data/orig'
		if os.path.exists(orig_path):
			a = os.listdir(orig_path)[0]
			img = cv2.imread(os.path.join(orig_path,a),0)
			shape = img.shape

		# Load the user's channel name
		ch_path_dir = SHARE_MEDIA_PATH + '/ch_name/'
		ch_path = ch_path_dir + 'ch_name.txt'
		ch_names = None
		if os.path.exists(ch_path):
			with open(ch_path, 'r') as f:
				ch_names = json.load(f)
		else:
			# Create a default channel name
			ch_names = {}
			ch_names_list = ['channel_%d'%i for i in range(1,20)]
			ch_names_list_no_underscore = ["channel %d"%i for i in range(1,20)]
			#for p in paths:
			temp_dict = []
			for i in range(1,20):	
				temp_dict.append(ch_names_list_no_underscore[i-1])
			ch_names["DINAVID_default"]=temp_dict
			if not os.path.exists(ch_path_dir):
				os.mkdir(ch_path_dir)
				
			with open(ch_path, 'w') as f:
				json.dump(ch_names, f)
		# Load the user's channel name base on the data name
		try:
			ch_name = ch_names[im_name] 
			ch_set_name = im_name
		except:
			#ch_name = ch_names["DINAVID_default"] 
			ch_set_name = "DINAVID_default"
			"""ch_names_list = ['channel_%d'%i for i in range(1,20)]
			ch_names_list_no_underscore = ["channel %d"%i for i in range(1,20)]
			temp_dict = []
			for i in range(1,20):	
				temp_dict.append(ch_names_list_no_underscore[i-1])
			ch_names[im_name]=temp_dict
			ch_name = ch_names[im_name]
			with open(ch_path, 'w') as f:
				json.dump(ch_names, f)"""
				
				
		"""if not isinstance(ch_name, list):   # if channel names are messed up when adding a new volume
			ch_name = ['channel_%d' % i for i in range(1, 20)]
			ch_names[im_name] = ch_name
			# update the ch_names file
			with open(ch_path, 'w') as f:
				json.dump(ch_names, f)"""
		
		# Load user's channel parameters: 
		# {'Data-I':[[gamma, brightness, offset],[,,],[,,]...], 'Data-II':[[,,],[,,],...]]}
		"""load_from_shared_folder = True
		if load_from_shared_folder:
			ch_params_dir = SHARE_MEDIA_PATH + '/ch_params/'
		else:
			ch_params_dir = HOME_MEDIA_PATH + uname + '/ch_params/'
		ch_params_path = ch_params_dir + 'ch_params.txt'
		ch_params = None
		if os.path.exists(ch_params_path):
			with open(ch_params_path, 'r') as f:
				ch_params = json.load(f)
			if not im_name in ch_params:
				ch_params[im_name]=[[1,1,0,ch_color_list[i-1]] for i in range(1,20)]
		else:
			# Create a default channel parameters
			ch_params = {}
			for p in paths:
				ch_params[p]=[[1,1,0,ch_color_list[i-1]] for i in range(1,20)]
			if not os.path.exists(ch_params_dir):
				os.makedirs(ch_params_dir)
				
			with open(ch_params_path, 'w') as f:
				json.dump(ch_params, f)"""
		
		return render(self.request, 'photos/workflow/index_test.html', {'ch_color_list':ch_color_list, 'im_name':im_name, 'imagepaths': paths, 'file_list' : file_list, 'dirpath':'../../'+dirpath, 'color_file_list' : color_file_list, 'color_dirpath' : color_dir_path, 'num_ch': num_ch, 'num_z':num_z, 'shape': shape, 'ch_names': ch_names[ch_set_name], 'ch_params': lut, 'lut_name': lut_name, 'lut_list' : luts.keys(), 'ch_names_list' : ch_names.keys(), 'ch_set_name' : ch_set_name})

		


	def post(self, request):
		form = PhotoForm(request.POST, request.FILES)
		if request.method == 'POST':
			if form.is_valid():
				handle_uploaded_file(request)
				data = { 'message' : 'This is a successful ajax request in upload_image.'};	
						
																					 

		else:
			data = { 'message' : 'This is not a successful ajax request in upload_image.',
				};	 
		return redirect(reverse('photos:workflow_test'))
		

		
#This is the view that hosts the main functionality of the page.
#Test elements in WorkflowTestView first, and then move here if it works.
class WorkflowCompactView(View):
	def get(self, request):

		uname = request.user.username
		if not uname or uname.isspace():
			 uname = 'default'
		user_path = HOME_MEDIA_PATH + uname
		if not os.path.exists(user_path):
			os.mkdir(user_path)

		color_file_list = "None"
		color_dir_path = "None"
		num_ch = "None"
		num_z = "None"
		shape = "None"
		file_list = "None"
		dirpath = None
		base_path = HOME_MEDIA_PATH+uname+'/data'
		
		ch_color_list = ['#0000ff', '#ff0000', '#00ff00' , '#f58231', '#4363d8', '#911eb4', '#42d4f4', '#f032e6', '#bfef45', '#fabebe', '#469990', '#e6beff', '#9A6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075']
		paths = get_paths(uname)#os.listdir(DATA_FILES_PATH)
		
		#If no image is selected, choose the default AQtest_human image. 
		if not os.path.exists(base_path):
			os.symlink(os.path.join(DATA_FILES_PATH,'AQtest_human'),base_path)
		#The symlink is broken
		if not os.path.exists(os.readlink(base_path)):
			os.unlink(base_path)
			os.symlink(os.path.join(DATA_FILES_PATH,'AQtest_human'),base_path)
		try:
			im_name = os.readlink(base_path).split('/')[-1]
		except:
			im_name = "None"
	
		#Load lut
		lut_file = ch_params_path = SHARE_MEDIA_PATH + '/ch_params/luts.txt'
		f = open(lut_file) 
		luts = json.load(f)
		f.close()
		
		if im_name in luts.keys():
			lut = luts[im_name]
			lut_name = im_name
		else:
			lut = luts['DINAVID_default']
			lut_name = 'DINAVID_default'
	
	
		#Create a default colorized image of the first four channels. 
		#However, this may be deprecated as once the page is loaded, a javascript function to apply the 
		#visualization parameters to the image is called.
		if os.path.exists(base_path):
			stack_name_list = os.listdir(base_path)
			#Deal with composite images. This should probably be moved to a different file or to blackpill
			stack_path = HOME_MEDIA_PATH+uname+'/data/stacks/'
			orig_path = HOME_MEDIA_PATH+uname+'/data/orig'
			colorstack_path = HOME_MEDIA_PATH+uname+'/data/colorstacks/'
			colorstack_orig_path = HOME_MEDIA_PATH+uname+'/data/colorstack_orig/'
			
			
			#Only need to do this if the preview does not exist already.
			if not os.path.exists(stack_path):
				
				if not os.path.exists(stack_path):
					os.mkdir(stack_path)
				if not os.path.exists(colorstack_path):
					os.mkdir(colorstack_path)
				if not os.path.exists(colorstack_orig_path):
					os.mkdir(colorstack_orig_path)
				stack_name = os.path.join(base_path,stack_name_list[0])
				
				#make sure an image volume is actually present.
				if os.path.isfile(stack_name):
					stack = tiff.imread(stack_name)
					shape = stack.shape
					
					#Extend to 4D to fit the multi-channel theme.
					if len(shape)==3:
						stack = np.expand_dims(stack, axis=1)
						shape = stack.shape
						
						
					base_name = (stack_name.split('/')[-1]).split('.tif')[0]
					
					#Check correct dimensions
					if len(shape)==4:
						slices=shape[0]
						channels=shape[1]
						height = shape[2]
						width = shape[3]
						#This is a single-channel image and not an image with channels.
						if shape[3] == 3:
							channels=shape[3]
							height = shape[1]
							width = shape[2]
						
						max_intensity=float(np.max(stack))
						#Create png files bc only png files can be displayed on the web.
						for sl in range(0,slices):
							for ch in range(0,channels):
								
								fname = stack_path+base_name+'_z%04d_ch%02d.png' % (sl,ch)
								if uname in SPECIAL_USER_LIST:
									full_res_path = DATA_FILES_PATH+'/'+im_name+'/'+base_name
								else:
									full_res_path = DATA_FILES_PATH_NON_SPECIAL_USER +'/'+uname+'/'+im_name+'/'+base_name
								if not os.path.exists(full_res_path):
									os.mkdir(full_res_path)
								full_res_fname =  full_res_path+"/"+base_name+'_z%04d_ch%02d.png' % (sl,ch)
								if shape[3] == 3:
									xyslice = stack[sl,:,:,ch]						   
								else:
									xyslice = stack[sl,ch,:,:]  
								#Convert to 8-bit
								xyslice_256 = xyslice / max_intensity * 255.0
								cv2.imwrite(full_res_fname,np.uint8(xyslice_256))
								cv2.imwrite(fname,cv2.resize(np.uint8(xyslice_256),(0,0),fx=512.0/width, fy=512.0/width))
								
							#color
							
							#If 3 channel, the channel number will be the last axis.
							first_slice_8bit = np.uint8(stack[sl,:,:,:] / max_intensity * 255.0)
							if shape[3] == 3:
								first_slice_8bit = np.transpose(first_slice_8bit, (2,0,1))
								
							#Apply default LUT to the image. Only for the first 4 channels.
							lut_1=np.zeros((256,3),np.uint8)	
							for ii in range(0,256):
								lut_1[ii,0]=min(255.0/84.0*ii,255)
								lut_1[ii,1]=min(255.0/169.0*ii,255)
								lut_1[ii,2]=max(255.0/(255-170)*(ii-170),0)		  
							color_image_1 = np.zeros((height, width,3),np.uint8)
							color_image_1[:,:,0]=cv2.LUT(first_slice_8bit[0,:,:],lut_1[:,0])
							color_image_1[:,:,1]=cv2.LUT(first_slice_8bit[0,:,:],lut_1[:,1])
							color_image_1[:,:,2]=cv2.LUT(first_slice_8bit[0,:,:],lut_1[:,2])	
							
							if channels >= 2:
								lut_2=np.zeros((256,3),np.uint8)	
								for ii in range(0,256):
									lut_2[ii,0]=max(255.0/(255-170)*(ii-170),0)	
									lut_2[ii,1]=min(255.0/169.0*ii,255)
									lut_2[ii,2]=min(255.0/84.0*ii,255)	  
								color_image_2 = np.zeros((height, width,3),np.uint8)
								color_image_2[:,:,0]=cv2.LUT(first_slice_8bit[1,:,:],lut_2[:,0])
								color_image_2[:,:,1]=cv2.LUT(first_slice_8bit[1,:,:],lut_2[:,1])
								color_image_2[:,:,2]=cv2.LUT(first_slice_8bit[1,:,:],lut_2[:,2])	
							else :
								color_image_2 = np.zeros((height, width,3),np.uint8)
							
							if channels >= 3:
								lut_3=np.zeros((256,3),np.uint8)	
								for ii in range(0,256):
									lut_3[ii,1]=ii 
								color_image_3 = np.zeros((height, width,3),np.uint8)
								color_image_3[:,:,0]=cv2.LUT(first_slice_8bit[2,:,:],lut_3[:,0])
								color_image_3[:,:,1]=cv2.LUT(first_slice_8bit[2,:,:],lut_3[:,1])
								color_image_3[:,:,2]=cv2.LUT(first_slice_8bit[2,:,:],lut_3[:,2])	
							else :
								color_image_3 = np.zeros((height, width,3),np.uint8)
								
							if channels >= 4:   
								lut_4=np.zeros((256,3),np.uint8)	
								for ii in range(0,256):
									lut_4[ii,0]=min(255.0/127.0*ii,255)
									lut_4[ii,1]=max(255.0/(255-128)*(ii-128),0)	
									lut_4[ii,2]=min(255.0/127.0*ii,255)
								color_image_4 = np.zeros((height, width,3),np.uint8)
								color_image_4[:,:,0]=cv2.LUT(first_slice_8bit[3,:,:],lut_4[:,0])
								color_image_4[:,:,1]=cv2.LUT(first_slice_8bit[3,:,:],lut_4[:,1])
								color_image_4[:,:,2]=cv2.LUT(first_slice_8bit[3,:,:],lut_4[:,2])
							else:
								color_image_4 = np.zeros((height, width,3),np.uint8)
							color_image = np.maximum(np.maximum(np.maximum(color_image_1,color_image_2),color_image_3),color_image_4)
							color_name = colorstack_orig_path+'z%04d_color.png' % (sl)
							cv2.imwrite(color_name,color_image)
							color_name = colorstack_path+'z%04d_color.png' % (sl)
							cv2.imwrite(color_name,cv2.resize(color_image,(512,512)))
					os.symlink(full_res_path,orig_path)
				elif os.path.isdir(stack_name):
					#Read each image and store to stack
					if not os.path.exists(orig_path):
						os.symlink(stack_name, orig_path)
					for dirpath, dirnames, files in os.walk(stack_name):
						orig_file_list = files
					if orig_file_list != 'None':
						max_ch_num = 0
						min_ch_num = 1
						for f in orig_file_list:
							img = cv2.imread(os.path.join(stack_name,f),0)
							height = 900
							width = int(img.shape[1] * 900 / img.shape[0])
							dim = (width, height)
							resized = cv2.resize(img,dim)
							cv2.imwrite(os.path.join(stack_path,f.partition('.')[0]+'.png'),resized)
							res = re.search(r"_ch[0-9]+",f).span()
							channel_number_in_file = int(f[res[0]:res[1]][3:])
							if channel_number_in_file > max_ch_num:
								max_ch_num = channel_number_in_file
							if channel_number_in_file < min_ch_num:
								min_ch_num = channel_number_in_file	
						ch_color = ['#ff0000', '#00ff00', '#0000ff', '#f58231', '#4363d8', '#911eb4', '#42d4f4', '#f032e6', '#bfef45', '#fabebe', '#469990', '#e6beff', '#9A6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075']
						num_ch = max_ch_num - min_ch_num + 1
						uname = request.user.username
						if not uname or uname.isspace():
							uname = 'default'
						ch_path = HOME_MEDIA_PATH + uname + '/data/stacks/' 
						num_slices = len([name for name in os.listdir(ch_path) if os.path.isfile(os.path.join(ch_path, name))])/num_ch
						
						colorstack_path = HOME_MEDIA_PATH+uname+'/data/colorstacks/'
						if os.path.exists(colorstack_path):
							shutil.rmtree(colorstack_path)
						os.mkdir(colorstack_path)
						
						color_list = []
						#Hex string to tuple.
						for channel in range(0,num_ch):
							h = (ch_color[channel]).lstrip('#')
							color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))
							
						#Read the images from stacks
						file_list = sorted(os.listdir(ch_path))
						for slice in range(num_slices):
							s = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch]),0)
							ss = s.shape
							sss = list(ss)
							sss.append(num_ch)
							comp_stack = np.zeros(sss,np.uint8)
							
							comp_stack[:,:,0] = s
							for channel in range(1,num_ch):
								comp_stack[:,:,channel] = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch+channel]),0)
							sss = list(ss)
							sss.append(3)
							color_stack = np.zeros(list(sss),np.uint8)
							for ch in range(num_ch):
								for co in range(3):
									color_stack[:,:,co] = np.maximum(color_stack[:,:,co], comp_stack[:,:,ch]/255.0*color_list[ch][2-co] )
							
							color_name = colorstack_path+'z%04d_color.png' % (slice)
							cv2.imwrite(color_name,color_stack) #Generate slices of color images of all channels with the colors supplied.
						
			#Get number of channels and number of z-slices
			for dirpath, dirnames, files in os.walk(stack_path):
				file_list = files
			if file_list != 'None':
				file_list.sort()
				
				first = file_list[0]
				first_re = re.search(r"z[0-9]+_ch[0-9]+", first ).span()
				first_z_ch = first[first_re[0]:first_re[1]]
				first_z_ch = first_z_ch.split('z')[1].split('_ch')
				first_z = int(first_z_ch[0])
				first_ch = int(first_z_ch[1])
				
				last = file_list[-1]
				last_re = re.search(r"z[0-9]+_ch[0-9]+", last ).span()
				last_z_ch = last[last_re[0]:last_re[1]]
				last_z_ch = last_z_ch.split('z')[1].split('_ch')
				last_z = int(last_z_ch[0])
				last_ch = int(last_z_ch[1])
				
				num_ch = last_ch - first_ch + 1
				num_z = last_z - first_z + 1
				
				tt = repr(sys.prefix)

				img1 = cv2.imread(dirpath+first,0)
				shape = img1.shape
				
				for color_dirpath, dirnames, color_files in os.walk(colorstack_path):
					color_file_list = color_files
				color_file_list.sort() 
				color_dir_path = '../../'+color_dirpath.split('dinavid-gui/')[1]
				
			#Create maximum projection images (downsampled)
			max_proj_path = HOME_MEDIA_PATH+uname+'/data/max_proj/'
			if not os.path.exists(max_proj_path):
				os.mkdir(max_proj_path)
				vol = np.zeros((num_ch,num_z,shape[0],shape[1]))
				curr_ch = 0
				for z, f in enumerate(file_list):
					im = cv2.imread(os.path.join(dirpath,f),0)
					vol[curr_ch,z%num_z,:,:] = im
					curr_ch = (curr_ch+1)%num_ch
				for c in range(0,num_ch):
					vol[c,:,:,:] = np.clip(vol[c,:,:,:]/(np.max(vol[c,:,:,:])),0,1)*255.0
				for c in range(0,num_ch):
					cv2.imwrite(os.path.join(max_proj_path,"max_proj_ch%02d.png"%c), np.amax(vol[c,:,:,:],axis=0))	   
			   
		#Get the dimensions of the original image.	
		if dirpath is None:
			dirpath = 'None'
		else:
			dirpath = dirpath.split('dinavid-gui/')[1]
		if file_list is None:
			file_list = 'None'
		orig_path = HOME_MEDIA_PATH+uname+'/data/orig'
		if os.path.exists(orig_path):
			a = os.listdir(orig_path)[0]
			img = cv2.imread(os.path.join(orig_path,a),0)
			shape = img.shape

		# Load the user's channel name
		ch_path_dir = SHARE_MEDIA_PATH + '/ch_name/'
		ch_path = ch_path_dir + 'ch_name.txt'
		ch_names = None
		if os.path.exists(ch_path):
			with open(ch_path, 'r') as f:
				ch_names = json.load(f)
		else:
			# Create a default channel name
			ch_names = {}
			ch_names_list = ['channel_%d'%i for i in range(1,20)]
			ch_names_list_no_underscore = ["channel %d"%i for i in range(1,20)]
			#for p in paths:
			temp_dict = []
			for i in range(1,20):	
				temp_dict.append(ch_names_list_no_underscore[i-1])
			ch_names["DINAVID_default"]=temp_dict
			if not os.path.exists(ch_path_dir):
				os.mkdir(ch_path_dir)
				
			with open(ch_path, 'w') as f:
				json.dump(ch_names, f)
		# Load the user's channel name base on the data name
		try:
			ch_name = ch_names[im_name] 
			ch_set_name = im_name
		except:
			#ch_name = ch_names["DINAVID_default"] 
			ch_set_name = "DINAVID_default"
			"""ch_names_list = ['channel_%d'%i for i in range(1,20)]
			ch_names_list_no_underscore = ["channel %d"%i for i in range(1,20)]
			temp_dict = []
			for i in range(1,20):	
				temp_dict.append(ch_names_list_no_underscore[i-1])
			ch_names[im_name]=temp_dict
			ch_name = ch_names[im_name]
			with open(ch_path, 'w') as f:
				json.dump(ch_names, f)"""
				
				
		"""if not isinstance(ch_name, list):   # if channel names are messed up when adding a new volume
			ch_name = ['channel_%d' % i for i in range(1, 20)]
			ch_names[im_name] = ch_name
			# update the ch_names file
			with open(ch_path, 'w') as f:
				json.dump(ch_names, f)"""
		
		# Load user's channel parameters: 
		# {'Data-I':[[gamma, brightness, offset],[,,],[,,]...], 'Data-II':[[,,],[,,],...]]}
		"""load_from_shared_folder = True
		if load_from_shared_folder:
			ch_params_dir = SHARE_MEDIA_PATH + '/ch_params/'
		else:
			ch_params_dir = HOME_MEDIA_PATH + uname + '/ch_params/'
		ch_params_path = ch_params_dir + 'ch_params.txt'
		ch_params = None
		if os.path.exists(ch_params_path):
			with open(ch_params_path, 'r') as f:
				ch_params = json.load(f)
			if not im_name in ch_params:
				ch_params[im_name]=[[1,1,0,ch_color_list[i-1]] for i in range(1,20)]
		else:
			# Create a default channel parameters
			ch_params = {}
			for p in paths:
				ch_params[p]=[[1,1,0,ch_color_list[i-1]] for i in range(1,20)]
			if not os.path.exists(ch_params_dir):
				os.makedirs(ch_params_dir)
				
			with open(ch_params_path, 'w') as f:
				json.dump(ch_params, f)"""
		
		return render(self.request, 'photos/workflow/index_compact.html', {'ch_color_list':ch_color_list, 'im_name':im_name, 'imagepaths': paths, 'file_list' : file_list, 'dirpath':'../../'+dirpath, 'color_file_list' : color_file_list, 'color_dirpath' : color_dir_path, 'num_ch': num_ch, 'num_z':num_z, 'shape': shape, 'ch_names': ch_names[ch_set_name], 'ch_params': lut, 'lut_name': lut_name, 'lut_list' : luts.keys(), 'ch_names_list' : ch_names.keys(), 'ch_set_name' : ch_set_name})

		


	def post(self, request):
		form = PhotoForm(request.POST, request.FILES)
		if request.method == 'POST':
			if form.is_valid():
				handle_uploaded_file(request)
				data = { 'message' : 'This is a successful ajax request in upload_image.'};	
						
																					 

		else:
			data = { 'message' : 'This is not a successful ajax request in upload_image.',
				};	 
		return redirect(reverse('photos:workflow_compact'))
		
#Peforms a 3D visualization of the the selected subregion 
def visualization_3d(request):
	#The region of interest
	x1 = int(request.GET['x1_3d'])
	x2 = int(request.GET['x2_3d'])
	y1 = int(request.GET['y1_3d'])
	y2 = int(request.GET['y2_3d'])   
	
	red_ch = int(request.GET['red_3d'])  
	green_ch = int(request.GET['green_3d'])  
	blue_ch = int(request.GET['blue_3d'])  
	
	#Stretch in the z direction by this factor.
	z_correction = float(request.GET['z_correction'])  

	
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')
	user = request.user
	uname = user.username
	if not uname or uname.isspace():
		uname = 'default'
		
	ch_color = request.GET.getlist('ch_color_3d[]') #This will be an array of hex color strings.
	dis_ch = request.GET.getlist('dis_ch_3d[]') #This will be an array of 0s and 1s. Might be in string format.
	num_ch = int(request.GET.getlist('num_ch_3d')[0]) #Number of channels
	ch_gamma = request.GET.getlist('ch_gamma_3d[]')
	ch_brightness = request.GET.getlist('ch_brightness_3d[]')
	ch_offset = request.GET.getlist('ch_offset_3d[]')
	
	
	
	ch_path = HOME_MEDIA_PATH + uname + '/data/orig' 
	if not os.path.exists(ch_path):
		ch_path = HOME_MEDIA_PATH + uname + '/data/stacks/' 
	num_slices = len([name for name in os.listdir(ch_path) if os.path.isfile(os.path.join(ch_path, name))])/num_ch
	
	colorstack_path = HOME_MEDIA_PATH+uname+'/data/colorstacks_orig/'
	if os.path.exists(colorstack_path):
		shutil.rmtree(colorstack_path)
	os.mkdir(colorstack_path)
	
	color_list = []
	#Hex string to tuple.
	for channel in range(0,num_ch):
		h = (ch_color[channel]).lstrip('#')
		color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))
		
	#Read the images from stacks
	file_list = sorted(os.listdir(ch_path))
	for slice in range(int(num_slices)):  
		#If just doing RGB
		sss = list([y2-y1,x2-x1])
		sss.append(3)
		color_stack = np.zeros(list(sss),np.uint8)
		comp_stack = np.zeros(list(sss),np.uint8)
		color_stack[:,:,0] = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch+red_ch]),0)[y1:y2,x1:x2]
		color_stack[:,:,1] = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch+green_ch]),0)[y1:y2,x1:x2]
		color_stack[:,:,2] = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch+blue_ch]),0)[y1:y2,x1:x2]		
		
		color_name = colorstack_path+'z%04d_color.png' % (slice)
		cv2.imwrite(color_name,color_stack)
	
	for color_dirpath, dirnames, color_files in os.walk(colorstack_path):
		color_file_list = color_files
	color_file_list.sort() 
	color_dir_path = '../../'+color_dirpath.split('dinavid-gui/')[1]
		
		
	visual_temp = HOME_MEDIA_PATH  + uname + '/visual_temp/'
		
	if not os.path.exists(visual_temp):
		os.makedirs(visual_temp)

	input_address = colorstack_path
		
	visual_input_folder = HOME_MEDIA_PATH+uname+'/visualization_input/'
	visual_input_name = HOME_MEDIA_PATH+uname+'/visualization_input/preprocess_visual_' + datetime_now + '/'
   
	preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess/'

	preprocess_url = HOME_MEDIA_PATH+uname+'/preprocess_url/'
	preprocess_url_storage = HOME_MEDIA_PATH+uname+'/preprocess_url_storage/'
	
	if not os.path.exists(visual_input_name):
		os.makedirs(visual_input_name)

	if not os.path.exists(preprocess_download):
		os.makedirs(preprocess_download)

	if not os.path.exists(preprocess_url):
		os.makedirs(preprocess_url)

	if not os.path.exists(preprocess_url_storage):
		os.makedirs(preprocess_url_storage)

  
	W = 0
	H = 0
	Z = 0
	fnamenew = ""
	downsample_W = 1
	downsample_H = 1
	#Generate the 3D rendering.
	[W, H, Z, fnamenew, downsample_W, downsample_H] = tileImages_input_workflow(uname,datetime_now, visual_input_name, input_address, x1-x1, x2-x1, y1-y1, y2-y1)
	
	# Copy JSON file with image name and dimension
	writeJSON_input(W/downsample_W,H/downsample_H,Z,visual_input_name,z_correction);
	# Copy visual3d.js and test.html file
	copyFiles_input(visual_input_name);
	
	address_fake = VIZ_3D_ADDRESS
	
	visual_input_zip_folder = HOME_MEDIA_PATH+uname+'/visualization_input_zip'
	if not os.path.exists(visual_input_zip_folder):
		os.makedirs(visual_input_zip_folder)
	shutil.make_archive(os.path.join(visual_input_zip_folder,'preprocess_visual_' + datetime_now),'zip',visual_input_name)

	zipfilename_pre = visual_input_zip_folder +'/preprocess_visual_' + datetime_now +'.zip' 
	zipfilename_pre = '../../'+zipfilename_pre.split('dinavid-gui/')[1]
 
	if os.path.exists(visual_temp):
		shutil.rmtree(visual_temp)

	data = { 'message': 'This is a successful 3D view generation', 'address' : VIZ_3D_ADDRESS, 'display_name' : "preprocess_visual_"+ datetime_now, 'uname': uname, 'zip' : zipfilename_pre, 'red_ch': red_ch,'green_ch': green_ch,'blue_ch': blue_ch }
	return JsonResponse(data)	 
	
 

#Views used for demo
class DemoView(View):
	def get(self, request):
		#photos_list = Photo.objects.filter(user=request.user, iszip=False)
		color_file_list = "None"
		color_dir_path = "None"
		num_ch = "None"
		num_z = "None"
		shape = "None"
		file_list = "None"
		dirpath = None
		base_path = os.path.join(DATA_FILES_PATH,'demo/')
		ch_color_list = ['#ff0000', '#00ff00', '#0000ff', '#f58231', '#4363d8', '#911eb4', '#42d4f4', '#f032e6', '#bfef45', '#fabebe', '#469990', '#e6beff', '#9A6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075']
		

		max_ch_num = 0
		min_ch_num = 3	
		ch_color = ['#ff0000', '#00ff00', '#0000ff', '#f58231', '#4363d8', '#911eb4', '#42d4f4', '#f032e6', '#bfef45', '#fabebe', '#469990', '#e6beff', '#9A6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075']
		num_ch = max_ch_num - min_ch_num + 1
		stack_path = os.path.join(DATA_FILES_PATH,'demo/stacks/')
		num_slices = len([name for name in os.listdir(stack_path) if os.path.isfile(os.path.join(stack_path, name))])/num_ch
		
		colorstack_path = os.path.join(DATA_FILES_PATH,'demo/colorstacks_preset/')
			
		for dirpath, dirnames, files in os.walk(stack_path):
			file_list = files
		if file_list != 'None':
			file_list.sort()
			
			first = file_list[0]
			first_re = re.search(r"z[0-9]+_ch[0-9]+", first ).span()
			first_z_ch = first[first_re[0]:first_re[1]]
			first_z_ch = first_z_ch.split('z')[1].split('_ch')
			first_z = int(first_z_ch[0])
			first_ch = int(first_z_ch[1])
			
			last = file_list[-1]
			last_re = re.search(r"z[0-9]+_ch[0-9]+", last ).span()
			last_z_ch = last[last_re[0]:last_re[1]]
			last_z_ch = last_z_ch.split('z')[1].split('_ch')
			last_z = int(last_z_ch[0])
			last_ch = int(last_z_ch[1])
			
			num_ch = last_ch - first_ch + 1
			num_z = last_z - first_z + 1
			
			img1 = cv2.imread(dirpath+first,0)
			shape = img1.shape
			
			for color_dirpath, dirnames, color_files in os.walk(colorstack_path):
				color_file_list = color_files
			color_file_list.sort()

		orig_path = os.path.join(DATA_FILES_PATH,'demo/orig')
		if os.path.exists(orig_path):
			
			a = os.listdir(orig_path)[0]
			img = cv2.imread(os.path.join(orig_path,a),0)
			shape = img.shape
		dirpath = '../../media/demo_im/stacks/'
		color_dir_path = '../../media/demo_im/colorstacks_preset/'
		return render(self.request, 'photos/demo/index_demo.html', {'ch_color_list':ch_color_list, 'im_name':'demo', 'file_list' : file_list, 'dirpath':dirpath, 'color_file_list' : color_file_list, 'color_dirpath' : color_dir_path, 'num_ch': num_ch, 'num_z':num_z, 'shape': shape})

# Update channel colors for the DEMO page.
def update_channel_color_demo(request):
	ch_color = request.GET.getlist('ch_color[]') #This will be an array of hex color strings.
	dis_ch = request.GET.getlist('dis_ch[]') #This will be an array of 0s and 1s. Might be in string format.
	num_ch = int(request.GET.getlist('num_ch')[0]) #Number of channels
	ch_path = os.path.join(HOME_MEDIA_PATH,'demo_im/stacks/' )
	num_slices = len([name for name in os.listdir(ch_path) if os.path.isfile(os.path.join(ch_path, name))])/num_ch
	
	ch_gamma = request.GET.getlist('ch_gamma[]')
	ch_brightness = request.GET.getlist('ch_brightness[]')
	ch_offset = request.GET.getlist('ch_offset[]')
	slice = int(request.GET['curr_slice'])
	
	#Equation to get updated value for each channel: new_val = pow(old_val, gamma)*brightness+offset;
	
	colorstack_path = os.path.join(HOME_MEDIA_PATH,'demo_im/colorstacks/' )
	if not os.path.exists(colorstack_path):
		#shutil.rmtree(colorstack_path)
		os.mkdir(colorstack_path)
		
	color_list = []
	#Hex string to tuple.
	for channel in range(0,num_ch):
		h = (ch_color[channel]).lstrip('#')
		color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))
		
	#Read the images from stacks
	file_list = sorted(os.listdir(ch_path))
	
	s = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch]),0)
	ss = s.shape
	sss = list(ss)
	sss.append(num_ch)
	comp_stack = np.zeros(sss,np.uint8)
	if dis_ch[0] == "1":
		comp_stack[:,:,0] = s
		comp_stack[:,:,0] = np.uint8(255.0*np.clip((np.power(comp_stack[:,:,0]/255.0,np.double(ch_gamma[0]))*np.double(ch_brightness[0])+np.double(ch_offset[0])),0,1))
	for channel in range(1,num_ch):
		if dis_ch[channel] == "1":
			comp_stack[:,:,channel] = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch+channel]),0)
			comp_stack[:,:,channel] = np.uint8(255.0*np.clip((np.power(comp_stack[:,:,channel]/255.0,np.double(ch_gamma[channel]))*np.double(ch_brightness[channel])+np.double(ch_offset[channel])),0,1))
	sss = list(ss)
	sss.append(3)
	color_stack = np.zeros(list(sss),np.uint8)
	for ch in range(num_ch):
		for co in range(3):
			color_stack[:,:,co] = np.maximum(color_stack[:,:,co], comp_stack[:,:,ch]/255.0*color_list[ch][2-co] )
			
	
	color_name = colorstack_path+'z%04d_color.png' % (slice)
	cv2.imwrite(color_name,color_stack)
	
	for color_dirpath, dirnames, color_files in os.walk(colorstack_path):
		color_file_list = color_files
	color_file = 'z%04d_color.png' % (slice)
	color_dir_path = '../../media/demo_im/colorstacks/'
	data = { 'message' : 'This is a successful ajax request in update_channel_color_demo', 'color_dirpath': color_dir_path, 'color_file': color_file };
	   
	return JsonResponse(data)

#Function for zooming in the 2D view. This time, assume the cropped images are saved somewhere
def zoom_in_cropped(request):
	
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')
	user = request.user
	uname = user.username
	if not uname or uname.isspace():
		 uname = 'default'
	im_name = request.GET['im_name_3d']
	to_update = int(request.GET['to_update_proj'])
	
	max_proj_path = HOME_MEDIA_PATH+uname+'/data/max_proj/' 
	color_max_proj_path = HOME_MEDIA_PATH+uname+'/max_proj_color/'
	max_proj_path_pre = os.path.join(color_max_proj_path,im_name)
	max_proj_file=os.path.join(max_proj_path_pre,"max_proj_color.png")
	
	color_stacks_path = HOME_MEDIA_PATH+uname+'/data/colorstacks/' 
	file_list = os.listdir(color_stacks_path)
	
	#Which area to zoom into.
	x1 = int(request.GET['x1_3d'])
	x2 = int(request.GET['x2_3d'])
	y1 = int(request.GET['y1_3d'])
	y2 = int(request.GET['y2_3d'])   
	

	ch_color = request.GET.getlist('ch_color_3d[]') #This will be an array of hex color strings.
	dis_ch = request.GET.getlist('dis_ch_3d[]') #This will be an array of 0s and 1s. Might be in string format.
	num_ch = int(request.GET.getlist('num_ch_3d')[0]) #Number of channels
	ch_gamma = request.GET.getlist('ch_gamma_3d[]')
	ch_brightness = request.GET.getlist('ch_brightness_3d[]')
	ch_offset = request.GET.getlist('ch_offset_3d[]')
	slice = int(request.GET['curr_slice_3d'])

	color_list = []
	#Hex string to tuple.
	for channel in range(0,num_ch):
		h = (ch_color[channel]).lstrip('#')
		color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))
  
	if not os.path.exists(color_max_proj_path):
		os.mkdir(color_max_proj_path)
	if not os.path.exists(max_proj_path_pre):
		os.mkdir(max_proj_path_pre) 

	
	
	#Create Zoom images
	ch_path = HOME_MEDIA_PATH+uname+'/cropped_slices/' #cropped path.
	colorstack_path = HOME_MEDIA_PATH+uname+'/zoom/'
	if not os.path.exists(colorstack_path):
		os.mkdir(colorstack_path)
	 
	#Read the images from stacks
	file_list = sorted(os.listdir(ch_path))
	s = cv2.imread(os.path.join(ch_path,file_list[0]),0)
	ss = s.shape
	sss = list(ss)
	sss.append(num_ch)
	comp_stack = np.zeros(sss,np.uint8)
	#Apply gamma, offset, brightness for the selected channels.
	if dis_ch[0] == "1":
		comp_stack[:,:,0] = s
		comp_stack[:,:,0] = np.uint8(255.0*np.clip((np.power(comp_stack[:,:,0]/255.0,np.double(ch_gamma[0]))*np.double(ch_brightness[0])+np.double(ch_offset[0])),0,1))
	for channel in range(1,num_ch):
		if dis_ch[channel] == "1":
			tempch = cv2.imread(os.path.join(ch_path,file_list[channel]),0)
			comp_stack[:,:,channel] = tempch
			comp_stack[:,:,channel] = np.uint8(255.0*np.clip((np.power(comp_stack[:,:,channel]/255.0,np.double(ch_gamma[channel]))*np.double(ch_brightness[channel])+np.double(ch_offset[channel])),0,1))
			
	#Generate the color zoom image
	sss = list(ss)
	sss.append(3)
	color_stack = np.zeros(list(sss),np.uint8)
	for ch in range(num_ch):
		for co in range(3):
			color_stack[:,:,co] = np.maximum(color_stack[:,:,co], comp_stack[:,:,ch]/255.0*color_list[ch][2-co] )
	color_name = colorstack_path+'zoom.png'
	cv2.imwrite(color_name,color_stack)
	
	#Return the paths to display
	for color_dirpath, dirnames, color_files in os.walk(colorstack_path):
		color_file_list = color_files
	color_file = color_file_list[0]
	color_dir_path = '../../'+color_dirpath.split('dinavid-gui/')[1]
	max_proj_file = '../../'+max_proj_file.split('dinavid-gui/')[1]
	
	data = { 'message' : 'This is a successful ajax request in zoom_in_cropped',
				'x1'			:	 x1				   ,
				'x2'			:	 x2				   ,
				'y1'			:	 y1				   ,
				'y2'			:	 y2				   ,	 
				'ch_color'	  :	 ch_color			 ,
				'dis_ch'		:	 dis_ch			   ,
				'num_ch'		:	 num_ch			   ,
				'ch_gamma'	  :	 ch_gamma			 ,
				'ch_brightness' :	 ch_brightness		,
				'ch_offset'	 :	 ch_offset			,
				'slice'		 :	 slice			   ,
				'zoom_file'	 :	 color_file,
				'zoom_path'	 :	 color_dir_path,
				'max_proj_file' :	 max_proj_file
			};
	   
	return JsonResponse(data) 


	
#Function for zooming in the 2D view
def zoom_in(request):
	
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')
	user = request.user
	uname = user.username
	if not uname or uname.isspace():
		uname = 'default'
	im_name = request.GET['im_name_3d']
	to_update = int(request.GET['to_update_proj'])
	
	max_proj_path = HOME_MEDIA_PATH+uname+'/data/max_proj/' 
	color_max_proj_path = HOME_MEDIA_PATH+uname+'/max_proj_color/'
	max_proj_path_pre = os.path.join(color_max_proj_path,im_name)
	max_proj_file=os.path.join(max_proj_path_pre,"max_proj_color.png")
	
	color_stacks_path = HOME_MEDIA_PATH+uname+'/data/colorstacks/' 
	file_list = os.listdir(color_stacks_path)
	
   
	if to_update > 0:
		update_channel_color(request)
	
	#Which area to zoom into.
	x1 = int(request.GET['x1_3d'])
	x2 = int(request.GET['x2_3d'])
	y1 = int(request.GET['y1_3d'])
	y2 = int(request.GET['y2_3d'])   
	
		
	ch_color = request.GET.getlist('ch_color_3d[]') #This will be an array of hex color strings.
	dis_ch = request.GET.getlist('dis_ch_3d[]') #This will be an array of 0s and 1s. Might be in string format.
	num_ch = int(request.GET.getlist('num_ch_3d')[0]) #Number of channels
	ch_gamma = request.GET.getlist('ch_gamma_3d[]')
	ch_brightness = request.GET.getlist('ch_brightness_3d[]')
	ch_offset = request.GET.getlist('ch_offset_3d[]')
	slice = int(request.GET['curr_slice_3d'])

	color_list = []
	#Hex string to tuple.
	for channel in range(0,num_ch):
		h = (ch_color[channel]).lstrip('#')
		color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))

	if not os.path.exists(color_max_proj_path):
		os.mkdir(color_max_proj_path)
	if not os.path.exists(max_proj_path_pre):
		os.mkdir(max_proj_path_pre)
		
	#Create max projection color image to replace the "full-resolution" images.
	if to_update > 0:
		file_list = sorted(os.listdir(color_stacks_path))
		im = cv2.imread(os.path.join(color_stacks_path,file_list[0]))
		max_proj_size = im.shape
		max_proj = np.zeros((len(file_list),max_proj_size[0],max_proj_size[1],max_proj_size[2]),np.uint8)
		for i,f in enumerate(file_list): 
			max_proj[i,:,:,:] = cv2.imread(os.path.join(color_stacks_path,f))
		cv2.imwrite(max_proj_file,np.amax(max_proj,axis=0))
	
	
	#Create Zoom images
	ch_path = HOME_MEDIA_PATH +uname + '/data/orig/' #originally orig
	colorstack_path = HOME_MEDIA_PATH+uname+'/zoom/'
	cropped_path = HOME_MEDIA_PATH+uname+'/cropped_slices/'
	if not os.path.exists(colorstack_path):
		os.mkdir(colorstack_path)
		
	if os.path.exists(cropped_path):
		shutil.rmtree(cropped_path)
	if not os.path.exists(cropped_path):
		os.mkdir(cropped_path)	
	 
	#Read the images from stacks
	file_list = sorted(os.listdir(ch_path))
	s = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch]),0)
	s = 1.0*s[y1:y2,x1:x2] #Get the zoomed image area
	cv2.imwrite(os.path.join(cropped_path,"ch%02d.png"%0),np.uint8(s))
	ss = s.shape
	sss = list(ss)
	sss.append(num_ch)
	comp_stack = np.zeros(sss,np.uint8)
	#Apply gamma, offset, brightness for the selected channels.
	if dis_ch[0] == "1":
		comp_stack[:,:,0] = s
		comp_stack[:,:,0] = np.uint8(255.0*np.clip((np.power(comp_stack[:,:,0]/255.0,np.double(ch_gamma[0]))*np.double(ch_brightness[0])+np.double(ch_offset[0])),0,1))
	for channel in range(1,num_ch):	
		tempch = cv2.imread(os.path.join(ch_path,file_list[slice*num_ch+channel]),0)
		tempch = 1.0*tempch[y1:y2,x1:x2]
		#Write the channel subregions
		cv2.imwrite(os.path.join(cropped_path,"ch%02d.png"%channel),np.uint8(tempch))
		if dis_ch[channel] == "1":
			comp_stack[:,:,channel] = tempch
			comp_stack[:,:,channel] = np.uint8(255.0*np.clip((np.power(comp_stack[:,:,channel]/255.0,np.double(ch_gamma[channel]))*np.double(ch_brightness[channel])+np.double(ch_offset[channel])),0,1))
			
	#Generate the color zoom image
	sss = list(ss)
	sss.append(3)
	color_stack = np.zeros(list(sss),np.uint8)
	for ch in range(num_ch):
		for co in range(3):
			color_stack[:,:,co] = np.maximum(color_stack[:,:,co], comp_stack[:,:,ch]/255.0*color_list[ch][2-co] )
	color_name = colorstack_path+'zoom.png'
	cv2.imwrite(color_name,color_stack)
	
	#Return the paths to display
	for color_dirpath, dirnames, color_files in os.walk(colorstack_path):
		color_file_list = color_files
	color_file = color_file_list[0]
	color_dir_path = '../../'+color_dirpath.split('dinavid-gui/')[1]
	max_proj_file = '../../'+max_proj_file.split('dinavid-gui/')[1]
	
	data = { 'message' : 'This is a successful ajax request in zoom_in',
				'x1'			:	 x1				   ,
				'x2'			:	 x2				   ,
				'y1'			:	 y1				   ,
				'y2'			:	 y2				   ,	 
				'ch_color'	  :	 ch_color			 ,
				'dis_ch'		:	 dis_ch			   ,
				'num_ch'		:	 num_ch			   ,
				'ch_gamma'	  :	 ch_gamma			 ,
				'ch_brightness' :	 ch_brightness		,
				'ch_offset'	 :	 ch_offset			,
				'slice'		 :	 slice			   ,
				'zoom_file'	 :	 color_file,
				'zoom_path'	 :	 color_dir_path,
				'max_proj_file' :	 max_proj_file
			};
	   
	return JsonResponse(data) 
	
#Apply a LUT to a single channel preprocessed image.
def lut_preprocess(request):
	user = request.user 
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	paths = HOME_MEDIA_PATH + uname + '/preprocess/'
	
	lut_paths = HOME_MEDIA_PATH + uname + '/preprocess_lut/' 
	if os.path.exists(lut_paths):
		shutil.rmtree(lut_paths)
	os.mkdir(lut_paths)
	
	#LUT chosen
	lut_opt = int(request.GET['lut_opt'])
	
	#Coordinates to apply them on.
	x1_2d = int(request.GET['x1_2d'])
	x2_2d = int(request.GET['x2_2d'])
	y1_2d = int(request.GET['y1_2d'])
	y2_2d = int(request.GET['y2_2d'])
	
	lut_chosen = None
	
	if lut_opt == 1:
		lut_chosen = cv2.COLORMAP_BONE
	elif lut_opt == 2:
		lut_chosen = cv2.COLORMAP_HOT
	elif lut_opt == 3:
		lut_chosen = cv2.COLORMAP_PINK
	elif lut_opt == 4:
		lut_chosen = cv2.COLORMAP_OCEAN
	elif lut_opt == 5:
		lut_chosen = cv2.COLORMAP_JET
	elif lut_opt == 6:
		lut_chosen = cv2.COLORMAP_RAINBOW	
	elif lut_opt == 7:
		lut_chosen = cv2.COLORMAP_HSV  
		
	#Go through each image.
	for root,dirs,files in os.walk(paths):
		f_list =  []
		for name in files:
			im = cv2.imread( os.path.join(root,name),0)
			if(lut_chosen):
				im_temp = im[y1_2d:y2_2d,x1_2d:x2_2d]
				im_lut = cv2.applyColorMap(im_temp, lut_chosen) #Apply the LUT
			else:
				im_lut = im[y1_2d:y2_2d,x1_2d:x2_2d] #No LUT chosen
			cv2.imwrite(  os.path.join(lut_paths,name), im_lut)
			f_list.append(name)
		f_list.sort()
	  
	viz_link = "../../" + lut_paths.split('dinavid-gui/')[1]
	
	data = { 'message' : 'This is a successful ajax request in lut_preprocess',
			'viz_link' : viz_link,
			'file_list' : f_list
			};
	   
	return JsonResponse(data) 
	
#Apply a LUT to a single channel preprocessed image preview
def lut_preprocess_preview(request):
  
	user = request.user 
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	lut_opt = int(request.GET['lut_opt_prev'])
	preview_paths = HOME_MEDIA_PATH + uname + '/preprocess_preview/' 
	preprocess_orig_to_compare = HOME_MEDIA_PATH+uname+'/preprocess_orig_prev/'
	
	orig_lut_paths = HOME_MEDIA_PATH + uname + '/preprocess_orig_lut/' 
	lut_paths = HOME_MEDIA_PATH + uname + '/preprocess_preview_lut/' 
	if os.path.exists(orig_lut_paths):
		shutil.rmtree(orig_lut_paths)
	os.mkdir(orig_lut_paths)
	if os.path.exists(lut_paths):
		shutil.rmtree(lut_paths)
	os.mkdir(lut_paths)
	
	#LUT chosen
	lut_chosen = None
	if lut_opt == 1:
		lut_chosen = cv2.COLORMAP_BONE
	elif lut_opt == 2:
		lut_chosen = cv2.COLORMAP_HOT
	elif lut_opt == 3:
		lut_chosen = cv2.COLORMAP_PINK
	elif lut_opt == 4:
		lut_chosen = cv2.COLORMAP_OCEAN
	elif lut_opt == 5:
		lut_chosen = cv2.COLORMAP_JET
	elif lut_opt == 6:
		lut_chosen = cv2.COLORMAP_RAINBOW	
	elif lut_opt == 7:
		lut_chosen = cv2.COLORMAP_HSV	  
		
	#Apply LUT to the original files
	for root,dirs,files in os.walk(preprocess_orig_to_compare):
		orig_f_list =  []
		for name in files:
			im = cv2.imread( os.path.join(root,name),0)
			im_lut = cv2.applyColorMap(im, lut_chosen) 
			cv2.imwrite(  os.path.join(orig_lut_paths,name), im_lut)
			orig_f_list.append(name)
		orig_f_list.sort()
	
	#Apply LUT to the processed files
	for root,dirs,files in os.walk(preview_paths):
		f_list =  []
		for name in files:
			im = cv2.imread( os.path.join(root,name),0)
			im_lut = cv2.applyColorMap(im, lut_chosen) 
			cv2.imwrite(  os.path.join(lut_paths,name), im_lut)
			f_list.append(name)
		f_list.sort()
	
		
	viz_link = "../../" + lut_paths.split('dinavid-gui/')[1]
	viz_link_orig = "../../" + orig_lut_paths.split('dinavid-gui/')[1]
	
	data = { 'message' : 'This is a successful ajax request in lut_preprocess_preview',
			'viz_link' : viz_link,
			'viz_link_orig' : viz_link_orig,
			'f_list' : f_list,
			'orig_f_list' : orig_f_list
			};
	   
	return JsonResponse(data) 
	
#Creates a new subwindowed view of the segmentation of the whole volume.
def refresh_seg_subwindow(request):
	#Coordinates of subregion to apply this to.
	x1_2d = int(request.GET['x1_2d'])
	x2_2d = int(request.GET['x2_2d'])
	y1_2d = int(request.GET['y1_2d'])
	y2_2d = int(request.GET['y2_2d'])
	nuc_channel_in = int(request.GET['nuclei_channel'])
	num_ch = int(request.GET['numch_2d'])
	num_z = int(request.GET['numz_2d'])
	
	user = request.user 
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	orig_dirname = HOME_MEDIA_PATH + uname + '/data/orig'
	files = os.listdir(orig_dirname)
	orig_to_compare = HOME_MEDIA_PATH + uname + '/refresh_seg'
	if os.path.exists(orig_to_compare):
		shutil.rmtree(orig_to_compare)
	if not os.path.exists(orig_to_compare):
		os.mkdir(orig_to_compare)
	
	#Original view
	for index, file in enumerate(sorted(files)[nuc_channel_in:-1:num_ch]):
		#Color image (3 channel)
		im_slice = cv2.imread(os.path.join(orig_dirname, file),0)
		orig_imgs = os.path.join(orig_to_compare, file)
		cv2.imwrite(os.path.splitext(orig_imgs)[0]+'.png', im_slice[y1_2d:y2_2d,x1_2d:x2_2d])
	orig_f_list = sorted(os.listdir(orig_to_compare))
	
	
	#Generate subwindow view.
	seg_path_main = HOME_MEDIA_PATH+uname+'/seg_results/'
	all_subdirs = [x[0] for x in os.walk(seg_path_main)]
	all_subdirs.sort()
	seg_path = [s for s in all_subdirs if "color" in s][0]
	
	f_list = [f for f in os.listdir(seg_path) if os.path.isfile(os.path.join(seg_path,f))]	
	f_list.sort()
	
	seg_sub_path = HOME_MEDIA_PATH+uname+'/seg_sub/'
	
	if os.path.exists(seg_sub_path):
		shutil.rmtree(seg_sub_path)
	if not os.path.exists(seg_sub_path):
		os.mkdir(seg_sub_path)
	for f in f_list:	 
		im=cv2.imread(os.path.join(seg_path,f)) #Read segmented result
		cv2.imwrite(os.path.join(seg_sub_path,f),im[y1_2d:y2_2d,x1_2d:x2_2d,:]) #Save subwindowed result
	
	viz_link = "../../" + seg_sub_path.split('dinavid-gui/')[1]
	orig_viz_link = "../../" + orig_to_compare.split('dinavid-gui/')[1]
	
	data = { 'message' : 'This is a successful ajax request in refresh_seg_subwindow',
			'viz_link' : viz_link,
			'f_list' : f_list,
			'orig_f_list' : orig_f_list,
			'orig_viz_link' : orig_viz_link
			};
	return JsonResponse(data) 
	

#Updates the maximum projection image	
def update_max_projection(request):
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')
	user = request.user
	uname = user.username
	if not uname or uname.isspace():
		uname = 'default'
	im_name = request.GET['im_name_3d']
	to_update = int(request.GET['to_update_proj'])
	
	max_proj_path = HOME_MEDIA_PATH+uname+'/data/max_proj/' 
	color_max_proj_path = HOME_MEDIA_PATH+uname+'/max_proj_color/'
	max_proj_path_pre = os.path.join(color_max_proj_path,im_name)
	max_proj_file=os.path.join(max_proj_path_pre,"max_proj_color.png")
	
	color_stacks_path = HOME_MEDIA_PATH+uname+'/data/colorstacks/' 
	file_list = os.listdir(color_stacks_path)
	

	#Which area to zoom into.
	x1 = int(request.GET['x1_3d'])
	x2 = int(request.GET['x2_3d'])
	y1 = int(request.GET['y1_3d'])
	y2 = int(request.GET['y2_3d'])   
	

	ch_color = request.GET.getlist('ch_color_3d[]') #This will be an array of hex color strings.
	dis_ch = request.GET.getlist('dis_ch_3d[]') #This will be an array of 0s and 1s. Might be in string format.
	num_ch = int(request.GET.getlist('num_ch_3d')[0]) #Number of channels
	ch_gamma = request.GET.getlist('ch_gamma_3d[]')
	ch_brightness = request.GET.getlist('ch_brightness_3d[]')
	ch_offset = request.GET.getlist('ch_offset_3d[]')
	slice = int(request.GET['curr_slice_3d'])
	


	color_list = []
	#Hex string to tuple.
	for channel in range(0,num_ch):
		h = (ch_color[channel]).lstrip('#')
		color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))
  
	#Create max projection color image to replace the "full-resolution" images.
	if not os.path.exists(color_max_proj_path):
		os.mkdir(color_max_proj_path)
	if not os.path.exists(max_proj_path_pre):
		os.mkdir(max_proj_path_pre) 
   
	
	#Take max before applying LUT   
	
	file_list = sorted(os.listdir(max_proj_path))
	im = cv2.imread(os.path.join(max_proj_path,file_list[0]),0)
	max_proj_size = im.shape
	max_proj = np.zeros((max_proj_size[0],max_proj_size[1],num_ch),np.uint8)
	color_stack_max_proj = np.zeros((max_proj_size[0],max_proj_size[1],3),np.uint8)
	for channel,f in enumerate(file_list):
		max_proj[:,:,channel] = cv2.imread(os.path.join(max_proj_path,f),0)
		max_proj[:,:,channel] = np.uint8(255.0*np.clip((np.power(max_proj[:,:,channel]/255.0,np.double(ch_gamma[channel]))*np.double(ch_brightness[channel])+np.double(ch_offset[channel])),0,1))
	for ch in range(num_ch):
		for co in range(3):
			color_stack_max_proj[:,:,co] = np.maximum(color_stack_max_proj[:,:,co], max_proj[:,:,ch]/255.0*color_list[ch][2-co] ) 
			cv2.imwrite(max_proj_file,color_stack_max_proj)

	
	max_proj_file = '../../'+max_proj_file.split('dinavid-gui/')[1]
	
	data = { 'message' : 'This is a successful ajax request in update_max_file',
				'max_proj_file' :	 max_proj_file
			};
	   
	return JsonResponse(data) 
	
	
#Test rotation for a certain image volume. Not available for all image volumes.
class Psuedo3dView(View):
	def get(self, request):
	
		uname = request.user.username
		if not uname or uname.isspace():
			 uname = 'default'
		user_path = HOME_MEDIA_PATH + uname
		if not os.path.exists(user_path):
			os.mkdir(user_path)
		
		photos_list = Photo.objects.filter(user=request.user, iszip=False)
		color_file_list = "None"
		color_dir_path = "None"
		num_ch = "None"
		num_z = "None"
		shape = "None"
		file_list = "None"
		dirpath = None
		base_path = HOME_MEDIA_PATH+uname+'/data'
		
		
		
		ch_color_list = ['#0000ff', '#ff0000', '#00ff00' , '#f58231', '#4363d8', '#911eb4', '#42d4f4', '#f032e6', '#bfef45', '#fabebe', '#469990', '#e6beff', '#9A6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075']
		paths = os.listdir(DATA_FILES_PATH)

		try:
			im_name = os.readlink(base_path).split('/')[-1]
		except:
			im_name = "None"
		 
			   
		#Get the dimensions of the original image.	
		if dirpath is None:
			dirpath = 'None'
		else:
			dirpath = dirpath.split('dinavid-gui/')[1]
		if file_list is None:
			file_list = 'None'
		orig_path = HOME_MEDIA_PATH+uname+'/data/orig'
		if os.path.exists(orig_path):
			a = os.listdir(orig_path)[0]
			img = cv2.imread(os.path.join(orig_path,a),0)
			shape = img.shape

		# Load the user's channel name
		ch_path_dir = SHARE_MEDIA_PATH + '/ch_name/'
		ch_path = ch_path_dir + 'ch_name.txt'
		ch_names = None
		if os.path.exists(ch_path):
			with open(ch_path, 'r') as f:
				ch_names = json.load(f)
		else:
		#TODO: DO NOT CREATE DEFAULT CHANNEL SET. USE DINAVID DEFAULT
			# Create a default channel name
			ch_names = {}
			ch_names_list = ['channel_%d'%i for i in range(1,20)]
			ch_names_list_no_underscore = ["channel %d"%i for i in range(1,20)]
			for p in paths:
				temp_dict = []
				for i in range(1,20):	
					temp_dict.append(ch_names_list_no_underscore[i-1])
				ch_names[p]=temp_dict
				
			if not os.path.exists(ch_path_dir):
				os.mkdir(ch_path_dir)
				
			with open(ch_path, 'w') as f:
				json.dump(ch_names, f)
				
		# Load the user's channel name base on the data name
		try:
			ch_name = ch_names[im_name] 
		except:
			ch_names_list = ['channel_%d'%i for i in range(1,20)]
			ch_names_list_no_underscore = ["channel %d"%i for i in range(1,20)]
			temp_dict = []
			for i in range(1,20):	
				temp_dict.append(ch_names_list_no_underscore[i-1])
			ch_names[im_name]=temp_dict
			ch_name = ch_names[im_name]
			with open(ch_path, 'w') as f:
				json.dump(ch_names, f)
		if not isinstance(ch_name, list):   # if channel names are messed up when adding a new volume
			ch_name = ['channel_%d' % i for i in range(1, 20)]
			ch_names[im_name] = ch_name
			# update the ch_names file
			with open(ch_path, 'w') as f:
				json.dump(ch_names, f)


		load_from_shared_folder = True
		if load_from_shared_folder:
			ch_params_dir = SHARE_MEDIA_PATH + '/ch_params/'
		else:
			ch_params_dir = HOME_MEDIA_PATH +uname + '/ch_params/'
		ch_params_path = ch_params_dir + 'ch_params.txt'
		ch_params = None
		if os.path.exists(ch_params_path):
			with open(ch_params_path, 'r') as f:
				ch_params = json.load(f)
			if not im_name in ch_params:
				ch_params[im_name]=[[1,1,0,ch_color_list[i-1]] for i in range(1,20)]
		else:
			# Create a default channel parameters
			ch_params = {}
			for p in paths:
				ch_params[p]=[[1,1,0,ch_color_list[i-1]] for i in range(1,20)]
			if not os.path.exists(ch_params_dir):
				os.makedirs(ch_params_dir)
				
			with open(ch_params_path, 'w') as f:
				json.dump(ch_params, f)
				
				
		rotations_path = base_path + '/rotations'
		
		try:
			file_list = os.listdir(rotations_path)
		except:
			return redirect(reverse('photos:workflow_compact'), message="Psuedo 3D is not supported for this image volume")
			
		file_list.sort()
				
		first = file_list[0]
		first_re = re.search(r"_ch[0-9]+", first ).span()
		first_z_ch = first[first_re[0]:first_re[1]]
		first_z_ch = first_z_ch.split('_ch')
		first_ch = int(first_z_ch[1])
		
		last = file_list[-1]
		last_re = re.search(r"_ch[0-9]+", last ).span()
		last_z_ch = last[last_re[0]:last_re[1]]
		last_z_ch = last_z_ch.split('_ch')
		last_ch = int(last_z_ch[1])
		
		num_ch = last_ch - first_ch + 1	
			
		
		rotations_path = '../../'+rotations_path.split('dinavid-gui/')[1]
		
		return render(self.request, 'photos/psuedo_3d/index.html', {'ch_color_list':ch_color_list, 'rotations_path': rotations_path, 'im_name':im_name, 'imagepaths': paths, 'num_ch': num_ch, 'ch_names': ch_names[im_name], 'ch_params': ch_params[im_name]})

	def post(self, request):
		pass
 
#Generate max projection of rotation images that gives a 3D impression using rotated 2D slices. 
def gen_rot_color_images(request):
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	x_rot = int(request.GET['x_rot'])
	y_rot = int(request.GET['y_rot'])
	
	fname_base = "rot_%dx_%dy_ch"%(y_rot,x_rot) # Example: "rot_0x_0y_ch00.png"
	
	ch_color = request.GET.getlist('ch_color[]') #This will be an array of hex color strings.
	dis_ch = request.GET.getlist('dis_ch[]') #This will be an array of 0s and 1s. Might be in string format.
	ch_gamma = request.GET.getlist('ch_gamma[]')
	ch_brightness = request.GET.getlist('ch_brightness[]')
	ch_offset = request.GET.getlist('ch_offset[]')

	base_path = HOME_MEDIA_PATH+uname+'/data'
	rotations_path = base_path + '/rotations'
	file_list = os.listdir(rotations_path)
	file_list.sort()
		  
	#find first file
	first = file_list[0]
	first_re = re.search(r"_ch[0-9]+", first ).span()
	first_z_ch = first[first_re[0]:first_re[1]]
	first_z_ch = first_z_ch.split('_ch')
	first_ch = int(first_z_ch[1])
	
	#find last file
	last = file_list[-1]
	last_re = re.search(r"_ch[0-9]+", last ).span()
	last_z_ch = last[last_re[0]:last_re[1]]
	last_z_ch = last_z_ch.split('_ch')
	last_ch = int(last_z_ch[1])
	
	num_ch = last_ch - first_ch + 1   

	color_list = []
	#Hex string to tuple.
	for channel in range(0,num_ch):
		h = (ch_color[channel]).lstrip('#')
		color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))
	
	#Max projection
	im = cv2.imread(os.path.join(rotations_path,fname_base+"00.png"),0)
	max_proj_size = im.shape
	max_proj = np.zeros((max_proj_size[0],max_proj_size[1],num_ch),np.uint8)
	color_stack_max_proj = np.zeros((max_proj_size[0],max_proj_size[1],3),np.uint8)
	for channel in range(num_ch):
		if dis_ch[channel] == "1":
			max_proj[:,:,channel] = cv2.imread(os.path.join(rotations_path,fname_base+"%02d.png"%channel),0)
			max_proj[:,:,channel] = 1.0*max_proj[:,:,channel]/np.max(max_proj[:,:,channel])*255.0
			max_proj[:,:,channel] = np.uint8(255.0*np.clip((np.power(max_proj[:,:,channel]/255.0,np.double(ch_gamma[channel]))*np.double(ch_brightness[channel])+np.double(ch_offset[channel])),0,1))
	
	base_path = HOME_MEDIA_PATH+uname+'/data'
	rotations_color_path = base_path + '/rotations_color'
	if not os.path.exists(rotations_color_path):
		os.mkdir(rotations_color_path)
	
	for ch in range(num_ch):
		for co in range(3):
			color_stack_max_proj[:,:,co] = np.maximum(color_stack_max_proj[:,:,co], max_proj[:,:,ch]/255.0*color_list[ch][2-co] )		
			
	color_file_path = os.path.join(rotations_color_path,"color_rot.png")
	cv2.imwrite(color_file_path,color_stack_max_proj)
			
	color_file = '../../'+color_file_path.split('dinavid-gui/')[1]   


	data = { 'message' : 'This is a successful ajax request in gen_rot_color_images.',
				'color_file' :  color_file
			};		
	return JsonResponse(data)
	
	

# Upload a new volume	
def handle_uploaded_file(request):
	file = request.FILES['file']
	fname=file.name
	
	#Make sure file type is tif or tiff.
	if not fname.endswith('tif'):
		if not fname.endswith('tiff'):
			return #with some error
	
	#Make sure it is a composite tiff
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	
	im_name = os.path.splitext(fname)[0]
	if uname in SPECIAL_USER_LIST:
		dir = os.path.join(DATA_FILES_PATH,im_name)
	else:
		dir = os.path.join(DATA_FILES_PATH_NON_SPECIAL_USER,uname,im_name)
	#Filename already exists
	if os.path.exists(dir):
		return #probaly with some error
	os.mkdir(dir)	
	
	#Write uploaded file to disk.
	with open(os.path.join(dir,fname), 'wb+') as destination:
		for chunk in file.chunks():
			destination.write(chunk)
	
	
	src = dir
	
	base_path_user = HOME_MEDIA_PATH + uname
	if not os.path.exists(base_path_user):
		os.makedirs(base_path_user)
	dst = HOME_MEDIA_PATH + uname + '/data' 
	
	#Create symbolic link to point the current image to view to the just uplaoded iamge.
	if os.path.exists(dst):
		if os.path.islink(dst): 
			os.unlink(dst)
	os.symlink(src, dst)
	paths = get_paths(uname)

	#The below is commented out as we want to have channel name sets.

	# Load the user's channel name
	#ch_path_dir = HOME_MEDIA_PATH + uname + '/ch_name'
	# Load shared channel name
	"""ch_path_dir = SHARE_MEDIA_PATH	+ '/ch_name'
	ch_path = ch_path_dir+'/ch_name.txt'
	
	ch_names = None
	if os.path.exists(ch_path):
		with open(ch_path, 'r') as f:
			ch_names = json.load(f)"""
	"""else:
	#TODO: DO NOT CREATE DEFAULT CHANNEL SET. USE DINAVID DEFAULT
		# Create a default channel name
		ch_names = {}
		ch_names_list = ['channel_%d'%i for i in range(1,20)]
		ch_names_list_no_underscore = ["channel %d"%i for i in range(1,20)]
		for p in paths:
			temp_dict = {}
			for i in range(1,20):	
				temp_dict[ch_names_list[i-1]]=ch_names_list_no_underscore[i-1]
			ch_names[p]=temp_dict
		if not os.path.exists(ch_path_dir):
				os.mkdir(ch_path_dir)
		with open(ch_path, 'w') as f:
			json.dump(ch_names, f)
		# Load the user's channel name base on the data name"""
	"""try:
		ch_name = ch_names[im_name] 
	except:
		ch_names_list = ['channel_%d'%i for i in range(1,20)]
		ch_names_list_no_underscore = ["channel %d"%i for i in range(1,20)]
		temp_dict = {}
		for i in range(1,20):	
			temp_dict[ch_names_list[i-1]]=ch_names_list_no_underscore[i-1]
		ch_names[im_name]=temp_dict
		ch_name = ch_names[im_name]
		with open(ch_path, 'w') as f:
			json.dump(ch_names, f)"""
	return


#Upload a segmentation.
#Note that the uploaded segmentation should consist of a non-color-coded '.tif' file where each segmented nucleus is assigned a unique intensity value. 
#The background should have an intensity of 0. 	
#If you previously ran a segmentation on DINAVID and wish to upload that segmentation, then you should upload the 'three_d_result.tif' file under the 'three_d_result' directory in the downloaded zip file. 
def upload_segmentation(request): 

	file = request.FILES['image']
	fname=file.name
	
	#Make sure file type is tif or tiff.
	if not fname.endswith('tif'):
		if not fname.endswith('tiff'):
			data = { 'message' : 'This is an unsuccessful ajax request for uploading a segmentation.', 'success' : False
			};		
			return JsonResponse(data)
			
	#Make sure it is a composite tiff
	uname = request.user.username
	if not uname or uname.isspace():
		 uname = 'default'
	
	#Erase the contents in the temp seg folder, which is user/seg_results 
	savepath=HOME_MEDIA_PATH+uname+'/seg_results/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	savepath=HOME_MEDIA_PATH+uname+'/seg_results/uploaded_seg/3d_result/'
	if not os.path.exists(savepath):
		os.makedirs(savepath)
	
	#Write segmentation file to disk
	with open(os.path.join(savepath,fname), 'wb+') as destination:
		for chunk in file.chunks():
			destination.write(chunk)

	data = { 'message' : 'This is a successful ajax request for uploading a segmentation.', 'success' : True
			};		
	return JsonResponse(data)
	
 #Views used for selecting a volume
class SelectVolumeView(View):
	def get(self, request):
		paths = get_paths(request.user.username)
		return render(self.request, 'photos/workflow/select_volume.html', {'imagepaths': paths})
		
	def post(self, request):
		form = PhotoForm(request.POST, request.FILES)
		if request.method == 'POST':
			if form.is_valid():
				handle_uploaded_file(request)
				data = { 'message' : 'This is a successful ajax request in upload_image.'};	
		else:
			data = { 'message' : 'This is not a successful ajax request in upload_image.',
				};	 
		#return data
		return redirect(reverse('photos:workflow_compact'))
   
#Return list of paths to images that the user has permission to access:
def get_paths(user):

	if not user or user.isspace():
		 user = 'default'

	#User is in special user list, which can access the shared directory.
	if(user in SPECIAL_USER_LIST):
		paths = os.listdir(DATA_FILES_PATH)
		#Don't show the demo images.
		if 'demo' in paths: 
			paths.remove('demo')
		if '13384' in paths: 
			paths.remove('13384')
		paths = [ x for x in paths if "bbbc" not in x ]
		paths = [ x for x in paths if "deepsynth" not in x ]
		paths = [ x for x in paths if "eight_chan" not in x ]
		paths = [ x for x in paths if "dapi_dup" not in x ]
		return paths
	
	#User is not a special user, so can only access their own images.
	user_path = os.path.join(DATA_FILES_PATH_NON_SPECIAL_USER,user)
	if not os.path.exists(user_path):
		os.makedirs(user_path)
	
	return os.listdir(os.path.join(DATA_FILES_PATH_NON_SPECIAL_USER,user))
	