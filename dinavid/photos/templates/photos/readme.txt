This directory, /photos/templates/photos contains the following files
base.html - template for most of the website.
base_compact.html - template for the workflow page and test pages, where we need a more compact interface.
/workflow - directory hosting the html for the workflow and test pages
/workflow/index_test.html - test new html elements here, when succesful move to /workflow/index_compact
/workflow/index_compact.html - Main functionality of the site is displayed here.
/tool_intro - the html for the tutorial page
/demo - the html for the demo page

Most files are deprecated. However, some of the following may still be useful in an old version of DINAVID
/basic_upload,/drag_and_drop_upload, /progess_bar_upload - for uploading files
/visual, /visualinput, /visualdisplay - for displaying in 3D.
/csv, /download, /list- pages to download visualization

Directories that are not as useful as they are an old version of the current system.
/morphology, /operations, /process - deprecated. 