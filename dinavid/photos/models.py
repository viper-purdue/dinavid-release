

from django.db import models
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.contrib.auth.models import User
from django.conf import settings
import os

from PIL import Image as Img
#import StringIO

import tifffile as tiff
#import cv2

def user_directory_path(instance, filename):
    return '{0}/{1}/{2}'.format(instance.user.username,"data", filename)


class Photo(models.Model):
	file = models.FileField(upload_to=user_directory_path,max_length=255)
	uploaded_at = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User, null=True, blank=True,on_delete=models.CASCADE)
	iszip = models.BooleanField(default=False)

	def save(self, *args, **kwargs):
	
		if self.file:
			if self.file.name.lower().endswith('.tif') or self.file.name.lower().endswith('.tiff'):
				super(Photo, self).save(*args, **kwargs)
				return True

		self.file.name='You did not upload a file with the correct extension'
		return False
		



	class Meta:
	    ordering = ["file"]



def user_directory_path1(instance, filename):
    filename = os.path.basename(filename)
    return '{0}/{1}/{2}'.format(instance.user.username,"url_storage", filename)


class Result(models.Model):
	file = models.FileField(upload_to=user_directory_path1,max_length=255)
	uploaded_at = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User, null=True, blank=True,on_delete=models.CASCADE)
	iszip = models.BooleanField(default=False)

	class Meta:
	    ordering = ["file"]


class Visual(models.Model):
	address = models.CharField(max_length=255)
	display_name = models.CharField(max_length=255)
	user = models.ForeignKey(User, null=True, blank=True,on_delete=models.CASCADE)

	class Meta:
	    ordering = ["display_name"]

def user_directory_path_pre(instance, filename):
    filename = os.path.basename(filename)
    return '{0}/{1}/{2}'.format(instance.user.username,"preprocess_url", filename)


class PreprocessResult(models.Model):
	file = models.FileField(upload_to=user_directory_path_pre,max_length=255)
	uploaded_at = models.DateTimeField(auto_now_add=True)
	user = models.ForeignKey(User, null=True, blank=True,on_delete=models.CASCADE)
	iszip = models.BooleanField(default=False)

	class Meta:
	    ordering = ["file"]

class VisualInput(models.Model):
	address = models.CharField(max_length=255)
	display_name = models.CharField(max_length=255)
	user = models.ForeignKey(User, null=True, blank=True,on_delete=models.CASCADE)

	class Meta:
	    ordering = ["display_name"]