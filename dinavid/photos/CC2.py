# colorcodeRemoval.py
#
# Script for getting binary to color coded volume
#
# Implemented by David Ho, Soonam Lee, Chichen Fu 
# Date: 08-29-2017 7:00PM

#--------------------------------------------------------------------------

import skimage.io as io
import numpy as np
from tifffile import tifffile
from skimage import util
from skimage import measure
from scipy.ndimage.measurements import label
from skimage.morphology import erosion, dilation, ball
from scipy.interpolate import RegularGridInterpolator
# from itertools import count,ifilterfalse
import scipy.misc
import time
import os
from skimage.color import rgb2gray
import cv2
import shutil

from dinavid.photos.path_names import *


def CC2(Y,X,Z,uname,thSmall,colorcode,watershed,seg_dilate,segdilate_se_size):

	windowSize = 40
	thColormap = 50
	# thSmall = 20
	B_morp = 1



	dirIn = DATA_MEDIA_PATH + uname + '/watershed_temp/'
	dirRGB = DATA_MEDIA_PATH + uname + "/color_coded/";
	dirBW = DATA_MEDIA_PATH + uname + "/cc_small_removed/";
	
	if os.path.exists(dirRGB):
		shutil.rmtree(dirRGB)
	if os.path.exists(dirBW):
		shutil.rmtree(dirBW)
	if not os.path.exists(dirRGB):
		os.makedirs(dirRGB)
	if not os.path.exists(dirBW):
		os.makedirs(dirBW)

	# Measure time
	start1 = time.time()

	## Read images
	bw = util.img_as_ubyte(np.zeros([X,Y,Z]))
	bw1 = util.img_as_ubyte(np.zeros([X,Y,Z]))
	if watershed == "YES":
		for ii in range(0,Z):
			filename = dirIn + "z%04d.tif" % (ii+1)
			bw[:,:,ii] = rgb2gray(io.imread(filename))
			bw1[:,:,ii] = io.imread(filename)
	else:
		for ii in range(0,Z):
			filename = dirIn + "%04d.png" % (ii+1)
			bw[:,:,ii] = rgb2gray(io.imread(filename))
			bw1[:,:,ii] = io.imread(filename)

#	print("read volume done")

	# initialization (Soonam 9/8)
	bw3 = util.img_as_ubyte(np.zeros([X,Y,Z,3])) # RGB volume
	bw_r = np.zeros([X,Y,Z])
	bw_g = np.zeros([X,Y,Z])
	bw_b = np.zeros([X,Y,Z])


	# added by David on 9/5
	# erosion
	bw = erosion(bw,ball(B_morp))

	## Connected components
	cc = measure.label(bw,connectivity = 1)
	props = measure.regionprops(cc);

	print("pass cc")

	## Colormap - Read text files saved from MATLAB
	cmap = []
	ins = open(CMAP_PATH, "r")
	for line in ins:
		line = line.strip().split("\t")
		line2 = [float(n) for n in line];
		line3 = [int(line2[0]), int(line2[1]), int(line2[2])]
		cmap.append(line3)

	ins.close()

	cmap2 = []

	# Dark color removal from colormap
	for i in range(0,len(cmap)):
		if cmap[i][0] > thColormap or cmap[i][1] > thColormap or cmap[i][2] > thColormap:
			cmap2.append(cmap[i])

	print("colormap done")


	## Assign color index and small area removal
	# thSmall = 50

	N = 0 # the number of nuclei

	for ii in range(0,np.amax(cc)):
	#	print(ii, props[ii].area)
		coord = props[ii].coords

		if props[ii].area > thSmall: # added by David on 9/5
			N = N+1
			# coord = np.argwhere(cc == ii)
			coord_max = np.amax(coord,axis = 0)
			coord_min = np.amin(coord,axis = 0)
			# Define a 3D window containing a nucleus
			bw_window = bw[np.amax([0,coord_min[0] - windowSize]):np.amin([X-1,coord_max[0] + windowSize]), np.amax([0,coord_min[1] - windowSize]):np.amin([Y-1,coord_max[1] + windowSize]) ,np.amax([0,coord_min[2] - windowSize]):np.amin([Z-1,coord_max[2] + windowSize])]
			# Choose a color which is not used in the cropped window
			kk = 1
			while kk in bw_window:
				kk = kk+1

		else:	# small area removal process
			kk = 0;
			for i in range(props[ii].area):
				bw1[coord[i,0], coord[i,1], coord[i,2]] = kk;

		for i in range(props[ii].area):
			bw[coord[i,0], coord[i,1], coord[i,2]] = kk;	# Changed by Soonam on 9/14


	print(("number of nuclei: " + str(N)))
	print("small area removal done!")

	
	## Assign colors
	if colorcode =="YES":
		for ii in range(0,X):
			for jj in range(0,Y):
				for kk in range(0,Z):
					if bw[ii,jj,kk] != 0:
						bw_r[ii,jj,kk] = cmap2[bw[ii,jj,kk]-1][0]
						bw_g[ii,jj,kk] = cmap2[bw[ii,jj,kk]-1][1]
						bw_b[ii,jj,kk] = cmap2[bw[ii,jj,kk]-1][2]


		# dilation
		bw_r = dilation(bw_r, ball(B_morp))
		bw_g = dilation(bw_g, ball(B_morp))
		bw_b = dilation(bw_b, ball(B_morp))

		# added by Shuo on 10/16/2019
		# erosion
		# B = 1
		if seg_dilate == "YES":
			bw_r = dilation(bw_r, ball(segdilate_se_size))
			bw_g = dilation(bw_g, ball(segdilate_se_size))
			bw_b = dilation(bw_b, ball(segdilate_se_size))

		
		bw3[:,:,:,0] = bw_r
		bw3[:,:,:,1] = bw_g
		bw3[:,:,:,2] = bw_b
		


		print("wrote color coded images")


	## Write images
		for ii in range(0,Z):
			filename = dirRGB + "z%04d" %(ii+1) + ".png"
			cv2.imwrite(filename,bw3[:,:,ii,:])
			filename = dirBW + "z%04d" %(ii+1) + ".png"
			cv2.imwrite(filename,bw1[:,:,ii])


	if colorcode =="NO":
		for ii in range(0,Z):
			filename = dirBW + "z%04d" %(ii+1) + ".png"
			cv2.imwrite(filename,bw1[:,:,ii])
	


	## Measure time
	end1 = time.time()
	print(("time: " + str(end1-start1) + " s"))

