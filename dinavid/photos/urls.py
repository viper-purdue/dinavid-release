
from django.conf.urls import url

from . import views
app_name = 'photos'
urlpatterns = [
    url(r'^clear/$', views.clear_database, name='clear_database'), #Clear the database of list of items
    url(r'^clearresults/$', views.clear_results, name='clear_results'), #Clear list of segmentation results
    url(r'^basic-upload/$', views.BasicUploadView.as_view(), name='basic_upload'), #Basic upload page. Deprecated
    url(r'^progress-bar-upload/$', views.ProgressBarUploadView.as_view(), name='progress_bar_upload'), #Upload page with progress bar. Deprecated
    url(r'^drag-and-drop-upload/$', views.DragAndDropUploadView.as_view(), name='drag_and_drop_upload'),#Upload page with drag and drop bar. Deprecated
    url(r'^tool_intro/$', views.IntroView.as_view(), name='tool_intro'), # tools intro page
    url(r'^visualdisplay/$', views.VisualDisplayView.as_view(), name='visualdisplay'), #3d Display
    url(r'^clearinputvisual/$', views.clear_visual_input, name='clear_visual_input'),
    url(r'^print_sequal/$', views.print_sequal, name='print_sequal'),
    url(r'^csv/$', views.export_users_csv, name='export_users_csv'),     #exporting url with dummy data
    url(r'^workflow_test/$', views.WorkflowTestView.as_view(), name='workflow_test'),   #download page, under folder workflow/index
    url(r'^workflow_compact/$', views.WorkflowCompactView.as_view(), name='workflow_compact'),   #download page, under folder workflow/index
    url(r'^workflow_analysis/$', views.workflow_analysis, name='workflow_analysis'), 
    url(r'^workflow_preview/$', views.workflow_preview, name='workflow_preview'), #Preview the the preprocessing
    url(r'^workflow_process/$', views.workflow_process, name='workflow_process'),  #Actually perform the preprocessing
    url(r'^workflow_seg_preview/$', views.workflow_seg_preview, name='workflow_seg_preview'), #Preview the segmentation
    url(r'^workflow_seg/$', views.workflow_seg, name='workflow_seg'), #Actually perform the segmentation
    url(r'^workflow_quantification/$', views.workflow_quantification, name='workflow_quantification'), #draw_scatter plot
    url(r'^workflow_quantification_preview/$', views.workflow_quantification_preview, name='workflow_quantification_preview'), #draw_scatter plot on subvolume
    url(r'^update_workflow_quantification/$', views.update_workflow_quantification, name='update_workflow_quantification'), #draw_scatter plot
    url(r'^update_workflow_quantification_preview/$', views.update_workflow_quantification_preview, name='update_workflow_quantification_preview'), #draw_scatter plot on subvolume
    url(r'^update_channel_color/$', views.update_channel_color, name='update_channel_color'), #update the color channels
    url(r'^update_channel_name/$', views.update_channel_name, name='update_channel_name'),  # update user's input for channel name
	url(r'^load_channel_names/$', views.load_channel_names, name='load_channel_names'),  # load LUT
    url(r'^update_channel_params/$', views.update_channel_params, name='update_channel_params'),  # update user's input for selector parameters
    url(r'^load_channel_params/$', views.load_channel_params, name='load_channel_params'),  # load LUT
    url(r'^save_channel_params/$', views.save_channel_params, name='save_channel_params'),  # save LUT	
    url(r'^select_image_display/$', views.select_image_display, name='select_image_display'), #Select image to display
    url(r'^visualization_3d/$', views.visualization_3d, name='visualization_3d'),#renders a subregion in 3D
    url(r'^update_channel_color_slice/$', views.update_channel_color_slice, name='update_channel_color_slice'), #update the color channels slice by slice instead of doing all at a time.
    url(r'^zoom_in/$', views.zoom_in, name='zoom_in'), #Used for zoom function
    url(r'^zoom_in_cropped/$', views.zoom_in_cropped, name='zoom_in_cropped'), #Used for zoom function
    url(r'^lut_preprocess/$', views.lut_preprocess, name='lut_preprocess'), #Used for applying an LUT to preprocessed results
    url(r'^lut_preprocess_preview/$', views.lut_preprocess_preview, name='lut_preprocess_preview'), #Used for applying an LUT to preprocessed results
    url(r'^refresh_seg_subwindow/$', views.refresh_seg_subwindow, name='refresh_seg_subwindow'), #Update the window to preview segmentation in real time.
    url(r'^update_max_projection/$', views.update_max_projection, name='update_max_projection'), #Update the maximum intensity projection in real time
    url(r'^psuedo_3d/$', views.Psuedo3dView.as_view(), name='psuedo_3d'),
    url(r'^gen_rot_color_images/$', views.gen_rot_color_images, name='gen_rot_color_images'),   #generate the 360 view for a certain image
    url(r'^demo_index/$', views.DemoView.as_view(), name='demo_index'),   #download page, under folder workflow/index
    url(r'^update_channel_color_demo/$', views.update_channel_color_demo, name='update_channel_color_demo'),   #download page, under folder workflow/index
    url(r'^select_volume/$', views.SelectVolumeView.as_view(), name='select_volume'),  #Used to select which volume to display/process
    url(r'^getting_started/$', views.GetStartedView.as_view(), name='getting_started'), #intro page
    url(r'^upload_segmentation/$', views.upload_segmentation, name='upload_segmentation'), #upload segmentation to server
]
