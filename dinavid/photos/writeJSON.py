
# -*- coding: utf-8 -*-
import sys
import json
import os

from dinavid.photos.path_names import *

# Make it work for Python 2+3 and with Unicode
import io
try:
    to_unicode = str
except NameError:
    to_unicode = str

# Define data
#data = {'a list': [1, 42, 3.141, 1337, 'help', u'€'],
#        'a string': 'bla',
#        'another dict': {'foo': 'bar',
#                         'key': 'value',
#                         'the answer': 42}}

def writeJSON(X,Y,Z,dirVis):

	# Read JSON file
	with open(VIZ_PATH+"default.json") as data_file:
	    data = json.load(data_file)

	data['objects'][0]['volume']['url'] = "tileImage.png"
	data['objects'][0]['volume']['res'] = [X, Y, Z];
	data['objects'][0]['slices']['properties']['zoom'] = (280.0/X)

	# Write JSON file
	with io.open(dirVis + 'default.json', 'w', encoding='utf8') as outfile:
	    str_ = json.dumps(data,
	                      indent=2, sort_keys=False,
	                      separators=(',', ': '), ensure_ascii=False)
	    outfile.write(to_unicode(str_))
