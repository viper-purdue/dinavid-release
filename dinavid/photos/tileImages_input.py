#
# Script for stitching images from multiple 
# 
# Implemented by Soonam Lee
# Date: 09-20-2017 5:00PM
#--------------------------------------------------------------------------

import skimage.io as io
import numpy as np
import scipy.misc
import tifffile as tiff
from skimage import util
from skimage import measure
from scipy.ndimage.measurements import label
from scipy import ndimage as ndi
import time
import os
from skimage.color import rgb2gray
import cv2
import zipfile
import shutil
from skimage.morphology import watershed
from skimage.feature import peak_local_max
######## background subtract ############
from dinavid.photos.background_subtractor import subtract_background_rolling_ball
import pwd
import grp


#Used paths
from dinavid.photos.path_names import *


def recursive_file_permissions(path,mode):
    os.chmod(path,mode)
    for item in glob.glob(path+'/*'):
        if os.path.isdir(item):
            recursive_file_permissions(os.path.join(path,item),mode)
        else:
            try:
                os.chmod(os.path.join(path,item),mode)
            except:
                print('File permissions on {0} not updated due to error.'.format(os.path.join(path,item)))
    

#Performs preprocessing steps and preprocessing previews.
#If slice==-1, it is a preview
def tileImages_input_dropdown_nuc(nuc_channel_in,uname,datetime_now,dirVis,preprocess_download_path,path,x1,x2,y1,y2, operation, resolution, k_size,  \
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel,num_z, num_ch, preprocess_orig_to_compare, slice=-1): 
	
	if preprocess_orig_to_compare is not None:
		#Make the path if it does not exist
		if os.path.exists(preprocess_orig_to_compare):
			shutil.rmtree(preprocess_orig_to_compare)
		if not os.path.exists(preprocess_orig_to_compare):
			os.makedirs(preprocess_orig_to_compare)

	
	nuc_channel_in = int(nuc_channel_in)
	start1 = time.time()
	num_z  = int(num_z)
	num_ch = int(num_ch)

	file_base = None
	# Need to find out X, Y and Z from images in PATH - Soonam
	# Also, load first file in path and check whether it is color image or gray image
	for dirname, subdirs, files in os.walk(path):
		filename = files[0]

		file_base = filename[:-4] #WE NEED TO FIND A BETTER WAY TO GENERATE THESE NAMES
		filepath = os.path.join(dirname, filename);
			
		#Find the dimensions of the image.
		img = cv2.imread(filepath,0);
		height = img.shape[0]
		width = img.shape[1]


	X = width
	Y = height
	Z = num_z
	print(("X = ", X));
	print(("Y = ", Y));
	print(("Z = ", Z));

	# Make tiled image. It should be squared image to input
	tile_W = int(np.ceil(np.sqrt(float(Z))));
	tile_H = tile_W;
	
	x1 = int(x1)
	x2 = int(x2)
	y1 = int(y1)
	y2 = int(y2)

	num_ops = len(operation)
	
	
	if not os.path.exists(dirVis):
		os.makedirs(dirVis)
		
	#Create filename based on input parameters
	fnamenew = file_base

	for op_num in range(0,num_ops):
		
		#figure out downsampling factor
		which_op = operation[op_num]
		print(which_op);
		
		#figure out tile images
		if op_num == 0:
			factor_downsample = resolution[op_num]
			if which_op == "downsampling":
				if factor_downsample == "8x":
					factor_downsample = 8.0
				elif factor_downsample == "4x": 
					factor_downsample = 4.0
				elif factor_downsample == "2x": 
					factor_downsample = 2.0   
				else:
					factor_downsample = 1.0
			else:
				factor_downsample = 1.0	
		
			#size of each image after downsample
			Y_ds = np.int((y2-y1)/factor_downsample)
			X_ds = np.int((x2-x1)/factor_downsample)
			I = util.img_as_ubyte(np.zeros([Z,y2-y1,x2-x1]))	
			
			for index, file in enumerate(sorted(files)[nuc_channel_in:-1:num_ch]):
				if slice != -1 and slice != index:
					continue
				#Color image (3 channel)
				im_slice = cv2.imread(os.path.join(dirname, file),0)
				I[index,:,:] = np.float32(im_slice[y1:y2,x1:x2])#/np.max(vol)*255
				
				orig_imgs = os.path.join(preprocess_orig_to_compare, file)
				cv2.imwrite(os.path.splitext(orig_imgs)[0]+'.png', im_slice[y1:y2,x1:x2])

			# Initialization

			I_result = util.img_as_ubyte(np.zeros([Z, Y_ds, X_ds]))
		else:
			#size of each image after downsample
			sizes = I_result.shape
			Y_ds = np.int(sizes[1]/factor_downsample)
			X_ds = np.int(sizes[2]/factor_downsample)
		
			I = I_result;

			I_result = util.img_as_ubyte(np.zeros([Z, Y_ds, X_ds]));
			
		#Perform operation for each slice			
		for num_slice in range(0,Z):
			if slice != -1 and slice != num_slice:
				continue
			if which_op == "downsampling": 
				I_result[num_slice] = cv2.resize(I[num_slice], (np.int(X_ds), np.int(Y_ds)), interpolation=cv2.INTER_NEAREST)	
				if num_slice == 0:				
					fnamenew = fnamenew + "_ds-%d" % ( factor_downsample )			
			elif which_op == "smoothing":	
				sigma[op_num]=np.float(sigma[op_num])
				k_size[op_num]=np.int(k_size[op_num])				
				I_result[num_slice] = gaussian_2d_filter(I[num_slice],k_size[op_num],sigma[op_num])
				
				if sigma[op_num] <= 0:
					#This equation is per https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html#Mat%20getGaussianKernel(int%20ksize,%20double%20sigma,%20int%20ktype)
					sigma[op_num] = 0.3*((k_size[op_num]-1)*0.5 - 1) + 0.8 
				if num_slice == 0:				
					fnamenew = fnamenew + "_gb-%d-%.1f" % ( k_size[op_num], sigma[op_num] )
			elif which_op == "thresholding":
				threshold[op_num] = np.int(threshold[op_num])
				I_result[num_slice] = single_threshold(I[num_slice],threshold[op_num],threshtype[op_num])
				if num_slice == 0:				
					fnamenew = fnamenew + "_th-%s-%d" % ( threshtype[op_num], threshold[op_num] )
			elif which_op == "morph":
				#Get size of structuring element
				se_size[op_num] = int(se_size[op_num])
				
				#Get type of structuring element
				se_type_flag=cv2.MORPH_RECT
				if se_type[op_num] == "rect":
					se_type_flag=cv2.MORPH_RECT
				elif se_type[op_num] == "ellipse":
					se_type_flag=cv2.MORPH_ELLIPSE
				elif se_type[op_num] == "cross":
					se_type_flag=cv2.MORPH_CROSS
			
				#Construct structuring element
				struct_el = cv2.getStructuringElement(se_type_flag,(se_size[op_num],se_size[op_num]))
			
				#Perform structuring element based on selection
				if morph_type[op_num] == "erosion":
					I_result[num_slice] = cv2.erode(I[num_slice],struct_el,iterations = 1)
				elif morph_type[op_num] == "dilation":
					I_result[num_slice] = cv2.dilate(I[num_slice],struct_el,iterations = 1)
				elif morph_type[op_num] == "opening":
					I_result[num_slice] = cv2.morphologyEx(I[num_slice],cv2.MORPH_OPEN,struct_el)
				elif morph_type[op_num] == "closing":
					I_result[num_slice] = cv2.morphologyEx(I[num_slice],cv2.MORPH_CLOSE,struct_el)
				
				if num_slice == 0:				
					fnamenew = fnamenew + "_%s-%s-%d" % ( morph_type[op_num], se_type[op_num], se_size[op_num] )	
			elif which_op == "watershed":
				distance = ndi.distance_transform_edt(I[num_slice])
				local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3)),
							labels=I[num_slice])
				markers = ndi.label(local_maxi)[0]
				labels = watershed(-distance, markers, mask=I[num_slice])
				I_result[num_slice] = labels
				if num_slice == 0:				
					fnamenew = fnamenew + "_ws"
			elif which_op == "rollingball":
				ball_size[op_num]=np.int(ball_size[op_num])
				I_result[num_slice], _ = subtract_background_rolling_ball(I[num_slice], ball_size[op_num])				
				if num_slice == 0:				
					fnamenew = fnamenew + "_rbs-%d" % ( ball_size[op_num] )	
			elif which_op == "medianfilter":
				mediankernel[op_num]=np.int(mediankernel[op_num])
				I_result[num_slice]=cv2.medianBlur(I[num_slice],mediankernel[op_num])
				if num_slice == 0:				
					fnamenew = fnamenew + "_med-%d" % ( mediankernel[op_num] )	
			elif which_op == "mediansub":
				mediansubkernel[op_num]=np.int(mediansubkernel[op_num])
				I_result[num_slice]=cv2.medianBlur(I[num_slice],mediansubkernel[op_num])
				
				#Get rid of situation of subtracting two unsigned ints.
				for yy in range(0,Y_ds):
					for xx in range(0,X_ds): 
						aa = np.int(I[num_slice,yy,xx])-np.int(I_result[num_slice,yy,xx])
						if aa < 0:
							I_result[num_slice,yy,xx]=0
						else:
							I_result[num_slice,yy,xx]=np.ubyte(aa)
				if num_slice == 0:				
					fnamenew = fnamenew + "_medsub-%d" % ( mediansubkernel[op_num] )
			else: #No operation		
				I_result[num_slice] = I[num_slice]			

	
	Y_size = I_result.shape[1]
	X_size = I_result.shape[2]
	
	if slice == -1:
		Itiled=util.img_as_ubyte(np.zeros([tile_H*Y_size,tile_W*X_size]));
		for index in range(0,Z):
			Itiled[np.int(index/tile_H)*Y_size:(np.int(index/tile_H)+1)*Y_size,(index%tile_W)*X_size:(index%tile_W+1)*X_size] = I_result[index];


		# Write stitched image
		if not os.path.exists(dirVis):
			os.makedirs(dirVis)
		fname = dirVis + "tileImage.png"
		cv2.imwrite(fname,Itiled);

		
	#Make the path to host the temporary filtered files
	if preprocess_download_path is not None:
		#Make the path if it does not exist
		if os.path.exists(preprocess_download_path):
			shutil.rmtree(preprocess_download_path)
		if not os.path.exists(preprocess_download_path):
			os.makedirs(preprocess_download_path)	
			
		#Save each image.
		for index in range(0,Z):
			if slice != -1 and slice != index:
				continue
			image = I_result[index]
			
			#Write file to disk			
			
			filename = (fnamenew+"_z%04d.png"%index)
			filepath = os.path.join(preprocess_download_path, filename)
			cv2.imwrite(filepath,image)

		zipaddress = HOME_MEDIA_PATH+uname+'/preprocess_url_storage/'
		if not os.path.exists(zipaddress):
			os.makedirs(zipaddress)

		zipfilename = HOME_MEDIA_PATH+uname+'/preprocess_url_storage/preprocess_'+ datetime_now + '_' + fnamenew +'.zip'
		zf = zipfile.ZipFile(zipfilename, "w")
	
		lenDirPath = len(preprocess_download_path)
		for dirname, subdirs, files in os.walk(preprocess_download_path):
			for filename in files:
					filepath = os.path.join(dirname, filename)
					zf.write(filepath , filepath[lenDirPath :])
		zf.close()
	## Measure time
	end1 = time.time()
	print((end1-start1))	

	return X,Y,Z, fnamenew, X_size, Y_size;

def tileImages_input_dropdown(uname,datetime_now,dirVis,preprocess_download_path,path,x1,x2,y1,y2, operation, resolution, k_size,  \
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel): 
		
	start1 = time.time()

	file_base = None
	# Need to find out X, Y and Z from images in PATH
	# Also, load first file in path and check whether it is color image or gray image
	kk = 0;
	for dirname, subdirs, files in os.walk(path):
		for filename in sorted(files):
			# We need only one image
			if (kk > 0):
				break;
			if file_base is None:
				file_base = filename[:-10] 
			filepath = os.path.join(dirname, filename);
			I = io.imread(filepath);
			# Check whether it is color or gray image
			if (len(np.shape(I)) == 2):
				[Y, X] = np.shape(I);
				boolColor = 0;
			elif (len(np.shape(I)) > 2):
				[Y, X, _] = np.shape(I);
				boolColor = 1;
			kk = kk + 1;

	# Number of files (Z) in path - Soonam
	listTmp = os.listdir(path);
	Z = len(listTmp);
	print(("X = ", X));
	print(("Y = ", Y));
	print(("Z = ", Z));

	# Make tiled image. It should be squared image to input
	tile_W = int(np.ceil(np.sqrt(float(Z))));
	tile_H = tile_W;
	# boolColor = -1;
	
	x1 = int(x1)
	x2 = int(x2)
	y1 = int(y1)
	y2 = int(y2)

	num_ops = len(operation)
	
	if not os.path.exists(dirVis):
		os.makedirs(dirVis)
		
	#Create filename based on input parameters
	fnamenew = file_base

	for op_num in range(0,num_ops):
		
		#figure out downsampling factor
		which_op = operation[op_num]
		print(which_op);
		factor_downsample = resolution[op_num]
		if which_op == "downsampling":
			if factor_downsample == "8x":
				factor_downsample = 8.0
			elif factor_downsample == "4x": 
				factor_downsample = 4.0
			elif factor_downsample == "2x": 
				factor_downsample = 2.0   
			else:
				factor_downsample = 1.0
		else:
			factor_downsample = 1.0	
		
		#figure out tile images
		if op_num == 0:
			#size of each image after downsample
			Y_ds = np.int((y2-y1)/factor_downsample)
			X_ds = np.int((x2-x1)/factor_downsample)
			
			if (boolColor == 0):
				I = util.img_as_ubyte(np.zeros([Z,y2-y1,x2-x1]))	
			else:
				I = util.img_as_ubyte(np.zeros([Z,y2-y1,x2-x1,3]))
			for dirname, subdirs, files in os.walk(path):
				for index, filename in enumerate(sorted(files)):
					filepath = os.path.join(dirname, filename);
					orig_img = io.imread(filepath);	
					
					if (boolColor == 0):
						I[index,:,:] = orig_img[y1:y2,x1:x2]	
					else :				
						I[index,:,:] = orig_img[y1:y2,x1:x2,:]		
				
			# Initialization
			if (boolColor == 0):
				I_result = util.img_as_ubyte(np.zeros([Z, Y_ds, X_ds]));
			elif (boolColor == 1):
				I_result = util.img_as_ubyte(np.zeros([Z, Y_ds, X_ds, 3]));
		else:
			#size of each image after downsample
			sizes = I_result.shape
			Y_ds = np.int(sizes[1]/factor_downsample)
			X_ds = np.int(sizes[2]/factor_downsample)
		
			I = I_result;
			if (boolColor == 0):
				I_result = util.img_as_ubyte(np.zeros([Z, Y_ds, X_ds]));
			elif (boolColor == 1):
				I_result = util.img_as_ubyte(np.zeros([Z, Y_ds, X_ds, 3]));
			
		#Perform operation for each slice			
		for num_slice in range(0,Z):
			if which_op == "downsampling": 
				I_result[num_slice] = cv2.resize(I[num_slice], (np.int(X_ds), np.int(Y_ds)), interpolation=cv2.INTER_NEAREST)	
				if num_slice == 0:				
					fnamenew = fnamenew + "_ds-%d" % ( factor_downsample )			
			elif which_op == "smoothing":	
				sigma[op_num]=np.float(sigma[op_num])
				k_size[op_num]=np.int(k_size[op_num])				
				I_result[num_slice] = gaussian_2d_filter(I[num_slice],k_size[op_num],sigma[op_num])
				
				if sigma[op_num] <= 0:
					#This equation is per https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html#Mat%20getGaussianKernel(int%20ksize,%20double%20sigma,%20int%20ktype)
					sigma[op_num] = 0.3*((k_size[op_num]-1)*0.5 - 1) + 0.8 
				if num_slice == 0:				
					fnamenew = fnamenew + "_gb-%d-%.1f" % ( k_size[op_num], sigma[op_num] )
			elif which_op == "thresholding":
				threshold[op_num] = np.int(threshold[op_num])
				I_result[num_slice] = single_threshold(I[num_slice],threshold[op_num],threshtype[op_num])
				if num_slice == 0:				
					fnamenew = fnamenew + "_th-%s-%d" % ( threshtype[op_num], threshold[op_num] )
			elif which_op == "morph":
				#Get size of structuring element
				se_size[op_num] = int(se_size[op_num])
				
				#Get type of structuring element
				se_type_flag=cv2.MORPH_RECT
				if se_type[op_num] == "rect":
					se_type_flag=cv2.MORPH_RECT
				elif se_type[op_num] == "ellipse":
					se_type_flag=cv2.MORPH_ELLIPSE
				elif se_type[op_num] == "cross":
					se_type_flag=cv2.MORPH_CROSS
			
				#Construct structuring element
				struct_el = cv2.getStructuringElement(se_type_flag,(se_size[op_num],se_size[op_num]))
			
				#Perform structuring element based on selection
				if morph_type[op_num] == "erosion":
					I_result[num_slice] = cv2.erode(I[num_slice],struct_el,iterations = 1)
				elif morph_type[op_num] == "dilation":
					I_result[num_slice] = cv2.dilate(I[num_slice],struct_el,iterations = 1)
				elif morph_type[op_num] == "opening":
					I_result[num_slice] = cv2.morphologyEx(I[num_slice],cv2.MORPH_OPEN,struct_el)
				elif morph_type[op_num] == "closing":
					I_result[num_slice] = cv2.morphologyEx(I[num_slice],cv2.MORPH_CLOSE,struct_el)
				
				if num_slice == 0:				
					fnamenew = fnamenew + "_%s-%s-%d" % ( morph_type[op_num], se_type[op_num], se_size[op_num] )	
			elif which_op == "watershed":
				distance = ndi.distance_transform_edt(I[num_slice])
				local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3)),
							labels=I[num_slice])
				markers = ndi.label(local_maxi)[0]
				labels = watershed(-distance, markers, mask=I[num_slice])
				I_result[num_slice] = labels
				if num_slice == 0:				
					fnamenew = fnamenew + "_ws"
			elif which_op == "rollingball":
				ball_size[op_num]=np.int(ball_size[op_num])
				I_result[num_slice], _ = subtract_background_rolling_ball(I[num_slice], ball_size[op_num])				
				if num_slice == 0:				
					fnamenew = fnamenew + "_rbs-%d" % ( ball_size[op_num] )	
			elif which_op == "medianfilter":
				mediankernel[op_num]=np.int(mediankernel[op_num])
				I_result[num_slice]=cv2.medianBlur(I[num_slice],mediankernel[op_num])
				if num_slice == 0:				
					fnamenew = fnamenew + "_med-%d" % ( mediankernel[op_num] )	
			elif which_op == "mediansub":
				mediansubkernel[op_num]=np.int(mediansubkernel[op_num])
				I_result[num_slice]=cv2.medianBlur(I[num_slice],mediansubkernel[op_num])
				
				#Get rid of situation of subtracting two unsigned ints.
				for yy in range(0,Y_ds):
					for xx in range(0,X_ds): 
						aa = np.int(I[num_slice,yy,xx])-np.int(I_result[num_slice,yy,xx])
						if aa < 0:
							I_result[num_slice,yy,xx]=0
						else:
							I_result[num_slice,yy,xx]=np.ubyte(aa)
				if num_slice == 0:				
					fnamenew = fnamenew + "_medsub-%d" % ( mediansubkernel[op_num] )	
	
	Y_size = I_result.shape[1]
	X_size = I_result.shape[2]
	
	if (boolColor == 0):
		Itiled=util.img_as_ubyte(np.zeros([tile_H*Y_size,tile_W*X_size]));
		
		for index in range(0,Z):
			Itiled[np.int(index/tile_H)*Y_size:(np.int(index/tile_H)+1)*Y_size,(index%tile_W)*X_size:(index%tile_W+1)*X_size] = I_result[index];
	
	else:
		Itiled=util.img_as_ubyte(np.zeros([tile_H*Y_size,tile_W*X_size,3]));
		for index in range(0,Z):
			# Tiled each stack of color images
			Itiled[np.int(index/tile_H)*Y_size:(np.int(index/tile_H)+1)*Y_size,(index%tile_W)*X_size:(index%tile_W+1)*X_size,:] = I_result[index];


	# Write stitched image
	if not os.path.exists(dirVis):
		os.makedirs(dirVis)
	fname = dirVis + "tileImage.png"
	cv2.imwrite(fname,Itiled);

		
	#Make the path to host the temporary filtered files
	if preprocess_download_path is not None:
		#Make the path if it does not exist
		if not os.path.exists(preprocess_download_path):
			os.makedirs(preprocess_download_path)
			#Delete all files in current folder
		preprocess_download_path=os.path.join(preprocess_download_path, 'preprocess_'+ datetime_now  + '_' + fnamenew )	
		if not os.path.exists(preprocess_download_path):
			os.makedirs(preprocess_download_path)
			
		#Save each image.
		for index in range(0,Z):
			if (boolColor == 0):
				# Tiled each stack of gray scale images
				image = I_result[index] #Itiled[np.int(index/tile_H)*Y_size:(np.int(index/tile_H)+1)*Y_size,(index%tile_W)*X_size:(index%tile_W+1)*X_size]
			else:
				# Tiled each stack of color images
				image = I_result[index]#Itiled[np.int(index/tile_H)*Y_size:(np.int(index/tile_H)+1)*Y_size,(index%tile_W)*X_size:(index%tile_W+1)*X_size,:]
			
			#Write file to disk			
			
			filename = (fnamenew+"_z%04d.png"%index)
			filepath = os.path.join(preprocess_download_path, filename)
			cv2.imwrite(filepath,image)

		zipaddress = HOME_MEDIA_PATH+uname+'/preprocess_url_storage/'
		if not os.path.exists(zipaddress):
			os.makedirs(zipaddress)

		zipfilename = HOME_MEDIA_PATH+uname+'/preprocess_url_storage/preprocess_'+ datetime_now + '_' + fnamenew +'.zip'
		zf = zipfile.ZipFile(zipfilename, "w")
	
		lenDirPath = len(preprocess_download_path)
		for dirname, subdirs, files in os.walk(preprocess_download_path):
			for filename in files:
					filepath = os.path.join(dirname, filename)
					zf.write(filepath , filepath[lenDirPath :])
		zf.close()

	## Measure time
	end1 = time.time()
	print((end1-start1))	

	return X,Y,Z, fnamenew, X_size, Y_size;


def tileImages_input(uname,datetime_now,X,Y,Z,dirVis,preprocess_download_path,path,x1,x2,y1,y2,if_resolution=False, \
	factor_downsample=1.0,if_filter=False,k_size=3,sigma=0,if_thresh=False,threshold=20, \
	threshtype="bi",if_Bsubtrac = False,ball_size=30,morph_type=False,se_size=None,se_type=None, prewater = False):
	

	# Measure time
	start1 = time.time()

	file_base = None
	# Need to find out X, Y and Z from images in PATH 
	# Also, load first file in path and check whether it is color image or gray image
	kk = 0;
	for dirname, subdirs, files in os.walk(path):
		# print(files)
		for filename in sorted(files):
			# print(filename)
			# We need only one image
			if (kk > 0):
				break;
			if file_base is None:
				file_base = filename[:-10] 
			filepath = os.path.join(dirname, filename);
			I = io.imread(filepath);
			# Check whether it is color or gray image
			if (len(np.shape(I)) == 2):
				[Y, X] = np.shape(I);
				boolColor = 0;
			elif (len(np.shape(I)) > 2):
				[Y, X, _] = np.shape(I);
				boolColor = 1;
			kk = kk + 1;

	# Number of files (Z) in path - Soonam
	listTmp = os.listdir(path);
	Z = len(listTmp);
	print(("X = ", X));
	print(("Y = ", Y));
	print(("Z = ", Z));

	# Make tiled image. It should be squared image to input
	tile_W = int(np.ceil(np.sqrt(float(Z))));
	tile_H = tile_W;
	# boolColor = -1;
	
	x1 = int(x1)
	x2 = int(x2)
	y1 = int(y1)
	y2 = int(y2)

	#size of each image after downsample
	Y_ds = np.int((y2-y1)/factor_downsample)
	X_ds = np.int((x2-x1)/factor_downsample)
	# Initialization
	if (boolColor == 0):
		Itiled = util.img_as_ubyte(np.zeros([tile_H*Y_ds,tile_W*X_ds]));

	elif (boolColor == 1):
		Itiled = util.img_as_ubyte(np.zeros([tile_H*Y_ds, tile_W*X_ds, 3]));

	# Load all files in path
	for dirname, subdirs, files in os.walk(path):
		for index, filename in enumerate(sorted(files)):
			# print(filename)
			filepath = os.path.join(dirname, filename);
			orig_img = io.imread(filepath);	
			
			if (boolColor == 0):
				I = orig_img[y1:y2,x1:x2]	
			else :				
				I = orig_img[y1:y2,x1:x2,:]	
				
			### Down sample the tile_images by factor of 2or4, Shuo 08-26-2019
			[Y, X] = np.shape(I)

			if if_resolution:
				I = cv2.resize(I, (np.int(X/factor_downsample), np.int(Y/factor_downsample)), interpolation=cv2.INTER_NEAREST)
			
			#add background subtraction 
			if if_Bsubtrac:
				I, _ = subtract_background_rolling_ball(I, ball_size)
			#filter the 2d image if if_filter is Ture edited by Chad 8/27/2019 
			if if_filter:
				I = gaussian_2d_filter(I,k_size,sigma)

			#Perform threshold if option selected.
			if if_thresh:
				I = single_threshold(I,threshold,threshtype)
				
			#Perform morphological operator
			if morph_type is not False:
			
				#Get size of structuring element
				se_size = int(se_size)
				
				#Get type of structuring element
				se_type_flag=cv2.MORPH_RECT
				if se_type == "rect":
					se_type_flag=cv2.MORPH_RECT
				elif se_type == "ellipse":
					se_type_flag=cv2.MORPH_ELLIPSE
				elif se_type == "cross":
					se_type_flag=cv2.MORPH_CROSS
			
				#Construct structuring element
				struct_el = cv2.getStructuringElement(se_type_flag,(se_size,se_size))
			
				#Perform structuring element based on selection
				if morph_type == "erosion":
					I = cv2.erode(I,struct_el,iterations = 1)
				elif morph_type == "dilation":
					I = cv2.dilate(I,struct_el,iterations = 1)
				elif morph_type == "opening":
					I = cv2.morphologyEx(I,cv2.MORPH_OPEN,struct_el)
				elif morph_type == "closing":
					I = cv2.morphologyEx(I,cv2.MORPH_CLOSE,struct_el)

			if prewater:
				distance = ndi.distance_transform_edt(I)
				local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3)),
							labels=I)
				markers = ndi.label(local_maxi)[0]
				labels = watershed(-distance, markers, mask=I)
				I = labels
							
			Y_size = I.shape[0]
			X_size = I.shape[1]
			if (boolColor == 0):
				I = rgb2gray(I);
				# Tiled each stack of gray scale images
				Itiled[np.int(index/tile_H)*Y_size:(np.int(index/tile_H)+1)*Y_size,(index%tile_W)*X_size:(index%tile_W+1)*X_size] = I;
			else:
				# Tiled each stack of color images
				Itiled[np.int(index/tile_H)*Y_size:(np.int(index/tile_H)+1)*Y_size,(index%tile_W)*X_size:(index%tile_W+1)*X_size,:] = I;
				
	if not os.path.exists(dirVis):
		os.makedirs(dirVis)

	# Write stitched image
	fname = dirVis + "tileImage.png"
	cv2.imwrite(fname,Itiled);

	#Create filename based on input parameters
	fnamenew = file_base
	if if_resolution:
		fnamenew = fnamenew + "_ds-%d" % ( factor_downsample )
	if if_filter:
		if sigma <= 0:
			#This equation is per https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html#Mat%20getGaussianKernel(int%20ksize,%20double%20sigma,%20int%20ktype)
			sigma = 0.3*((k_size-1)*0.5 - 1) + 0.8 
		fnamenew = fnamenew + "_gb-%d-%.1f" % ( k_size, sigma )
	if if_thresh:
		fnamenew = fnamenew + "_th-%s-%d" % ( threshtype, threshold )
	if morph_type:
		fnamenew = fnamenew + "_%s-%s-%d" % ( morph_type, se_type, se_size )
	if prewater:
		fnamenew = fnamenew + "_ws"
	if if_Bsubtrac:
		fnamenew = fnamenew + "_bsub_%d" % ( ball_size )
	### add this if want to save sub-coordinates
	#if x1 != 0 or y1 !=0 or x2 != X or y2 != Y:
	#	fnamenew = fnamenew + "_sub-x-%s-%s-y-%s-%s" % ( x1, x2, y1, y2 )
		
	#Make the path to host the temporary filtered files
	if preprocess_download_path is not None:
		#Make the path if it does not exist
		if not os.path.exists(preprocess_download_path):
			os.makedirs(preprocess_download_path)
			#Delete all files in current folder
		preprocess_download_path=os.path.join(preprocess_download_path, 'preprocess_'+ datetime_now  + '_' + fnamenew )	
		if not os.path.exists(preprocess_download_path):
			os.makedirs(preprocess_download_path)
			
		#Save each image.
		for index in range(0,Z):
			if (boolColor == 0):
				# Tiled each stack of gray scale images
				image = Itiled[np.int(index/tile_H)*Y_size:(np.int(index/tile_H)+1)*Y_size,(index%tile_W)*X_size:(index%tile_W+1)*X_size]
			else:
				# Tiled each stack of color images
				image = Itiled[np.int(index/tile_H)*Y_size:(np.int(index/tile_H)+1)*Y_size,(index%tile_W)*X_size:(index%tile_W+1)*X_size,:]
			
			#Write file to disk			
			
			filename = (fnamenew+"_z%04d.png"%index)
			filepath = os.path.join(preprocess_download_path, filename)
			cv2.imwrite(filepath,image)

		zipfilename = HOME_MEDIA_PATH+uname+'/preprocess_url_storage/preprocess_'+ datetime_now + '_' + fnamenew +'.zip'
		zf = zipfile.ZipFile(zipfilename, "w")
	
		lenDirPath = len(preprocess_download_path)
		for dirname, subdirs, files in os.walk(preprocess_download_path):
			for filename in files:
					filepath = os.path.join(dirname, filename)
					zf.write(filepath , filepath[lenDirPath :])
		zf.close()

		#shutil.rmtree(HOME_MEDIA_PATH+uname+'/preprocess/')

	## Measure time
	end1 = time.time()
	print((end1-start1))

	return X,Y,Z, fnamenew;

#Filter a 2d image with a gaussian filter
#chad 08-27-2019
def gaussian_2d_filter(in_img,k_size,sigma):
	if k_size < 3:
		k_size = 3
	elif k_size > 21:
		k_size = 21
	elif k_size % 2 == 0:
		k_size = k_size -1
	out = cv2.GaussianBlur(in_img,(k_size,k_size),sigma)
	return out


#Threshold a 3D image with a given threshold.
#Alain 08-27-2019
def single_threshold(img, thresh, threshtype):	
	#cv2.threshold only works for 2D it seems
	#see https://docs.opencv.org/3.3.1/d7/d4d/tutorial_py_thresholding.html
	#binary threshold
	if threshtype=="bi":
		retval,img=cv2.threshold(img,thresh,255,cv2.THRESH_BINARY) 
	else:
		#make all pixels lower than the threshold zero
		#This is called clamping
		retval,img = cv2.threshold(img,thresh,255,cv2.THRESH_TOZERO) 		
	return img


#Creates a 3D visualization with color
def tileImages_input_workflow(uname,datetime_now, dirVis,input_address, x1, x2, y1, y2):
	start1 = time.time()

	file_base = None
	# Need to find out X, Y and Z from images in PATH - Soonam
	# Also, load first file in path and check whether it is color image or gray image
	for dirname, subdirs, files in os.walk(input_address):
		# print(files)
		for filename in sorted(files):
			# print(filename)
			# We need only one image
			if file_base is None:
				file_base = filename[:-4] #WE NEED TO FIND A BETTER WAY TO GENERATE THESE NAMES
				filepath = os.path.join(dirname, filename);
				
				I = io.imread(filepath)
				[Y,X,channels]=np.shape(I)
		Z = len(files)

	print(("X = ", X));
	print(("Y = ", Y));
	print(("Z = ", Z));

	# Make tiled image. It should be squared image to input
	tile_W = int(np.ceil(np.sqrt(float(Z))));
	tile_H = tile_W;
	
	x1 = int(x1)
	x2 = int(x2)
	y1 = int(y1)
	y2 = int(y2)
	Y_size = y2-y1
	X_size = x2-x1
	
	if not os.path.exists(dirVis):
		os.makedirs(dirVis)
		
	#Create filename based on input parameters
	fnamenew = file_base	
	
	Itiled=util.img_as_ubyte(np.zeros([tile_H*Y_size,tile_W*X_size,3]));
	
	for dirname, subdirs, files in os.walk(input_address):
		# print(files)
		s = sorted(files)
		for index,filename in enumerate(s):	
			filepath = os.path.join(dirname, filename)	
			I = io.imread(filepath)
			Itiled[np.int(index/tile_H)*Y_size:(np.int(index/tile_H)+1)*Y_size,(index%tile_W)*X_size:(index%tile_W+1)*X_size,:] = I[y1:y2,x1:x2,:];

	# Write stitched image
	if not os.path.exists(dirVis):
		os.makedirs(dirVis)
	fname = dirVis + "tileImage.png"
	#io.imsave(fname,Itiled);
	cv2.imwrite(fname,Itiled);

	## Measure time
	end1 = time.time()
	print((end1-start1))	

	return [X_size,Y_size,Z, fnamenew, 1, 1];

