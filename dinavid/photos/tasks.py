
import time
from datetime import datetime
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views import View
from .forms import PhotoForm
from .models import Photo, Result, Visual, VisualInput, PreprocessResult
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from dinavid.photos.test_all_pad import *
import os
import zipfile
from django.core.files import File
import shutil
import subprocess
from dinavid.photos.CC2 import CC2
# visualization
from dinavid.photos.tileImages import tileImages
from dinavid.photos.tileImages_input import tileImages_input,tileImages_input_dropdown,tileImages_input_dropdown_nuc, tileImages_input_workflow
from dinavid.photos.writeJSON import writeJSON
from dinavid.photos.copyFiles import copyFiles
# timezone
from pytz import timezone
import cv2

from celery import shared_task

from scipy.ndimage.measurements import label
from scipy import ndimage as ndi
from skimage import util
from skimage.morphology import watershed
from skimage.feature import peak_local_max
import tifffile as tiff

#Used paths
from dinavid.photos.path_names import *

from natsort import natsorted


@shared_task
def celery_process_cellpose(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,watershed_option,seg_dilate,segdilate_se_size,is_preview=False):
 
	results = []
	
	
	if x1 < 0:
		print(input_address)
		print((os.listdir(input_address)[0]))
		v = cv2.imread(os.path.join(input_address,os.listdir(input_address)[0]),0)
		
		x1 = 0
		y1 = 0
		x2 = v.shape[1]
		y2 = v.shape[0]
		
	
	src = DATA_MEDIA_PATH+uname+'/result/'
	subprocess.run(["python", "-m", "cellpose", "--dir", input_address, \
		"--pretrained_model", modelname, "--savedir", src, "--diameter", "0.", "--no_npy", "--save_png" ])

	#May need to rename the files cellpose generated?
	#  %04d.png starting from 0
	for index, file in enumerate(natsorted(os.listdir(src))):
		os.rename(os.path.join(src,file), os.path.join(src, "%04d.png" % index))
	
	
	dst = DATA_MEDIA_PATH + uname + '/watershed_temp/'
	for file in os.listdir(src):
		src_file = os.path.join(src, file)
		dst_file = os.path.join(dst, file)
		shutil.move(src_file,dst_file)
		
	thSmall = int(thSmall)
	segdilate_se_size = int(segdilate_se_size)
	if colorcode =="YES":
		CC2(W,H,Z,uname,thSmall,colorcode,watershed_option,seg_dilate,segdilate_se_size);
		path = DATA_MEDIA_PATH+uname+'/color_coded/'
	else:
		CC2(W,H,Z,uname,thSmall,colorcode,watershed_option,seg_dilate,segdilate_se_size);
		path = DATA_MEDIA_PATH+uname+'/cc_small_removed/'

	visual_name = DATA_MEDIA_PATH+uname+'/visualization/visual_' + datetime_now + '/'

	if not os.path.exists(visual_name):
		os.makedirs(visual_name)
	
	# # Generate tileImages
	tileImages(W,H,Z,visual_name,path);
	# # Copy JSON file with image name and dimension
	writeJSON(W,H,Z,visual_name);
	# # Copy visual3d.js and test.html file
	copyFiles(visual_name);

	if colorcode =="YES":
		if os.path.exists(path + '/binary/'):
			shutil.rmtree(path + '/binary/')
		if not os.path.exists(path + '/binary/')	:
			os.makedirs(path + '/binary/')
		subprocess.call(['cp', '-r', DATA_MEDIA_PATH+uname+'/cc_small_removed/.', path + '/binary'])

		vol = None
		savepath_bin = path + '/binary'
		for dirname, subdirs, files in os.walk(savepath_bin):
			files_sorted= sorted(files)
			for index, filename in enumerate(files_sorted):
				processed_im = cv2.imread(os.path.join(dirname,filename),0)
				if vol is None:
					shape = processed_im.shape
					vol = np.zeros((len(files),shape[0],shape[1]),np.uint16)
				vol[index,:,:]=processed_im
			#Perform a 3D watershed
			distance = ndi.distance_transform_edt(vol)
			local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3,3)),
						labels=vol)
			markers = ndi.label(local_maxi)[0]
			labels = watershed(-distance, markers, mask=vol)
			vol = labels
		
			savepath_3d = os.path.join(path,'three_d_result')
			if not os.path.exists(savepath_3d):
					os.makedirs(savepath_3d)
			tiff.imsave(os.path.join(savepath_3d,'three_d_result.tif'),vol)

	results_path = DATA_MEDIA_PATH+uname+'/results/'
	if not os.path.exists(results_path):
		os.mkdir(results_path)
	zipfilename = os.path.join(results_path,'results_'+ datetime_now +'.zip')
	zf = zipfile.ZipFile(zipfilename, "w")
	
	lenDirPath = len(path)
	for dirname, subdirs, files in os.walk(path):
		for filename in files:
			filepath = os.path.join(dirname, filename)
			zf.write(filepath , filepath[lenDirPath :])
	zf.close()
	
	return results

@shared_task
def celery_process_nisnet(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,watershed_option,seg_dilate,segdilate_se_size,is_preview=False):
 
	results = []
	
	
	if x1 < 0:
		print(input_address)
		print((os.listdir(input_address)[0]))
		v = cv2.imread(os.path.join(input_address,os.listdir(input_address)[0]),0)
		
		x1 = 0
		y1 = 0
		x2 = v.shape[1]
		y2 = v.shape[0]
		
	
	Z,W,H = testonvolume_using_blocks_nisnet(input_address, modelname, uname, x1, x2, y1, y2)
	

	src = DATA_MEDIA_PATH+uname+'/result/'
	dst = DATA_MEDIA_PATH + uname + '/watershed_temp/'
	for file in os.listdir(src):
		src_file = os.path.join(src, file)
		dst_file = os.path.join(dst, file)
		shutil.move(src_file,dst_file)
		
	thSmall = int(thSmall)
	segdilate_se_size = int(segdilate_se_size)
	if colorcode =="YES":
		CC2(W,H,Z,uname,thSmall,colorcode,watershed_option,seg_dilate,segdilate_se_size);
		path = DATA_MEDIA_PATH+uname+'/color_coded/'
	else:
		CC2(W,H,Z,uname,thSmall,colorcode,watershed_option,seg_dilate,segdilate_se_size);
		path = DATA_MEDIA_PATH+uname+'/cc_small_removed/'

	visual_name = DATA_MEDIA_PATH+uname+'/visualization/visual_' + datetime_now + '/'

	if not os.path.exists(visual_name):
		os.makedirs(visual_name)
	
	# # Generate tileImages
	tileImages(W,H,Z,visual_name,path);
	# # Copy JSON file with image name and dimension
	writeJSON(W,H,Z,visual_name);
	# # Copy visual3d.js and test.html file
	copyFiles(visual_name);

	if colorcode =="YES":
		if os.path.exists(path + '/binary/'):
			shutil.rmtree(path + '/binary/')
		if not os.path.exists(path + '/binary/')	:
			os.makedirs(path + '/binary/')
		subprocess.call(['cp', '-r', DATA_MEDIA_PATH+uname+'/cc_small_removed/.', path + '/binary'])

		vol = None
		savepath_bin = path + '/binary'
		for dirname, subdirs, files in os.walk(savepath_bin):
			files_sorted= sorted(files)
			for index, filename in enumerate(files_sorted):
				processed_im = cv2.imread(os.path.join(dirname,filename),0)
				if vol is None:
					shape = processed_im.shape
					vol = np.zeros((len(files),shape[0],shape[1]),np.uint16)
				vol[index,:,:]=processed_im
			#Perform a 3D watershed
			distance = ndi.distance_transform_edt(vol)
			local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3,3)),
						labels=vol)
			markers = ndi.label(local_maxi)[0]
			labels = watershed(-distance, markers, mask=vol)
			vol = labels
		
			savepath_3d = os.path.join(path,'three_d_result')
			if not os.path.exists(savepath_3d):
					os.makedirs(savepath_3d)
			tiff.imsave(os.path.join(savepath_3d,'three_d_result.tif'),vol)

	results_path = DATA_MEDIA_PATH+uname+'/results/'
	if not os.path.exists(results_path):
		os.mkdir(results_path)
	zipfilename = os.path.join(results_path,'results_'+ datetime_now +'.zip')
	zf = zipfile.ZipFile(zipfilename, "w")
	
	lenDirPath = len(path)
	for dirname, subdirs, files in os.walk(path):
		for filename in files:
			filepath = os.path.join(dirname, filename)
			zf.write(filepath , filepath[lenDirPath :])
	zf.close()
	
	return results

@shared_task
def celery_process(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,watershed_option,seg_dilate,segdilate_se_size,is_preview=False):
 
	results = []
	
	
	if x1 < 0:
		print(input_address)
		print((os.listdir(input_address)[0]))
		v = cv2.imread(os.path.join(input_address,os.listdir(input_address)[0]),0)
		
		x1 = 0
		y1 = 0
		x2 = v.shape[1]
		y2 = v.shape[0]
		
	
	Z,W,H = testonvolume(input_address, modelname, uname, x1, x2, y1, y2)
	
	if watershed_option == "YES":
		subprocess.call(['java', '-jar', WATERSHED_FILE_PATH, uname])
	else:
		src = DATA_MEDIA_PATH+uname+'/result/'
		dst = DATA_MEDIA_PATH + uname + '/watershed_temp/'
		for file in os.listdir(src):
			src_file = os.path.join(src, file)
			dst_file = os.path.join(dst, file)
			shutil.move(src_file,dst_file)
		
	thSmall = int(thSmall)
	segdilate_se_size = int(segdilate_se_size)
	if colorcode =="YES":
		CC2(W,H,Z,uname,thSmall,colorcode,watershed_option,seg_dilate,segdilate_se_size);
		path = DATA_MEDIA_PATH+uname+'/color_coded/'
	else:
		CC2(W,H,Z,uname,thSmall,colorcode,watershed_option,seg_dilate,segdilate_se_size);
		path = DATA_MEDIA_PATH+uname+'/cc_small_removed/'

	visual_name = DATA_MEDIA_PATH+uname+'/visualization/visual_' + datetime_now + '/'

	if not os.path.exists(visual_name):
		os.makedirs(visual_name)
	
	# # Generate tileImages
	tileImages(W,H,Z,visual_name,path);
	# # Copy JSON file with image name and dimension
	writeJSON(W,H,Z,visual_name);
	# # Copy visual3d.js and test.html file
	copyFiles(visual_name);

	if colorcode =="YES":
		if os.path.exists(path + '/binary/'):
			shutil.rmtree(path + '/binary/')
		if not os.path.exists(path + '/binary/')	:
			os.makedirs(path + '/binary/')
		subprocess.call(['cp', '-r', DATA_MEDIA_PATH+uname+'/cc_small_removed/.', path + '/binary'])

		vol = None
		savepath_bin = path + '/binary'
		for dirname, subdirs, files in os.walk(savepath_bin):
			files_sorted= sorted(files)
			for index, filename in enumerate(files_sorted):
				processed_im = cv2.imread(os.path.join(dirname,filename),0)
				if vol is None:
					shape = processed_im.shape
					vol = np.zeros((len(files),shape[0],shape[1]),np.uint16)
				vol[index,:,:]=processed_im
			#Perform a 3D watershed
			distance = ndi.distance_transform_edt(vol)
			local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3,3)),
						labels=vol)
			markers = ndi.label(local_maxi)[0]
			labels = watershed(-distance, markers, mask=vol)
			vol = labels
		
			savepath_3d = os.path.join(path,'three_d_result')
			if not os.path.exists(savepath_3d):
					os.makedirs(savepath_3d)
			tiff.imsave(os.path.join(savepath_3d,'three_d_result.tif'),vol)

	results_path = DATA_MEDIA_PATH+uname+'/results/'
	if not os.path.exists(results_path):
		os.mkdir(results_path)
	zipfilename = os.path.join(results_path,'results_'+ datetime_now +'.zip')
	zf = zipfile.ZipFile(zipfilename, "w")
	
	lenDirPath = len(path)
	for dirname, subdirs, files in os.walk(path):
		for filename in files:
			filepath = os.path.join(dirname, filename)
			zf.write(filepath , filepath[lenDirPath :])
	zf.close()
	
	return results

@shared_task
def make_visual_celery(uname,datetime_now,X,Y,Z,dirVis,preprocess_download_path,path,x1,x2,y1,y2,if_resolution=False, \
	factor_downsample=1.0,if_filter=False,k_size=3,sigma=0,if_thresh=False,threshold=20, \
	threshtype="bi",if_Bsubtrac = False,ball_size=30,morph_type=False,se_size=None,se_type=None, prewater = False):
	W=0;H=0;Z=0;
	visual_input_name = dirVis;
	preprocess_download = preprocess_download_path;
	input_address = path;
	W, H, Z, fnamenew = tileImages_input(uname,datetime_now,W,H,Z,visual_input_name,preprocess_download,input_address, x1,x2,y1,y2, \
		if_resolution = if_resolution, factor_downsample =  factor_downsample,if_filter = if_filter,k_size = k_size,sigma = sigma, \
		if_thresh=if_thresh,threshold=threshold,threshtype=threshtype,morph_type=morph_type,se_size=se_size,se_type=se_type, \
		if_Bsubtrac = if_Bsubtrac, ball_size=ball_size, prewater=prewater);
	return W, H, Z, fnamenew
	
	
@shared_task
def make_visual_celery_dropdown(uname,datetime_now, dirVis, preprocess_download_path, path, x1, x2, y1, y2, operation, resolution, k_size,  \
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel):
	W=0;H=0;Z=0;
	visual_input_name = dirVis;
	preprocess_download = preprocess_download_path;
	input_address = path;
	W, H, Z, fnamenew, downsample_W, downsample_H = tileImages_input_dropdown(uname,datetime_now, visual_input_name,preprocess_download,input_address, x1, x2, y1, y2, operation, resolution, k_size,  \
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel);
	return W, H, Z, fnamenew, downsample_W, downsample_H
	
@shared_task
def make_visual_celery_dropdown_nuc(nuc_channel_in,uname,datetime_now, dirVis, preprocess_download_path, path, x1, x2, y1, y2, operation, resolution, k_size,  \
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel,num_z, num_ch, preprocess_orig_to_compare, slice=-1):
	W=0;H=0;Z=0;
	visual_input_name = dirVis;
	preprocess_download = preprocess_download_path;
	input_address = path;
	W, H, Z, fnamenew, downsample_W, downsample_H = tileImages_input_dropdown_nuc(nuc_channel_in,uname,datetime_now, visual_input_name,preprocess_download,input_address, x1, x2, y1, y2, operation, resolution, k_size,  \
		sigma, threshtype, threshold, morph_type, se_type, se_size, ball_size, mediankernel,mediansubkernel,num_z, num_ch, preprocess_orig_to_compare, slice);
	return W, H, Z, fnamenew, downsample_W, downsample_H

@shared_task
def remove_orig_files(uname, path):
	if os.path.exists(path):
		shutil.rmtree(path)
	results = []
	return results


@shared_task
def delete_extra_copy(colorcode,path,uname,zipfilename):
	
	results = []

	os.remove(zipfilename)

	if colorcode =="YES":
		shutil.rmtree(path)

	shutil.rmtree(HOME_MEDIA_PATH+uname+'/result/')
	if os.path.exists(HOME_MEDIA_PATH+uname+'/data/'):	
		shutil.rmtree(HOME_MEDIA_PATH+uname+'/data/')
	if os.path.exists(HOME_MEDIA_PATH+uname+'/preprocess_temp/'):
		shutil.rmtree(HOME_MEDIA_PATH+uname+'/preprocess_temp/')
	shutil.rmtree(HOME_MEDIA_PATH+uname+'/watershed_temp/')
	shutil.rmtree(HOME_MEDIA_PATH+uname+'/cc_small_removed/')
	shutil.rmtree(HOME_MEDIA_PATH+uname+'/visualization/')


	return results

@shared_task
def delete_extra_copy_pre(uname,input_address,visual_input_folder):
	
	results = []

	if os.path.exists(input_address):	
		shutil.rmtree(input_address)

	if os.path.exists(visual_input_folder):	
		shutil.rmtree(visual_input_folder)

	zipfilename_folder = HOME_MEDIA_PATH+uname+'/preprocess_url_storage/'
	if os.path.exists(zipfilename_folder):	
		shutil.rmtree(zipfilename_folder)

	process_folder = HOME_MEDIA_PATH+uname+'/preprocess/'
	if os.path.exists(process_folder):	
		shutil.rmtree(process_folder)
		
		
	return results
	
@shared_task
def clean_up_folder(uname):
	results = []

	folder = HOME_MEDIA_PATH+uname
	for filename in os.listdir(folder):
		file_path = os.path.join(folder, filename)
		try:
			if os.path.isfile(file_path) or os.path.islink(file_path):
				os.unlink(file_path)
			elif os.path.isdir(file_path):
				shutil.rmtree(file_path)
		except Exception as e:
			print(('Failed to delete %s. Reason: %s' % (file_path, e)))
		
		
	return results

@shared_task 
def make_visual_celery_workflow(uname,datetime_now, dirVis, preprocess_download, path, x1, x2, y1, y2):
	visual_input_name = dirVis
	preprocess_download = preprocess_download
	input_address = path
	W, H, Z, fnamenew, downsample_W, downsample_H = tileImages_input_workflow(uname,datetime_now, visual_input_name,preprocess_download,input_address, x1, x2, y1, y2)
	return  W, H, Z, fnamenew, downsample_W, downsample_H 
	
