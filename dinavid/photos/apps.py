

from django.apps import AppConfig


class PhotosConfig(AppConfig):
    name = 'dinavid.photos'
