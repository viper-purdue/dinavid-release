#This file contains some options that can be configured


#Options: 'NO_GPU', 'GPU_SINGLE_MACHINE', 'GPU_MULTI_MACHINE'
#Whether or not to use GPU/Celery to do the backend segmentation. Used in segment_options.py
GPU_OPTION = 'GPU_SINGLE_MACHINE'

#Addresses and account names for subprocess scp'ing in views.py and segment_options.py
#This is not needed for a single-machine setup.
REMOTE_ACCOUNT_NAME = ""
REMOTE_ADDRESS = ""