
import torch
from torch.autograd import Function
from itertools import repeat
import numpy as np
import torch.nn.functional as F
# Intersection = dot(A, B)
# Union = dot(A, A) + dot(B, B)
# The Dice loss function is defined as
# 1/2 * intersection / union
#
# The derivative is 2[(union * target - 2 * intersect * input) / union^2]


def dice_loss(input,target):
    """
    input is a torch variable of size BatchxnclassesxHxW representing log probabilities for each class
    target is a 1-hot representation of the groundtruth, shoud have same size as the input
    """
    # 
    input = input.squeeze(1)
    target = target.squeeze(1)
    # print input.size()
    assert input.size() == target.size(), "Input sizes must be equal."
    assert input.dim() == 4, "Input must be a 4D Tensor."
    uniques=np.unique(target.cpu().data[0].numpy())
    assert set(list(uniques))<=set([0,1]), "target must only contain zeros and ones"

    # probs=F.softmax(input)

    # print(input)
    # print(probs)

    probs = input
    num=probs*target#b,c,h,w--p*g
    num=torch.sum(num,dim=3)#b,c,h
    num=torch.sum(num,dim=2)
    num=torch.sum(num,dim=1)
    

    den1=probs*probs#--p^2
    den1=torch.sum(den1,dim=3)#b,c,h
    den1=torch.sum(den1,dim=2)
    den1=torch.sum(den1,dim=1)
    

    den2=target*target#--g^2
    den2=torch.sum(den2,dim=3)#b,c,h
    den2=torch.sum(den2,dim=2)#b,c
    den2=torch.sum(den2,dim=1)
    

    dice=2*(num/(den1+den2))
    # print dice.size()
    dice_eso=dice#we ignore bg dice val, and take the fg

    dice_total=-1*torch.sum(dice_eso)/dice_eso.size(0)#divide by batch_sz

    return dice_total

