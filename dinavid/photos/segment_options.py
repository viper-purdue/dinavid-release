
import os
import numpy as np
import cv2
import time
from datetime import datetime
from pytz import timezone
import shutil

from scipy.ndimage.measurements import label, center_of_mass
from scipy import ndimage as ndi
from skimage import util
from skimage.morphology import watershed, erosion, dilation, disk, closing, opening
from skimage.feature import peak_local_max
import tifffile as tiff
from scipy.spatial.distance import pdist

from .models import Photo, Result, Visual, VisualInput, PreprocessResult
from .tasks import celery_process, celery_process_nisnet, clean_up_folder, delete_extra_copy, remove_orig_files
import subprocess



import zipfile


#Used paths
from dinavid.photos.path_names import *

from dinavid.photos.dinavid_options import *

#Performs a segmentation that does cellpose
#Since cellpose segments in 2D, do 3D merging like layercake
def segment_cellpose(user,path_to_images,modelname,colorcode,thSmall,use_watershed,x1=-1,x2=-1,y1=-1,y2=-1,has_processed=0,nuc_in_seg=0,num_ch_seg=1):
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	if has_processed==0:
		path = HOME_MEDIA_PATH + uname + '/data/orig'
		preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess/'
		if os.path.exists(preprocess_download):
			shutil.rmtree(preprocess_download)
		if not os.path.exists(preprocess_download):
			os.makedirs(preprocess_download)
		for dirname, subdirs, files in os.walk(path):	
			for index, file in enumerate(sorted(files)[nuc_in_seg:-1:num_ch_seg]):
				shutil.copy(os.path.join(path,file),os.path.join(preprocess_download,file))	
	
	tz = timezone('US/Eastern')
	datetime_now = str(datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S'))

	#Remove files already in the remote GPU server.
	input_address = path_to_images
	
	savepath=HOME_MEDIA_PATH+uname+'/seg_results/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	if not os.path.exists(savepath):
		os.makedirs(savepath)

	copy_address = HOME_MEDIA_PATH + uname + '/'


	if GPU_OPTION == 'NO_GPU':
		#Without celery
		celery_process_cellpose(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
	elif GPU_OPTION == 'GPU_MULTI_MACHINE':
		#Remove files already in the remote GPU server.
		input_address = path_to_images#HOME_MEDIA_PATH + uname + '/preprocess'
		task_remove_orig = remove_orig_files.delay(uname,input_address)
		task_remove_orig.get()
		
		subprocess.call(['scp', '-r', input_address, '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, copy_address)])
		mytask = celery_process_cellpose.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
		
		result_address = DATA_MEDIA_PATH+uname+'/color_coded'
		subprocess.call(['scp', '-r', '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, result_address ), HOME_MEDIA_PATH+uname+'/'])	
		
	elif GPU_OPTION == 'GPU_SINGLE_MACHINE':
		#Using celery
		mytask = celery_process_cellpose.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
	

	if colorcode =="YES":
		path = HOME_MEDIA_PATH+uname+'/color_coded/'
	else:
		path = HOME_MEDIA_PATH+uname+'/cc_small_removed/'


	visual_name = HOME_MEDIA_PATH+uname+'/visualization/visual_' + datetime_now + '/'
	visual_copy = HOME_MEDIA_PATH+uname+'/visualization/'
	if not os.path.exists(visual_name):
		os.makedirs(visual_name)

	zipfilename = HOME_MEDIA_PATH+uname+'/results/results_'+ datetime_now +'.zip' 
		
	if colorcode =="YES":
		shutil.move(HOME_MEDIA_PATH+uname+'/color_coded',savepath)
		savepath = savepath + 'color_coded'
	else:
		shutil.move(HOME_MEDIA_PATH+uname+'/cc_small_removed',savepath)
		savepath = savepath + 'cc_small_removed'	
		
	return (savepath,zipfilename)

#Performs a preview of segmentation does cellpose
#Since cellpose segments in 2D, do 3D merging like layercake
def segment_cellpose_preview(user,path_to_images,modelname,colorcode,thSmall,use_watershed,x1=-1,x2=-1,y1=-1,y2=-1,has_processed=0,nuc_in_seg=0,num_ch_seg=1):
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	if has_processed==0:
		path = HOME_MEDIA_PATH + uname + '/data/orig'
		preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess/'
		if os.path.exists(preprocess_download):
			shutil.rmtree(preprocess_download)
		if not os.path.exists(preprocess_download):
			os.makedirs(preprocess_download)
		for dirname, subdirs, files in os.walk(path):	
			for index, file in enumerate(sorted(files)[nuc_in_seg:-1:num_ch_seg]):
				shutil.copy(os.path.join(path,file),os.path.join(preprocess_download,file))	
	
	tz = timezone('US/Eastern')
	datetime_now = str(datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S'))

	#Remove files already in the remote GPU server.
	input_address = path_to_images
	
	savepath=HOME_MEDIA_PATH+uname+'/seg_results_preview/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	if not os.path.exists(savepath):
		os.makedirs(savepath)

	copy_address = HOME_MEDIA_PATH + uname + '/'


	if GPU_OPTION == 'NO_GPU':
		#Without celery
		celery_process_cellpose(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
	elif GPU_OPTION == 'GPU_MULTI_MACHINE':
		#Remove files already in the remote GPU server.
		input_address = path_to_images#HOME_MEDIA_PATH + uname + '/preprocess'
		task_remove_orig = remove_orig_files.delay(uname,input_address)
		task_remove_orig.get()
		
		subprocess.call(['scp', '-r', input_address, '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, copy_address)])
		mytask = celery_process_cellpose.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
		
		result_address = DATA_MEDIA_PATH+uname+'/color_coded'
		subprocess.call(['scp', '-r', '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, result_address ), HOME_MEDIA_PATH+uname+'/'])	
		
	elif GPU_OPTION == 'GPU_SINGLE_MACHINE':
		#Using celery
		mytask = celery_process_cellpose.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
	

	if colorcode =="YES":
		path = HOME_MEDIA_PATH+uname+'/color_coded/'
	else:
		path = HOME_MEDIA_PATH+uname+'/cc_small_removed/'


	visual_name = HOME_MEDIA_PATH+uname+'/visualization/visual_' + datetime_now + '/'
	visual_copy = HOME_MEDIA_PATH+uname+'/visualization/'
	if not os.path.exists(visual_name):
		os.makedirs(visual_name)

	zipfilename = HOME_MEDIA_PATH+uname+'/results/results_'+ datetime_now +'.zip' 
		
	if colorcode =="YES":
		shutil.move(HOME_MEDIA_PATH+uname+'/color_coded',savepath)
		savepath = savepath + 'color_coded'
	else:
		shutil.move(HOME_MEDIA_PATH+uname+'/cc_small_removed',savepath)
		savepath = savepath + 'cc_small_removed'	
		
	return savepath
	

#Performs a segmentation that first does nisnet
def segment_nisnet(user,path_to_images,modelname,colorcode,thSmall,use_watershed,x1=-1,x2=-1,y1=-1,y2=-1,has_processed=0,nuc_in_seg=0,num_ch_seg=1):
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	if has_processed==0:
		path = HOME_MEDIA_PATH + uname + '/data/orig'
		preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess/'
		if os.path.exists(preprocess_download):
			shutil.rmtree(preprocess_download)
		if not os.path.exists(preprocess_download):
			os.makedirs(preprocess_download)
		for dirname, subdirs, files in os.walk(path):	
			for index, file in enumerate(sorted(files)[nuc_in_seg:-1:num_ch_seg]):
				shutil.copy(os.path.join(path,file),os.path.join(preprocess_download,file))	
	
	tz = timezone('US/Eastern')
	datetime_now = str(datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S'))

	#Remove files already in the remote GPU server.
	input_address = path_to_images
	
	savepath=HOME_MEDIA_PATH+uname+'/seg_results/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	if not os.path.exists(savepath):
		os.makedirs(savepath)

	copy_address = HOME_MEDIA_PATH + uname + '/'


	if GPU_OPTION == 'NO_GPU':
		#Without celery
		celery_process_nisnet(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
	elif GPU_OPTION == 'GPU_MULTI_MACHINE':
		#Remove files already in the remote GPU server.
		input_address = path_to_images#HOME_MEDIA_PATH + uname + '/preprocess'
		task_remove_orig = remove_orig_files.delay(uname,input_address)
		task_remove_orig.get()
		
		subprocess.call(['scp', '-r', input_address, '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, copy_address)])
		mytask = celery_process_nisnet.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
		
		result_address = DATA_MEDIA_PATH+uname+'/color_coded'
		subprocess.call(['scp', '-r', '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, result_address ), HOME_MEDIA_PATH+uname+'/'])	
		
	elif GPU_OPTION == 'GPU_SINGLE_MACHINE':
		#Using celery
		mytask = celery_process_nisnet.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
	

	if colorcode =="YES":
		path = HOME_MEDIA_PATH+uname+'/color_coded/'
	else:
		path = HOME_MEDIA_PATH+uname+'/cc_small_removed/'


	visual_name = HOME_MEDIA_PATH+uname+'/visualization/visual_' + datetime_now + '/'
	visual_copy = HOME_MEDIA_PATH+uname+'/visualization/'
	if not os.path.exists(visual_name):
		os.makedirs(visual_name)

	zipfilename = HOME_MEDIA_PATH+uname+'/results/results_'+ datetime_now +'.zip' 
		
	if colorcode =="YES":
		shutil.move(HOME_MEDIA_PATH+uname+'/color_coded',savepath)
		savepath = savepath + 'color_coded'
	else:
		shutil.move(HOME_MEDIA_PATH+uname+'/cc_small_removed',savepath)
		savepath = savepath + 'cc_small_removed'	
		
	return (savepath,zipfilename)
	
#Performs a preview of segmentation that first does nisnet	
def segment_nisnet_preview(user,path_to_images,modelname,colorcode,thSmall,use_watershed,x1=-1,x2=-1,y1=-1,y2=-1,has_processed=0,nuc_in_seg=0,num_ch_seg=1):
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	if has_processed==0:
		path = HOME_MEDIA_PATH + uname + '/data/orig'
		preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess/'
		if os.path.exists(preprocess_download):
			shutil.rmtree(preprocess_download)
		if not os.path.exists(preprocess_download):
			os.makedirs(preprocess_download)
		for dirname, subdirs, files in os.walk(path):	
			for index, file in enumerate(sorted(files)[nuc_in_seg:-1:num_ch_seg]):
				shutil.copy(os.path.join(path,file),os.path.join(preprocess_download,file))	
	
	tz = timezone('US/Eastern')
	datetime_now = str(datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S'))

	#Remove files already in the remote GPU server.
	input_address = path_to_images
	
	savepath=HOME_MEDIA_PATH+uname+'/seg_results_preview/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	if not os.path.exists(savepath):
		os.makedirs(savepath)

	copy_address = HOME_MEDIA_PATH + uname + '/'


	if GPU_OPTION == 'NO_GPU':
		#Without celery
		celery_process_nisnet(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
	elif GPU_OPTION == 'GPU_MULTI_MACHINE':
		#Remove files already in the remote GPU server.
		input_address = path_to_images#HOME_MEDIA_PATH + uname + '/preprocess'
		task_remove_orig = remove_orig_files.delay(uname,input_address)
		task_remove_orig.get()
		
		subprocess.call(['scp', '-r', input_address, '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, copy_address)])
		mytask = celery_process_nisnet.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
		
		result_address = DATA_MEDIA_PATH+uname+'/color_coded'
		subprocess.call(['scp', '-r', '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, result_address ), HOME_MEDIA_PATH+uname+'/'])	
		
	elif GPU_OPTION == 'GPU_SINGLE_MACHINE':
		#Using celery
		mytask = celery_process_nisnet.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
	

	if colorcode =="YES":
		path = HOME_MEDIA_PATH+uname+'/color_coded/'
	else:
		path = HOME_MEDIA_PATH+uname+'/cc_small_removed/'


	visual_name = HOME_MEDIA_PATH+uname+'/visualization/visual_' + datetime_now + '/'
	visual_copy = HOME_MEDIA_PATH+uname+'/visualization/'
	if not os.path.exists(visual_name):
		os.makedirs(visual_name)

	zipfilename = HOME_MEDIA_PATH+uname+'/results/results_'+ datetime_now +'.zip' 
		
	if colorcode =="YES":
		shutil.move(HOME_MEDIA_PATH+uname+'/color_coded',savepath)
		savepath = savepath + 'color_coded'
	else:
		shutil.move(HOME_MEDIA_PATH+uname+'/cc_small_removed',savepath)
		savepath = savepath + 'cc_small_removed'	
		
	return savepath
	
	
	
#Performs a segmentation that first does deepsynth
def segment_deepsynth(user,path_to_images,modelname,colorcode,thSmall,use_watershed,x1=-1,x2=-1,y1=-1,y2=-1,has_processed=0,nuc_in_seg=0,num_ch_seg=1):
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	if has_processed==0:
		path = HOME_MEDIA_PATH + uname + '/data/orig'
		preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess/'
		if os.path.exists(preprocess_download):
			shutil.rmtree(preprocess_download)
		if not os.path.exists(preprocess_download):
			os.makedirs(preprocess_download)
		for dirname, subdirs, files in os.walk(path):	
			for index, file in enumerate(sorted(files)[nuc_in_seg:-1:num_ch_seg]):
				shutil.copy(os.path.join(path,file),os.path.join(preprocess_download,file))	
	
	tz = timezone('US/Eastern')
	datetime_now = str(datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S'))

	#Remove files already in the remote GPU server.
	input_address = path_to_images
	
	savepath=HOME_MEDIA_PATH+uname+'/seg_results/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	if not os.path.exists(savepath):
		os.makedirs(savepath)

	copy_address = HOME_MEDIA_PATH + uname + '/'


	if GPU_OPTION == 'NO_GPU':
		#Without celery
		celery_process(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
	elif GPU_OPTION == 'GPU_MULTI_MACHINE':
		#Remove files already in the remote GPU server.
		input_address = path_to_images#HOME_MEDIA_PATH + uname + '/preprocess'
		task_remove_orig = remove_orig_files.delay(uname,input_address)
		task_remove_orig.get()
		
		subprocess.call(['scp', '-r', input_address, '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, copy_address)])
		mytask = celery_process.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
		
		result_address = DATA_MEDIA_PATH+uname+'/color_coded'
		subprocess.call(['scp', '-r', '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, result_address ), HOME_MEDIA_PATH+uname+'/'])	
		
	elif GPU_OPTION == 'GPU_SINGLE_MACHINE':
		#Using celery
		mytask = celery_process.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
	

	if colorcode =="YES":
		path = HOME_MEDIA_PATH+uname+'/color_coded/'
	else:
		path = HOME_MEDIA_PATH+uname+'/cc_small_removed/'


	visual_name = HOME_MEDIA_PATH+uname+'/visualization/visual_' + datetime_now + '/'
	visual_copy = HOME_MEDIA_PATH+uname+'/visualization/'
	if not os.path.exists(visual_name):
		os.makedirs(visual_name)

	zipaddress = HOME_MEDIA_PATH+uname+'/results/'
	if not os.path.exists(zipaddress):
		os.makedirs(zipaddress)

	zipfilename = HOME_MEDIA_PATH+uname+'/results/results_'+ datetime_now +'.zip' 
	zf = zipfile.ZipFile(zipfilename, "w")
	
	if colorcode =="YES":
		shutil.move(HOME_MEDIA_PATH+uname+'/color_coded',savepath)
		savepath = savepath + 'color_coded'
	else:
		shutil.move(HOME_MEDIA_PATH+uname+'/cc_small_removed',savepath)
		savepath = savepath + 'cc_small_removed'	
	lenDirPath = len(savepath)	
	for dirname, subdirs, files in os.walk(savepath):
		for filename in files:
			filepath = os.path.join(dirname, filename)
			zf.write(filepath , filepath[lenDirPath :])
	zf.close()	
		
	return (savepath,zipfilename)
	
#Performs a preview of segmentation that first does deepsynth	
def segment_deepsynth_preview(user,path_to_images,modelname,colorcode,thSmall,use_watershed,x1=-1,x2=-1,y1=-1,y2=-1,has_processed=0,nuc_in_seg=0,num_ch_seg=1):
	
	
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	if has_processed==0:
		path = HOME_MEDIA_PATH + uname + '/data/orig'
		preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess/'
		if os.path.exists(preprocess_download):
			shutil.rmtree(preprocess_download)
		if not os.path.exists(preprocess_download):
			os.makedirs(preprocess_download)
		for dirname, subdirs, files in os.walk(path):	
			for index, file in enumerate(sorted(files)[nuc_in_seg:-1:num_ch_seg]):
				shutil.copy(os.path.join(path,file),os.path.join(preprocess_download,file))	
	
	
	#celeryprocess(request,modelname,colorcode,x1,x2,y1,y2,thSmall,watershed,dataselect) is below
	tz = timezone('US/Eastern')
	datetime_now = str(datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S'))

	
	#Remove files already in the remote GPU server.
	input_address = path_to_images#HOME_MEDIA_PATH + uname + '/preprocess'
	
	savepath=HOME_MEDIA_PATH+uname+'/seg_results_preview/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	if not os.path.exists(savepath):
		os.makedirs(savepath)

	
	#To add the choice for dilate the result
	if GPU_OPTION == 'NO_GPU':
		#Without celery
		celery_process(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0,is_preview=True)
	elif GPU_OPTION == 'GPU_MULTI_MACHINE':
		#Remove files already in the remote GPU server.
		input_address = path_to_images
		copy_address = HOME_MEDIA_PATH + uname + '/preprocess_preview/'
		
		task_remove_orig = remove_orig_files.delay(uname,copy_address)
		
		task_remove_orig.get()
		
		##copy all files in user folder to remote machine
		subprocess.call(['scp', '-r', input_address, '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, copy_address)])

		#To add the choice for dilate the result
		mytask = celery_process.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0)
		mytask.get()
		
		result_address = DATA_MEDIA_PATH+uname+'/color_coded'
		subprocess.call(['scp', '-r', '{}@{}:{}'.format(REMOTE_ACCOUNT_NAME, REMOTE_ADDRESS, result_address ), HOME_MEDIA_PATH+uname+'/'])	
		
	elif GPU_OPTION == 'GPU_SINGLE_MACHINE':
		#Using celery
		mytask = celery_process.delay(input_address,modelname,uname,x1,x2,y1,y2,colorcode,datetime_now,thSmall,use_watershed,"NO",0,is_preview=True)
		mytask.get()

	if colorcode =="YES":
		savepath = savepath + 'color_coded'
		shutil.move(HOME_MEDIA_PATH+uname+'/color_coded',savepath)
	else:
		savepath = savepath + 'cc_small_removed'	
		shutil.move(HOME_MEDIA_PATH+uname+'/cc_small_removed',savepath)
			
	return savepath	
	

#Performs a segmentation that first does a threshold and then does a watershed.
def segment_3d_watershed(user,path_to_images,use_otsu,seg_thresh_val,has_processed=0,nuc_in_seg=0,num_ch_seg=1):

	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	if has_processed==0:
		path = HOME_MEDIA_PATH + uname + '/data/orig'
		preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess/'
		if os.path.exists(preprocess_download):
			shutil.rmtree(preprocess_download)
		if not os.path.exists(preprocess_download):
			os.makedirs(preprocess_download)
		for dirname, subdirs, files in os.walk(path):	
			for index, file in enumerate(sorted(files)[nuc_in_seg:-1:num_ch_seg]):
				shutil.copy(os.path.join(path,file),os.path.join(preprocess_download,file))

	threshold = np.int(seg_thresh_val)
	#Determine whether to use otsu
	if use_otsu == True or str(use_otsu).lower() == "true":
		thresh_flag = cv2.THRESH_BINARY+cv2.THRESH_OTSU
		threshold=0
	else:
		thresh_flag = cv2.THRESH_BINARY
		
	savepath=HOME_MEDIA_PATH+uname+'/seg_results/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	if not os.path.exists(savepath):
		os.makedirs(savepath)
		
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')	
	savepath=os.path.join(savepath,'seg_' + datetime_now + '_3d_watershed')
	if not os.path.exists(savepath):
		os.makedirs(savepath)
		
	vol = None
		
	#Walk through each image
	for dirname, subdirs, files in os.walk(path_to_images):
		files_sorted= sorted(files)
		for index, filename in enumerate(files_sorted):
			processed_im = cv2.imread(os.path.join(dirname,filename),0)
			shape = processed_im.shape
			if vol is None:
				vol = np.zeros((len(files),shape[0],shape[1]),np.uint16)
			retval,img = cv2.threshold(processed_im,threshold,255,thresh_flag) 
			
			vol[index,:,:]=img
			
		#Perform a 3D watershed
		distance = ndi.distance_transform_edt(vol)
		local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3,3)),
					labels=vol)
		markers = ndi.label(local_maxi)[0]
		labels = watershed(-distance, markers, mask=vol)
		vol = labels
		
		
		#save to individual slices for visualization,
		for zz in range(0,vol.shape[0]):
			cv2.imwrite(os.path.join(savepath,files_sorted[zz]),vol[zz,:,:])
		savepath_3d = os.path.join(savepath,'three_d_result')
		if not os.path.exists(savepath_3d):
			os.makedirs(savepath_3d)
		tiff.imsave(os.path.join(savepath_3d,'three_d_result.tif'),vol)
		
		#Color Coding
		## Colormap - Read file from cmap.txt
		savepath = savepath + '/color_coded'
		if not os.path.exists(savepath):
			os.mkdir(savepath)
		cmap = []
		thColormap = 50
		ins = open(CMAP_PATH, "r")
		for line in ins:
			line = line.strip().split("\t")
			line2 = [float(n) for n in line];
			line3 = [int(line2[0]), int(line2[1]), int(line2[2])]
			cmap.append(line3)

		ins.close()

		cmap2 = []

		num_colors = 0
		# Dark color removal from colormap
		for i in range(0,len(cmap)):
			if cmap[i][0] > thColormap or cmap[i][1] > thColormap or cmap[i][2] > thColormap:
				cmap2.append(cmap[i])
				num_colors = num_colors + 1

		print("colormap done")
		Z = vol.shape[0]
		Y = vol.shape[1]
		X = vol.shape[2]
		bw3 = util.img_as_ubyte(np.zeros([Z,Y,X,3]))
		
		for ii in range(0,Z):	
			for jj in range(0,Y):
				for kk in range(0,X):
					if vol[ii,jj,kk] != 0:
						bw3[ii,jj,kk,:] = cmap2[(vol[ii,jj,kk]-1)%num_colors]
						#bw3[ii,jj,kk,0] = cmap2[(vol[ii,jj,kk]-1)%num_colors][0]
						#bw3[ii,jj,kk,1] = cmap2[(vol[ii,jj,kk]-1)%num_colors][1]
						#bw3[ii,jj,kk,2] = cmap2[(vol[ii,jj,kk]-1)%num_colors][2]
			cv2.imwrite(os.path.join(savepath,"z%04d.png"%ii),bw3[ii,:,:,:])	

	zipfilename = None
	zipaddress = HOME_MEDIA_PATH+uname+'/results/'
	if not os.path.exists(zipaddress):
		os.makedirs(zipaddress)

	zipfilename = HOME_MEDIA_PATH+uname+'/results/results_'+ datetime_now +'.zip' 
	zf = zipfile.ZipFile(zipfilename, "w")

	preprocess_download_path=os.path.join(HOME_MEDIA_PATH+uname+'/seg_results/','seg_' + datetime_now + '_3d_watershed')
	lenDirPath = len(preprocess_download_path)
	for dirname, subdirs, files in os.walk(preprocess_download_path):
		for filename in files:
			filepath = os.path.join(dirname, filename)
			zf.write(filepath , filepath[lenDirPath :])
	zf.close()

	return savepath
	
#Performs a preview of the processing steps
def segment_3d_watershed_preview(user,path_to_images,use_otsu,seg_thresh_val,x1=0,x2=0,y1=0,y2=0,has_processed=0,nuc_in_seg=0,num_ch_seg=1):
	
	uname = user.username 
	if not uname  or uname.isspace():
		uname = 'default'
	if has_processed==0:
		pass

	threshold = np.int(seg_thresh_val)
	#Determine whether to use otsu
	if use_otsu == True or str(use_otsu).lower() == "true":
		thresh_flag = cv2.THRESH_BINARY+cv2.THRESH_OTSU
		threshold=0
	else:
		thresh_flag = cv2.THRESH_BINARY
		
	savepath=HOME_MEDIA_PATH+uname+'/seg_results_preview/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	if not os.path.exists(savepath):
		os.makedirs(savepath)
		
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')	
	savepath=os.path.join(savepath,'seg_' + datetime_now + '_3d_watershed')
	if not os.path.exists(savepath):
		os.makedirs(savepath)
		
	vol = None
		
	
	for dirname, subdirs, files in os.walk(path_to_images):
		files_sorted= sorted(files)
		#Walk through each image from the preprocessing side
		for index, filename in enumerate(files_sorted):
			processed_im = cv2.imread(os.path.join(dirname,filename),0)
			shape = processed_im.shape
			y_sub = int(shape[0])
			x_sub = int(shape[1])
			if vol is None:
				vol = np.zeros((len(files),y_sub,x_sub),np.uint16)
			retval,img = cv2.threshold(processed_im,threshold,255,thresh_flag) 
			
			vol[index,:,:]=img
			
		#Perform a 3D watershed
		distance = ndi.distance_transform_edt(vol)
		local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3,3,3)),
					labels=vol)
		markers = ndi.label(local_maxi)[0]
		labels = watershed(-distance, markers, mask=vol)
		vol = labels
		#Color Coding
		cmap = []
		thColormap = 50
		ins = open(CMAP_PATH, "r")
		for line in ins:
			line = line.strip().split("\t")
			line2 = [float(n) for n in line];
			line3 = [int(line2[0]), int(line2[1]), int(line2[2])]
			cmap.append(line3)

		ins.close()

		cmap2 = []

		num_colors = 0
		# Dark color removal from colormap
		for i in range(0,len(cmap)):
			if cmap[i][0] > thColormap or cmap[i][1] > thColormap or cmap[i][2] > thColormap:
				cmap2.append(cmap[i])
				num_colors = num_colors + 1

		
		print("colormap done")
		Z = vol.shape[0]
		Y = vol.shape[1]
		X = vol.shape[2]
		bw3 = util.img_as_ubyte(np.zeros([Z,Y,X,3]))
		
		for ii in range(0,Z):	
			for jj in range(0,Y):
				for kk in range(0,X):
					if vol[ii,jj,kk] != 0:
						bw3[ii,jj,kk,:] = cmap2[(vol[ii,jj,kk]-1)%num_colors]
						#bw3[ii,jj,kk,0] = cmap2[(vol[ii,jj,kk]-1)%num_colors][0]
						#bw3[ii,jj,kk,1] = cmap2[(vol[ii,jj,kk]-1)%num_colors][1]
						#bw3[ii,jj,kk,2] = cmap2[(vol[ii,jj,kk]-1)%num_colors][2]
			cv2.imwrite(os.path.join(savepath,"z%04d.png"%ii),bw3[ii,:,:,:])
		
	savepath_3d = os.path.join(savepath,'three_d_result')
	if not os.path.exists(savepath_3d):
		os.makedirs(savepath_3d)
	tiff.imsave(os.path.join(savepath_3d,'three_d_result.tif'),vol)
		
	return savepath
	
# The optimized layercake method
def segment_layercake(user,path_to_images,use_otsu,seg_thresh_val,has_processed=0,nuc_in_seg=0,num_ch_seg=1,radius=2,morph_radius=1,morph_option="none"):
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	if has_processed==0:
		path = HOME_MEDIA_PATH + uname + '/data/orig'
		preprocess_download = HOME_MEDIA_PATH+uname+'/preprocess/'
		if os.path.exists(preprocess_download):
			shutil.rmtree(preprocess_download)
		if not os.path.exists(preprocess_download):
			os.makedirs(preprocess_download)
		for dirname, subdirs, files in os.walk(path):	
			for index, file in enumerate(sorted(files)[nuc_in_seg:-1:num_ch_seg]):
				shutil.copy(os.path.join(path,file),os.path.join(preprocess_download,file))
	
	savepath=HOME_MEDIA_PATH+uname+'/seg_results/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	if not os.path.exists(savepath):
		os.makedirs(savepath)
	
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')	
	savepath=os.path.join(savepath,'seg_' + datetime_now + '_layercake')
	if not os.path.exists(savepath):
		os.makedirs(savepath)
	
	vol = None
	threshold = np.int(seg_thresh_val)
	if use_otsu == True or str(use_otsu).lower() == "true":
		thresh_flag = cv2.THRESH_BINARY+cv2.THRESH_OTSU
		threshold=0
	else:
		thresh_flag = cv2.THRESH_BINARY
	cum_labels=0
	cents = []
	mapped = {}
	mapped[0]=0
	#Walk through each image
	for dirname, subdirs, files in os.walk(path_to_images):
		files_sorted= sorted(files)
		for index, filename in enumerate(files_sorted):
			processed_im = cv2.imread(os.path.join(dirname,filename),0)
			shape = processed_im.shape
			if vol is None:
				vol = np.zeros((len(files),shape[0],shape[1]),np.uint32)
			retval,img = cv2.threshold(processed_im,threshold,255,thresh_flag)
			
			#Morphology
			if morph_option == "dilation":
				img = dilation(img,disk(morph_radius))
			elif morph_option == "closing":
				img = closing(img,disk(morph_radius))
			elif morph_option == "erosion":
				img = erosion(img,disk(morph_radius))
			elif morph_option == "opening":
				img = opening(img,disk(morph_radius))
			
			#Perform a 2D watershed
			distance = ndi.distance_transform_edt(img)
			local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3,3)),
						labels=img)
			markers = ndi.label(local_maxi)[0]
			labels = watershed(-distance, markers, mask=img)
			max_label = np.max(labels)
			
			#Assign unique label to each disk in each focal plane
			labels[labels!=0] += cum_labels
			cum_labels += max_label
			vol[index,:,:] = labels
			
			#Create map of connected components
			centers = center_of_mass(
				labels > 0, labels, np.delete(np.unique(labels), 0))
			if len(centers) != 0:
				cents.append(np.concatenate(
					[np.full((len(centers), 1), index), np.array(centers)], axis=1))

		#Calculate centroids
		cents = np.concatenate(cents, axis=0)
		mapped = np.array([np.arange(1, len(cents)+1),np.arange(1,len(cents)+1)]).transpose()
		for jj, (label_a, label_b) in enumerate(mapped):
			slice_num = int(cents[jj,0])
			num_before = len(np.where(cents[:,0]<=slice_num)[0])
			# get next slice coords
			all_coords = cents[np.where(cents[:,0]==slice_num+1)[0],:]
			center_coords = cents[label_a-1,1:]
			dist_vector = np.sum((all_coords[:,1:]-center_coords)**2, axis=1)<=radius**2
			mapped[np.where(dist_vector==True)[0]+num_before,1]=label_b		# print(cents)


		#Apply mapped values to the volume
		mapped = dict(list(zip(mapped[:,0], mapped[:,1])))
		mapped[0] = 0
		vol=np.vectorize(mapped.get)(vol)
	
		
		
			
		#save to individual slices for visualization,
		for zz in range(0,vol.shape[0]):
			cv2.imwrite(os.path.join(savepath,files_sorted[zz]),vol[zz,:,:])
			
		
			
		savepath_3d = os.path.join(savepath,'three_d_result')
		if not os.path.exists(savepath_3d):
			os.makedirs(savepath_3d)
		tiff.imsave(os.path.join(savepath_3d,'three_d_result.tif'),vol)	
		
		#Color Coding
		savepath = savepath + '/color_coded'
		if not os.path.exists(savepath):
			os.mkdir(savepath)
		cmap = []
		thColormap = 50
		ins = open(CMAP_PATH, "r")
		for line in ins:
			line = line.strip().split("\t")
			line2 = [float(n) for n in line];
			line3 = [int(line2[0]), int(line2[1]), int(line2[2])]
			cmap.append(line3)

		ins.close()

		cmap2 = []

		num_colors = 0
		# Dark color removal from colormap
		for i in range(0,len(cmap)):
			if cmap[i][0] > thColormap or cmap[i][1] > thColormap or cmap[i][2] > thColormap:
				cmap2.append(cmap[i])
				num_colors = num_colors + 1

		print("colormap done")
		Z = vol.shape[0]
		Y = vol.shape[1]
		X = vol.shape[2]
		bw3 = util.img_as_ubyte(np.zeros([Z,Y,X,3]))
		
		for ii in range(0,Z):	
			for jj in range(0,Y):
				for kk in range(0,X):
					if vol[ii,jj,kk] != 0:
						bw3[ii,jj,kk,:] = cmap2[(vol[ii,jj,kk]-1)%num_colors]
			cv2.imwrite(os.path.join(savepath,"z%04d.png"%ii),bw3[ii,:,:,:])
	
	zipfilename = None
	zipaddress = HOME_MEDIA_PATH+uname+'/results/'
	if not os.path.exists(zipaddress):
		os.makedirs(zipaddress)

	zipfilename = zipfilename = HOME_MEDIA_PATH+uname+'/results/results_'+ datetime_now +'.zip' 
	zf = zipfile.ZipFile(zipfilename, "w")

	preprocess_download_path=os.path.join(HOME_MEDIA_PATH+uname+'/seg_results/','seg_' + datetime_now + '_layercake')
	lenDirPath = len(preprocess_download_path)
	for dirname, subdirs, files in os.walk(preprocess_download_path):
		for filename in files:
			filepath = os.path.join(dirname, filename)
			zf.write(filepath , filepath[lenDirPath :])
	zf.close()
	
	
	
	return (savepath, zipfilename)


# The optimzed layercake method
def segment_layercake_preview(user,path_to_images,use_otsu,seg_thresh_val,has_processed=0,nuc_in_seg=0,num_ch_seg=1,radius=2,morph_radius=1,morph_option="none"):
	uname = user.username
	if not uname  or uname.isspace():
		uname = 'default'
	if has_processed==0:
		pass
	savepath=HOME_MEDIA_PATH+uname+'/seg_results_preview/'
	if os.path.exists(savepath):
		shutil.rmtree(savepath)
	if not os.path.exists(savepath):
		os.makedirs(savepath)
		
	tz = timezone('US/Eastern')
	datetime_now = datetime.now(tz).strftime('%Y-%m-%d-%H-%M-%S')	
	savepath=os.path.join(savepath,'seg_' + datetime_now + '_layercake')
	if not os.path.exists(savepath):
		os.makedirs(savepath)
	
	vol = None
	threshold = np.int(seg_thresh_val)
	if use_otsu == True or str(use_otsu).lower() == "true":
		thresh_flag = cv2.THRESH_BINARY+cv2.THRESH_OTSU
		threshold=0
	else:
		thresh_flag = cv2.THRESH_BINARY
	cum_labels=0
	cents = []
	mapped = {}
	mapped[0]=0
	#Walk through each image
	for dirname, subdirs, files in os.walk(path_to_images):
		files_sorted= sorted(files)
		for index, filename in enumerate(files_sorted):
			processed_im = cv2.imread(os.path.join(dirname,filename),0)
			shape = processed_im.shape
			if vol is None:
				vol = np.zeros((len(files),shape[0],shape[1]),np.uint32)
			retval,img = cv2.threshold(processed_im,threshold,255,thresh_flag)
			
			#Morphology
			if morph_option == "dilation":
				img = dilation(img,disk(morph_radius))
			elif morph_option == "closing":
				img = closing(img,disk(morph_radius))
			elif morph_option == "erosion":
				img = erosion(img,disk(morph_radius))
			elif morph_option == "opening":
				img = opening(img,disk(morph_radius))
			
			#Perform a 2D watershed
			distance = ndi.distance_transform_edt(img)
			local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3,3)),
						labels=img)
			markers = ndi.label(local_maxi)[0]
			labels = watershed(-distance, markers, mask=img)
			max_label = np.max(labels)
			
			#Assign unique label to each disk in each focal plane
			labels[labels!=0] += cum_labels
			cum_labels += max_label
			vol[index,:,:] = labels
			
			#Create map of connected components
			centers = center_of_mass(
				labels > 0, labels, np.delete(np.unique(labels), 0))
			if len(centers) != 0:
				cents.append(np.concatenate(
					[np.full((len(centers), 1), index), np.array(centers)], axis=1))
		#the actual layercake here. join if within some radius.
		
		#Calculate centroids
		cents = np.concatenate(cents, axis=0)
		mapped = np.array([np.arange(1, len(cents)+1),np.arange(1,len(cents)+1)]).transpose()
		for jj, (label_a, label_b) in enumerate(mapped):
			slice_num = int(cents[jj,0])
			num_before = len(np.where(cents[:,0]<=slice_num)[0])
			# get next slice coords
			all_coords = cents[np.where(cents[:,0]==slice_num+1)[0],:]
			center_coords = cents[label_a-1,1:]
			dist_vector = np.sum((all_coords[:,1:]-center_coords)**2, axis=1)<=radius**2
			mapped[np.where(dist_vector==True)[0]+num_before,1]=label_b		
	
					
		mapped = dict(list(zip(mapped[:,0], mapped[:,1])))
		mapped[0] = 0
			#Apply mapped values to the volume
		vol=np.vectorize(mapped.get)(vol)
		
		
		#Color Coding
		## Colormap - Read text files saved from MATLAB
		if not os.path.exists(savepath):
			os.mkdir(savepath)
		savepath_3d = os.path.join(savepath,'three_d_result')
		if not os.path.exists(savepath_3d):
			os.makedirs(savepath_3d)
		tiff.imsave(os.path.join(savepath_3d,'three_d_result.tif'),vol)
		cmap = []
		thColormap = 50
		ins = open(CMAP_PATH, "r")
		for line in ins:
			line = line.strip().split("\t")
			line2 = [float(n) for n in line];
			line3 = [int(line2[0]), int(line2[1]), int(line2[2])]
			cmap.append(line3)

		ins.close()

		cmap2 = []

		num_colors = 0
		# Dark color removal from colormap
		for i in range(0,len(cmap)):
			if cmap[i][0] > thColormap or cmap[i][1] > thColormap or cmap[i][2] > thColormap:
				cmap2.append(cmap[i])
				num_colors = num_colors + 1

		print("colormap done")
		Z = vol.shape[0]
		Y = vol.shape[1]
		X = vol.shape[2]
		bw3 = util.img_as_ubyte(np.zeros([Z,Y,X,3]))
		
		for ii in range(0,Z):	
			for jj in range(0,Y):
				for kk in range(0,X):
					if vol[ii,jj,kk] != 0:
						bw3[ii,jj,kk,:] = cmap2[(vol[ii,jj,kk]-1)%num_colors]
						#bw3[ii,jj,kk,0] = cmap2[(vol[ii,jj,kk]-1)%num_colors][0]
						#bw3[ii,jj,kk,1] = cmap2[(vol[ii,jj,kk]-1)%num_colors][1]
						#bw3[ii,jj,kk,2] = cmap2[(vol[ii,jj,kk]-1)%num_colors][2]
			cv2.imwrite(os.path.join(savepath,"z%04d.png"%ii),bw3[ii,:,:,:])
	return savepath
	
#These functions are used for pdist
def calc_row_idx(k, n):
    return int(math.ceil((1/2.) * (- (-8*k + 4 *n**2 -4*n - 7)**0.5 + 2*n -1) - 1))

def elem_in_i_rows(i, n):
    return i * (n - 1 - i) + (i*(i + 1))//2

def calc_col_idx(k, i, n):
    return int(n - elem_in_i_rows(i + 1, n) + k)

def condensed_to_square(k, n):
    i = calc_row_idx(k, n)
    j = calc_col_idx(k, i, n)
    return i, j
