
import os

import numpy as np
import cv2
import time
from datetime import datetime
from pytz import timezone
import shutil

from scipy.ndimage.measurements import label
from scipy import ndimage as ndi
from skimage.morphology import watershed
from skimage.feature import peak_local_max
import tifffile as tiff

from .models import Photo, Result, Visual, VisualInput, PreprocessResult
from .tasks import celery_process
import subprocess

import glob
import skimage
from skimage import measure
from skimage.morphology import remove_small_objects
from skimage.morphology import watershed
from skimage.morphology import binary_dilation
import csv
import json
import time
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
plt.switch_backend('agg')

from dinavid.photos.path_names import *


def local_dialation(points,diala_wt,max_x,max_y,max_z):

    a = points
    minx,miny,minz = np.min(points,axis=0)
    normal_points = a-np.min(points,axis=0)
    len_x,len_y,len_z = np.max(normal_points,axis=0)+1

	#x_offset
    x_off_bf = min(minx,diala_wt)
    x_off_af = min(max_x - len_x - minx,diala_wt)
    y_off_bf = min(miny,diala_wt)
    y_off_af = min(max_y - len_y - miny,diala_wt)
    z_off_bf = min(minz,diala_wt)
    z_off_af = min(max_z - len_z - minx,diala_wt)
	
    orig_matrix = np.zeros((len_x+x_off_bf+x_off_af,len_y+y_off_bf+y_off_af,len_z+z_off_bf+z_off_af))
    for i in normal_points:
        orig_matrix[i[0]+ x_off_bf][i[1]+y_off_bf][i[2]+z_off_bf] = 1

    out_matrix = binary_dilation(orig_matrix,np.ones((diala_wt,diala_wt,diala_wt)))
    
    mask_matrix = out_matrix - orig_matrix

    idx_x,idx_y,idx_z = np.where(mask_matrix == 1)
    idx_x = idx_x+minx - x_off_bf
    idx_y = idx_y+miny - y_off_bf
    idx_z = idx_z+minz - z_off_bf
    idx_x = idx_x.reshape(1,len(idx_x))
    idx_y = idx_y.reshape(1,len(idx_y))
    idx_z = idx_z.reshape(1,len(idx_z))
    resul_mask = np.concatenate((idx_x,idx_y,idx_z),axis = 0)
    return resul_mask.transpose(),points

def scatter_plot(request,uname,diala_wt,if_scatter = False, xmin = 0,xmax = 0,ymin = 0,ymax = 0,x_disp_type = 0,x_disp_chan = 0 ,y_disp_type = 0,y_disp_chan = 1, gated_xmin=0, gated_ymin=0, gated_xmax=0, gated_ymax=0, is_preview=False, im_x0=-1,im_x1=-1,im_y0=-1,im_y1=-1):
	
	uname = request.user.username
	if not uname  or uname.isspace():
		uname = 'default'
	ch_color = request.GET.getlist('ch_color[]')
	ch_gamma = request.GET.getlist('ch_gamma[]')
	ch_brightness = request.GET.getlist('ch_brightness[]')
	ch_offset = request.GET.getlist('ch_offset[]')
	dis_ch = request.GET.getlist('dis_ch[]')
	#note diala_wt in string
	diala_wt = int(diala_wt)




	min_x = xmin; max_x = xmax; min_y = ymin; max_y = ymax
	if not if_scatter:
		xmin = 0;xmax = 0;ymin = 0;ymax = 0

	

	if is_preview:
		mask_folders = sorted(glob.glob(HOME_MEDIA_PATH+uname+'/seg_results_preview/*'))
	else:	
		mask_folders = sorted(glob.glob(HOME_MEDIA_PATH+uname+'/seg_results/*'))
	mask_folders = mask_folders[-1]
	mask_folder_address = mask_folders+'/**/*.tif'
	
	mask_name = glob.glob(mask_folder_address)
	orig_img_address = HOME_MEDIA_PATH+uname+'/data/orig/*.png'
	ori_img_name = sorted(glob.glob(orig_img_address))
	if len(ori_img_name)==0:
		orig_img_address = HOME_MEDIA_PATH+uname+'/data/orig/*.tif'
		ori_img_name = sorted(glob.glob(orig_img_address))	
	if len(ori_img_name)==0:
		orig_img_address = HOME_MEDIA_PATH+uname+'/data/orig/*.tiff'
		ori_img_name = sorted(glob.glob(orig_img_address))	

	scatter_plot_data_dir = HOME_MEDIA_PATH+uname+'/scatter_plt_data'
	if not os.path.exists(scatter_plot_data_dir):
		os.makedirs(scatter_plot_data_dir)
	csv_file_path = os.path.join(scatter_plot_data_dir,'centroids_tuple.csv')
	cur_time = time.time()
	scatter_plot_dir_path = os.path.join(scatter_plot_data_dir,'xyplot/')
	if not os.path.exists(scatter_plot_dir_path):
		os.mkdir(scatter_plot_dir_path)
	else:
		shutil.rmtree(scatter_plot_dir_path)
		os.mkdir(scatter_plot_dir_path)	
	scatter_plot_path = os.path.join(scatter_plot_dir_path,'scatter_plot'+str(cur_time)+'.png')


	
	#put all original image into a 3D stack 
	colorfiles = os.listdir(HOME_MEDIA_PATH+uname+'/data/colorstacks')
	slices = len(colorfiles)
	channels = int(len(ori_img_name)/slices)


	if im_x0 == -1:
		labels_imgj = tiff.imread(mask_name[0])
	else:
		labels_imgj = tiff.imread(mask_name[0])
		aaa = labels_imgj.shape
	
	orig_img_stack = []
	
	for i in range(0,slices):
		per_slice_channel_stack = []#stacks of channel images for each slice 
		for j in range(0,channels):
			cur_img_name =  ori_img_name[i*channels+j]
			if im_x0 == -1:
				tmp_img = skimage.io.imread(cur_img_name)
			else:
				tmp_img = skimage.io.imread(cur_img_name)[im_y0:im_y1,im_x0:im_x1]
			per_slice_channel_stack.append(tmp_img)
		slice_img_multi_channel = np.stack(per_slice_channel_stack)
		orig_img_stack.append(slice_img_multi_channel)
	aq_test_human = np.stack(orig_img_stack)


	
	max_label = np.max(labels_imgj)
	centroid_3D_list = []
	centroid_label = []
	props = measure.regionprops(labels_imgj)

	#This gets you the centroid
	#(np.average(props[max_label].coords,0))

	#Reassign labels
	for prop in props:
		try:
			if prop.area > 20:
				centroid_3D_list.append(np.average(prop.coords,0))
				centroid_label.append(prop.label)
		except:
			break
			
	centroid_3D_list_int = np.uint16(centroid_3D_list)	


	#Determine shape of image, and which is field the channel field and which is the slice field
	shape = aq_test_human.shape
	if len(shape)==4:
		slices = shape[0]
		channels = shape[1]
		height = shape[2]
		width = shape[3]
		if shape[3] == 3:
			channels = shape[3]
			height = shape[1]
			width = shape[2]
			
			
	print((slices, channels, height, width))
	nuclear_channel = 0

	#How many pixels to extend beyond each nucleus to define
	ext = diala_wt

	#Calculate the statistics for each channel
	max_list = []
	min_list = []
	sum_list = []
	count_list = []
	mean_list = []
	std_list = []
	pos_list = []
	centroid_index_gated_cell = []
	idx = 0

	#Export CSV
	with open(csv_file_path , 'w') as csvfile:
		#Header for the CSV file
		csvwriter = csv.writer(csvfile, delimiter=',',lineterminator='\n') 
		header_text = [['Object','PosZ','PosY','PosX'], ['Mean_ch'+str(x) for x in range(1,channels + 1)], \
			['SD_ch'+str(x) for x in range(1,channels + 1)], ['Sum_ch'+str(x) for x in range(1,channels + 1)], \
			['Count_ch'+str(x) for x in range(1,channels + 1)], ['Max_ch'+str(x) for x in range(1,channels + 1)], \
			['Min_ch'+str(x) for x in range(1,channels + 1)] ]
		flattened_header = [val for sublist in header_text for val in sublist]
		csvwriter.writerow(flattened_header)
		for index, centroid in enumerate(centroid_3D_list):
			label = centroid_label[index]
			centroid_surround_points,centroid_points =  local_dialation(props[index].coords,ext,slices,height,width)
			centroid_surround_points_tuple = tuple(map(tuple,centroid_surround_points.T))
			posZ = centroid[0]
			posY = centroid[1]
			posX = centroid[2]
			if posY >= int(gated_ymin) and posY <= int(gated_ymax) and posX >= int(gated_xmin) and posX <= int(gated_xmax):	# if the centroid is in the selected region
				centroid_index_gated_cell.append(index)
			max_val = np.zeros(channels)
			min_val = np.zeros(channels)    
			sum_val = np.zeros(channels)
			count_val = np.zeros(channels)
			mean_val = np.zeros(channels)
			st_dev_val = np.zeros(channels)
			
			#Statistics needed for each channel
			for ch_num in range(channels):
				if shape[3] == 3:
					result_mask = aq_test_human[:,:,:,ch_num][centroid_surround_points_tuple]
				else:
					result_mask = aq_test_human[:,ch_num,:,:][centroid_surround_points_tuple]
				
				result_mask_nonzero = result_mask[result_mask!=0]
				if result_mask_nonzero.any():
					max_val[ch_num] = np.max(result_mask_nonzero)
					min_val[ch_num] = np.min(result_mask_nonzero)
					sum_val[ch_num] = np.sum(result_mask_nonzero)
					count_val[ch_num] = len(result_mask_nonzero)
					mean_val[ch_num] = sum_val[ch_num] / count_val[ch_num]
					st_dev_val[ch_num] = result_mask_nonzero.std()
				else:#No results found
					max_val[ch_num] = 0
					min_val[ch_num] = 0
					sum_val[ch_num] = 0
					count_val[ch_num] = 0
					mean_val[ch_num] = 0
					st_dev_val[ch_num] = 0

			#These are only used for plotting.
			pos_list.append(centroid)
			max_list.append(max_val)
			min_list.append(min_val)
			sum_list.append(sum_val)
			count_list.append(count_val)
			mean_list.append(mean_val)
			std_list.append(st_dev_val)

			#Flatten the values into a row
			values_list = [[index,posZ,posY,posX],mean_val,st_dev_val,sum_val,count_val,max_val,min_val]
			flattened_values = [val for sublist in values_list for val in sublist]
			csvwriter.writerow(flattened_values)


	
	
	
	#choose disp channel and type 
	#control command in disp_ctr_list [x_disp_type, y_disp_type, x_disp_chan, y_disp_chan ]
	x_disp_type = int(x_disp_type); y_disp_type = int(y_disp_type)
	x_disp_chan = int(x_disp_chan); y_disp_chan = int(y_disp_chan)
	if x_disp_chan < 0:
		x_disp_chan = 0
	elif x_disp_chan >= channels:
		x_disp_chan = channels -1
		
	if y_disp_chan < 0:
		y_disp_chan = 0
	elif y_disp_chan >= channels:
		y_disp_chan = channels -1
	#type control flg: 0-mean 1-min 2-max 3-sum 4-std 5-count 6-pos
	disp_type_list = [mean_list,min_list,max_list,sum_list,std_list,count_list]#,pos_list]
	x_disp_list = disp_type_list[x_disp_type]
	y_disp_list = disp_type_list[y_disp_type]

	user_bound = False
	


	pltx_unbox = []
	plty_unbox = []
	pltx_box = []
	plty_box = []
	boxed_idx = []
	unboxed_idx = []

	total_x = []
	total_y = []
	if gated_xmin==0 and gated_ymin==0 and gated_xmax==0 and gated_ymax==0:
		for i in np.arange(len(pos_list)):
			#modify to change display channel
			tmp_x = x_disp_list[i][x_disp_chan]
			tmp_y = y_disp_list[i][y_disp_chan]

			if (tmp_x >= min_x) and (tmp_x <= max_x):
				if (tmp_y >= min_y) and (tmp_y <= max_y):
					pltx_box.append(tmp_x)
					plty_box.append(tmp_y)
					boxed_idx.append(centroid_label[i])
				else:
					pltx_unbox.append(tmp_x)
					plty_unbox.append(tmp_y)
					unboxed_idx.append(centroid_label[i])
			else:
				pltx_unbox.append(tmp_x)
				plty_unbox.append(tmp_y)
				unboxed_idx.append(centroid_label[i])
			total_x.append(tmp_x)
			total_y.append(tmp_y)
	else:
		for i in np.arange(len(pos_list)):
			#modify to change display channel
			tmp_x = x_disp_list[i][x_disp_chan]
			tmp_y = y_disp_list[i][y_disp_chan]
			if i in centroid_index_gated_cell:
				pltx_box.append(tmp_x)
				plty_box.append(tmp_y)
				boxed_idx.append(centroid_label[i])
			else:
				pltx_unbox.append(tmp_x)
				plty_unbox.append(tmp_y)
				unboxed_idx.append(centroid_label[i])

			total_x.append(tmp_x)
			total_y.append(tmp_y)

	#Plot data
	px_lb = 0; px_hb = np.max(total_x); py_lb = 0; py_hb = np.max(total_y)
	
	fig = plt.figure(num = None,figsize = (5.12,5.12),dpi = 100)
	if user_bound:
		plt.xlim(px_lb,px_hb)
		plt.ylim(py_lb,py_hb)
		plt.scatter(pltx_unbox,plty_unbox,s=3,c = 'b')
		plt.scatter(pltx_box,plty_box,s=3,c = 'r')	
	else:
		plt.scatter(pltx_unbox,plty_unbox,s=3,c = 'b')
		plt.scatter(pltx_box,plty_box,s=3,c = 'r')

	disp_type_str = ['Mean', 'Min', 'Max', 'Sum', 'Standard Deviation', 'Count']
	# # Load the user's channel name
	
	
	ch_path = HOME_MEDIA_PATH + 'share_folder/ch_name/ch_name.txt'
	ch_names = None
	im_name = request.GET['im_name']
	if os.path.exists(ch_path):
		with open(ch_path, 'r') as f:
			ch_names = json.load(f)
	ch_name = ch_names[im_name]
	plt.xlabel(ch_name[int(x_disp_chan)] + ' ' + disp_type_str[x_disp_type])
	plt.ylabel(ch_name[int(y_disp_chan)] + ' ' + disp_type_str[y_disp_type])

	ax = plt.gca()
	fig.canvas.draw()
	data = np.fromstring(fig.canvas.tostring_rgb(),dtype=np.uint8,sep='')
	data = data.reshape((fig.canvas.get_width_height()[0],fig.canvas.get_width_height()[1],3))
	scatter_plot = data
	cv2.imwrite(scatter_plot_path,cv2.cvtColor(scatter_plot, cv2.COLOR_RGB2BGR))


	plot_height = data.shape[0]
	plot_width 	= data.shape[1]
	top_left_coor 		= (64, 61)
	top_right_coor 		= (461, 61)
	bottom_left_coor 	= (64, 456)
	bottom_right_coor 	= (461, 456)
	plot_width_pxl 	= bottom_right_coor[0] - top_left_coor[0]
	plot_height_pxl = bottom_right_coor[1] - top_left_coor[1]
	
	x_lim = ax.get_xlim()
	y_lim = ax.get_ylim()
	plot_width 	= x_lim[1]-x_lim[0]
	plot_height = y_lim[1]-y_lim[0]
	pixel_per_unit_X = plot_width_pxl/plot_width
	pixel_per_unit_Y = plot_height_pxl/plot_height
	pixel_per_unit = (pixel_per_unit_X, pixel_per_unit_Y)
	


	plot_info = {'plot_height': plot_height, 'plot_width': plot_width, 
					'top_left_coor':top_left_coor, 'bottom_right_coor': bottom_right_coor}
	scatter_plot_shape = data.shape	#(H, W)
	scatter_plot_coor = (top_left_coor[0], top_left_coor[1], bottom_right_coor[0], bottom_right_coor[1])	# (X1,Y1,X2,Y2)
	# X is the scatter plot in array

	outfig = np.copy(aq_test_human)

	shape = aq_test_human.shape
	props_len = len(props)

	max_plt_val = np.ones(shape[1])*np.max(aq_test_human)
	max_plt_val2 = np.ones(shape[3])*np.max(aq_test_human)
	if if_scatter:
		out_nuclei_idx = boxed_idx
	else:
		out_nuclei_idx = []

	for i in out_nuclei_idx:  # now unboxd,need to change to boxed
		centroid_idx = centroid_label.index(i)
		coord_cur = props[centroid_idx].coords
		
		for j in coord_cur:
			if shape[3] == 3:
				outfig[j[0]][j[1]][j[2]][:] = max_plt_val2
			else:
				outfig[j[0],:,j[1],j[2]] = max_plt_val

	chosen_nucli_disp_path = os.path.join(scatter_plot_data_dir,'nuclei_on_ori_img/')
	
	if not os.path.exists(chosen_nucli_disp_path):
		os.mkdir(chosen_nucli_disp_path)
	else:
		shutil.rmtree(chosen_nucli_disp_path)
		os.mkdir(chosen_nucli_disp_path)
	
	stack = outfig
	shape = stack.shape
	
	base_name =  'nuclei_on_img'
	
	if len(shape)==4:
		slices=shape[0]
		channels=shape[1]
		height = shape[2]
		width = shape[3]
		
		if shape[3] == 3:
			channels=shape[3]
			height = shape[1]
			width = shape[2]
		
		max_intensity=float(np.max(stack))
		#Create png files bc only png files can be displayed on the web.
		for sl in range(0,slices):
			#color
			
			#If 3 channel, the channel number will be the last axis.
			first_slice_8bit = np.uint8(stack[sl,:,:,:] / max_intensity * 255.0)
			if shape[3] == 3:
				first_slice_8bit = np.transpose(first_slice_8bit, (2,0,1))			
				
			color_list = []
			#Hex string to tuple.
			for channel in range(0,channels):
				h = (ch_color[channel]).lstrip('#')
				color_list.append(tuple(int(h[i:i+2], 16) for i in (0, 2, 4)))	
			aaaaaaaa = first_slice_8bit.shape	
			color_stack = np.zeros((height, width,3),np.uint8)
			comp_stack = np.zeros((channels,height, width),np.uint8)
			for channel in range(channels):
				if dis_ch[channel] == "1":
					comp_stack[channel,:,:] = np.uint8(255.0*np.clip((np.power(first_slice_8bit[channel,:,:]/255.0,np.double(ch_gamma[channel]))*np.double(ch_brightness[channel])+np.double(ch_offset[channel])),0,1))	
					
			for ch in range(channels):
				for co in range(3):
					color_stack[:,:,co] = np.maximum(color_stack[:,:,co], comp_stack[ch,:,:]/255.0*color_list[ch][2-co] )
						
			color_name = chosen_nucli_disp_path+base_name+'_z%04d_color.png' % (sl)
			cv2.imwrite(color_name,color_stack)
	x_disp_len = len(x_disp_list)	
	centroid_3D_list_len = len(centroid_3D_list)
	return chosen_nucli_disp_path, csv_file_path, scatter_plot_path, channels, scatter_plot_shape, scatter_plot_coor, pixel_per_unit, x_lim, y_lim
