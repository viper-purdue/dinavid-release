
# -*- coding: utf-8 -*-
from shutil import copyfile
# import os
# import io
from dinavid.photos.path_names import *


# Define data
#data = {'a list': [1, 42, 3.141, 1337, 'help', u'€'],
#        'a string': 'bla',
#        'another dict': {'foo': 'bar',
#                         'key': 'value',
#                         'the answer': 42}}

def copyFiles_input(dirVis):
	dirIn = VIZ_INPUT_PATH;
	dirOut = dirVis;
	fname1 = "visual3d.js";
	fname2 = "test.html";

	copyfile(dirIn + fname1, dirOut + fname1);
	copyfile(dirIn + fname2, dirOut + fname2);
