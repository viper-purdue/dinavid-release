
import sys

from dinavid.photos.model import *
import numpy as np
import argparse
from dinavid.photos.dataloader import *
import torch.utils.data
import os
import torchvision.utils as v_utils
import scipy.misc
import torch.nn.functional as F
import math
import cv2
import shutil
from tqdm import tqdm

from dinavid.photos.path_names import *
from dinavid.photos.dinavid_options import *
import tifffile as tiff

sys.path.append(MODELS_HOME_PATH)
#Used for nisnet
#from dinavid.photos.watershed3d import watershed3d
import torch.nn as nn

from dinavid.photos.nisnet.models import nisnet
from dinavid.photos.nisnet.options.test_options import TestOptions
from skimage.morphology import erosion, ball, dilation, cube
from dinavid.photos.nisnet.utils.mean_average_precision import MeanAveragePrecisionIoU
from dinavid.photos.nisnet.utils.instance_seg import instance_seg, small_obj_removal
from dinavid.photos.nisnet.dataset.dataloaders import CustomizedDatasetTest
from dinavid.photos.nisnet.utils.overlay_masks import apply_mask3d
from dinavid.photos.nisnet.dataset.data_utils import reshape


def create_data_loader_nisnet(opt, num_workers, shuffle=True):
    # inference only support batch size = 1
    
    full_dataset = CustomizedDatasetTest(opt)
    test_loader = torch.utils.data.DataLoader(full_dataset, 
                                                batch_size=1, 
                                                shuffle=shuffle, 
                                                num_workers=num_workers,
                                                drop_last=False)
    return test_loader


def load_model_nisnet(filename, model):
    with open(filename, "rb") as f:
        if GPU_OPTION == 'NO_GPU':
            state_dict = torch.load(model_name,map_location=torch.device('cpu'))
        elif GPU_OPTION == 'GPU_MULTI_MACHINE':
            state_dict = torch.load(model_name)
        elif GPU_OPTION == 'GPU_SINGLE_MACHINE':
            state_dict = torch.load(model_name)
    model.load_state_dict(state_dict)
    
    return model


def load_model_nisent(filename, model):
    print(f"Load snapshot from: {os.path.abspath(filename)}")
    with open(filename, "rb") as f:
        state_dict = torch.load(f)
    model.load_state_dict(state_dict)
    return model

def create_nisnet_loss(opt):
    if opt.loss_func == 'FocalTverskyLoss':
        loss_func = [nisnet.MSELoss(), nisnet.TverskyLoss(alpha=0.3, beta=0.7), nisnet.FocalLoss()]
    elif opt.loss_func == 'BCEDiceLoss':
        loss_func = [nisnet.MSELoss(), nisnet.DiceLoss(), nn.BCELoss()]
    else:
        raise NameError('loss name is incorrect')
    return loss_func


def test_using_blocks_nisnet(set_loader, model, loss, opt):
    print(f"Predicting {len(set_loader)} files with loss {type(loss)}")
    with torch.no_grad():
        model.eval()
        model.to(opt.dev)
        idx = 0

        result_images = []
        result_centroids = []
        for sample in tqdm(set_loader):
            orig_img, inputs, targets = sample['orig_img'], sample['image'], sample['target']
            # inference only support batch size = 1
            block=opt.block_size
            _, _, size_h, size_w, size_z = orig_img.shape
            w_p = int(math.ceil(size_w/float(block/2))*(block/2))
            h_p = int(math.ceil(size_h/float(block/2))*(block/2))
            z_p = int(math.ceil(size_z/float(block/2))*(block/2))

            padz = z_p-size_z
            padh = h_p-size_h
            padw = w_p-size_w
            print("x: " + str(h_p) + ", y: " + str(w_p) + ", z: "+ str(z_p))
            input_numpy = np.transpose(np.squeeze(inputs.data[0].cpu().numpy()), (2,0,1))
            input_padded = np.pad(input_numpy, ((0, padz), (0, padh), (0, padw)), 'symmetric')
            input_inference = np.pad(input_padded, ((int(block/4),int(block/4)), (int(block/4), int(block/4)), (int(block/4), int(block/4))), 'symmetric')
            
            inputs_sub = np.zeros([1,1,block,block,block])
            output = np.zeros([5,z_p,h_p,w_p])  # 5 channels
            for kk in range(0,int(w_p/(block/2))):
                for jj in range(0,int(h_p/(block/2))):
                    for ii in range(0,int(z_p/(block/2))):
                        # print(int(block/2)*jj, int(block/2)*(jj+2))
                        inputs_sub[0,0,:,:,:] = input_inference[int(block/2)*ii:int(block/2)*(ii+2),int(block/2)*jj:int(block/2)*(jj+2),int(block/2)*kk:int(block/2)*(kk+2)]
                        inputs_sub = np.transpose(inputs_sub, (0,1,3,4,2))
                        inputs_sub = torch.from_numpy(inputs_sub).float()
                        inputs_sub = inputs_sub.to(opt.dev)
                        output_sub = model(inputs_sub)
                        output_sub = np.squeeze(output_sub.data.cpu().numpy())
                        output_sub = np.transpose(output_sub, (0,3,1,2))
                        output[:, int(block/2)*ii:int(block/2)*(ii+1),int(block/2)*jj:int(block/2)*(jj+1),int(block/2)*kk:int(block/2)*(kk+1)] = output_sub[:,int(block/4):int(3*block/4),int(block/4):int(3*block/4),int(block/4):int(3*block/4)]
                        # print('output:', (int(block/2)*jj, int(block/2)*(jj+1)), (int(block/2)*jj, int(block/2)*(jj+1)))
                        # print('output_sub:', (int(block/4),int(3*block/4)), (int(block/4), int(3*block/4)))
                        inputs_sub = inputs_sub.data.cpu().numpy()
                        inputs_sub = np.transpose(inputs_sub, (0,1,4,2,3))

            output = np.transpose(output[:,0:size_z,0:size_h,0:size_w].astype(np.float32), (0,2,3,1))
            decoded = [nisnet.decode(output, None, opt.binning, opt.nm_size, opt.ctr_threshold)]
            # Add all numpy arrays to a list
            result_images.extend([{"img_orig": o.cpu().numpy(),
                                   "inputs": i.cpu().numpy(),
                                   "targets": t.cpu().numpy(),
                                   "vectors": d[0],
                                   "votes": d[1],
                                   "class_ids": d[2],
                                   "class_probs": d[3],
                                   "centroids": d[4]} for o, i, t, d in zip(orig_img, inputs, targets, decoded)])

            # Add image_id to centroid locations and add to list
            result_centroids.extend([np.stack(ctr for ctr in d[5]) for d in decoded])
            idx = idx + 1
    return result_images, result_centroids

def output_nisnet(save_dir, result_images, result_centroids,opt):

    print('--------------- Instance segmentation using marker-controlled 3D watershed -----------')
    
    for i, sample in enumerate(result_images):
        orig_img = np.uint8(np.transpose(sample['img_orig'][0], (2,0,1)))
        seg_mask = np.transpose(sample['class_ids'][0], (2,0,1))
        if opt.morph<0:
            morph = erosion(seg_mask, ball(abs(opt.morph)))
        elif opt.morph>0:
            morph = dilation(seg_mask, ball(abs(opt.morph)))
        else:
            morph = np.squeeze(seg_mask)
        vector_vol = np.transpose(sample['vectors'], (3,1,2,0))

        gray_seg, color_seg = instance_seg(vector_vol, morph, save_dir, opt)

        print(color_seg.shape)
        (size_z, size_h, size_w,color) = color_seg.shape

        for i in range(size_z): 
            cv2.imwrite(os.path.join(save_dir, f"{str(i+1).zfill(4)}.png"), gray_seg[i,:,:])
    return size_z, size_w, size_h


def testonvolume_using_blocks_nisnet(dataroot, name, username,x1,x2,y1,y2):


    #Create 3D version
    slice_names = sorted(os.listdir(dataroot))
    num_slices = len(slice_names)
    volume_in_3d = None
    
    for index, s in enumerate(slice_names):
        im = cv2.imread(os.path.join(dataroot,s),0)
        print( im.shape)
        if volume_in_3d is None:
            volume_in_3d = np.zeros((num_slices,im.shape[0],im.shape[1]),np.uint8)
        volume_in_3d[index,:,:] = im
    
    dataroot3d = DATA_MEDIA_PATH + username + "/nisnet_3d_to_process/syn/"
    
    #Remove old directory
    if os.path.exists(dataroot3d):
        shutil.rmtree(dataroot3d)
    #Make the directory
    if not os.path.exists(dataroot3d):
        os.makedirs(dataroot3d, exist_ok = True )
    
    #Transpose because nisnet needs order in X,Y,Z
    volume_in_3d = np.transpose(volume_in_3d, (1,2,0))
    print(volume_in_3d.shape)
    tiff.imwrite(os.path.join(dataroot3d, "nisnet_3d_result.tiff"), volume_in_3d)

    save_dir = DATA_MEDIA_PATH + username + "/result/" 
    model_name = CHECKPOINT_PATH + "nisnet/NISNet3D_V1-V4.pth"
    
    
    if os.path.exists(save_dir):
        shutil.rmtree(save_dir)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    
    opt, unknown = TestOptions().parse()
    
    dataroot3d = DATA_MEDIA_PATH + username + "/nisnet_3d_to_process/"
    #Set options
    opt.data_dir=dataroot3d
    opt.model_dir=model_name
    opt.mode = "test"
    opt.backbone = "resAttUNet"
    opt.block_size=128 
    opt.eval= 0 
    opt.k_val= 1 
    opt.condition_erode= 0 
    opt.small_obj_vox= 20
    opt.n_workers = 0
    
    # Load datasets
    test_loader = create_data_loader_nisnet(opt, num_workers=opt.n_workers, shuffle=False)

    # Create loss function
    loss = create_nisnet_loss(opt)
    # Load model
    model = nisnet.NISNet(opt)
    model = nn.DataParallel(model)
    model = load_model_nisent(opt.model_dir, model)
    
    result_images, result_centroids = test_using_blocks_nisnet(test_loader, model, loss, opt) 
      
    (size_z, size_h, size_w) = output_nisnet(save_dir, result_images, result_centroids,opt)
    
    
    #for i, sample in enumerate(result_images):
    #    volume = watershed3d(sample['class_ids'][0], sample['centroids'][0])
    #    volume = np.transpose(volume, (2,0,1))
    
    #size_z, size_h, size_w = volume.shape
    #print(volume.shape)
    #for i in range(size_z): 
    #    cv2.imwrite(os.path.join(save_dir, f"{str(i+1).zfill(4)}.png"), volume[i,:,:])
    
    
    return size_z, size_h, size_w
 



 
def testonvolume(dataroot, name, username,x1,x2,y1,y2):


    batchsize = 1
    img_dir = dataroot
    save_dir = DATA_MEDIA_PATH + username + "/result/" 
    model_name = CHECKPOINT_PATH + name + "/unet.pkl"
    block = 64

    if os.path.exists(save_dir):
        shutil.rmtree(save_dir)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    if os.path.exists(DATA_MEDIA_PATH + username + '/watershed_temp/'):
        shutil.rmtree(DATA_MEDIA_PATH + username + '/watershed_temp/')
    if not os.path.exists(DATA_MEDIA_PATH + username + '/watershed_temp/'):
        os.makedirs(DATA_MEDIA_PATH + username + '/watershed_temp/')


    x1 = int(x1)
    x2 = int(x2)
    y1 = int(y1)
    y2 = int(y2)

    dataset = testDataset_all(dataroot, x1,x2,y1,y2)
    img_batch = torch.utils.data.DataLoader(dataset,batch_size=batchsize,shuffle=False, num_workers=0)

    #Using GPU
    #unet = torch.load(model_name)
    #No GPU
    #unet = torch.load(model_name,map_location=torch.device('cpu'))
    if GPU_OPTION == 'NO_GPU':
        unet = torch.load(model_name,map_location=torch.device('cpu'))
    elif GPU_OPTION == 'GPU_MULTI_MACHINE':
        unet = torch.load(model_name)
    elif GPU_OPTION == 'GPU_SINGLE_MACHINE':
        unet = torch.load(model_name)
    
    if isinstance(unet,torch.nn.DataParallel):
        unet = unet.module
    size_z, size_h, size_w = dataset.getsize()

    b2 = int(block/2)
    w_p = int(math.ceil(size_w/float(b2))*(b2))
    h_p = int(math.ceil(size_h/float(b2))*(b2))
    z_p = int(math.ceil(size_z/float(b2))*(b2))
    
    print(w_p,h_p,z_p)

    padz = z_p-size_z
    padh = h_p-size_h
    padw = w_p-size_w

    print(("x: " + str(h_p) + ", y: " + str(w_p) + ", z: "+ str(z_p)))


    for i, data in enumerate(img_batch):

            inputs = Variable(data)#.cuda()


            input_numpy = inputs.data[0].cpu().numpy()
            input_padded = np.pad(input_numpy, ((0, padz), (0, padh), (0, padw)), 'reflect')
            b4 = int(block/4)
            input_inference = np.pad(input_padded, ((b4, b4), (b4, b4), (b4, b4)), 'reflect')


            inputs_sub = np.zeros([1,1,block,block,block])

            output = np.zeros([z_p,h_p,w_p])

            b2 = int(block/2)
            b4 = int(block/4)
            b34 = int(3*block/4)
            for kk in range(0,int(w_p/(b2))):

                for jj in range(0,int(h_p/(b2))):

                    for ii in range(0,int(z_p/(b2))):

                        inputs_sub[0,0,:,:,:] = input_inference[(b2)*ii:(b2)*(ii+2),(b2)*jj:(b2)*(jj+2),(b2)*kk:(b2)*(kk+2)]

                        inputs_sub = torch.from_numpy(inputs_sub).float()

                        if GPU_OPTION == 'GPU_MULTI_MACHINE':
                            inputs_sub = inputs_sub.cuda()
                        elif GPU_OPTION == 'GPU_SINGLE_MACHINE':
                            inputs_sub = inputs_sub.cuda()
                            
                        inputs_sub = Variable(inputs_sub)

                        output_sub = unet(inputs_sub)

                        output_sub = torch.squeeze(output_sub)
                        
                        output_sub = output_sub.data.cpu().numpy()
                        output_sub[output_sub > 0.5] = 1
                        output_sub[output_sub <= 0.5] = 0

                        output[(b2)*ii:(b2)*(ii+1),(b2)*jj:(b2)*(jj+1),(b2)*kk:(b2)*(kk+1)] = output_sub[b4:b34,b4:b34,b4:b34]

                   
                        inputs_sub = inputs_sub.data.cpu().numpy()

    for i in range(size_z):
        cv2.imwrite(save_dir + "%04d" %(i+1) + '.png', output[i,0:size_h,0:size_w])
    return size_z, size_w, size_h

            
