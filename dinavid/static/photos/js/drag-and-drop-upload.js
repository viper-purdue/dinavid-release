$(function () {

  $(".js-upload-photos").click(function () {
    $("#fileupload").click();
  });

  $("#fileupload").fileupload({
    dataType: 'json',
    beforeSend : function(e,data) {
       $("#upload_box_text").append(
          "<h2>Uploading Images</h2>"
        );
    },
    
    done: function (e, data) {
      if (data.result.is_valid) {
        $("#gallery tbody").prepend(
          "<tr><td><a href='" + data.result.url + "'>" + data.result.name + "</a></td></tr>"
        );
        $("#upload_box_text").append(
          "<h2>Preparing Preview</h2>"
        );
        top.location.reload(true);
      }
    }
  });

});
