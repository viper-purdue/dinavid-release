load_ready = 1; // Global Variable for Progress Bar
function showProgressBar_update_channel_color_slice(){
  var i = 0;
  if (i == 0){
    i = 1;
    var width = 0;
    var elem = document.getElementById("progress_update_channel_color_slice");
    elem.value = "0";
    elem.style.visibility = 'visible';	
    var id = setInterval(frame, 10);
      function frame() {
        if (width >= 100) {
          clearInterval(id);
          i = 0;
          // hide progress bar
          elem.style.visibility = 'hidden';
        } else {
          width+=10;
      lem.value = width;     
        }
      }
    
  }
}
function showProgressBar_gen_3D_visualization(){
  var i = 0;
  if (i == 0){
    i = 1;
    var width = 0;
    var elem = document.getElementById("progress_gen_3D_visualization");
    elem.value = "0";
    // elem.style.visibility = 'visible';
    document.getElementById("progressDiv2").style.visibility = 'visible';
    load_ready = 0;
    var id = setInterval(frame, 100);
      function frame() {
        if (width >= 100) {
          if(load_ready==1){
            clearInterval(id);
            i = 0;
            // hide progress bar
            // elem.style.visibility = 'hidden';
            document.getElementById("progressDiv2").style.visibility = 'hidden';
          }
        } else {
          width+=4;
          elem.value = width;
          if(load_ready==1){
            elem.value = 100;
            document.getElementById("progressDiv2").style.visibility = 'hidden';
            load_ready = 0;
            // window.alert(load_ready);
          }
        }
      }
  }
}
function showProgressBar_workflow_processing(){
  var i = 0;
  if (i == 0){
    i = 1;
    var width = 0;
    var elem = document.getElementById("progress_workflow_processing");
    elem.value = "0";
    // elem.style.visibility = 'visible';
    document.getElementById("progressDiv3").style.visibility = 'visible';
    load_ready = 0;
    var id = setInterval(frame, 100);
      function frame() {
        if (width >= 100) {
          if(load_ready==1){
            clearInterval(id);
            i = 0;
            // hide progress bar
            // elem.style.visibility = 'hidden';
            document.getElementById("progressDiv3").style.visibility = 'hidden';
          }
        } else {
          width+=2;
          elem.value = width;
          if(load_ready==1){
            elem.value = 100;
            document.getElementById("progressDiv3").style.visibility = 'hidden';
            load_ready = 0;
            // window.alert(load_ready);
          }
        }
      }
  }
} 
function showProgressBar_workflow_seg(){
  var i = 0;
  if (i == 0){
    i = 1;
    var width = 0;
    var elem = document.getElementById("progress_workflow_seg");
    elem.value = "0";
    // elem.style.visibility = 'visible';
    document.getElementById("progressDiv4").style.visibility = 'visible';
    load_ready = 0;
    var id = setInterval(frame, 100);
      function frame() {
        if (width >= 100) {
          if(load_ready==1){
            clearInterval(id);
            i = 0;
            // hide progress bar
            // elem.style.visibility = 'hidden';
            document.getElementById("progressDiv4").style.visibility = 'hidden';
          }
        } else {
          width+=2;
          elem.value = width;
          if(load_ready==1){
            elem.value = 100;
            document.getElementById("progressDiv4").style.visibility = 'hidden';
            load_ready = 0;
            // window.alert(load_ready);
          }
        }
      }
  }
}
function showProgressBar_workflow_quantification(){
  var i = 0;
  if (i == 0){
    i = 1;
    var width = 0;
    var elem = document.getElementById("progress_workflow_quantification");
    elem.value = "0";
    // elem.style.visibility = 'visible';
    document.getElementById("progressDiv5").style.visibility = 'visible';
    load_ready = 0;
    var id = setInterval(frame, 100);
      function frame() {
        if (width >= 100) {
          if(load_ready==1){
            clearInterval(id);
            i = 0;
            // hide progress bar
            // elem.style.visibility = 'hidden';
            document.getElementById("progressDiv5").style.visibility = 'hidden';
          }
        } else {
          width+=2;
          elem.value = width;
          if(load_ready==1){
            elem.value = 100;
            document.getElementById("progressDiv5").style.visibility = 'hidden';
            load_ready = 0;
            // window.alert(load_ready);
          }
        }
      }
  }
}

function showProgressBar_update_workflow_quantification(){
  var i = 0;
  if (i == 0){
    i = 1;
    var width = 0;
    var elem = document.getElementById("progress_update_workflow_quantification");
    elem.value = "0";
    // elem.style.visibility = 'visible';
    document.getElementById("progressDiv6").style.visibility = 'visible';
    load_ready = 0;
    var id = setInterval(frame, 100);
      function frame() {
        if (width >= 100) {
          if(load_ready==1){
            clearInterval(id);
            i = 0;
            // hide progress bar
            // elem.style.visibility = 'hidden';
            document.getElementById("progressDiv6").style.visibility = 'hidden';
          }
        } else {
          width+=2;
          elem.value = width;
          if(load_ready==1){
            elem.value = 100;
            document.getElementById("progressDiv6").style.visibility = 'hidden';
            load_ready = 0;
            // window.alert(load_ready);
          }
        }
      }
  }
}