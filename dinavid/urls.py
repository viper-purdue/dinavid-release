from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.urls import path
from django.views.generic import TemplateView
from django.contrib import admin
from django.contrib.auth import views as auth_views


urlpatterns = [
    path(r'', TemplateView.as_view(template_name='home.html'), name='home'),
    path(r'photos/', include('dinavid.photos.urls')),
    path('admin/', admin.site.urls),
    path(r'login/', auth_views.LoginView.as_view(redirect_authenticated_user=True), {'template_name': 'login.html'}, name='login'),
    path(r'logout/', auth_views.LogoutView.as_view(), {'template_name': 'logged_out.html'}, name='logout'),
	path(r'contact/', TemplateView.as_view(template_name='contact.html'), name='contact'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
