import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dinavid.settings')

app = Celery('dinavid',broker='pyamqp://blackconn:pholack@128.46.69.160/photonhost',backend='rpc://')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.update(
    CELERYD_MAX_TASKS_PER_CHILD = 1
)
