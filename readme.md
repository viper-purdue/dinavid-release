<!---
#This is the source code package for the DINAIVD system.

#This software is covered by US patents and copyright.
#This source code is to be used for academic research purposes only, and no commercial use is allowed.
-->

*****
Term of Use and License

Version 2.0

September 12, 2023


This work was partially supported by a George M. O’Brien Award from the National Institutes of Health 
under grant NIH/NIDDK P30 DK079312 and the endowment of the Charles William Harrison Distinguished 
Professorship at Purdue University.

Copyright and Intellectual Property

The software/code is distributed under Creative Commons license
Attribution-NonCommercial-ShareAlike - CC BY-NC-SA

You are free to:
* Share — copy and redistribute the material in any medium or format
* Adapt — remix, transform, and build upon the material
* The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:
* Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use. (See below for paper citation)
* NonCommercial — You may not use the material for commercial purposes.
* ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
* No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

For more information see:
https://creativecommons.org/licenses/by-nc-sa/4.0/


The data is distributed under Creative Commons license
Attribution-NonCommercial-NoDerivs - CC BY-NC-ND

You are free to:
* Share — copy and redistribute the material in any medium or format
* The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:
* Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* NonCommercial — You may not use the material for commercial purposes.
* NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
* No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

For More Information see:
https://creativecommons.org/licenses/by-nc-nd/4.0/



Attribution

In any publications you produce based on using our software/code or data we ask that you cite the following paper  
S\. Han, A. Chen, S. Lee, C. Fu, C. Yang, L. Wu, S. Winfree, T.M. El-Achkar, K. W. Dunn, P. Salama, and E. J. Delp, "DINAVID: A Distributed and Networked Image Analysis System for Volumetric Image Data," Bioinformatics, BMC, May 2022

Privacy Statement

We are committed to the protection of personal privacy and have adopted a policy to  protect information about individuals. 
When using our software/source code we do not collect any information about you or your use of the code.

How to Contact Us

The source code/software and data is distributed "as is." 
We do not accept any responsibility for the operation of the software.
We can only provided limited help in running our code. 
Note: the software/code developed under Linux. 
We do not do any work with the Windows OS and cannot provide much help in running our software/code on the Windows OS

If you have any questions contact: imart@ecn.purdue.edu

*****

# DINAVID: A Distributed and Networked Image Analysis System for Volumetric Image Data

## Introduction

DINAVID is designed for fast and accurate quantitative analysis of large 3D microscopy volumes. 
DINAVID supports traditional imaging processing tools such as filtering, thresholding, background subtraction, watershed, and morphological operations as well as deep learning-based 3D segmentation tools for fluorescent microscopy images. 
DINAVID also supports 2D real-time visualization on volumes and 3D visualization on subvolumes.

## Installation Guide 

1.  Introduction

    1.  DINAVID is only supported on Linux-based systems

    2.  DINAVID supports the following set-ups

        1.  Single-machine set-up

        2. Multi-machine set-up

            1.  One machine, which we will call the web server, handles
                HTML requests using Django, and does simple operations

            2.  The second machine, which we will call the GPU machine
                or the compute machine, handles the deep-learning
                inferencing

2.  General Installation

	1. Copy the contents of this zip file into a directory of choice. 
	An example directory is `/home/your-user/public_html/dinavid-gui`.
	This will be refered to as `HOME_PATH` below.
	
		1. Ensure permissions of files are set correctly. 
		
			1. You will need to be able to execute `manage.py`

    2.  Conda  environment

        1.  Install conda from
            <https://conda.io/projects/conda/en/latest/user-guide/install/index.html>

        2. Create an environment with `python >= 3.7`
			
			1. We recommend `python=3.7`

        3. Install packages from the given `conda_requirements.txt` file
		
		4. This environment is used for handling backend processing.

	3.  venv environment
	
		1. Enter these commands: 
		
			`sudo apt install python3.8-venv`
		
			`python3.8 -m venv venv_name`
		
			`source venv_name/bin/activate`
		
			`pip3.8 install --upgrade pip`
		
			`pip3.8 install -r venv_requirements.txt`
		
			`deactivate` (when done using the environment)
		
		2. This environment is used to handle `mod_wsgi` ( see the Apache section below )

    4.  Apache

        1.  Install apache2 web server from
            <http://httpd.apache.org/docs/current/install.html>

        2. Install modwsgi from
            <https://modwsgi.readthedocs.io/en/master/installation.html>
            

            1.  Ensure that the version of modwsgi you are installing is
                compatible with the version of apache and version of
                python you have installed
				
			2. If you run into issues, see https://modwsgi.readthedocs.io/en/develop/user-guides/virtual-environments.html and
			 https://modwsgi.readthedocs.io/en/develop/user-guides/installation-issues.html


        3. Edit the following files

			0. Add `WSGIApplicationGroup %{GLOBAL}` to the `apache2.conf` file.

            1.  Depending on the version of apache you have installed,
                `000-default.conf` under the directory where apache2 is
                installed. An example for a ubuntu machine is
                `/etc/apache2/sites-available/000-default.conf`

                1.  The items you need to edit in the template are:

                    0.  Line 17 `<Directory /path/to/data>`

                        1.  Point this towards the directory where the
                            volumes will be hosted on your machine

					
					1.  Line 24 `<Directory /path/to/data_special>`

                        1.  Point this towards the directory where the
                            volumes for special users will be hosted on your machine

                    2. Lines 59 `Alias /~micros/static /path/to/static`
                        And 60 `<Directory /path/to/static >`

                        1.  Point this towards the directory where the
                            static files are hosted, i.e. the logos.

                    3. Lines 65 `Alias /~micros/media /path/to/media`
                        And 66`<Directory /path/to/media>`

                        1.  Point this towards the directory where the
                            intermediate files are stored. This should
                            be the same as `HOME_MEDIA_PATH` below.

                    4. Line 73 `<Directory /path/to/dinavid>`

                        1.  Point this towards `HOME_PATH+ 'dinavid'`

                    5.  Line 80 `WSGIDaemonProcess dinavid
                        python-path=/python/path
                        python-home=/python/home`

                        1.  Point `python-path` to `HOME_PATH`

                        2.  Point `python-home` to venv environment you
                            have installed

                    6. Line 83 `WSGIScriptAlias /~micros
                        /path/to/wsgi.py`

                        1.  Point this towards the `wsgi.py` file
						
						
					7. If you are still having permission issues, you may need to grant permission to 
						the home directories of where `/path/to/data` and `/path/to/data_special` are.
						
			2. Under `mods-available`, in `wsgi.conf`
			
				1. add the line `WSGIPythonHome "/path/to/your/venv"`
					where `/path/to/your/venv` is the venv environment you
                            have installed

    5.  Rabbitmq

        1.  Install rabbitmq from
            <https://www.rabbitmq.com/download.html>
			
			1. For example, `sudo apt-get install rabbitmq-server`
			

		

3.  Single-machine setup

    1.  Celery/rabbitmq set up

        1.  Create a host

            1.  `sudo rabbitmqctl add_vhost vhostname`

        2. Create an admin profile

            1.  `sudo rabbitmqctl add_user mqaccount mqpassword`

            2.  `sudo rabbitmqctl set_user_tags mqaccount administrator`

            3.  `sudo rabbitmqctl set_permissions -p vhostname mqaccount ".*" ".*" ".*"`


4.  Multi-machine setup

    1.  Follow the instructions from the single-machine set-up for celery/rabbitmq

    2.  SSH setup from the web-server to the GPU machine and vice versa

        1.  This is needed so DINAVID can transfer files between the
            web-server and GPU machine in a secure manner without the
            need to enter passwords manually.

        2. On the web-server, generate a pair of ssh keys:

            1.  `ssh-keygen -b 4096 -t rsa`

            2.  Do not enter a passphrase when prompted.

        3. Send the key to the GPU machine. username_gpu is the
            account name you use to log on to the GPU machine

            1.  `ssh-copy-id username_gpu@gpu.domain`

            2.  You would need to enter the password you use to log on
                to the GPU machine

            3.  If successful, you can now `ssh username_gpu@gpu.domain`
                without entering a password

        4. On the GPU machine, generate a pair of ssh keys:

            1.  `ssh-keygen -b 4096 -t rsa`

            2.  Do not enter a passphrase when prompted.

        5.  Send the key to the web-server. username_ws is the account
            name you use to log on to the web-server

            1.  `ssh-copy-id username_ws@ws.domain`

            2.  You would need to enter the password you use to log on
                to the web-server machine

            3.  If successful, you can now ssh `username_ws@ws.domain`
                without entering a password

5.  DINAVID customization: These are some items that you will need to
    edit for DINAVID to work on your system

    1. ` settings.py`

        1.  `ALLOWED_HOSTS`: Change this to your domain name.

        2. `SECRET_KEY`: This is used to provide cryptographic signing,
            and should be set to a unique, unpredictable value. See
            <https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-SECRET_KEY>
            for more information.

            1.  One way to generate a secret key is using the following
                command: `python -c 'from django.core.management.utils import get_random_secret_key;`
               `print(get_random_secret_key())'`

        3. CELERY_BROKER_URL: Should be set to
            `pyamqp://mqaccount:mqpassword@your_site_ip/vhostname`
            where `mqaccount`, `mqpassword`, and `vhostname` were from the  
            Celery/rabbitmq set up section, and `your_site_ip` is the IP address of your
            instance of DINAVID.

    2.  `celery_tasks.py`

        1.  The broker on line 6 should be set to
            `pyamqp://mqaccount:mqpassword@your_site_ip/vhostname`
            where `mqaccount`, `mqpassword`, and `vhostname` are from the Celery/rabbitmq set up section section, 
            and `your_site_ip` is the IP address of your
            instance of DINAVID.

    3.  `photos/dinavid_options.py`

        1.  `GPU_OPTION`: Whether or not to use GPU/Celery to do the
            backend segmentation. The options are `NO_GPU`,
            `GPU_SINGLE_MACHINE`, `GPU_MULTI_MACHINE`

        2. `REMOTE_ACCOUNT_NAME` and `REMOTE_ADDRESS`: This is only
            necessary for a multi-machine setup, where `GPU_OPTION='GPU_MULTI_MACHINE'` . 
            The account and address is the
            account you use to log on to the GPU machine. It should be
            the same account as the one you created ssh keys for.

	4. `manage.py`
		
		1. Edit the first line so that `/path/to/environment` points to the conda environment you created.
		
		2. Run the command `python manage.py migrate` to initialize the database `db.sqlite3` that stores user account information .
		
		3. Enter these commands so apache is able to write to the database.
		
			1. `chgrp www-data db.sqlite3`
			2. `sudo chmod 664 db.sqlite3`

    5.  `photos/path_names.py`
	
		Note: All paths should have a trailing `/` at the end.

        1.  `HOME_PATH` : The directory where the application lives.
            Files and directories under this include folders called
            `dinavid` and `media`, and files `manage.py`and `db.sqlite3`. 


        2. `HOME_MEDIA_PATH` : The storage space in the web server to
            store user files. Suggested: `HOME_MEDIA_PATH = HOME_PATH+'media/'`.
			
			1. In a console, enter the command `sudo chown -R www-data:www-data HOME_MEDIA_PATH` , replacing `HOME_MEDIA_PATH` with the actual path,
			so the web server can access this.

        3. `SHARE_MEDIA_PATH` : Folder containing files that need to
            be shared across all accounts. Suggested: `SHARE_MEDIA_PATH  = HOME_MEDIA_PATH + 'share_folder'`

        4. `DATA_MEDIA_PATH` : On a single machine set-up, this should
            be set the same as `HOME_MEDIA_PATH`. On a multi machine
            set-up, this is the storage space for user files in the GPU
            machine.

        5.  `SPECIAL_USER_LIST` : A list of usernames that are special
            users. This is used if you want special users to share access to images under the
            directory `DATA_FILES_PATH`. Images uploaded by non-special
            users cannot be accessed by any other user.

        6. `DATA_FILES_PATH` : Directory containing the images to be
            loaded onto DINAVID for special users. Images in this
            directory cannot be accessed by non-special users.
			
			1. In a console, enter `sudo chgrp -R www-data DATA_FILES_PATH` , replacing `DATA_FILES_PATH` with the actual path,
			so the web server can access this.
			
			2. Under this directory, create a directory called `Kidney_Cortex_Human`, and place the provided example image under the new directory.


        7. `DATA_FILES_PATH_NON_SPECIAL_USER` : Directory
            containing images uploaded by non-special users.
			
			1. In a console, enter `sudo chgrp -R www-data DATA_FILES_PATH_NON_SPECIAL_USER`, replacing `DATA_FILES_PATH_NON_SPECIAL_USER` with the actual path,
			so the web server can access this.

        8. `CMAP_PATH` : File containing a list of colors, used for
            color coding segmentation results. One is provided at
            `HOME_PATH +"/dinavid/photos/cmap.txt"`

        9.  `VIZ_PATH` : Directory containing files needed for 3D
            visualization. The files are provided at:
            `HOME_MEDIA_PATH + "/vis/"`

        10.  `VIZ_INPUT_PATH` : Directory containing more files needed
            for 3D visualization. The files are provided at:
            `HOME_MEDIA_PATH + "/vis_input/"`

        11. `CHECKPOINT_PATH` : Checkpoints to the DeepSynth segmentation
            model. Provided at `HOME_PATH + "/dinavid/photos/checkpoint/" `, 
            but on a multi-machine
            setup, copy these files to the GPU machine and set the
            appropriate path.

        12. `WATERSHED_FILE_PATH` : Path to a jar containing watershed.
            Provided at `HOME_PATH + '/dinavid/watershed.jar'`. This will
            be deprecated in a future update to support a python-based version of watershed.

        13. `MODELS_HOME_PATH` : Path need in `test_all_pad.py` to add
            to the system path. Should be set to `MODELS_HOME_PATH = HOME_PATH + '/dinavid/photos/'`

        14. `VIZ_3D_ADDRESS` : A URL needed to display the 3D
            visualization. Should be set to `https://your.domain.ext/~micros/media/`

	6. After editing the files, restart the apache server.
	
		1.  Depending on the version of your operating system and
            apache, this could look like:

            1.  `sudo systemctl restart apache2`
			
			
6.  Creating accounts

	1.  First create an admin user of django by entering `python manage.py createsuperuser` 
	
    2.  Go to https://yoursite.domain/~micros/admin/ to create additional accounts for users of dinavid, using the admin account

7.  Manually Loading Images Onto DINAVID for special users

    1.  DINAVID supports two types of volumes

        2.  Multichannel 3D tif files

        2. Multichannel files separated by channel and slice into
            individual images

    2.  3D tifs

        1.  Under `DATA_FILES_PATH`, create a new directory
            corresponding to the name of the new 3D.tif you wish to
            upload

        2. Place your tif file under the new directory

        3. The first time you select that image in DINAVID, all
            necessary supplementary files will be generated to visualize
            and process that image. This process might take a few
            seconds to a few minutes, depending on the size of your
            volume.

    3.  Individual images separated by channel and slice

        1.  Under `DATA_FILES_PATH`, create a new directory
            corresponding to the name of the volume

        2. Under the new directory, create a subdirectory with the same
            as the directory you have just created

        3. Place the images under this newest subdirectory.

            1.  The expected file format is: `\*_z%04d_ch%02d.{tif/png}`

        4. The first time you select that image in DINAVID, all
            necessary supplementary files will be generated to visualize
            and process that image. This process might take a few
            seconds to a few minutes, depending on the size of your
            volume.

8.  Deployment:

    1.  Restart apache every time a change is made

        1.  Depending on the version of your operating system and
            apache, this could look like:

            1.  `sudo systemctl restart apache2`

    2.  If Celery is enabled:

        1.  Single-machine setup

            1.  On the server, create a screen using this command `screen`

            2.  Activate the conda environment you have created 

            3.  Go to the `HOME_PATH` directory and enter the following
                command: `celery -A dinavid worker -l info`

            4.  If successful, you should see a “celery is ready”
                message

            5.  Detach from the screen by pressing `ctrl+a+d`

            6.  Whenever changes are made to the system and celery needs
                to be restarted, resume the screen using `screen -r`

            7.  Press `ctrl+c` to end the celery process.

            8.  Enter the command `celery -A dinavid worker -l info` to restart the celery process.

            9.  Detach from the screen by pressing `ctrl+a+d`

        2. Multi-machine setup

            1.  For a multi-machine setup, follow the same steps as the
                multi-machine setup above, except that you enter these
                commands in the GPU machine that you have.





##  Adding Segmentation Methods to DINAVID 

DINAVID supports the integration of new segmentation methods. 
New segmentation methods can be added to DINAVID as long as they are implemented in Python and properly modified so that the input and output directories are consistent with DINAVID. 
Any inputs or parameters that are user-provided need to be added as an option when adding to the user interface. 
Both Python-based machine learning and traditional, image processing-based segmentation methods implemented in Python can be added to DINAVID.

###  To add a new pre-processing step, in the DINAVID system, make these changes:

1.  In `dinavid/photos/templates/photos/workflow/index_compact.html`,
    under the `addProcess()` JavaScript function, you need to do three
    things.

    1.  The first is to add to the list of possible processes by adding
        `<option value=your_preprocessing_step> Name of your preprocessing step </option>`
        to the selection with name `operation[]`.

    2.  The second step is to add your actual processing step by adding
        ` <div id=your_new_div- + procId +  style=display:none> HTML for selecting parameters (if any) </div> `.

    3.  The third step is to allow your processing step to show up when
        you select it. Under `changeProcessOption(procId)`, add
        `document.getElementById(your_new_div- +procId).style.display = none;`
        under the comment that says “Hide all other processing steps".
        Make sure the html ID you use matches the one in step 1.

2.  In `dinavid/photos/templates/photos/views.py`, under
    `workflow_process()` and `workflow_preview()`, you would need to
    add
    `your_param = request.GET.getlist(your_param[])` for each new
    parameter you have for your processing step.

    1.  You would need to add your parameter to the argument list of
        `make_visual_dropdown_nuc()` and
        `make_visual_dropdown_nuc_preview()`. Remember to do this for
        the function definition and each time this function is called,
        which is under `workflow_process()` and
        `workflow_preview()`.

3.  In `dinavid/photos/templates/photos/tasks.py`, also add your
    parameters to `make_visual_celery_dropdown_nuc()`. This function is
    called in
    `make_visual_dropdown_nuc()` and
    `make_visual_dropdown_nuc_preview()`.

4.  In `dinavid/photos/templates/photos/tileImages_input.py`, also add
    your parameters to `tileImages_input_dropdown_nuc()`. This is also
    where you implement your function. Under the if-else chain under the
    comment that says “Perform operation for each slice”, add a new
    `elif` statement that says
    `elif which_op == "your_preprocessing_step”`. The text to put here
    is the option value you put in the selection list in
    `index_compact.html`.

5.  After you make these changes, you would need to restart your Apache
    Server. This is usually done by `sudo systemctl restart apache2`.

6.  In your compute node where Celery is running, make the same changes
    for `tasks.py` and `tileImages_input.py`. These two files should be
    an exact copy of the files with the same name in the web server.
    After you make these changes in the compute node, you would need to
    restart the Celery worker. To do so, `Control+C` where Celery is
    running and enter `celery -A dinavid worker -l info`

### Adding a new segmentation technique, in the DINAVID system, make the following changes:

1.  In `dinavid/photos/templates/photos/workflow/index_compact.html`,
    under the select element with `id="seg_opt”`,

    1.  Add an option for your new segmentation technique.
        `<option value=your_new_seg> Name of your segmentation technique </option>`

    2.  Near the `watershed-content` or `deepsynth-content` divs, create
        a new div
        `<div id= your_new_seg -content style=display:none; border:dotted>`
        and add your segmentation parameters inside this div.

    3.  In the `seg_opt_on_change()` function, add a new `if` statement
        to correctly display the correct segmentation parameters when
        the selection box in 1a is changed.

2.  In `dinavid/photos/templates/photos/views.py`, under
    `workflow_seg()` and `workflow_seg_preview()`

    1.  Under the comment that says “Perform segmentation,” add a clause
        to the `elif` statement for your segmentation technique.
        `elif seg_opt == your_new_seg`. The name should match the option
        value you chose in Step 1.

    2.  You would need to add
        `your_param = request.GET.getlist(your_param[])` for each new
        parameter you have for your processing step under the `elif`
        clause.

    3.  Make a call to your segmentation algorithm that will be defined
        in `segment_options.py`.

3.  In `dinavid/photos/templates/photos/segment_options.py`

    1.  Implement your new segmentation technique in this file. You may
        want two separate functions, one for a preview and one for the
        actual segmentation. The preview may be done on the web server,
        while the segmentation should only be done on the compute node.

    2.  Your segmentation technique should return the path to where the
        segmentation results are stored.

    3.  If your segmentation technique utilizes the compute node, you
        would need to add a separate call in `tasks.py` that Celery will
        utilize. For more information on this, see the document on
        Celery, or see http://www.celeryproject.org/ or
        https://github.com/celery/celery.

4.  After you make these changes, you would need to restart your Apache
    Server. This is usually done by `sudo systemctl restart apache2`.

5.  In your compute/GPU node where Celery is running, make the same
    changes for `tasks.py`. This file should be an exact copy of the
    files with the same name in the web server. After you make these
    changes in the compute node, you would need to restart the Celery
    worker. To do so, `Control+C` where Celery is running and enter
    `celery -A dinavid worker -l info`.

##   Description of Visualization Implementation 

Whenever a visualization setting is changed, the change to the 2D slice
is instantaneous. This is accomplished via an Asynchronous JavaScript
and XML (AJAX) request from the browser when a
change in user input is detected. In particular, when a change is
detected, the browser sends an AJAX request to the system. The system
takes the user input and generates a new image with the new
visualization parameters and sends this image back to the user’s
browser. Each image corresponds to a single slice of the 3D volume. Each
AJAX request only handles one slice of the volume for efficiency. If the
user wants to view different slices, a new image is generated each time
a new slice is examined.

With regard to 3D visualization, DINAVID uses WebGL 
for interactive rendering and visualization. In particular, volume
rendering with ray casting is implemented with WebGL for the ability to
“see through” the 3D image volume. We first combine the images to be
visualized into one large mosaic file which allows the data to be more
accessible to the rendering engine. The visualization is real-time and
interactive and also permits a user to adjust the brightness, gamma,
and offset of each displayed channel.

To generate a 3D visualization of a region of interest, the user needs
to select a region on the slice being displayed by dragging a box that
represents the region of interest. The user also specifies three
channels of a volume to render in 3D. DINAVID generates the necessary
files for rendering while the user’s browser handles the rendering using
local GPU acceleration on the users computer, if available. The
rendering engine is a JavaScript library known as
ShareVol using WebGL. Similar to 2D visualization, we
have the capability for adjusting gamma, brightness, and offset for each
of the red, green, and blue channels as given in the manuscript.
Note that the input to ShareVol is always a grayscale image and colors
are randomly selected to differentiate between each 2D slice when all
slices are stacked together in a 3D volume. Since the colors present in
the 3D visualization should match the colors used in the 2D
visualization, we modified ShareVol to permit it to accept color images
as input. One drawback in using WebGL is that WebGL does not support all
the functionalities of OpenGL including 3D textures. Hence, a typical
OpenGL approach such as loading the image stacks to generate a 3D
texture input becomes impractical. Alternatively, the
stack of images are saved as 2D textures with a mosaic configuration.

##  Description of Available Pre-processing Methods 

For Gaussian filtering, used to smooth or blur an image, we allow the
user to adjust the filter kernel size using the parameter $\sigma$ that
controls the width of Gaussian kernel. Alternatively, the median filter
is used to remove noise from the image and we allow a user to adjust the
window size of the kernel. Median background subtraction is used to
remove the background from an image by subtracting a median filtered
image from the original image. Thresholding is also available including
simple thresholding and Otsu’s method. Clamping removes
low intensity values in the image. Pixels whose intensity is lower than
the threshold are set to 0, while pixels whose intensity is greater than
the threshold remains the same.

2D morphological operations for binary images, such as erosion,
dilation, opening, and closing, are available to determine how local
content of the image is shaped relative to a given flat structuring
element. The structuring element is defined by its shape and size. In
addition, rolling ball  is one of the options made
available for background subtraction. All functions, with the
exception of rolling ball subtraction, are implemented via
OpenCV .

##  List of Default Colors Used in Visualization 

The default colors applied to each channel of the image volume for
visualizing slices, in order, are \[‘\#0000ff’, ’\#ff0000’, ’\#00ff00’
, ‘\#f58231’, ‘\#4363d8’, ‘\#911eb4’, ‘\#42d4f4’, ‘\#f032e6’,
‘\#bfef45’, ‘\#fabebe’, ‘\#469990’, ‘\#e6beff’, ‘\#9a6324’, ‘\#fffac8’,
‘\#800000’, ‘\#aaffc3’, ‘\#808000’, ‘\#ffd8b1’, ‘\#000075’\]. This
list is adapted from https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/.

##  DINAVID Hardware Description 

For a single-machine set-up, here is an example configuration we have tested:


-   CPU: Intel Core i7-6900K
-   RAM: 128GB
-   GPU: NVIDIA Titan XP 12GB RAM per GPU (4 GPUs)
-   Storage Driver: 1TB SSD + 10TB HDD


For a multi-machine set-up, here is an example configuration we have tested:

Network-Based User Interface Server

-   CPU: Intel Core i7-6900K
-   RAM: 128GB
-   GPU: NVIDIA GeForce GTX 1080 (1 GPU)
-   Storage Driver: 1TB SSD + 10TB HDD

Compute/GPU Server

-   CPU: Intel Core i7-6900K
-   RAM: 128GB
-   GPU: NVIDIA Titan XP 12GB RAM per GPU (4 GPUs)
-   Storage Driver: 1TB SSD + 10TB HDD

Note that DINAVID system can also be used with a cheaper GPU card for the image analysis.

##  Recommended Network Bandwith 

In general, we recommend that a user have a network connection of
100Mbps and a less than \$1000 graphics card (An example is the NVIDIA GeForce RTX 2060.) in the computer system
they are using to access DINAVID. This enables users to upload volumes
and download analysis results. For smaller volumes, a lower network
bandwidth may be sufficient. Please refer to the table below for a
recommendation of minimum network bandwidth based on the volume a user
uploads and analyzes.

| **Example Volume Size (Voxels)**      | **Recommended Minimum Bandwidth (Mbps)**  |
| -----------                           | -----------                               |
| 128 * 128 * 64                        | 10                                        |
| 256 * 256 * 64                        | 10                                        |
| 512 * 512 * 128                       | 25                                        |
| 1024 * 1024 * 128                     | 25                                        |
| 2048 * 2048 * 128                     | 50                                        |
| 5000 * 5000 * 128                     | 100                                       |
| 10000 * 10000 * 128                   | 100                                       |

##  Implementation Details for Data and User Request Handling 

Once the user uploads volumes to DINAVID,
Django  is used to process user requests and to
schedule and load data for processing on the computational and GPU
nodes. Users send HTTP  requests to Django through
their browser. For example, a request for displaying images will cause
Django to forward the request to the appropriate “view.” Each “view” is
a Python script that defines how to process the request. During the
processing of the requests, the views may interact with the “model”
script for reading or writing data depending on what is required. For
example, the “view” needs to know what images/volumes need to be
retrieved from the system storage for the requested operation. Django
will then return a response to the user’s browser, often by dynamically
creating an HTML page and by inserting the retrieved data into
placeholders in an HTML template.

DINAVID also employs Celery  to support task
scheduling such as task queuing and distributed message passing. Task
queues are used as a strategy to distribute the workload between
multiple threads or computational nodes. Tasks can execute
asynchronously (in the background) or synchronously (wait until ready).
DINAVID also supports parallel task processing for multiple users. This
is achieved by RabbitMQ , a message broker that
accepts and forwards messages. The combination of RabbitMQ and Celery
achieves the management and scheduling of tasks from different users. It
can automatically distribute the user’s data to different computational
nodes for processing. The resultant information will be stored in the
system’s storage for users to access when needed.


## Image Example Used In The Manuscript
The volume used in the DINAVID paper is provided in this distribution with the file name:  
`example_image/Kidney_Cortex_Human.tif`

If you follow these settings in DINAVID, the segmentation result should be a volume provided in this distribution with the file name:  
`example_image/seg_result.tif`


Settings:  
Nuclear Channel: Channel 1  
Pre-processing step: Gaussian Smoothing, Kernel Size: 3, Sigma: 0  
Segmentation: DeepSynth, Model 1, No watershed, Remove Small Components: 0  